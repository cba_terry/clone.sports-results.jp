<?php
// src/Item.php

namespace Entities;

/**
 * @Entity @Table(name="item")
 **/
class Item
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $name;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $no;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $sex;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $flag_item;
    /**
     * @Column(name="`order`", type="integer", nullable=true)
     **/
    protected $order;
    /**
     * @Column(type="integer", nullable=true, options={"default" = 1})
     **/
    protected $number_referee_d;
    /**
     * @Column(type="integer", nullable=true, options={"default" = 1})
     **/
    protected $number_referee_e;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param mixed $game
     */
    public function setGame($game)
    {
        $this->game = $game;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * @param mixed $no
     */
    public function setNo($no)
    {
        $this->no = $no;
    }

    /**
     * @return mixed
     */
    public function getFlagItem()
    {
        return $this->flag_item;
    }

    public function isPlayTwice()
    {
        return  $this->flag_item;
    }

    /**
     * @param mixed $flag_item
     */
    public function setFlagItem($flag_item)
    {
        $this->flag_item = $flag_item;
    }

     /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getNumberRefereeD()
    {
        return $this->number_referee_d;
    }

    /**
     * @param mixed $number_referee_d
     */
    public function setNumberRefereeD($number_referee_d)
    {
        $this->number_referee_d = $number_referee_d;
    }

    /**
     * @return mixed
     */
    public function getNumberRefereeE()
    {
        return $this->number_referee_e;
    }

    /**
     * @param mixed $number_referee_e
     */
    public function setNumberRefereeE($number_referee_e)
    {
        $this->number_referee_e = $number_referee_e;
    }

    public function __toString()
    {
        return $this->name;
    }
}

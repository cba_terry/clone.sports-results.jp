<?php
// src/Game.php

namespace Entities;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * @Entity @Table(name="tournament")
 * @Entity(repositoryClass="Entities\Repository\TournamentRepository")
 **/
class Tournament
{
    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    /** 
     * @Column(type="string")
     **/
    protected $name;

    /**
     * @ManyToOne(targetEntity="Association")
     * @JoinColumn(name="association_id", referencedColumnName="id")
     */
    protected $association;

    /**
     * @Column(type="date", nullable=true)
     **/
    protected $startTime;

    /**
     * @Column(type="date", nullable=true)
     **/
    protected $endTime;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $place;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $place_url;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $area;
    /**
     * @Column(type="simple_array", nullable=true)
     **/
    protected $class;
    /**
     * @OneToOne(targetEntity="PublicSetting", mappedBy="tournament", cascade={"persist", "remove"})
     */
    protected $public_setting;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $image;
    /**
     * @Column(type="text", nullable=true)
     **/
    protected $text_display_single;
    /**
     * @Column(type="text", nullable=true)
     **/
    protected $text_display_group;
    /**
     * @Column(type="text", nullable=true)
     **/
    protected $person_represent_name;
    /**
     * @OneToMany(targetEntity="Game", mappedBy="tournament", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    private $games;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $status;
    /**
     * @OneToMany(targetEntity="FinalSetting", mappedBy="tournament", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $finalsettings;

    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->finalsettings = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAssociation()
    {
        return $this->association;
    }

    /**
     * @param mixed $association
     */
    public function setAssociation($association)
    {
        $this->association = $association;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return mixed
     */
    public function getPlaceUrl()
    {
        return $this->place_url;
    }

    /**
     * @param mixed $place_url
     */
    public function setPlaceUrl($place_url)
    {
        $this->place_url = $place_url;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getTextDisplaySingle()
    {
        return $this->text_display_single;
    }

    /**
     * @param mixed $image
     */
    public function setTextDisplaySingle($text_display_single)
    {
        $this->text_display_single = $text_display_single;
    }

    /**
     * @return mixed
     */
    public function getTextDisplayGroup()
    {
        return $this->text_display_group;
    }

    /**
     * @param mixed $image
     */
    public function setTextDisplayGroup($text_display_group)
    {
        $this->text_display_group = $text_display_group;
    }

    /**
     * @return mixed
     */
    public function getPersonRepresentName()
    {
        return $this->person_represent_name;
    }

    /**
     * @param mixed $image
     */
    public function setPersonRepresentName($person_represent_name)
    {
        $this->person_represent_name = $person_represent_name;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Add games
     *
     * @param \Entities\Game $games
     * @return Tournament
     */
    public function addGame(\Entities\Game $games)
    {
        $this->games[] = $games;

        return $this;
    }

    /**
     * Remove games
     *
     * @param \Entities\Game $games
     */
    public function removeGame(\Entities\Game $games)
    {
        $this->games->removeElement($games);
    }

    /**
     * @return mixed
     */
    public function getGamesActive()
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('active', true));

        return $games = $this->games->matching($criteria);
    }

    public function checkClass($class)
    {
        return in_array($class, $this->getClass());
    }

    /**
     * @return mixed
     */
    public function getPublicSetting()
    {
        return $this->public_setting;
    }

    /**
     * @param mixed $public_setting
     * @return PuclicSetting
     */
    public function addPublicSetting($public_setting)
    {
        $this->public_setting = $public_setting;

        return $this;
    }


    /**
     * Set public_setting
     *
     * @param \Entities\PublicSetting $publicSetting
     * @return Tournament
     */
    public function setPublicSetting(\Entities\PublicSetting $publicSetting = null)
    {
        $this->public_setting = $publicSetting;

        return $this;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Tournament
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SingleItemScore
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add FinalSetting
     *
     * @param \Entities\FinalSetting $finalsetting
     * @return Game
     */
    public function addFinalSetting(\Entities\FinalSetting $finalsetting)
    {
        $this->finalsettings[] = $finalsetting;

        return $this;
    }

    /**
     * Get FinalSetting
     *
     */
    public function getFinalSettings()
    {
        return $this->finalsettings;
    }

    /**
     * Remove finalsettings
     *
     * @param \Entities\FinalSetting $finalsettings
     */
    public function removeFinalsetting(\Entities\FinalSetting $finalsettings)
    {
        $this->finalsettings->removeElement($finalsettings);
    }
}

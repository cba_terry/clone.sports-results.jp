<?php

namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

class Group
{
	protected $id;
	protected $heats;

	public function __construct($id = null)
	{
		$this->heats = new ArrayCollection();

		$this->id = $id;
	}

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function addHeat(\Entities\Heat $heat)
	{
		$this->heats[] = $heat;

		return $this;
	}

	public function removeHeat(\Entities\Heat $heat)
	{
		$this->heats->removeElement($heat);
	}

	public function getHeats()
	{
		return $this->heats;
	}

	public function getNumberHeat()
	{
		return $this->heats->count();
	}
}
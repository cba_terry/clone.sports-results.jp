<?php
namespace Entities;

class Rank 
{
	protected $rank;
	protected $element;


	/**
	 * [__construct description]
	 * @param [type]  $element [description]
	 * @param integer $rank    [description]
	 */
	public function __construct($element, $rank = 1)
	{
		$this->rank = $rank;
		$this->element = $element;
	}


	/**
	 * [getRank description]
	 * @return [type] [description]
	 */
	public function getRank()
	{
		return $this->rank;
	}


	/**
	 * [getElement description]
	 * @return [type] [description]
	 */
	public function getElement()
	{
		return $this->element;
	}


	/**
	 * [distanceAnotherRank description]
	 * @param  Rank   $rank        [description]
	 * @param  string $comparation [description]
	 * @param  array  $params      [description]
	 * @return [type]              [description]
	 */
	public function distanceAnotherRank(Rank $rank, $comparation = '', $params = [])
	{
		if(is_callable([$this->getElement(), $comparation]) && is_callable([$rank->getElement(), $comparation])){

			$distance = call_user_func_array([$rank->getElement(), $comparation], $params) - call_user_func_array([$this->getElement(), $comparation], $params);

			return ($distance) ? formatScore($distance) : '-';
		};

		return false;
	}
}
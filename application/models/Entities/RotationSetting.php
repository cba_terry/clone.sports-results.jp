<?php
// src/RotationSetting.php

namespace Entities;

/**
 * @Entity @Table(name="rotation_setting")
 **/
class RotationSetting
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @Column(type="string")
     **/
    protected $name;
    /**
     * @Column(name="`order`", type="integer")
     **/
    protected $order;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RotationSetting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return RotationSetting
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set game
     *
     * @param \Entities\Game $game
     * @return RotationSetting
     */
    public function setGame(\Entities\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \Entities\Tournament 
     */
    public function getGame()
    {
        return $this->game;
    }
}

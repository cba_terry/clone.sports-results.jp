<?php
// src/Product.php

namespace Entities;

/**
 * @Entity @Table(name="referee")
 * @Entity(repositoryClass="Entities\Repository\RefereeRepository")
 **/
class Referee
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @Column(type="string")
     **/
    protected $item;
    /**
     * @Column(type="integer")
     **/
    protected $score_type;
    /**
     * @Column(type="string")
     **/
    protected $referee_name;
    /**
     * @Column(type="string")
     **/
    protected $password;
    /**
     * @Column(type="integer")
     **/
    protected $referee_type;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $hash;
    /** 
     * @Column(type="smallint", nullable=true)
     **/
    protected $flag_active;

    public static $refereeTypeMap = [
        3  => 'D1',
        4  => 'D2',
        5  => 'D3',
        6  => 'D4',
        7  => 'D5',
        8  => 'E1',
        9  => 'E2',
        10 => 'E3',
        11 => 'E4',
        12 => 'E5',
        13 => 'Time1',
        14 => 'Time2',
        15 => 'Line1',
        16 => 'Line2'
    ];

    const NORMAL    = 0;
    const MAIN      = 1;
    const CHIEF     = 2;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return Referee
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    public function checkItem($item)
    {
        if($this->getItem() == $item) return true;
        return false;
    }
    
    /**
     * Set score_type
     *
     * @param integer $scoreType
     * @return Referee
     */
    public function setScoreType($scoreType)
    {
        $this->score_type = $scoreType;

        return $this;
    }

    /**
     * Get score_type
     *
     * @return integer 
     */
    public function getScoreType()
    {
        return $this->score_type;
    }

    public function checkScoreType($score_type)
    {
        $refereeScoreType = $this->getScoreType();

        if($score_type == $refereeScoreType) return true;
        return false;
    }
    /**
     * Set referee_name
     *
     * @param string $refereeName
     * @return Referee
     */
    public function setRefereeName($refereeName)
    {
        $this->referee_name = $refereeName;

        return $this;
    }

    /**
     * Get referee_name
     *
     * @return string 
     */
    public function getRefereeName()
    {
        return $this->referee_name;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Referee
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set referee_type
     *
     * @param integer $refereeType
     * @return Referee
     */
    public function setRefereeType($refereeType)
    {
        $this->referee_type = $refereeType;

        return $this;
    }

    /**
     * Get referee_type
     *
     * @return integer 
     */
    public function getRefereeType()
    {
        return $this->referee_type;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Referee
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set game
     *
     * @param \Entities\Game $game
     * @return Referee
     */
    public function setGame(\Entities\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Entities\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set flag_active
     *
     * @param integer $flagActive
     * @return Referee
     */
    public function setFlagActive($flagActive)
    {
        $this->flag_active = $flagActive;

        return $this;
    }

    /**
     * Get flag_active
     *
     * @return integer 
     */
    public function getFlagActive()
    {
        return $this->flag_active;
    }

    public function isNormalReferee()
    {
        return $this->referee_type === Referee::NORMAL;
    }

    public function isChiefReferee()
    {
        return $this->referee_type === Referee::CHIEF;
    }

    public function isMainReferee()
    {
        return $this->referee_type === Referee::MAIN;
    }

    public function getScoreTypeMap()
    {
        if(array_key_exists($this->score_type, self::$refereeTypeMap))
            return self::$refereeTypeMap[$this->score_type];

        return null;
    }
}

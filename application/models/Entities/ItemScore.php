<?php
namespace Entities;

class ItemScore 
{
	protected $name;
	protected $score;

	public function __construct($name, $score)
	{
		$this->name = $name;
		$this->score = $score;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getScore()
	{
		return $this->score;
	}
}
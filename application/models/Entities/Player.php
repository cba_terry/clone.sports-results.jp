<?php
// src/Product.php

namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * @Entity @Table(name="player")
 * @Entity(repositoryClass="Entities\Repository\PlayerRepository")
 **/
class Player
{
    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @ManyToOne(targetEntity="Tournament")
     * @JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    /** 
     * @Column(name="`group`", type="integer", nullable=true)
     **/
    protected $group;
    /** 
     * @Column(type="integer", nullable=true)
     **/
    protected $heat;
    /** 
     * @Column(type="string", nullable=true)
     **/
    protected $player_no;
    /** 
     * @Column(type="string", nullable=true)
     **/
    protected $player_name;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $school_no;
    /** 
     * @Column(type="string", nullable=true)
     **/
    protected $school_name;
    /** 
     * @Column(type="string", nullable=true)
     **/
    protected $school_name_ab;
    /** 
     * @Column(type="integer", nullable=true)
     **/
    protected $grade;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $prefecture;
    /** 
     * @Column(type="smallint")
     **/
    protected $sex;
    /** 
     * @Column(type="smallint", nullable=true)
     **/
    protected $flag;
    /** 
     * @Column(type="smallint", nullable=true)
     **/
    protected $flag_cancle;
    /** 
     * @Column(name="`order`", type="integer", nullable=true)
     **/
    protected $order;
    /**
     * @OneToMany(targetEntity="Score", mappedBy="player", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $scores;
    /**
     * @OneToMany(targetEntity="SingleItemScore", mappedBy="player", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $single_item_scores;
    /**
     * @OneToOne(targetEntity="SingleTotalScore", cascade={"persist", "remove"})
     * @JoinColumn(name="single_total_score_id", referencedColumnName="id")
     */
    protected $single_total_score;
    /**
     * @OneToOne(targetEntity="ItemTotalScore", cascade={"persist", "remove"})
     * @JoinColumn(name="item_total_score_id", referencedColumnName="id")
     */
    protected $item_total_score;
    /**
     * @ManyToOne(targetEntity="SchoolGroup", cascade={"persist", "remove"})
     * @JoinColumn(name="school_id", referencedColumnName="id")
     */
    protected $school;
    /**
     * @OneToMany(targetEntity="Order", mappedBy="player", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $orders;
    /** 
     * @Column(type="integer", nullable=true)
     **/
    protected $pass_play_type;

    public function __clone() {
        $this->id = null;
        $this->school = null;
        $this->scores = null;
        $this->single_total_score = null;
        $this->item_total_score = null;
        $this->single_item_scores = null;
        $this->orders = null;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scores             = new ArrayCollection();
        $this->orders             = new ArrayCollection();
        $this->single_item_scores = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param integer $group
     * @return Player
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return integer 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set heat
     *
     * @param integer $heat
     * @return Player
     */
    public function setHeat($heat)
    {
        $this->heat = $heat;

        return $this;
    }

    /**
     * Get heat
     *
     * @return integer 
     */
    public function getHeat()
    {
        return $this->heat;
    }

    /**
     * Set player_no
     *
     * @param string $playerNo
     * @return Player
     */
    public function setPlayerNo($playerNo)
    {
        $this->player_no = $playerNo;

        return $this;
    }

    /**
     * Get player_no
     *
     * @return string 
     */
    public function getPlayerNo()
    {
        return $this->player_no;
    }

    /**
     * Set player_name
     *
     * @param string $playerName
     * @return Player
     */
    public function setPlayerName($playerName)
    {
        $this->player_name = $playerName;

        return $this;
    }

    /**
     * Get player_name
     *
     * @return string 
     */
    public function getPlayerName()
    {
        return $this->player_name;
    }

    /**
     * Set school_no
     *
     * @param string $schoolNo
     * @return Player
     */
    public function setSchoolNo($schoolNo)
    {
        $this->school_no = $schoolNo;

        return $this;
    }

    /**
     * Get school_no
     *
     * @return string 
     */
    public function getSchoolNo()
    {
        return $this->school_no;
    }

    /**
     * Set school_name
     *
     * @param string $schoolName
     * @return Player
     */
    public function setSchoolName($schoolName)
    {
        $this->school_name = $schoolName;

        return $this;
    }

    /**
     * Get school_name
     *
     * @return string 
     */
    public function getSchoolName()
    {
        return $this->school_name;
    }

    /**
     * Set school_name_ab
     *
     * @param string $schoolNameAb
     * @return Player
     */
    public function setSchoolNameAb($schoolNameAb)
    {
        $this->school_name_ab = $schoolNameAb;

        return $this;
    }

    /**
     * Get school_name_ab
     *
     * @return string 
     */
    public function getSchoolNameAb()
    {
        return ($this->school) ? $this->school->getSchoolNameAb() : null;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     * @return Player
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer 
     */
    public function getGrade()
    {
        if(!$this->grade)  return '';
        
        return $this->grade;
    }

    /**
     * Set prefecture_name
     *
     * @param string $prefectureName
     * @return Player
     */
    public function setPrefecture($prefecture)
    {
        $this->prefecture = $prefecture;

        return $this;
    }

    /**
     * Get prefecture_name
     *
     * @return string
     */
    public function getPrefecture()
    {
        return $this->prefecture;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return Player
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    public function getSexString()
    {
        if($this->sex){
            return '男子';
        }else {
            return '女子';
        }
    }

    public function isMale()
    {
        return $this->sex;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Player
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    public function isPlayAsSingle()
    {
        return !$this->flag;
    }

    public function isPlayAsGroup()
    {
        return $this->flag;
    }

    /**
     * Set game
     *
     * @param \Entities\Game $game
     * @return Player
     */
    public function setGame(\Entities\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Entities\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set tournament
     *
     * @param \Entities\Tournament $tournament
     * @return Player
     */
    public function setTournament(\Entities\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \Entities\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    public function addScore(\Entities\Score $score)
    {
        $score->setPlayer($this);

        $this->scores[] = $score;
    }

    public function getScores()
    {
        return $this->scores;
    }

    /**
     * Add single_item_scores
     *
     * @param \Entities\SingleItemScore $singleItemScores
     * @return Player
     */
    public function addSingleItemScore(\Entities\SingleItemScore $singleItemScore)
    {
        $this->single_item_scores[] = $singleItemScore;

        return $this;
    }


    /**
     * [registerSingleItemScore description]
     * @param  \Entities\SingleItemScore $singleItemScore [description]
     * @param  \Entities\Item            $item            [description]
     * @return [type]                                     [description]
     */
    public function registerSingleItemScore(\Entities\SingleItemScore $singleItemScore, \Entities\Item $item)
    {
        $singleItemScore->setItem($item->getName());
        $singleItemScore->setPlayer($this);

        if(!$this->single_item_scores->contains($singleItemScore)) $this->addSingleItemScore($singleItemScore);

        $score1  = $this->getItemScore($item, \Entities\SingleItemScore::ROUND1);
        $score2  = $this->getItemScore($item, \Entities\SingleItemScore::ROUND2);

        // case of female
        if($item->isPlayTwice() && $score1 && $score2)
        {
            if($score1->getFinalScore() == $score2->getFinalScore())
            {
                // case equal, final score  = score which have higer E total
                $e1total = $score1->getEScore();
                $e2total = $score2->getEScore();
                $score  = ($e1total > $e2total) ? $score1 : $score2;
            }
            else
            {
                $score = ($score1->getFinalScore() > $score2->getFinalScore()) ? $score1 : $score2;
            }
        }
        else
        {    
            $score = ($score1) ? $score1 : $score2;
        }

        // in case of male, take the first result
        if($this->isMale()) $score = $score1;

        // make sure all old score value will set be false 
        // before set a one of them to best score
        if($score1) $score1->setBestScoreFlag(false);
        if($score2) $score2->setBestScoreFlag(false);

        if($score) $score->setBestScoreFlag(true);

        return $this;
    }

    /**
     * Remove single_item_scores
     *
     * @param \Entities\SingleItemScore $singleItemScores
     */
    public function removeSingleItemScore(\Entities\SingleItemScore $singleItemScores)
    {
        $this->single_item_scores->removeElement($singleItemScores);
    }

    /**
     * Get single_item_scores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSingleItemScores()
    {
        return $this->single_item_scores;
    }

    /**
     * sort single_item_scores by item
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemScores()
    {
        $itemScores = [];

        foreach ($this->getItems() as $item) {

            $score = $this->getBestItemScore($item);

            if($score) $itemScores[$item->getName()] = $score;
        }

        return $itemScores;
    }

    public function getItems()
    {
        return $this->game->getItems();
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getTotalFinalScore()
    {
        $totalScore = 0;

        $scores = $this->getItemScores();

        foreach ($scores as $score) {
            $totalScore += (float)$score->getFinalScore();
        }

        return formatScore($totalScore);
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getTotalDScore()
    {
        $dScore = 0;

        $scores = $this->getItemScores();

        foreach ($scores as $score) {
            $dScore += (float)$score->getDScore();
        }

        return formatScore($dScore);
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getTotalEScore()
    {
        $eScore = 0;

        $scores = $this->getItemScores();

        foreach ($scores as $score) {
            $eScore += (float)$score->getEScore();
        }

        return formatScore($eScore);
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getTotalDemeritScore()
    {
        $demeritScore = 0;
        
        $scores = $this->getItemScores();

        foreach ($scores as $score) {
            $demeritScore += (float)$score->getDemeritScore();
        }

        return formatScore($demeritScore);
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Player
     */
    public function setOrder(\Entities\Order $order)
    {
        $orgOrder = $this->getOrder($order->getItem());

        if($orgOrder) 
        {
            $orgOrder->setOrder($order->getOrder());

            return $this;
        }

        $order->setPlayer($this);

        $this->orders[] = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder($item)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('item', (string) $item));

        $order = $this->orders->matching($criteria)->first();
        
        return $order;
    }

    /**
     * Add single_total_score
     *
     * @param \Entities\SingleTotalScore $singleTotalScores
     * @return Player
     */
    public function addSingleTotalScore(\Entities\SingleTotalScore $singleTotalScores)
    {
        $this->single_total_score = $singleTotalScores;

        return $this;
    }

    public function removeSingleTotalScore()
    {
        $this->single_total_score = null;
    }

    /**
     * Get single_total_score
     *
     * @return \Entities\SingleTotalScore
     */
    public function getSingleTotalScore()
    {
        return $this->single_total_score;
    }

    /**
     * Add item_total_score
     *
     * @param \Entities\ItemTotalScore $itemTotalScore
     * @return Player
     */
    public function addItemTotalScore(\Entities\ItemTotalScore $itemTotalScore)
    {
        $this->item_total_score = $itemTotalScore;

        return $this;
    }

    public function removeItemTotalScore()
    {
        $this->item_total_score = null;
    }

    /**
     * Get item_total_score
     *
     * @return \Entities\SingleTotalScore
     */
    public function getItemTotalScore()
    {
        return $this->item_total_score;
    }

    /**
     * Set flag_cancle
     *
     * @param integer $flagCancle
     * @return Player
     */
    public function setFlagCancle($flagCancle)
    {
        $this->flag_cancle = $flagCancle;

        return $this;
    }

    /**
     * Get flag_cancle
     *
     * @return integer 
     */
    public function getFlagCancle()
    {
        return $this->flag_cancle;
    }

    /**
     * Set school
     *
     * @param \Entities\SchoolGroup $school
     * @return Player
     */
    public function setSchool(\Entities\SchoolGroup $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \Entities\SchoolGroup 
     */
    public function getSchool()
    {
        return $this->school;
    }

        /**
     * [getItemScore description]
     * @param  \Entities\Item $item [description]
     * @param  integer        $round  [description]
     * @return [type]               [description]
     */
    public function getItemScore(\Entities\Item $item, $round = 1)
    {
        $scores = $this->single_item_scores->filter(function($score) use ($item, $round) {
            return $score->getItem() == $item->getName() && $score->getRound() == $round;
        });

        return $scores->first();
    }

    /**
     * [getItemScoreValue description]
     * @param  string  $item   [description]
     * @param  string  $method [description]
     * @param  integer $round    [description]
     * @return [float]         [description]
     */
    public function getItemScoreValue($item, $method = '', $round = 1)
    {
        $score = $this->getItemScore($item, $round);

        $value = ($score) ? call_user_func([$score, 'get' . $method]) : null;

        return $value;
    }


    /**
     * [getBestItemScore choosing best score item in 2 times play]
     * @param  \Entities\Item $item [the item]
     * @return [\Entities\SingleItemScore] [single item score]
     */
    public function  getBestItemScore(\Entities\Item $item)
    {
        if($this->isMale() || !$item->isPlayTwice())
        {
            // always get first score for male
            return $this->getItemScore($item, \Entities\SingleItemScore::ROUND1);
        }
        else
        {
            $score1 = $this->getItemScore($item, \Entities\SingleItemScore::ROUND1);
            $score2= $this->getItemScore($item, \Entities\SingleItemScore::ROUND2);

            $value1 = ($score1) ? $score1->getFinalScore() : 0;
            $value2 = ($score2) ? $score2->getFinalScore() : 0;

            return ($value1 > $value2) ? $score1 : $score2;
        }
    }
    

    /**
     * [getItemBestScoreValue description]
     * @param  \Entities\Item $item [description]
     * @param  string $method [description]
     * @return [type]         [description]
     */
    public function getItemBestScoreValue(\Entities\Item $item, $method = '')
    {
        $score = $this->getBestItemScore($item);

        $value = ($score) ? call_user_func([$score, 'get' . $method]) : 0;

        return $value;
    }

    /**
     * [hasItemScorePublished description]
     * @param  \Entities\Item $item  [description]
     * @param  integer        $round [description]
     * @return boolean               [description]
     */
    public function hasItemScorePublished(\Entities\Item $item, $round = null)
    {
        if($round) return $this->getItemScoreValue($item, 'Status', $round) == \Entities\SingleItemScore::PUBLISH;
        else
        {
            return $this->getItemScoreValue($item, 'Status', \Entities\SingleItemScore::ROUND1) == \Entities\SingleItemScore::PUBLISH 
                || $this->getItemScoreValue($item, 'Status', \Entities\SingleItemScore::ROUND2) == \Entities\SingleItemScore::PUBLISH;
        }
    }

    /**
     * [getItemEventFinalScore using only for event result]
     * @param  \Entities\Item $item [description]
     * @return [type]               [description]
     */
    public function getItemEventFinalScore(\Entities\Item $item)
    {
        if($this->isMale())
        {
            $score1 = $this->getItemScoreValue($item, 'FinalScore', \Entities\SingleItemScore::ROUND1);
            $score2 = $this->getItemScoreValue($item, 'FinalScore', \Entities\SingleItemScore::ROUND2);

            return ($score1+$score2)/2;
        }

        return $this->getItemBestScoreValue($item, 'FinalScore');
    }

    /**
     * [getCurrentPlay description]
     * @param  [type] $rotate [description]
     * @param  [type] $items  [description]
     * @return [type]         [description]
     */
    public function getCurrentPlay($rotate, $items)
    {
        $rotatesWithoutRelax = $rotate->rotations->filter(function($r) use ($items) {
            return $items->exists(function($k,$i) use ($r) {
                return $i->getName() == $r->getFirstItem();
            });
        });

        foreach ($items as $item) {

            if($this->hasItemScorePublished($item))
            {
                $rm = $rotatesWithoutRelax->filter(function($r) use ($item) {
                    return $r->getFirstItem() == $item->getName();
                })->first();

                $rotatesWithoutRelax->removeElement($rm);
            }
        }

        if($rotatesWithoutRelax->first()) {
            return $rotatesWithoutRelax->first();
        }

    }

    public function getSingleTotalScoreStatus()
    {
        if($this->single_total_score)
        {
            return $this->single_total_score->getStatus();
        }

        return false;
    }

    public function isTotalScorePublished()
    {
        if($this->single_total_score) return $this->single_total_score->getStatus() == \Entities\SingleTotalScore::PUBLISHED;

        return false;
    }

    public function isTotalScoreUnPublished()
    {
        return !$this->single_total_score 
                || $this->single_total_score->getStatus() == 0;     
    }

    public function getSingleTotalScoreId()
    {
        if($this->single_total_score)
        {
            return $this->single_total_score->getId();
        }

        return false;
    }

    public function timeAndLineAllowReInputSingleItem($item, $round = 1)
    {
        $singleItemScore = $this->getItemScore($item, $round);

        if(!$singleItemScore) return false;

        if ($singleItemScore->getTime1ScoreStatus() == 2 && $singleItemScore->getTime2ScoreStatus() == 2 
            && $singleItemScore->getLine1ScoreStatus() == 2 && $singleItemScore->getLine2ScoreStatus() == 2) return false;
        if ($singleItemScore->getTime1ScoreStatus() == 0 && $singleItemScore->getTime2ScoreStatus() == 0 
            && $singleItemScore->getLine1ScoreStatus() == 0 && $singleItemScore->getLine2ScoreStatus() == 0) return false;

        return true;
    }

    ///////////////////////////////////////////////////////////////////////
    ///                   detail score from Score table                 ///
    ///////////////////////////////////////////////////////////////////////
    
    public function getScore($item, $type = '', $round = 1)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('item', (string) $item))
                                        ->andWhere(Criteria::expr()->eq('type', $type))
                                        ->andWhere(Criteria::expr()->eq('round', $round));


        $score = $this->scores->matching($criteria)->first();

        return ($score) ? $score : null;
    }

    public function getScoreId($item, $type = '', $round = 1)
    {
        $score = $this->getScore($item, $type, $round);

        return ($score) ? $score->getId() : null;
    }

    public function getScoreStatus($item, $type = '', $round = 1)
    {
        $score = $this->getScore($item, $type, $round);

        return ($score) ? $score->getStatus() : null;
    }

    public function getScoreValue($item, $type = '', $round = 1)
    {
        $score = $this->getScore($item, $type, $round);

        return ($score) ? $score->getValue() : null;
    }

    public function getEScore($item, $round = 1)
    {
        $escore = 0;
 
        $sortScore = [];
 
        $numberOfEscore = $item->getNumberRefereeE();

        for ($i=1; $i <= $numberOfEscore; $i++)
        { 
            $sortScore[] = $this->getScoreValue($item, 'E'.$i, $round);
        }

        $eDemeritScore = $this->getScoreValue($item, 'EDemerit', $round);
  
        rsort($sortScore);
 
        if($numberOfEscore == 1)
        {
            $escore = $sortScore[0] - $eDemeritScore;
        }
        else if($numberOfEscore == 2)
        {
            $escore = ($sortScore[0] + $sortScore[1]) / 2 - $eDemeritScore;
        }
        else if($numberOfEscore == 3)
        {
            $escore = ($sortScore[0] + $sortScore[1]) / 2 - $eDemeritScore;
        }
        else
        {
            $avg = 0;
 
            // remove highest score and lowest score
 
            $sortScore = array_slice($sortScore, 1, count($sortScore)-2);

            foreach ($sortScore as $score) {
                $avg += $score;
            }
 
            $avg = $avg / count($sortScore);
 
            $escore = $avg - $eDemeritScore;
        }
 
        if ($escore < 0) $escore = 0;
 
        return $escore;
    }

    public function getDScore($item, $round = 1)
    {
        $dScore = 0;

        $numberOfDscore = $item->getNumberRefereeD();

        for ($i=1; $i <= $numberOfDscore; $i++) { 
            $dScore += $this->getScoreValue($item, 'D'.$i, $round);
        }

        $dScore = $dScore / $numberOfDscore;

        return $dScore;
    }

    public function getDemeritScore($item, $round = 1)
    {
        return $this->getScoreValue($item, 'TimeDemerit')  
                        + $this->getScoreValue($item, 'LineDemerit', $round)  
                        + $this->getScoreValue($item, 'OtherDemerit', $round);
    }

    public function getFinalScore($item, $round = 1)
    {
        return $this->getEScore($item, $round) 
                        + $this->getDScore($item, $round) 
                        - $this->getDemeritScore($item, $round);
    }

    public function getBestFinalScoreValue($item)
    {
        if($this->isMale())
        {
            return $this->getFinalScore($item);
        }
        else
        {
            $score1 = $this->getFinalScore($item);
            $score2 = $this->getFinalScore($item,2);

            return ($score1 > $score2) ? $score1 : $score2;
        }
    }

    public function getItemScoreStatus($item, $round = 1)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('item', (string) $item))
                                        ->andWhere(Criteria::expr()->eq('round', \Entities\Score::ROUND1));

        $singleItemScore1 = $this->scores->matching($criteria);

        $criteria = Criteria::create()->where(Criteria::expr()->eq('item', (string) $item))
                                        ->andWhere(Criteria::expr()->eq('round', \Entities\Score::ROUND2));

        $singleItemScore2 = $this->scores->matching($criteria);
 
        if($singleItemScore1->isEmpty() && $singleItemScore2->isEmpty()) 
            return \Entities\SingleItemScore::ENTRY;

        // if all score is entry, then singleItemScore is entry        
        if(!$singleItemScore1->exists(function($k,$s){
            return $s->getStatus() != \Entities\Score::ENTRY;
        }) && !$singleItemScore2->exists(function($k,$s){
            return $s->getStatus() != \Entities\Score::ENTRY;
        }))
            return \Entities\SingleItemScore::ENTRY;
 
        // if all score is publish, then singleItemScore is publish
        if(!$singleItemScore1->exists(function($k,$s){
            return $s->getStatus() != \Entities\Score::PUBLISH;
        }) && !$singleItemScore2->exists(function($k,$s){
            return $s->getStatus() != \Entities\Score::PUBLISH;
        })) 
 
            return \Entities\SingleItemScore::PUBLISH;
 
        // other case singleItemScore is modify 
        return \Entities\SingleItemScore::MODIFY;
    }

    public function timeAndLineAllowReInput($item, $round = 1)
    {
        $time1 = $this->getScoreStatus($item, 'Time1', $round);
        $time2 = $this->getScoreStatus($item, 'Time2', $round);

        $line1 = $this->getScoreStatus($item, 'Line1', $round);
        $line2 = $this->getScoreStatus($item, 'Line2', $round);

        if ($time1 == 2 && $time2 == 2 && $line1 == 2 && $line2 == 2) return false;
        if ($time1 == 0 && $time2 == 0 && $line1 == 0 && $line2 == 0) return false;

        return true;
    }

    public function isItemScorePublish($item, $round = 1)
    {
        return $this->getItemScoreStatus($item, $round) == \Entities\SingleItemScore::PUBLISH;
    }

    public function isItemScoreModify($item, $round = 1)
    {
        return $this->getItemScoreStatus($item, $round) == \Entities\SingleItemScore::MODIFY;   
    }

    public function isItemScoreEntry($item, $round = 1)
    {
        return $this->getItemScoreStatus($item, $round) == \Entities\SingleItemScore::ENTRY;      
    }

    public function hasItemScoreFull()
    {
        $int_item  = count($this->getItems());
        $int_score = count($this->getItemScores());
        return ($int_score >= $int_item) ? true : false;
    }

    public function setPassPlayType($type)
    {
        $this->pass_play_type = $type;
    }

    public function getPassPlayType()
    {
        return $this->pass_play_type;
    }

    public function passAsGroup()
    {
        return $this->pass_play_type === 1;
    }

    public function passAsSingle()
    {
        return $this->pass_play_type === 0;
    }
}

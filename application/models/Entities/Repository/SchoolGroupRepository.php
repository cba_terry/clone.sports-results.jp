<?php
namespace Entities\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
/**
 * SchoolGroupRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SchoolGroupRepository extends EntityRepository
{
	/**
	 * Get paged schools
	 * @param int $offset
	 * @param int $limit
	 * @return Paginator
	 */
	public function getPagedSchools($conditions = [], $offset = 0, $limit = 10)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();

		$qb->select('s')
			->from('Entities\SchoolGroup', 's')
			->join('s.game', 'g')
			->setMaxResults($limit)
			->setFirstResult($offset);

		if(isset($conditions['game']))
		{
			$qb->where('s.game = :game')->setParameter('game', $conditions['game']);
		}
		if(isset($conditions['school_name']) && $conditions['school_name'] != '')
		{
			$qb->andWhere('s.school_name_ab LIKE :school_name')->setParameter('school_name', '%' . urldecode($conditions['school_name']) . '%');
		}
		if(isset($conditions['school_no']) && $conditions['school_no'] != '')
		{
			$qb->andWhere('s.school_no = :school_no')->setParameter('school_no', $conditions['school_no']);
		}

		$query = $qb->getQuery();
		$paginator = new Paginator( $query );

		return $paginator;
	}

	public function getSchoolByGame($game_id = 0)
	{
		if ( ! empty($game_id)) {
			$em = $this->getEntityManager();
			$qb = $em->createQueryBuilder();

			$qb->select('s')
				->from('Entities\SchoolGroup', 's')
				->where('s.game = :game')->setParameter('game', $game_id);

			$q       = $qb->getQuery();
			$results = $q->execute();
			return $results;
		}
	}

	public function getGroupRanking(\Entities\Game $game, $start = null, $max = null)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('sg')
            ->from('Entities\SchoolGroup', 'sg')
            ->leftJoin('sg.group_scores', 's')
            ->where('sg.game = :game')->setParameter('game', $game)
            ->orderBy('s.total_score', 'DESC');

        if($start)  $qb->setFirstResult($start);
        if($max)    $qb->setMaxResults($max);

        $qb->andWhere('sg.cancel_flag IS NULL OR sg.cancel_flag = 0');
        
        $results = $qb->getQuery()->execute();

        $scoreRanks = [];

        // make score's rank
        foreach ($results as $school) {
        	// really it should be one to one relation
        	$groupScore = $school->getGroupScores()->first();

        	$score = ($groupScore && $groupScore->getTotalScore() != NULL) ? $groupScore->getTotalScore() : 0;

            $scoreRanks[] = $score;
        }

        $scoreRanks = array_unique($scoreRanks);

        // apply rank to school
        foreach ($results as &$school) {
        	$groupScore = $school->getGroupScores()->first();

        	$score = ($groupScore && $groupScore->getTotalScore() != NULL) ? $groupScore->getTotalScore() : 0;

        	$key = array_search($score, $scoreRanks);

        	$school->setRanking($key+1);
        }

        return $results;
    }
}
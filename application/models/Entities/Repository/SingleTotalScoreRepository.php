<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/23/2015
 * Time: 11:14 AM
 */

namespace Entities\Repository;

use Doctrine\ORM\EntityRepository;
use Entities\Game;

class SingleTotalScoreRepository extends EntityRepository
{
    public function updateStatus($arr_total_score_id = array(), $str_value = 0)
    {
        if ( ! empty($arr_total_score_id)) {
            $arr_total_score_id = (array) $arr_total_score_id;

            $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();

            $qb->update('Entities\SingleTotalScore', 's')
                ->set('s.status', '?1')
                ->where('s.id IN (?2)')
                ->setParameter(1, $str_value)
                ->setParameter(2, $arr_total_score_id);
            
            $q = $qb->getQuery();

            return $q->execute();
        }
        return false;
    }
}
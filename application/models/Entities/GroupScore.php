<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/13/2015
 * Time: 7:07 PM
 */

namespace Entities;
use Doctrine\ORM\Mapping\Entity;

/**
 * @Entity @Table(name="group_score")
 * @Entity(repositoryClass="Entities\Repository\GroupScoreRepository")
 **/
class GroupScore
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $rank;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $pass_rank;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $total_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item1_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item1_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item1_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item1_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item2_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item2_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item2_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item2_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item3_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item3_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item3_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item3_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item4_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item4_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item4_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item4_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item5_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item5_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item5_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item5_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item6_score;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item6_single_item_score_id1;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item6_single_item_score_id2;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item6_single_item_score_id3;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item1_demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item2_demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item3_demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item4_demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item5_demerit_score;
    /**
     * @Column(type="float", nullable=true)
     **/
    protected $item6_demerit_score;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item1_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item2_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item3_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item4_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item5_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item6_demerit_reason;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $updater;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return GroupScore
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set pass_rank
     *
     * @param integer $passRank
     * @return GroupScore
     */
    public function setPassRank($passRank)
    {
        $this->pass_rank = $passRank;

        return $this;
    }

    /**
     * Get pass_rank
     *
     * @return integer 
     */
    public function getPassRank()
    {
        return $this->pass_rank;
    }

    /**
     * Set total_score
     *
     * @param float $totalScore
     * @return GroupScore
     */
    public function setTotalScore($totalScore = null)
    {

        if(!$totalScore)
            $this->total_score = $this->item1_score 
                                    + $this->item2_score 
                                    + $this->item3_score 
                                    + $this->item4_score
                                    + $this->item5_score
                                    + $this->item6_score
                                    - $this->item1_demerit_score
                                    - $this->item2_demerit_score
                                    - $this->item3_demerit_score
                                    - $this->item4_demerit_score
                                    - $this->item5_demerit_score
                                    - $this->item6_demerit_score
                                    ;

        else
            $this->total_score = $totalScore;

        return $this;
    }

    /**
     * Get total_score
     *
     * @return float 
     */
    public function getTotalScore()
    {
        return $this->total_score;
    }

    /**
     * Set demerit_score
     *
     * @param float $demeritScore
     * @return GroupScore
     */
    public function setDemeritScore($demeritScore)
    {
        $this->demerit_score = $demeritScore;

        return $this;
    }

    /**
     * Get demerit_score
     *
     * @return float 
     */
    public function getDemeritScore()
    {
        return $this->demerit_score;
    }

    /**
     * Set item1_score
     *
     * @param float $item1Score
     * @return GroupScore
     */
    public function setItem1Score($item1Score)
    {
        $this->item1_score = $item1Score;

        return $this;
    }

    /**
     * Get item1_score
     *
     * @return float 
     */
    public function getItem1Score()
    {
        return $this->item1_score;
    }

    /**
     * Set item2_score
     *
     * @param float $item2Score
     * @return GroupScore
     */
    public function setItem2Score($item2Score)
    {
        $this->item2_score = $item2Score;

        return $this;
    }

    /**
     * Get item2_score
     *
     * @return float 
     */
    public function getItem2Score()
    {
        return $this->item2_score;
    }

    /**
     * Set item3_score
     *
     * @param float $item3Score
     * @return GroupScore
     */
    public function setItem3Score($item3Score)
    {
        $this->item3_score = $item3Score;

        return $this;
    }

    /**
     * Get item3_score
     *
     * @return float 
     */
    public function getItem3Score()
    {
        return $this->item3_score;
    }

    /**
     * Set item4_score
     *
     * @param float $item4Score
     * @return GroupScore
     */
    public function setItem4Score($item4Score)
    {
        $this->item4_score = $item4Score;

        return $this;
    }

    /**
     * Get item4_score
     *
     * @return float 
     */
    public function getItem4Score()
    {
        return $this->item4_score;
    }

    /**
     * Set item5_score
     *
     * @param float $item5Score
     * @return GroupScore
     */
    public function setItem5Score($item5Score)
    {
        $this->item5_score = $item5Score;

        return $this;
    }

    /**
     * Get item5_score
     *
     * @return float 
     */
    public function getItem5Score()
    {
        return $this->item5_score;
    }

    /**
     * Set item6_score
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem6Score($item6Score)
    {
        $this->item6_score = $item6Score;

        return $this;
    }

    /**
     * Get item6_score
     *
     * @return float 
     */
    public function getItem6Score()
    {
        return $this->item6_score;
    }

    /**
     * setItem1DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem1DemeritScore($item1_demerit_score)
    {
        $this->item1_demerit_score = $item1_demerit_score;

        return $this;
    }

    /**
     * getItem1DemeritScore
     *
     * @return float 
     */
    public function getItem1DemeritScore()
    {
        return $this->item1_demerit_score;
    }

    /**
     * setItem2DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem2DemeritScore($item2_demerit_score)
    {
        $this->item2_demerit_score = $item2_demerit_score;

        return $this;
    }

    /**
     * getItem2DemeritScore
     *
     * @return float 
     */
    public function getItem2DemeritScore()
    {
        return $this->item2_demerit_score;
    }

    /**
     * setItem3DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem3DemeritScore($item3_demerit_score)
    {
        $this->item3_demerit_score = $item3_demerit_score;

        return $this;
    }

    /**
     * getItem3DemeritScore
     *
     * @return float 
     */
    public function getItem3DemeritScore()
    {
        return $this->item3_demerit_score;
    }

    /**
     * setItem4DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem4DemeritScore($item4_demerit_score)
    {
        $this->item4_demerit_score = $item4_demerit_score;

        return $this;
    }

    /**
     * getItem4DemeritScore
     *
     * @return float 
     */
    public function getItem4DemeritScore()
    {
        return $this->item4_demerit_score;
    }

    /**
     * setItem5DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem5DemeritScore($item5_demerit_score)
    {
        $this->item5_demerit_score = $item5_demerit_score;

        return $this;
    }

    /**
     * getItem5DemeritScore
     *
     * @return float 
     */
    public function getItem5DemeritScore()
    {
        return $this->item5_demerit_score;
    }

    /**
     * setItem6DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem6DemeritScore($item6_demerit_score)
    {
        $this->item6_demerit_score = $item6_demerit_score;

        return $this;
    }

    /**
     * getItem6DemeritScore
     *
     * @return float 
     */
    public function getItem6DemeritScore()
    {
        return $this->item6_demerit_score;
    }

    /**
     * setItem1DemeritReason
     *
     * @param float $item1_demerit_score
     * @return GroupScore
     */
    public function setItem1DemeritReason($item1_demerit_reason)
    {
        $this->item1_demerit_reason = $item1_demerit_reason;

        return $this;
    }

    /**
     * getItem1DemeritReason
     *
     * @return string 
     */
    public function getItem1DemeritReason()
    {
        return $this->item1_demerit_reason;
    }

    /**
     * setItem2DemeritReason
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem2DemeritReason($item2_demerit_reason)
    {
        $this->item2_demerit_reason = $item2_demerit_reason;

        return $this;
    }

    /**
     * getItem2DemeritReason
     *
     * @return string 
     */
    public function getItem2DemeritReason()
    {
        return $this->item2_demerit_reason;
    }

    /**
     * setItem3DemeritReason
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem3DemeritReason($item3_demerit_reason)
    {
        $this->item3_demerit_reason = $item3_demerit_reason;

        return $this;
    }

    /**
     * getItem3DemeritReason
     *
     * @return string 
     */
    public function getItem3DemeritReason()
    {
        return $this->item3_demerit_reason;
    }

    /**
     * setItem4DemeritReason
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem4DemeritReason($item4_demerit_reason)
    {
        $this->item4_demerit_reason = $item4_demerit_reason;

        return $this;
    }

    /**
     * getItem4DemeritScore
     *
     * @return string 
     */
    public function getItem4DemeritReason()
    {
        return $this->item4_demerit_reason;
    }

    /**
     * setItem5DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem5DemeritReason($item5_demerit_reason)
    {
        $this->item5_demerit_reason = $item5_demerit_reason;

        return $this;
    }

    /**
     * getItem5DemeritScore
     *
     * @return string 
     */
    public function getItem5DemeritReason()
    {
        return $this->item5_demerit_reason;
    }

    /**
     * setItem6DemeritScore
     *
     * @param float $item6Score
     * @return GroupScore
     */
    public function setItem6DemeritReason($item6_demerit_reason)
    {
        $this->item6_demerit_reason = $item6_demerit_reason;

        return $this;
    }

    /**
     * getItem6DemeritScore
     *
     * @return string 
     */
    public function getItem6DemeritReason()
    {
        return $this->item6_demerit_reason;
    }

    /**
     * setUpdater
     *
     * @return GroupScore 
     */
    public function setUpdater($updater)
    {
        $this->updater = $updater;

        return $this;
    }

    /**
     * getUpdater
     *
     * @return string 
     */
    public function getUpdater()
    {
        return $this->updater;
    }

    /**
     * Set school_group
     *
     * @param \Entities\SchoolGroup $schoolGroup
     * @return GroupScore
     */
    public function setSchoolGroup(\Entities\SchoolGroup $schoolGroup = null)
    {
        $this->school_group = $schoolGroup;

        return $this;
    }

    /**
     * Get school_group
     *
     * @return \Entities\SchoolGroup 
     */
    public function getSchoolGroup()
    {
        return $this->school_group;
    }

    public function setItem1SingleItemScoreId1($id)
    {
        $this->item1_single_item_score_id1 = $id;
    }

    public function setItem1SingleItemScoreId2($id)
    {
        $this->item1_single_item_score_id2 = $id;
    }

    public function setItem1SingleItemScoreId3($id)
    {
        $this->item1_single_item_score_id3 = $id;
    }

    public function setItem2SingleItemScoreId1($id)
    {
        $this->item2_single_item_score_id1 = $id;
    }

    public function setItem2SingleItemScoreId2($id)
    {
        $this->item2_single_item_score_id2 = $id;
    }

    public function setItem2SingleItemScoreId3($id)
    {
        $this->item2_single_item_score_id3 = $id;
    }

    public function setItem3SingleItemScoreId1($id)
    {
        $this->item3_single_item_score_id1 = $id;
    }

    public function setItem3SingleItemScoreId2($id)
    {
        $this->item3_single_item_score_id2 = $id;
    }

    public function setItem3SingleItemScoreId3($id)
    {
        $this->item3_single_item_score_id3 = $id;
    }

    public function setItem4SingleItemScoreId1($id)
    {
        $this->item4_single_item_score_id1 = $id;
    }

    public function setItem4SingleItemScoreId2($id)
    {
        $this->item4_single_item_score_id2 = $id;
    }

    public function setItem4SingleItemScoreId3($id)
    {
        $this->item4_single_item_score_id3 = $id;
    }

    public function setItem5SingleItemScoreId1($id)
    {
        $this->item5_single_item_score_id1 = $id;
    }

    public function setItem5SingleItemScoreId2($id)
    {
        $this->item5_single_item_score_id2 = $id;
    }

    public function setItem5SingleItemScoreId3($id)
    {
        $this->item5_single_item_score_id3 = $id;
    }

    public function setItem6SingleItemScoreId1($id)
    {
        $this->item6_single_item_score_id1 = $id;
    }

    public function setItem6SingleItemScoreId2($id)
    {
        $this->item6_single_item_score_id2 = $id;
    }

    public function setItem6SingleItemScoreId3($id)
    {
        $this->item6_single_item_score_id3 = $id;
    }
}

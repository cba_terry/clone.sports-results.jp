<?php
// src/Item.php

namespace Entities;

/**
 * @Entity @Table(name="final_setting")
 **/
class FinalSetting
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Tournament")
     * @JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    /**
     * @Column(type="text", nullable=true)
     **/
    protected $setting;
    /**
     * @Column(name="team", type="integer", nullable=true)
     **/
    protected $team;


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * @param mixed $tournament
     */
    public function setTournament($tournament)
    {
        $this->tournament = $tournament;
    }

    /**
     * @return mixed
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param mixed $no
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $no
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }
}

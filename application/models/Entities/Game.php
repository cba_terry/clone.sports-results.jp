<?php
// src/Game.php

namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Collection\RotationCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * @Entity @Table(name="game")
 * @Entity(repositoryClass="Entities\Repository\GameRepository")
 **/
class Game
{
    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /** 
     * @Column(type="boolean")
     **/
    protected $sex;
    /**
     * @Column(type="string")
     **/
    protected $class;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $preliminary;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $group_pass_number;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $single_pass_number;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $item_pass_number;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $input_type;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $input_final_round;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $input_line_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $input_time_score;
    /**
     * @ManyToOne(targetEntity="Tournament", inversedBy="games")
     * @JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    /**
     * @OneToMany(targetEntity="Item", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @OrderBy({"no" = "ASC"})
     */
    protected $items;
    /**
     * @OneToMany(targetEntity="Player", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $players;
    /**
     * @OneToMany(targetEntity="Referee", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $referees;
    /**
     * @OneToMany(targetEntity="Rotation", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $rotations;
    /**
     * @OneToMany(targetEntity="RotationSetting", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @OrderBy({"order" = "ASC"})
     */
    protected $rotationSettings;
    /**
     * @OneToMany(targetEntity="SchoolGroup", mappedBy="game", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $schoolGroups;
    /** 
     * @Column(type="boolean")
     **/
    protected $active;

    const INPUT_PAPER = 1;
    const INPUT_NORMAL = 0;


    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->referees = new ArrayCollection();
        $this->rotations = new ArrayCollection();
        $this->schoolGroups = new ArrayCollection();
        $this->rotationSettings = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSinglePassNumber()
    {
        return $this->single_pass_number;
    }

    /**
     * @param mixed $single_pass_number
     */
    public function setSinglePassNumber($single_pass_number)
    {
        $this->single_pass_number = $single_pass_number;
    }

    /**
     * @return mixed
     */
    public function getSex($bit = true)
    {
        return $this->sex;
    }


    /**
     * [getStrSex description]
     * @return [type] [description]
     */
    public function getStrSex()
    {
        return ($this->isMale()) ? '男子' : '女子';
    }


    /**
     * [setSex description]
     * @param [mix] $sex [description]
     */
    public function setSex($sex)
    {
        if($sex == '男子') $sex = true;
        if($sex == '女子') $sex = false;

        $this->sex = $sex;
    }

    /**
     * [isMale description]
     * @return boolean [description]
     */
    public function isMale()
    {
        return $this->sex;
    }


    /**
     * [isFemale description]
     * @return boolean [description]
     */
    public function isFemale()
    {
        return !$this->sex;
    }


    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }


    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    public function isActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * @return mixed
     */
    public function getPreliminary()
    {
        return $this->preliminary;
    }

    /**
     * @param mixed $preliminary
     */
    public function setPreliminary($preliminary)
    {
        $this->preliminary = $preliminary;
    }

    /**
     * @return mixed
     */
    public function getGroupPassNumber()
    {
        return $this->group_pass_number;
    }

    /**
     * @param mixed $group_pass_number
     */
    public function setGroupPassNumber($group_pass_number)
    {
        $this->group_pass_number = $group_pass_number;
    }

    /**
     * @return mixed
     */
    public function getItemPassNumber()
    {
        return $this->item_pass_number;
    }

    /**
     * @param mixed $item_pass_number
     */
    public function setItemPassNumber($item_pass_number)
    {
        $this->item_pass_number = $item_pass_number;
    }

    /**
     * @return mixed
     */
    public function getInputType()
    {
        return $this->input_type;
    }

    /**
     * @param mixed $input_type
     */
    public function setInputType($input_type)
    {
        $this->input_type = $input_type;
    }

    /**
     * [isInputPaper description]
     * @return boolean [description]
     */
    public function isInputPaper()
    {
        return $this->input_type == Game::INPUT_PAPER;
    }

    /**
     * [isInputNormal description]
     * @return boolean [description]
     */
    public function isInputNormal()
    {
        return $this->input_type == Game::INPUT_NORMAL;   
    }

    /**
     * @return mixed
     */
    public function getInputFinalRound()
    {
        return $this->input_final_round;
    }

    /**
     * @param mixed $input_final_round
     */
    public function setInputFinalRound($input_final_round)
    {
        $this->input_final_round = $input_final_round;
    }

    /**
     * @return mixed
     */
    public function getInputLineScore()
    {
        return $this->input_line_score;
    }

    /**
     * @param mixed $input_final_round
     */
    public function setInputLineScore($input_line_score)
    {
        $this->input_line_score = ($input_line_score) ? true : false;
    }

    /**
     * @return mixed
     */
    public function getInputTimeScore()
    {
        return $this->input_time_score;
    }

    /**
     * @param mixed $input_final_round
     */
    public function setInputTimeScore($input_time_score)
    {
        $this->input_time_score = ($input_time_score) ? true : false;
    }

    /**
     * @return mixed
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * @param mixed $tournament
     */
    public function setTournament($tournament)
    {
        $this->tournament = $tournament;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add items
     *
     * @param \Entities\Item $items
     * @return Game
     */
    public function addItem(\Entities\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Entities\Item $items
     */
    public function removeItem(\Entities\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Add players
     *
     * @param \Entities\Player $players
     * @return Game
     */
    public function addPlayer(\Entities\Player $players)
    {
        $this->players[] = $players;

        return $this;
    }

    /**
     * Remove players
     *
     * @param \Entities\Player $players
     */
    public function removePlayer(\Entities\Player $players)
    {
        $this->players->removeElement($players);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add referees
     *
     * @param \Entities\Referee $referees
     * @return Game
     */
    public function addReferee(\Entities\Referee $referees)
    {
        $this->referees[] = $referees;

        return $this;
    }

    /**
     * Remove referees
     *
     * @param \Entities\Referee $referees
     */
    public function removeReferee(\Entities\Referee $referees)
    {
        $this->referees->removeElement($referees);
    }


    /**
     * Get referees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReferees()
    {
        return $this->referees;
    }

    /**
     * @return mixed
     */
    public function getRotations()
    {
        return $this->rotations;
    }

    /**
     * @param Rotation $rotation
     */
    public function addRotation(\Entities\Rotation $rotation)
    {
        $this->rotations[] = $rotation;
    }

    /**
     * Remove rotations
     *
     * @param \Entities\Rotation $rotations
     */
    public function removeRotation(\Entities\Rotation $rotations)
    {
        $this->rotations->removeElement($rotations);
    }

    /**
     * Add schoolGroups
     *
     * @param \Entities\SchoolGroup $schoolGroups
     * @return Game
     */
    public function addSchoolGroup(\Entities\SchoolGroup $schoolGroups)
    {
        $this->schoolGroups[] = $schoolGroups;

        return $this;
    }

    public function checkSchoolExist ($schoolAB)
    {

        foreach ($this->schoolGroups as $school) {
            if($school->getSchoolNameAb() == $schoolAB)
                return $school;
        }

        return false;
    }

    /**
     * Remove schoolGroups
     *
     * @param \Entities\SchoolGroup $schoolGroups
     */
    public function removeSchoolGroup(\Entities\SchoolGroup $schoolGroups)
    {
        $this->schoolGroups->removeElement($schoolGroups);
    }

    /**
     * Get schoolGroups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSchoolGroups()
    {
        return $this->schoolGroups;
    }

    /**
     * Get schoolGroups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRotationSettings()
    {
        return $this->rotationSettings;
    }

    /**
     * set rotationSettings
     *
     * @param \Entities\SchoolGroup $rotationSetting
     * @return Game
     */
    public function setRotationSetting(\Entities\RotationSetting $rotationSetting)
    {
        $this->rotationSettings[] = $rotationSetting;

        return $this;
    }

    public function removeRotationSetting(\Entities\RotationSetting $rotationSetting)
    {
        $this->rotationSettings->removeElement($rotationSetting);
    }

    /**
     * [findItem]
     * @param  string $name
     * @return \Entities\Item
     */
    public function findItem($name = '')
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $name));

        return $this->items->matching($criteria)->first();
    }

    /**
     * getDefaultItemName
     *
     * take a name of item for score edit screen.
     * I choose the last game's item. (change it for free.)
     * 
     * @param  void
     * @return string 
     */

    public function getDefaultItemName()
    {
        $item = $this->items[count($this->items)-1];
        
        if(!$item) throw new Exception('Game did not have item');
        
        return $item->getName();
    }

    /**
     * getItemKeyByName
     *
     * found item's key by a name
     * 
     * @param  string
     * @return string 
     */

    public function getItemKeyByName($name)
    {
        foreach ($this->items as $item) {
            if($item->getName() === $name)
                return $item->getId();
        }

        return null;
    }

    public function getGameInfo($orderby = false)
    {
        $gameInfo = [];

        if ($orderby) {
            $criteria = Criteria::create()
                ->orderBy(['group' => Criteria::ASC, 'heat' => Criteria::ASC]);
            $players = $this->getPlayers()->matching($criteria);
        } else {
            $players = $this->getPlayers();
        }

        foreach ($players as $player) {
            $gameInfo['groupheats'][$player->getGroup()][$player->getHeat()] = $player->getHeat();
            $gameInfo['prefs'][] = $player->getPrefecture();
            $gameInfo['schools'][] = $player->getSchoolName();
            $gameInfo['nplayers'][] = $player->getPlayerName();    
        }

        return $gameInfo;
    }

    public function getGroupsHeat()
    {
        $groupsheat = [];

        foreach ($this->players as $player) {
            $groupsheat[$player->getGroup()][$player->getHeat()] = $player->getHeat();  
        }

        return $groupsheat;
    }



    /**
     * [findGroupCurrentPlay - find group current playing
     * by check score player (published) in last heat each group
     * if exist score, that mean group was finished,
     * if not exist score, that mean group being play]
     * 
     * @return [int $group] [description]
     */
    public function findGroupCurrentPlay()
    {
        $lastHeatsRotate = [];

        // rotation last heat in group
        foreach ($this->rotations as $rotation) {
            $lastHeatsRotate[$rotation->getGroup()] = $rotation;
        }

        ////////////////////////////////////////////////////////
        // condition for group current play is item's score   //
        // of last player in last heat in group must be empty //
        ////////////////////////////////////////////////////////
        
        $group = 1; // Set group current default is 1

        foreach ($lastHeatsRotate as $group => $rotation) {

            // get last item in play in heat
            $lastItem = $this->findLastItemNotRelax($rotation->getFirstItem());

            // last player in heat
            $criteria = Criteria::create()->where(Criteria::expr()->eq('group', $group))
                                            ->andWhere(Criteria::expr()->eq('heat', $rotation->getHeat()))
                                            ->orderBy(['order' => Criteria::ASC]);

            $player = $this->players->matching($criteria)->last();

            // if last player have score else next group active
            if($this->isInputPaper())
            {
                $scores = array_filter($player->getItemScores(), function($score) use ($lastItem) {
                    return $score->getItem() == $lastItem->getName() && $score->getStatus() == \Entities\SingleItemScore::PUBLISH;
                });
                
                if (!$scores) return $group;
            }
            else 
            {
                if($player->getItemScoreStatus($lastItem->getName()) != \Entities\SingleItemScore::PUBLISH) 
                return $group;
            }
        }

        return $group;
    }


    /**
     * [findRotateCurrentPlay description]
     * @return [type] [description]
     */
    public function findRotateCurrentPlay()
    {
        $currentRotate = 1;

        $groupCurrentPlay = $this->findGroupCurrentPlay();

        $rotationSettings   = $this->getRotationSettings();
        $groupRotations     = $this->findFirstRotationGroup($groupCurrentPlay);

        $rotateFirsts       = new RotationCollection($groupRotations, $rotationSettings);

        $lastHeat = $rotateFirsts->rotations->last()->getHeat();

        $items = $this->items;

        for($currentRotate = 1; $currentRotate <= $this->numberRotation(); $currentRotate++)
        {
            $rotateCurrent = $rotateFirsts->doRotate($currentRotate-1);

            // remove relax item
            $rotateCurrentWithoutRelax = $rotateCurrent->rotations->filter(function($r) use ($items) {

                $exists = $items->filter(function($i) use ($r){
                    return $i->getName() == $r->getFirstItem();
                })->count();

                return ($exists) ? true : false;

            });
            
            $lastItem = $rotateCurrentWithoutRelax->last();
            
            $criteria = Criteria::create()->where(Criteria::expr()->eq('group', $groupCurrentPlay))
                                            ->andWhere(Criteria::expr()->eq('heat', $lastHeat));

            $players = $this->players->matching($criteria);

            // if all player have published score, next rotate active
            $allPublish = true;
            if($this->isInputPaper())
            {
                foreach ($players as $player) {
                    $scores = array_filter($player->getItemScores(), function($score) use ($lastItem) {
                        return $score->getItem() == $lastItem->getFirstItem() && $score->getStatus() == \Entities\SingleItemScore::PUBLISH;
                    });

                    if (!count($scores)) $allPublish = false;
                }
            }
            else
            {
                foreach ($players as $player) {
                    if($player->getItemScoreStatus($lastItem->getFirstItem()) != \Entities\SingleItemScore::PUBLISH) $allPublish = false;
                }
            }

            if(false == $allPublish) break;
        }

        return $currentRotate;
    }


    /**
     * [fullRotate description]
     * @param  [type] $rotateFirst [description]
     * @return [type]            [description]
     */
    private function fullRotate($firstItem)
    {
        $fullRotate = $this->rotationSettings->toArray();

        while ($fullRotate[0]->getName() != $firstItem) {
            array_push($fullRotate, array_shift($fullRotate));
        }

        return $fullRotate;
    }


    /**
     * [findLastItem description]
     * @param  [type] $rotation [description]
     * @return [type]           [description]
     */
    private function findLastItemNotRelax($firstItem)
    {
        $fullRotate = $this->fullRotate($firstItem);

        $items = $this->items;

        // remove item relax out of rotate
        $itemsWithoutRelax = array_filter($fullRotate, function($r) use ($items) {

            $itemName = $r->getName();

            $exists = $items->filter(function($i) use ($itemName){
                return $i->getName() == $itemName;
            })->count();

            return ($exists) ? true:false;
        });

        // take last item from rotate after remove relax
        return end($itemsWithoutRelax);
    }


    /**
     * [findHeatRelaxInRotation description]
     * @param  [type] $group [description]
     * @return [type]        [description]
     */
    public function findHeatRelaxInRotation($group = 1, $rotate = 1)
    {
        $rotate = 1;
        $criteria = Criteria::create()->where(Criteria::expr()->eq('group', $group))
                                            ->orderBy(['heat' => Criteria::ASC]);

        $rotations = $this->rotations->matching($criteria);

        // detect first item for each heat
        $firstItemHeats = [];

        foreach ($rotations as $rotateHeat) {
            foreach ($this->rotationSettings as $rost) {
                if($rost->getOrder() == $rotateHeat->getFirstItem())
                    $firstItemHeats[$rotateHeat->getHeat()] = $rost->getName();
            }
        }

        $items = $this->items;

        $currentItemHeats = $this->rotateItem($firstItemHeats, $rotate);

        // remove non relax item
        $currentItemHeatRelax = array_filter($currentItemHeats, function($cit) use ($items) {

            $exists = $items->filter(function($i) use ($cit){
                return $i->getName() == $cit;
            })->count();

            return (!$exists) ? true : false;

        });

        // heat = key+1
        return $currentItemHeatRelax;
    }


    /**
     * [numberRotation description]
     * @return [type] [description]
     */
    public function numberRotation()
    {
        return $this->rotationSettings->count();
    }


    /**
     * [findFirstRotationGroup description]
     * @param  integer $group [description]
     * @return [type]         [description]
     */
    public function findFirstRotationGroup($group = 1)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('group', $group))
                                        ->orderBy(['heat' => Criteria::ASC]);

        $rotations = $this->rotations->matching($criteria);

        if($rotations->count()==0) throw new \Exception('Rotations empty, please setting them first');

        return $rotations;
    }

    /**
     * [rotateItem description]
     * @param  array   $items [description]
     * @param  integer $n     [description]
     * @return [type]         [description]
     */
    private function rotateItem($items = [], $n = 0)
    {
        if($n) {
        
            for($i = 1; $i < $n; $i++)
                array_push($items, array_shift($items));
        }

        return array_values($items);
    }


    public function __toString()
    {
        $sex = ($this->isMale()) ? '男子' : '女子';

        return $sex . $this->class;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/13/2015
 * Time: 7:07 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="single_item_score")
 * @Entity(repositoryClass="Entities\Repository\SingleItemScoreRepository")
 **/
class SingleItemScore
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Rotation")
     * @JoinColumn(name="rotation_id", referencedColumnName="id")
     */
    protected $rotation;
    /**
     * @ManyToOne(targetEntity="Player")
     * @JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;
    /**
     * @Column(type="string")
     **/
    protected $item;
    /**
     * @Column(name="`order`", type="integer", nullable=true)
     **/
    protected $order;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $status;
    /**
     * @Column(name="final_score", type="float",nullable=true)
     **/
    protected $finalScore;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $demerit_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d1_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d2_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d3_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d4_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $d5_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e1_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e2_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e3_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e4_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e5_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $e_demerit_score;
    /**
     * @Column(type="string",nullable=true)
     **/
    protected $time1_score;
    /**
     * @Column(type="string",nullable=true)
     **/
    protected $time2_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $time_demerit_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $line1_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $line2_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $line_demerit_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $other_demerit;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $d1_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $d2_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $d3_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $d4_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $d5_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $e1_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $e2_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $e3_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $e4_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $e5_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $time1_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $time2_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $line1_score_status;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $line2_score_status;
    /**
     * @Column(type="integer",nullable=true, options={"default" = 1})
     **/
    protected $round;
    /**
     * @Column(type="boolean",nullable=true)
     **/
    protected $best_score_flag;

    const ENTRY     = 0;
    const MODIFY    = 1;
    const PUBLISH   = 2;
    const ROUND1    = 1;
    const ROUND2    = 2;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return SingleItemScore
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return SingleItemScore
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SingleItemScore
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set final_score
     *
     * @param float $finalScore
     * @return SingleItemScore
     */
    public function setFinalScore($finalScore = null)
    {
        if($finalScore) $this->finalScore = $finalScore;

        else{
            $this->finalScore = ($this->d_score + $this->e_score - $this->demerit_score);
        }

        return $this;
    }

    /**
     * Get final_score
     *
     * @return float
     */
    public function getFinalScore()
    {
        return $this->finalScore;
    }

    /**
     * Set d_score
     *
     * @param float $dScore
     * @return SingleItemScore
     */
    public function setDScore($dScore = null, $numberOfDscore = null)
    {
        if($dScore) $this->d_score = $dScore;

        else {

            $this->d_score = 0;

            for ($i=1; $i <= $numberOfDscore; $i++) { 
                $property = 'd'. $i . '_score';
                $this->d_score += ($this->$property !== null) ? $this->$property : 0;
            }

            $this->d_score = $this->d_score / $numberOfDscore;
        }

        return $this;
    }

    /**
     * Get d_score
     *
     * @return float
     */
    public function getDScore()
    {
        return $this->d_score;
    }

    /**
     * Set e_score
     *
     * @param float $eScore
     * @return SingleItemScore
     */
    public function setEScore($eScore = null, $numberOfEscore = null)
    {
        if($eScore) $this->e_score = $eScore;

        else {
            
            $sortScore = [];

            for ($i=1; $i <= $numberOfEscore; $i++) { 
                $property = 'e'. $i . '_score';
                $sortScore[] = ($this->$property !== null) ? $this->$property : 0;
            }

            rsort($sortScore);

            if($numberOfEscore == 1)
            {
                $this->e_score = $sortScore[0] - $this->e_demerit_score;
            }
            else if($numberOfEscore == 2)
            {
                $this->e_score = ($sortScore[0] + $sortScore[1]) / 2 - $this->e_demerit_score;   
            }
            else if($numberOfEscore == 3)
            {
                $this->e_score = ($sortScore[0] + $sortScore[1]) / 2 - $this->e_demerit_score;
            }
            else
            {
                $avg = 0;
                // remove highest score and lowest score
                $sortScore = array_slice($sortScore, 1, count($sortScore)-2);

                foreach ($sortScore as $score) {
                    $avg += $score;    
                }

                $avg = $avg / count($sortScore);

                $this->e_score = $avg - $this->e_demerit_score;
            }
        }

        if ($this->e_score < 0) $this->e_score = 0;

        return $this;
    }

    /**
     * Get e_score
     *
     * @return float
     */
    public function getEScore()
    {
        return $this->e_score;
    }

    /**
     * Set demerit_score
     *
     * @param float $demeritScore
     * @return SingleItemScore
     */
    public function setDemeritScore($demeritScore = null)
    {
        if($demeritScore) $this->demerit_score = $demeritScore;

        else $this->demerit_score = ($this->other_demerit + $this->time_demerit_score + $this->line_demerit_score);

        return $this;
    }

    /**
     * Get demerit_score
     *
     * @return float
     */
    public function getDemeritScore()
    {
        return $this->demerit_score;
    }

    /**
     * Set d1_score
     *
     * @param float $d1Score
     * @return SingleItemScore
     */
    public function setD1Score($d1Score)
    {
        $this->d1_score = $d1Score;

        return $this;
    }

    /**
     * Get d1_score
     *
     * @return float
     */
    public function getD1Score()
    {
        return $this->d1_score;
    }

    /**
     * Set d2_score
     *
     * @param float $d2Score
     * @return SingleItemScore
     */
    public function setD2Score($d2Score)
    {
        $this->d2_score = $d2Score;

        return $this;
    }

    /**
     * Get d2_score
     *
     * @return float
     */
    public function getD2Score()
    {
        return $this->d2_score;
    }

    /**
     * Set d3_score
     *
     * @param float $d3Score
     * @return SingleItemScore
     */
    public function setD3Score($d3Score)
    {
        $this->d3_score = $d3Score;

        return $this;
    }

    /**
     * Get d3_score
     *
     * @return float
     */
    public function getD3Score()
    {
        return $this->d3_score;
    }

    /**
     * Set d4_score
     *
     * @param float $d4Score
     * @return SingleItemScore
     */
    public function setD4Score($d4Score)
    {
        $this->d4_score = $d4Score;

        return $this;
    }

    /**
     * Get d4_score
     *
     * @return float
     */
    public function getD4Score()
    {
        return $this->d4_score;
    }

    /**
     * Set d5_score
     *
     * @param float $d5Score
     * @return SingleItemScore
     */
    public function setD5Score($d5Score)
    {
        $this->d5_score = $d5Score;

        return $this;
    }

    /**
     * Get d5_score
     *
     * @return float
     */
    public function getD5Score()
    {
        return $this->d5_score;
    }

    /**
     * Set e1_score
     *
     * @param float $e1Score
     * @return SingleItemScore
     */
    public function setE1Score($e1Score)
    {
        $this->e1_score = $e1Score;

        return $this;
    }

    /**
     * Get e1_score
     *
     * @return float
     */
    public function getE1Score()
    {
        return $this->e1_score;
    }

    /**
     * Set e2_score
     *
     * @param float $e2Score
     * @return SingleItemScore
     */
    public function setE2Score($e2Score)
    {
        $this->e2_score = $e2Score;

        return $this;
    }

    /**
     * Get e2_score
     *
     * @return float
     */
    public function getE2Score()
    {
        return $this->e2_score;
    }

    /**
     * Set e3_score
     *
     * @param float $e3Score
     * @return SingleItemScore
     */
    public function setE3Score($e3Score)
    {
        $this->e3_score = $e3Score;

        return $this;
    }

    /**
     * Get e3_score
     *
     * @return float
     */
    public function getE3Score()
    {
        return $this->e3_score;
    }

    /**
     * Set e4_score
     *
     * @param float $e4Score
     * @return SingleItemScore
     */
    public function setE4Score($e4Score)
    {
        $this->e4_score = $e4Score;

        return $this;
    }

    /**
     * Get e4_score
     *
     * @return float
     */
    public function getE4Score()
    {
        return $this->e4_score;
    }

    /**
     * Set e5_score
     *
     * @param float $e5Score
     * @return SingleItemScore
     */
    public function setE5Score($e5Score)
    {
        $this->e5_score = $e5Score;

        return $this;
    }

    /**
     * Get e5_score
     *
     * @return float
     */
    public function getE5Score()
    {
        return $this->e5_score;
    }

    /**
     * Set e_demerit_score
     *
     * @param float $eDemeritScore
     * @return SingleItemScore
     */
    public function setEDemeritScore($eDemeritScore)
    {
        $this->e_demerit_score = $eDemeritScore;

        return $this;
    }

    /**
     * Get e_demerit_score
     *
     * @return float
     */
    public function getEDemeritScore()
    {
        return $this->e_demerit_score;
    }

    /**
     * Set time1_score
     *
     * @param string $time1Score
     * @return SingleItemScore
     */
    public function setTime1Score($time1Score)
    {
        $this->time1_score = $time1Score;

        return $this;
    }

    /**
     * Get time1_score
     *
     * @return string
     */
    public function getTime1Score()
    {
        return $this->time1_score;
    }

    /**
     * Set time2_score
     *
     * @param string $time2Score
     * @return SingleItemScore
     */
    public function setTime2Score($time2Score)
    {
        $this->time2_score = $time2Score;

        return $this;
    }

    /**
     * Get time2_score
     *
     * @return string
     */
    public function getTime2Score()
    {
        return $this->time2_score;
    }

    /**
     * Set time_demerit_score
     *
     * @param float $timeDemeritScore
     * @return SingleItemScore
     */
    public function setTimeDemeritScore($timeDemeritScore)
    {
        $this->time_demerit_score = $timeDemeritScore;

        return $this;
    }

    /**
     * Get time_demerit_score
     *
     * @return float 
     */
    public function getTimeDemeritScore()
    {
        return $this->time_demerit_score;
    }

    /**
     * Set line1_score
     *
     * @param float $line1Score
     * @return SingleItemScore
     */
    public function setLine1Score($line1Score)
    {
        $this->line1_score = $line1Score;

        return $this;
    }

    /**
     * Get line1_score
     *
     * @return float
     */
    public function getLine1Score()
    {
        return $this->line1_score;
    }

    /**
     * Set line2_score
     *
     * @param float $line2Score
     * @return SingleItemScore
     */
    public function setLine2Score($line2Score)
    {
        $this->line2_score = $line2Score;

        return $this;
    }

    /**
     * Get line2_score
     *
     * @return float
     */
    public function getLine2Score()
    {
        return $this->line2_score;
    }

    /**
     * Set line_demerit_score
     *
     * @param float $lineDemeritScore
     * @return SingleItemScore
     */
    public function setLineDemeritScore($lineDemeritScore)
    {
        $this->line_demerit_score = $lineDemeritScore;

        return $this;
    }

    /**
     * Get line_demerit_score
     *
     * @return float 
     */
    public function getLineDemeritScore()
    {
        return $this->line_demerit_score;
    }

    /**
     * Set other_demerit
     *
     * @param float $otherDemerit
     * @return SingleItemScore
     */
    public function setOtherDemerit($otherDemerit)
    {
        $this->other_demerit = $otherDemerit;

        return $this;
    }

    /**
     * Get other_demerit
     *
     * @return float
     */
    public function getOtherDemerit()
    {
        return $this->other_demerit;
    }

    /**
     * Set rotation
     *
     * @param \Entities\Rotation $rotation
     * @return SingleItemScore
     */
    public function setRotation(\Entities\Rotation $rotation = null)
    {
        $this->rotation = $rotation;

        return $this;
    }

    /**
     * Get rotation
     *
     * @return \Entities\Rotation
     */
    public function getRotation()
    {
        return $this->rotation;
    }

    /**
     * Set player
     *
     * @param \Entities\Player $player
     * @return SingleItemScore
     */
    public function setPlayer(\Entities\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Entities\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }


    /**
     * Get d1_score_status
     *
     * @return bool
     */
    public function getD1ScoreStatus()
    {
        return $this->d1_score_status;
    }

    /**
     * Set d1_score_status
     *
     * @param bool $d1_score_status
     * @return SingleItemScore
     */
    public function setD1ScoreStatus($d1_score_status)
    {
        $this->d1_score_status = $d1_score_status;

        return $this;
    }

    /**
     * Get d2_score_status
     *
     * @return bool
     */
    public function getD2ScoreStatus()
    {
        return $this->d2_score_status;
    }

    /**
     * Set d2_score_status
     *
     * @param bool $d2_score_status
     * @return SingleItemScore
     */
    public function setD2ScoreStatus($d2_score_status)
    {
        $this->d2_score_status = $d2_score_status;

        return $this;
    }

    /**
     * Get d3_score_status
     *
     * @return bool
     */
    public function getD3ScoreStatus()
    {
        return $this->d3_score_status;
    }

    /**
     * Set d3_score_status
     *
     * @param bool $d3_score_status
     * @return SingleItemScore
     */
    public function setD3ScoreStatus($d3_score_status)
    {
        $this->d3_score_status = $d3_score_status;

        return $this;
    }

    /**
     * Get d4_score_status
     *
     * @return bool
     */
    public function getD4ScoreStatus()
    {
        return $this->d4_score_status;
    }

    /**
     * Set d4_score_status
     *
     * @param bool $d4_score_status
     * @return SingleItemScore
     */
    public function setD4ScoreStatus($d4_score_status)
    {
        $this->d4_score_status = $d4_score_status;

        return $this;
    }

    /**
     * Get d5_score_status
     *
     * @return bool
     */
    public function getD5ScoreStatus()
    {
        return $this->d5_score_status;
    }

    /**
     * Set d5_score_status
     *
     * @param bool $d5_score_status
     * @return SingleItemScore
     */
    public function setD5ScoreStatus($d5_score_status)
    {
        $this->d5_score_status = $d5_score_status;

        return $this;
    }

        /**
     * Get e1_score_status
     *
     * @return bool
     */
    public function getE1ScoreStatus()
    {
        return $this->e1_score_status;
    }

    /**
     * Set e1_score_status
     *
     * @param bool $e1_score_status
     * @return SingleItemScore
     */
    public function setE1ScoreStatus($e1_score_status)
    {
        $this->e1_score_status = $e1_score_status;

        return $this;
    }

    /**
     * Get e2_score_status
     *
     * @return bool
     */
    public function getE2ScoreStatus()
    {
        return $this->e2_score_status;
    }

    /**
     * Set e2_score_status
     *
     * @param bool $e2_score_status
     * @return SingleItemScore
     */
    public function setE2ScoreStatus($e2_score_status)
    {
        $this->e2_score_status = $e2_score_status;

        return $this;
    }

    /**
     * Get e3_score_status
     *
     * @return bool
     */
    public function getE3ScoreStatus()
    {
        return $this->e3_score_status;
    }

    /**
     * Set e3_score_status
     *
     * @param bool $e3_score_status
     * @return SingleItemScore
     */
    public function setE3ScoreStatus($e3_score_status)
    {
        $this->e3_score_status = $e3_score_status;

        return $this;
    }

    /**
     * Get e4_score_status
     *
     * @return bool
     */
    public function getE4ScoreStatus()
    {
        return $this->e4_score_status;
    }

    /**
     * Set e4_score_status
     *
     * @param bool $e4_score_status
     * @return SingleItemScore
     */
    public function setE4ScoreStatus($e4_score_status)
    {
        $this->e4_score_status = $e4_score_status;

        return $this;
    }

    /**
     * Get e5_score_status
     *
     * @return bool
     */
    public function getE5ScoreStatus()
    {
        return $this->e5_score_status;
    }

    /**
     * Set e5_score_status
     *
     * @param bool $e5_score_status
     * @return SingleItemScore
     */
    public function setE5ScoreStatus($e5_score_status)
    {
        $this->e5_score_status = $e5_score_status;

        return $this;
    }

    /**
     * Get line1_score_status
     *
     * @return bool
     */
    public function getLine1ScoreStatus()
    {
        return $this->line1_score_status;
    }

    /**
     * Set line1_score_status
     *
     * @param bool $line1_score_status
     * @return SingleItemScore
     */
    public function setLine1ScoreStatus($line1_score_status)
    {
        $this->line1_score_status = $line1_score_status;

        return $this;
    }

    /**
     * Get line2_score_status
     *
     * @return bool
     */
    public function getLine2ScoreStatus()
    {
        return $this->line2_score_status;
    }

    /**
     * Set line2_score_status
     *
     * @param bool $line2_score_status
     * @return SingleItemScore
     */
    public function setLine2ScoreStatus($line2_score_status)
    {
        $this->line2_score_status = $line2_score_status;

        return $this;
    }

    /**
     * Get time1_score_status
     *
     * @return bool
     */
    public function getTime1ScoreStatus()
    {
        return $this->time1_score_status;
    }

    /**
     * Set time1_score_status
     *
     * @param bool $time1_score_status
     * @return SingleItemScore
     */
    public function setTime1ScoreStatus($time1_score_status)
    {
        $this->time1_score_status = $time1_score_status;

        return $this;
    }

    /**
     * Get time2_score_status
     *
     * @return bool
     */
    public function getTime2ScoreStatus()
    {
        return $this->time2_score_status;
    }

    /**
     * Set time2_score_status
     *
     * @param bool $time2_score_status
     * @return SingleItemScore
     */
    public function setTime2ScoreStatus($time2_score_status)
    {
        $this->time2_score_status = $time2_score_status;

        return $this;
    }

    public function getRound()
    {
        return $this->round;
    }

    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }


    /**
     * [setBestScoreFlag description]
     * @param [type] $isBestScore [description]
     */
    public function setBestScoreFlag($isBestScore)
    {
        $this->best_score_flag = $isBestScore;

        return $this;
    }

    /**
     * [isBestScore description]
     * @return boolean [description]
     */
    public function isBestScore()
    {
        return $this->best_score_flag == true;
    }
}

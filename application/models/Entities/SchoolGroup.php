<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/13/2015
 * Time: 7:07 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="school_group")
 * @Entity(repositoryClass="Entities\Repository\SchoolGroupRepository")
 **/
class SchoolGroup
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @Column(type="string")
     **/
    protected $school_name;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $school_name_ab;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $school_no;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $school_prefecture;
    /**
     * @OneToOne(targetEntity="GroupScore", mappedBy="school_group", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @JoinColumn(name="group_score_id", referencedColumnName="id")
     */
    protected $group_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    protected $cancel_flag;

    protected $ranking;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set school_name
     *
     * @param string $schoolName
     * @return SchoolGroup
     */
    public function setSchoolName($schoolName)
    {
        $this->school_name = $schoolName;

        return $this;
    }

    /**
     * Get school_name
     *
     * @return string 
     */
    public function getSchoolName()
    {
        return $this->school_name;
    }

    /**
     * Set school_name_ab
     *
     * @param string $schoolNameAb
     * @return SchoolGroup
     */
    public function setSchoolNameAb($schoolNameAb)
    {
        $this->school_name_ab = $schoolNameAb;

        return $this;
    }

    /**
     * Get school_name_ab
     *
     * @return string 
     */
    public function getSchoolNameAb()
    {
        return $this->school_name_ab;
    }

    /**
     * Set school_no
     *
     * @param string $schoolNo
     * @return SchoolGroup
     */
    public function setSchoolNo($schoolNo)
    {
        $this->school_no = $schoolNo;

        return $this;
    }

    /**
     * Get school_no
     *
     * @return string 
     */
    public function getSchoolNo()
    {
        return $this->school_no;
    }

    /**
     * Set game
     *
     * @param \Entities\Game $game
     * @return SchoolGroup
     */
    public function setGame(\Entities\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Entities\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Add group_score
     *
     * @param \Entities\GroupScore $groupScores
     * @return SchoolGroup
     */
    public function setGroupScore(\Entities\GroupScore $groupScore)
    {
        $this->group_score = $groupScore;

        return $this;
    }

    public function removeGroupScore()
    {
        $this->group_score = null;
    }

    /**
     * Get group_score
     *
     * @return \Entities\GroupScore
     */
    public function getGroupScore()
    {
        return $this->group_score;
    }

    /**
     * Set school_prefecture
     *
     * @param string $schoolPrefecture
     * @return SchoolGroup
     */
    public function setSchoolPrefecture($schoolPrefecture)
    {
        $this->school_prefecture = $schoolPrefecture;

        return $this;
    }

    /**
     * Get school_prefecture
     *
     * @return string 
     */
    public function getSchoolPrefecture()
    {
        return $this->school_prefecture;
    }

    /**
     *  setCancelFlag
     *
     * @param string $cancelFlag
     * @return SchoolGroup
     */
    public function setCancelFlag($cancelFlag)
    {
        $this->cancel_flag = ($cancelFlag) ? true : false;

        return $this;
    }

    /**
     * getCancelFlag
     *
     * @return boolean 
     */
    public function getCancelFlag()
    {
        return $this->cancel_flag;
    }

    public function setRanking($rank)
    {
        $this->ranking = $rank;
    }

    public function getRanking()
    {
        return $this->ranking;
    }
}

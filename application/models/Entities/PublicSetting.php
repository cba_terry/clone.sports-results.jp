<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/16/2015
 * Time: 12:56 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="public_setting")
 **/
class PublicSetting
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @OneToOne(targetEntity="Tournament")
     * @JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $news_timing;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $group_timing;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $single_timing;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $item_timing;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $monitor_result_time;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $monitor_ranking_group;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $monitor_ranking_interbal;
    /**
     * @Column(type="string", nullable=true)
     **/
    public $monitor_rotation_interbal;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $news_timing_publish;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $group_timing_publish;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $single_timing_publish;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $item_timing_publish;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $news_timing_type;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $group_timing_type;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $single_timing_type;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $item_timing_type;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $news_timing_publish_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $group_timing_publish_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $single_timing_publish_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $item_timing_publish_score;
    /**
     * @Column(type="boolean", nullable=true)
     **/
    public $news_timing_publish_rank;




    /**
     * @return mixed
     */
    public function getMonitorResultTime()
    {
        return $this->monitor_result_time;
    }

    /**
     * @param mixed $monitor_result_time
     */
    public function setMonitorResultTime($monitor_result_time)
    {
        $this->monitor_result_time = $monitor_result_time;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * @param mixed $tournament
     */
    public function setTournament($tournament)
    {
        $this->tournament = $tournament;
    }

    /**
     * @return mixed
     */
    public function getNewsTiming()
    {
        return $this->news_timing;
    }

    /**
     * @param mixed $news_timing
     */
    public function setNewsTiming($news_timing)
    {
        $this->news_timing = $news_timing;
    }

    /**
     * @return mixed
     */
    public function getGroupTiming()
    {
        return $this->group_timing;
    }

    /**
     * @param mixed $group_timing
     */
    public function setGroupTiming($group_timing)
    {
        $this->group_timing = $group_timing;
    }

    /**
     * @return mixed
     */
    public function getSingleTiming()
    {
        return $this->single_timing;
    }

    /**
     * @param mixed $single_timing
     */
    public function setSingleTiming($single_timing)
    {
        $this->single_timing = $single_timing;
    }

    /**
     * @return mixed
     */
    public function getItemTiming()
    {
        return $this->item_timing;
    }

    /**
     * @param mixed $item_timing
     */
    public function setItemTiming($item_timing)
    {
        $this->item_timing = $item_timing;
    }

    /**
     * @return mixed
     */
    public function getMonitorRankingGroup()
    {
        return $this->monitor_ranking_group;
    }

    /**
     * @param mixed $monitor_ranking_group
     */
    public function setMonitorRankingGroup($monitor_ranking_group)
    {
        $this->monitor_ranking_group = $monitor_ranking_group;
    }

    /**
     * @return mixed
     */
    public function getMonitorRankingInterbal()
    {
        return $this->monitor_ranking_interbal;
    }

    /**
     * @param mixed $monitor_ranking_interbal
     */
    public function setMonitorRankingInterbal($monitor_ranking_interbal)
    {
        $this->monitor_ranking_interbal = $monitor_ranking_interbal;
    }

    /**
     * @return mixed
     */
    public function getNewsTimingPublish()
    {
        return $this->news_timing_publish;
    }

    /**
     * @param mixed $news_timing_publish
     */
    public function setNewsTimingPublish($news_timing_publish)
    {
        $news_timing_publish = ($news_timing_publish) ? true : false;
        $this->news_timing_publish = $news_timing_publish;
    }

    /**
     * @return mixed
     */
    public function getGroupTimingPublish()
    {
        return $this->group_timing_publish;
    }

    /**
     * @param mixed $group_timing_publish
     */
    public function setGroupTimingPublish($group_timing_publish)
    {
        $group_timing_publish = ($group_timing_publish) ? true : false;
        $this->group_timing_publish = $group_timing_publish;
    }

    /**
     * @return mixed
     */
    public function getSingleTimingPublish()
    {
        return $this->single_timing_publish;
    }

    /**
     * @param mixed $single_timing_type
     */
    public function setSingleTimingPublish($single_timing_publish)
    {
        $single_timing_publish = ($single_timing_publish) ? true : false;
        $this->single_timing_publish = $single_timing_publish;
    }

    /**
     * @return mixed
     */
    public function getItemTimingPublish()
    {
        return $this->item_timing_publish;
    }

    /**
     * @param mixed $item_timing_publish
     */
    public function setItemTimingPublish($item_timing_publish)
    {
        $item_timing_publish = ($item_timing_publish) ? true : false;
        $this->item_timing_publish = $item_timing_publish;
    }

    /**
     * @return mixed
     */
    public function getNewsTimingType()
    {
        return $this->news_timing_type;
    }

    /**
     * @param mixed $news_timing_publish
     */
    public function setNewsTimingType($news_timing_type)
    {
        $news_timing_type = ($news_timing_type) ? true : false;
        $this->news_timing_type = $news_timing_type;
    }

    /**
     * @return mixed
     */
    public function getGroupTimingType()
    {
        return $this->group_timing_type;
    }

    /**
     * @param mixed $news_timing_type
     */
    public function setGroupTimingType($group_timing_type)
    {
        $group_timing_type = ($group_timing_type) ? true : false;
        $this->group_timing_type = $group_timing_type;
    }

    /**
     * @return mixed
     */
    public function getSingleTimingType()
    {
        return $this->single_timing_type;
    }

    /**
     * @param mixed $single_timing_type
     */
    public function setSingleTimingType($single_timing_type)
    {
        $single_timing_type = ($single_timing_type) ? true : false;
        $this->single_timing_type = $single_timing_type;
    }

    /**
     * @return mixed
     */
    public function getItemTimingType()
    {
        return $this->item_timing_type;
    }

    /**
     * @param mixed $item_timing_type
     */
    public function setItemTimingType($item_timing_type)
    {
        $item_timing_type = ($item_timing_type) ? true : false;
        $this->item_timing_type = $item_timing_type;
    }

    /**
     * @return mixed
     */
    public function getNewsTimingPublishScore()
    {
        return $this->news_timing_publish_score;
    }

    /**
     * @param mixed $news_timing_publish
     */
    public function setNewsTimingPublishScore($news_timing_publish_score)
    {
        $news_timing_publish_score = ($news_timing_publish_score) ? true : false;
        $this->news_timing_publish_score = $news_timing_publish_score;
    }

    /**
     * @return mixed
     */
    public function getGroupTimingPublishScore()
    {
        return $this->group_timing_publish_score;
    }

    /**
     * @param mixed $news_timing_publish_score
     */
    public function setGroupTimingPublishScore($group_timing_publish_score)
    {
        $group_timing_publish_score = ($group_timing_publish_score) ? true : false;
        $this->group_timing_publish_score = $group_timing_publish_score;
    }

    /**
     * @return mixed
     */
    public function getSingleTimingPublishScore()
    {
        return $this->single_timing_publish_score;
    }

    /**
     * @param mixed $single_timing_publish_score
     */
    public function setSingleTimingPublishScore($single_timing_publish_score)
    {
        $single_timing_publish_score = ($single_timing_publish_score) ? true : false;
        $this->single_timing_publish_score = $single_timing_publish_score;
    }

    /**
     * @return mixed
     */
    public function getItemTimingPublishScore()
    {
        return $this->item_timing_publish_score;
    }

    /**
     * @param mixed $item_timing_publish_score
     */
    public function setItemTimingPublishScore($item_timing_publish_score)
    {
        $item_timing_publish_score = ($item_timing_publish_score) ? true : false;
        $this->item_timing_publish_score = $item_timing_publish_score;
    }

    /**
     * Set monitor_rotation_interbal
     *
     * @param string $monitorRotationInterbal
     * @return PublicSetting
     */
    public function setMonitorRotationInterbal($monitorRotationInterbal)
    {
        $this->monitor_rotation_interbal = $monitorRotationInterbal;

        return $this;
    }

    /**
     * Get monitor_rotation_interbal
     *
     * @return string 
     */
    public function getMonitorRotationInterbal()
    {
        return $this->monitor_rotation_interbal;
    }

    public function setNewsTimingPublishRank($news_timing_publish_rank)
    {
        $this->news_timing_publish_rank = $news_timing_publish_rank;

        return $this;
    }

    public function getNewsTimingPublishRank()
    {
        return $this->news_timing_publish_rank;
    }
}

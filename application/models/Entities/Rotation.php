<?php
namespace Entities;

/**
 * @Entity @Table(name="rotation")
 * @Entity(repositoryClass="Entities\Repository\RotationRepository")
 **/
class Rotation
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    /**
     * @Column(name="`group`", type="integer", nullable=true)
     **/
    protected $group;
    /**
     * @Column(type="integer", nullable=true)
     **/
    protected $heat;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $first_item;
    /**
     * @OneToMany(targetEntity="SingleItemScore", mappedBy="rotation")
     */
    protected $single_item_scores;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->single_item_scores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param integer $group
     * @return Rotation
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return integer 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set heat
     *
     * @param integer $heat
     * @return Rotation
     */
    public function setHeat($heat)
    {
        $this->heat = $heat;

        return $this;
    }

    /**
     * Get heat
     *
     * @return integer 
     */
    public function getHeat()
    {
        return $this->heat;
    }

    /**
     * Set first_item
     *
     * @param integer $firstItem
     * @return Rotation
     */
    public function setFirstItem($firstItem)
    {
        $this->first_item = $firstItem;

        return $this;
    }

    /**
     * Get first_item
     *
     * @return integer 
     */
    public function getFirstItem()
    {
        return $this->first_item;
    }

    /**
     * Set game
     *
     * @param \Entities\Game $game
     * @return Rotation
     */
    public function setGame(\Entities\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Entities\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Add single_item_scores
     *
     * @param \Entities\SingleItemScore $singleItemScores
     * @return Rotation
     */
    public function addSingleItemScore(\Entities\SingleItemScore $singleItemScores)
    {
        $this->single_item_scores[] = $singleItemScores;

        return $this;
    }

    /**
     * Remove single_item_scores
     *
     * @param \Entities\SingleItemScore $singleItemScores
     */
    public function removeSingleItemScore(\Entities\SingleItemScore $singleItemScores)
    {
        $this->single_item_scores->removeElement($singleItemScores);
    }

    /**
     * Get single_item_scores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSingleItemScores()
    {
        return $this->single_item_scores;
    }
}

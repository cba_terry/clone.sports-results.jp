<?php

namespace Entities;

/**
 * @Entity @Table(name="task")
 **/
class Task
{
	/**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
	protected $id;
	/**
     * @Column(type="integer")
     **/
	protected $single_item_score_id;

	public function setSingleItemScoreId($scoreId)
	{
		$this->single_item_score_id = $scoreId;
	}

	public function getSingleItemScoreId()
	{
		return $this->single_item_score_id;
	}

}
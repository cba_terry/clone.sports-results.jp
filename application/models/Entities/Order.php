<?php
// src/Order.php

namespace Entities;

/**
 * @Entity @Table(name="`order`")
 **/
class Order
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @ManyToOne(targetEntity="Player")
     * @JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;
    /**
     * @Column(name="`order`", type="integer", nullable=true)
     **/
    protected $order;
    /**
     * @Column(type="string", nullable=true)
     **/
    protected $item;

    public function __construct($item, $order)
    {
        $this->item = (string) $item;
        $this->order = $order;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param mixed $sex
     */
    public function setPlayer(\Entities\Player  $player)
    {
        $this->player = $player;
    }

    public function __toString()
    {
        return (string) $this->order;
    }
}

<?php
// src/Game.php

namespace Entities;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @Entity @Table(name="user")
 * @Entity(repositoryClass="Entities\Repository\UserRepository")
 **/
class User
{
    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    /** 
     * @Column(type="string")
     **/
    protected $user_code;
    /** 
     * @Column(type="string")
     **/
    protected $user_name;
    /**
     * @Column(type="string")
     **/
    protected $password;
    /**
     * @Column(type="string", nullable = true)
     **/
    protected $hash;
    /**
     * @ManyToOne(targetEntity="Association")
     * @JoinColumn(name="association_id", referencedColumnName="id")
     */
    protected $association;
    /**
     * @Column(type="datetime")
     **/
    protected $created;
    /**
     * @Column(type="datetime")
     **/
    protected $updated;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @param mixed $user_name
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAssociation()
    {
        return $this->association;
    }

    /**
     * @param mixed $association
     */
    public function setAssociation($association)
    {
        $this->association = $association;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Set user_code
     *
     * @param string $userCode
     * @return User
     */
    public function setUserCode($userCode)
    {
        $this->user_code = $userCode;

        return $this;
    }

    /**
     * Get user_code
     *
     * @return string 
     */
    public function getUserCode()
    {
        return $this->user_code;
    }
}

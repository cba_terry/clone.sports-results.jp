<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/13/2015
 * Time: 7:07 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="item_total_score")
 **/
class ItemTotalScore
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item1_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item1_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score1_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score1_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item2_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item2_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score2_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score2_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item3_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item3_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score3_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score3_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item4_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item4_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score4_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score4_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item5_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item5_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score5_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score5_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item6_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $item6_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score6_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score6_id2;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item1_score
     *
     * @param float $item1Score
     * @return SingleTotalScore
     */
    public function setItem1Score($item1Score)
    {
        $this->item1_score = $item1Score;

        return $this;
    }

    /**
     * Get item1_score
     *
     * @return float 
     */
    public function getItem1Score()
    {
        return $this->item1_score;
    }

    /**
     * Set item1_rank
     *
     * @param float $item1Score
     * @return SingleTotalScore
     */
    public function setItem1Rank($item1Rank)
    {
        $this->item1_rank = $item1Rank;

        return $this;
    }

    /**
     * Get item1_rank
     *
     * @return float 
     */
    public function getItem1Rank()
    {
        return $this->item1_rank;
    }

    /**
     * Set single_item_score1_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore1Id1($id)
    {
        $this->single_item_score1_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score1_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore1Id2($id)
    {
        $this->single_item_score1_id2 = $id;

        return $this;
    }

    /**
     * Set item2_score
     *
     * @param float $item2Score
     * @return SingleTotalScore
     */
    public function setItem2Score($item2Score)
    {
        $this->item2_score = $item2Score;

        return $this;
    }

    /**
     * Get item2_score
     *
     * @return float 
     */
    public function getItem2Score()
    {
        return $this->item2_score;
    }

    /**
     * Set item2_rank
     *
     * @param float $item1Score
     * @return SingleTotalScore
     */
    public function setItem2Rank($item2Rank)
    {
        $this->item2_rank = $item2Rank;

        return $this;
    }

    /**
     * Get item2_rank
     *
     * @return float 
     */
    public function getItem2Rank()
    {
        return $this->item2_rank;
    }

    /**
     * Set single_item_score2_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore2Id1($id)
    {
        $this->single_item_score2_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score2_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore2Id2($id)
    {
        $this->single_item_score2_id2 = $id;

        return $this;
    }

    /**
     * Set item3_score
     *
     * @param float $item3Score
     * @return SingleTotalScore
     */
    public function setItem3Score($item3Score)
    {
        $this->item3_score = $item3Score;

        return $this;
    }

    /**
     * Get item3_score
     *
     * @return float 
     */
    public function getItem3Score()
    {
        return $this->item3_score;
    }

    /**
     * Set item3_rank
     *
     * @param float $item3Score
     * @return SingleTotalScore
     */
    public function setItem3Rank($item3Rank)
    {
        $this->item3_rank = $item3Rank;

        return $this;
    }

    /**
     * Get item3_rank
     *
     * @return float 
     */
    public function getItem3Rank()
    {
        return $this->item3_rank;
    }

    /**
     * Set single_item_score3_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore3Id1($id)
    {
        $this->single_item_score3_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score3_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore3Id2($id)
    {
        $this->single_item_score3_id2 = $id;

        return $this;
    }

    /**
     * Set item4_score
     *
     * @param float $item4Score
     * @return SingleTotalScore
     */
    public function setItem4Score($item4Score)
    {
        $this->item4_score = $item4Score;

        return $this;
    }

    /**
     * Get item4_score
     *
     * @return float 
     */
    public function getItem4Score()
    {
        return $this->item4_score;
    }

    /**
     * Set item4_rank
     *
     * @param float $item4Score
     * @return SingleTotalScore
     */
    public function setItem4Rank($item4Rank)
    {
        $this->item4_rank = $item4Rank;

        return $this;
    }

    /**
     * Get item4_rank
     *
     * @return float 
     */
    public function getItem4Rank()
    {
        return $this->item4_rank;
    }

    /**
     * Set single_item_score4_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore4Id1($id)
    {
        $this->single_item_score4_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score4_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore4Id2($id)
    {
        $this->single_item_score4_id2 = $id;

        return $this;
    }

    /**
     * Set item5_score
     *
     * @param float $item5Score
     * @return SingleTotalScore
     */
    public function setItem5Score($item5Score)
    {
        $this->item5_score = $item5Score;

        return $this;
    }

    /**
     * Get item5_score
     *
     * @return float 
     */
    public function getItem5Score()
    {
        return $this->item5_score;
    }

    /**
     * Set item5_rank
     *
     * @param float $item5Score
     * @return SingleTotalScore
     */
    public function setItem5Rank($item5Rank)
    {
        $this->item5_rank = $item5Rank;

        return $this;
    }

    /**
     * Get item5_rank
     *
     * @return float 
     */
    public function getItem5Rank()
    {
        return $this->item5_rank;
    }

    /**
     * Set single_item_score5_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore5Id1($id)
    {
        $this->single_item_score5_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score5_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore5Id2($id)
    {
        $this->single_item_score5_id2 = $id;

        return $this;
    }

    /**
     * Set item6_score
     *
     * @param float $item6Score
     * @return SingleTotalScore
     */
    public function setItem6Score($item6Score)
    {
        $this->item6_score = $item6Score;

        return $this;
    }

    /**
     * Get item6_score
     *
     * @return float 
     */
    public function getItem6Score()
    {
        return $this->item6_score;
    }

    /**
     * Set item6_rank
     *
     * @param float $item6Score
     * @return SingleTotalScore
     */
    public function setItem6Rank($item6Rank)
    {
        $this->item6_rank = $item6Rank;

        return $this;
    }

    /**
     * Get item6_rank
     *
     * @return float 
     */
    public function getItem6Rank()
    {
        return $this->item6_rank;
    }

    /**
     * Set single_item_score6_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore6Id1($id)
    {
        $this->single_item_score6_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score5_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore6Id2($id)
    {
        $this->single_item_score6_id2 = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDemeritScore()
    {
        return $this->demerit_score;
    }

    /**
     * @param mixed $demerit_score
     */
    public function setDemeritScore($demerit_score)
    {
        $this->demerit_score = $demerit_score;
    }
}

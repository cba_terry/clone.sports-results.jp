<?php

namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="score")
 * @Entity(repositoryClass="Entities\Repository\ScoreRepository")
 **/
class Score
{
	/**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
	protected $id;
	/**
     * @ManyToOne(targetEntity="Player", inversedBy="scores")
     * @JoinColumn(name="player_id", referencedColumnName="id")
     */
	protected $player;
	/**
     * @Column(type="float")
     **/
	protected $value;
	/**
     * @Column(type="string")
     **/
	protected $type;
	/**
     * @Column(type="integer")
     **/
	protected $status;
	/**
     * @Column(type="string")
     **/
	protected $item;
	/**
     * @Column(type="integer")
     **/
	protected $round;
	/**
     * @Column(type="boolean")
     **/
	protected $picked;

	const ENTRY     = 0;
    const MODIFY    = 1;
    const PUBLISH   = 2;
    const ROUND1    = 1;
    const ROUND2    = 2;

    public function getId()
    {
    	return $this->id;
    }

	public function setPlayer(\Entities\Player $player)
	{
		$this->player = $player;
	}

	public function getPlayer()
	{
		return $this->player;
	}


	public function getValue()
	{
		return $this->value;
	}

	public function setValue($value)
	{
		$this->value = $value;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type)
	{
		$this->type = $type;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getItem()
	{
		return $this->item;
	}

	public function setItem($item)
	{
		$this->item = $item;
	}

	public function getRound()
	{
		return $this->round;
	}

	public function setRound($round)
	{
		$this->round = $round;
	}

	public function setPicked()
	{
		$this->picked = true;
	}

	public function setUnPick()
	{
		$this->picked = false;
	}

	public function isPicked()
	{
		return $this->picked;
	}
}
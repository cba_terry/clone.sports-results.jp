<?php
// src/Game.php

namespace Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="association")
 **/
class Association
{
    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    /**
     * @Column(type="string")
     **/
    protected $name;

    /**
     * @Column(type="string")
     **/
    protected $en_name;

    /**
     * @Column(type="string")
     **/
    protected $item;

    /**
     * @Column(type="string")
     **/
    protected $account;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEnName()
    {
        return $this->en_name;
    }

    /**
     * @param mixed $en_name
     */
    public function setEnName($en_name)
    {
        $this->en_name = $en_name;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }
}

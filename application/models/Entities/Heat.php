<?php

namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Collection\RotationCollection;

class Heat
{
	protected $id;
	protected $group;
	protected $players;

	public function __construct($id = null, $group = null)
	{
		$this->id = $id;
		$this->group = $group;

		$this->players = new ArrayCollection();
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}


	/**
	 * add Player
	 * @param \Entities\Player
	 */
	public function addPlayer(\Entities\Player $player)
	{
		$this->players[] = $player;
		
		return $this;
	}


	/**
	 * remove Player
	 * 
	 * @param  \Entities\Player
	 * @return [type]
	 */
	public function removePlayer(\Entities\Player $player)
	{
		$this->heats->removeElement($player);
	}


	/**
	 * get Players
	 * @return [collection]
	 */
	public function getPlayers()
	{
		return $this->players;
	}


	/**
	 * set Group
	 * @param \Entities\Group
	 */
	public function setGroup(\Entities\Group $group)
	{
		$this->group = $group;

		return $this;
	}

	/**
	 * get Group
	 * @return  [\Entities\Group]
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * hasGroupSchoolPlay
	 *
	 * @return  [bool]
	 */
	public function hasGroupSchoolPlay()
	{
		return $this->players->exists(function($key, $player){
			return $player->isPlayAsGroup();
		});
	}

	/**
	 * getListPlayerName
	 *
	 * @return  [\Entities\SchoolGroup]
	 */
	public function findSchoolGroup()
	{
		$player = $this->players->filter(function($player) {
			return $player->isPlayAsGroup();
		})
		->first();

		return ($player) ? $player->getSchool() : false;
	}

	/**
	 * getSchoolScores
	 *
	 * @return  [collection]
	 */
	public function getSchoolScores($singleItemScore = false)
	{
		// loc ra nhung nguoi choi tap the
		$groupPlayers = $this->players->filter(function($player){
				return $player->isPlayAsGroup();
			});

		$iterator = $groupPlayers->getIterator();

		$game = $groupPlayers->first()->getGame();

		$items = $game->getItems();

		$schoolScores = new ArrayCollection();

		foreach ($items as $item) {

			$schoolItemScore = 0;

			if($game->isInputNormal() && $singleItemScore==false)
			{
				// lay diem cua 3 nguoi cao nhat
				$iterator->uasort(function ($a, $b) use ($item) {
				    return ($a->getBestFinalScoreValue($item) > $b->getBestFinalScoreValue($item)) ? -1 : 1;
				});

				$scoreSorted = array_slice(iterator_to_array($iterator), 0, 3);

				foreach ($scoreSorted as $player) {
					$schoolItemScore += $player->getBestFinalScoreValue($item);
				}
			}
			else
			{
				// lay diem cua 3 nguoi cao nhat
				$iterator->uasort(function ($a, $b) use ($item) {
				    return ($a->getItemBestScoreValue($item, 'FinalScore') > $b->getItemBestScoreValue($item, 'FinalScore')) ? -1 : 1;
				});

				$scoreSorted = array_slice(iterator_to_array($iterator), 0, 3);

				foreach ($scoreSorted as $player) {
					$schoolItemScore += $player->getItemBestScoreValue($item, 'FinalScore');
				}
			}

			$schoolScores->add(new \Entities\ItemScore($item->getName(), $schoolItemScore));
		}

		return $schoolScores;
	}

	/**
	 * getSchoolScoreItem
	 *
	 * @return  [int]
	 */
	public function getSchoolScoreItem(\Entities\Item $item, $singleItemScore = false)
	{
		$schoolScores = $this->getSchoolScores($singleItemScore);

		$score = $schoolScores->filter(function($scoreItem) use ($item) {
			return $scoreItem->getName() == $item->getName();
		})->first();

		return ($score) ? $score->getScore() : 0;
	}

	/**
	 * getSchoolTotalItem
	 *
	 * @return  [int]
	 */
	public function getSchoolTotalItem()
	{
		// loc ra nhung nguoi choi tap the
		$groupPlayers = $this->players->filter(function($player){
			return $player->isPlayAsGroup();
		});

		$iterator = $groupPlayers->getIterator();
		
		// lay diem cua 3 nguoi cao nhat
		$iterator->uasort(function ($a, $b) {
		    return ($a->getTotalFinalScore() > $b->getTotalFinalScore()) ? -1 : 1;
		});
		$scoreSorted = array_slice(iterator_to_array($iterator), 0, 3);

		$totalScore = 0;

		if ($scoreSorted) {
			foreach ($scoreSorted as $player) {
				$totalScore += $player->getTotalFinalScore();
			}
		}

		return $totalScore;
	}


	/**
	 * getSchoolScoreTotal
	 *
	 * @return  [int]
	 */
	public function getSchoolScoreTotal($singleItemScore = false)
	{
		$schoolScores = $this->getSchoolScores($singleItemScore);

		$totalScore = 0;

		foreach ($schoolScores as $itemScore) {
			$totalScore += $itemScore->getScore();
		}

		return $totalScore;
	}


	/**
	 * getSchoolGroupScore
	 *
	 * @return  [\Entities\GroupScore]
	 */
	public function getSchoolGroupScore()
	{
		$school = $this->findSchoolGroup();

		return $school->getGroupScore();
	}


	/**
	 * getSchoolItemDemeritScore
	 *
	 * @param   $[item] [the name of item]
	 * @return  [\Entities\GroupScore]
	 */
	public function getSchoolItemDemeritScore(\Entities\Item $item)
	{
		$score = $this->getSchoolGroupScore();

		$fnGetTeamDemeritScore  = 'getItem'.$item->getNo().'DemeritScore';

		$teamDemeritScore = ($score) ? $score->$fnGetTeamDemeritScore() : 0;

		return $teamDemeritScore;
	}


	/**
	 * getPlayersPlayAsGroup
	 *
	 * @return  [collection]
	 */
	public function getPlayersPlayAsGroup($order = false, $item = null)
	{

		$players =  $this->players->filter(function($player){
			return $player->isPlayAsGroup();
		});

		if($order && $item)
		{

			$iterator = $players->getIterator();
			$iterator->uasort(function ($a, $b) use ($item){
			    $aorder = ($a->getOrder($item)) ? $a->getOrder($item)->getOrder() : $a->getPlayerNo();
				$border = ($a->getOrder($item)) ? $b->getOrder($item)->getOrder() : $b->getPlayerNo();

			    return ($aorder < $border) ? -1 : 1;
			});
			
			$players = new ArrayCollection(iterator_to_array($iterator));
		}

		return $players;
	}


	/**
	 * getPlayersPlayAsSingle
	 *
	 * @return  [collection]
	 */
	public function getPlayersPlayAsSingle($order = false, $item = null)
	{
		$players = $this->players->filter(function($player){
			return $player->isPlayAsSingle();
		});

		if($order && $item)
		{
			$iterator = $players->getIterator();
			$iterator->uasort(function ($a, $b) use ($item){
				
				$aorder = ($a->getOrder($item)) ? $a->getOrder($item)->getOrder() : $a->getPlayerNo();
				$border = ($a->getOrder($item)) ? $b->getOrder($item)->getOrder() : $b->getPlayerNo();

			    return ($aorder < $border) ? -1 : 1;
			});
			
			$players = new ArrayCollection(iterator_to_array($iterator));
		}

		return $players;
	}


	/**
	 * [getRotations description]
	 * @return [type] [description]
	 */
	public function getRotations()
	{
		$game = $this->players->first()->getGame();

		$heat = $this;

		// get heat's first item
		$rotationSettings 	= $game->getRotationSettings();
		$groupRotations		= $game->findFirstRotationGroup($this->getGroup());

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);
		
		$firstItem = $rotateFirsts->filterHeat($this->getId())->first()->getFirstItem();
		
		// rotate the rotation when meet first item
		while ($rotateFirsts->rotations->first()->getFirstItem() != $firstItem) {
			$rotateFirsts->doRotate(1);
		}

		return $rotateFirsts;
	}

	/**
	 * [rotateItem description]
	 * @param  array   $items [description]
	 * @param  integer $n     [description]
	 * @return [type]         [description]
	 */
	private function rotateItem($items = [], $n = 0)
	{
		if($n) {
		
			for($i = 0; $i < $n; $i++)
				array_push($items, array_shift($items));
		}

		return array_values($items);
	}

	public function isOneOfThreeBest($player, $item, $singleItemScore = false)
	{
		// loc ra nhung nguoi choi tap the
		$groupPlayers = $this->players->filter(function($p) use ($item, $singleItemScore){

				$game = $p->getGame();

				if($game->isInputNormal() && $singleItemScore == false)
				{
					$playerScore = $p->getBestFinalScoreValue($item);
				}else
				{
					$playerScore = $p->getItemBestScoreValue($item, 'FinalScore');
				}

				return $p->isPlayAsGroup() && $playerScore != null;
			});

		if(!$groupPlayers->count()) return false;

		$iterator = $groupPlayers->getIterator();

		$game = $groupPlayers->first()->getGame();

		if($game->isInputNormal())
		{
			// lay diem cua 3 nguoi cao nhat
			$iterator->uasort(function ($a, $b) use ($item) {
			    return ($a->getBestFinalScoreValue($item) > $b->getBestFinalScoreValue($item)) ? -1 : 1;
			});

			$scoreSorted = array_slice(iterator_to_array($iterator), 0, 3);

			foreach ($scoreSorted as $bestPlayer) {
				if($bestPlayer->getId() == $player->getId()) return true;
			}
		}
		else
		{
			// lay diem cua 3 nguoi cao nhat
			$iterator->uasort(function ($a, $b) use ($item) {
			    return ($a->getItemBestScoreValue($item, 'FinalScore') > $b->getItemBestScoreValue($item, 'FinalScore')) ? -1 : 1;
			});

			$scoreSorted = array_slice(iterator_to_array($iterator), 0, 3);

			foreach ($scoreSorted as $bestPlayer) {
				if($bestPlayer->getId() == $player->getId()) return true;
			}
		}
	}
}
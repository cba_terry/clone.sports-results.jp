<?php
namespace Entities\Collection;

use Doctrine\Common\Collections\ArrayCollection;

class RotationCollection
{
	public $rotations;
	public $rotationSetting;

	public function __construct($rotations, $rotationSetting)
	{
		$this->rotations = clone($rotations);
		$this->rotationSetting = clone($rotationSetting);
	}

	public function filterGroup($group)
	{
		return $this->rotations->filter(function($rotate) use ($group){
			return $rotate->getGroup() == $group;
		});
	}

	public function filterHeat($heat)
	{
		return $this->rotations->filter(function($rotate) use ($heat){
			return $rotate->getHeat() == $heat;
		});
	}

	public function filterItem($item)
	{
		return $this->rotations->filter(function($rotate) use ($item){
			return $rotate->getFirstItem() == $item;
		});
	}

	public function doRotate($n = 1)
	{
		$currentRotations = new ArrayCollection();

		$rotationSetting = $this->rotationSetting;

		if($this->rotations->count() < $this->rotationSetting->count())
		{
			$rotationSetting = $rotationSetting->filter(function($rst){
				return !preg_match('/休み/', $rst->getName());
			});

			$resetIndex = new ArrayCollection();

			foreach ($rotationSetting as $element) {
				$resetIndex->add($element);
			}

			$rotationSetting = $resetIndex;
		}

		$this->rotations->map(function($rotation) use ($rotationSetting, $n){

			if($n) {

				$currentPosition = $rotationSetting->filter(function($rst) use ($rotation) {
	            	return $rst->getName() == $rotation->getFirstItem();
	            })->first();

	            if(!$currentPosition) throw new Exception('RotationSetting was not set for this item');

	            $indexOfNextPosition = $indexOfCurrentPosition = $rotationSetting->indexOf($currentPosition);

	            for($i = 1; $i <= $n; $i++)
	            {
	            	$indexOfNextPosition = $indexOfNextPosition+1;

	            	// meet end of array
	            	if($indexOfNextPosition == $rotationSetting->indexOf($rotationSetting->first()) + $rotationSetting->count())
	            	{
	            		// reset
	            		$nextPosition = $rotationSetting->first();
	            		$indexOfNextPosition = $rotationSetting->indexOf($rotationSetting->first());
	            	}
	            	else
	            	{
	            		$nextPosition = $rotationSetting->get($indexOfNextPosition);
	            	}
	            }

	            $rotation->setFirstItem($nextPosition->getName());
	        }
		});

		return $this;
	}
}
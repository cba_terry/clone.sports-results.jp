<?php
namespace Entities\Collection;

use Doctrine\Common\Collections\ArrayCollection;

class GroupPlayCollection extends ArrayCollection
{
	
	/**
	 * getListPlayerName
	 *
	 * @return  [array]
	 */
	public function getListPlayerName()
	{
		$names = [];

		foreach ($this as $group) {
			foreach ($group->getHeats() as $heat) {
				foreach ($heat->getPlayers() as $player) {
					$names[] = $player->getPlayerName();
				}	
			}	
		}

		return $names;
	}


	/**
	 * getListPlayerName
	 *
	 * @return  [array]
	 */
	public function getListSchoolName()
	{
		$names = [];

		foreach ($this as $group) {
			foreach ($group->getHeats() as $heat) {
				$players = $heat->getPlayers();

				foreach ($players as $player) {
					if(FALSE === array_search($player->getSchool()->getSchoolNameAb(), $names))
						$names[] = $player->getSchool()->getSchoolNameAb();
				}
			}	
		}

		return $names;
	}


	/**
	 * getListPrefectureName
	 *
	 * @return  [array]
	 */
	public function getListPrefectureName()
	{
		$names = [];

		foreach ($this as $group) {
			foreach ($group->getHeats() as $heat) {
				$players = $heat->getPlayers();

				foreach ($players as $player) {
					if(FALSE === array_search($player->getSchool()->getSchoolPrefecture(), $names))
						$names[] = $player->getSchool()->getSchoolPrefecture();
				}
			}	
		}

		return $names;
	}


	/**
	 * [pushIntoGroupHeat description]
	 * @param  array  $players [description]
	 * @return collection    [description]
	 */
	public static function pushIntoGroupHeat($players = [])
	{
		$groups = new GroupPlayCollection();

        foreach ($players as $key => $player) {

            $pGroup = $player->getGroup();
            $pHeat  = $player->getHeat();
            // kiem tra ton tai cua group trong list
            $existsGroup = $groups->filter(function($g) use ($pGroup) {
                return $g->getId() == $pGroup;
            })->first();

            if($existsGroup)
            {
                $heats = $existsGroup->getHeats();
                // kiem tra ton tai cua heat trong list
                $existsHeat = $heats->filter(function($h) use ($pHeat) {
                    return $h->getId() == $pHeat;
                })->first();

                if(!$existsHeat)
                {
                    // neu khong ton tai heat thi khoi tao heat moi
                    $oHeat = new \Entities\Heat($pHeat, $existsGroup->getId());
                    $existsGroup->addHeat($oHeat);
                }

                $oHeat->addPlayer($player);
            }
            else
            {
                // neu khong ton tai group thi khoi tao ca group va heat
                $oGroup = new \Entities\Group($pGroup);
                $oHeat  = new \Entities\Heat($pHeat, $pGroup);

                $oHeat->addPlayer($player);
                $oGroup->addHeat($oHeat);

                $groups[] = $oGroup;
            }
        }

        return $groups;
	}
}
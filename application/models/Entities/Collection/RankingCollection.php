<?php
namespace Entities\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\Rank;

class RankingCollection extends ArrayCollection
{
	/**
	 * [findRank - find rank by given object element]
	 * @param  [object] $element [any kind object want to make ranking]
	 * @return [collection]  [collection rank, each rank object hold rank value and object element]
	 */
	public function findRank($element)
	{
		$hereAmI = $this->filter(function($rank) use ($element) {
			return $rank->getElement() == $element;
		})->first(); 

		return ($hereAmI) ? $hereAmI->getRank() : null;
	}


	/**
	 * [from]
	 * @param  integer $from [description]
	 * @return [RankingCollection] [description]
	 */
	public function from($from = 1)
	{
		return $this->filter(function($rank) use ($from) {
			return $rank->getRank() >= $from;
		});
	}


	/**
	 * [to]
	 * @param  integer $to [description]
	 * @return [RankingCollection] [description]
	 */
	public function to($to = 9999)
	{
		return $this->filter(function($rank) use ($to) {
			return $rank->getRank() <= $to;
		});
	}

	public function containsElement($element)
	{
		$rank = $this->filter(function($rank) use ($element) {
			return $rank->getElement() === $element;
		});

		return ($rank->count()) ? true : false;
	}


	/**
	 * [implementRanking]
	 * @param  array $elements    [list of element in order to make rank]
	 * @param  string $comparation [function name for compare]
	 * @param  array  $params      [params for function compare]
	 * @return [RankingCollection] [description]
	 */
	public static function implementRanking($elements = [], $comparation = '', $params = [])
	{
		$ranking = new RankingCollection;

		// sort desc by $comparation
		usort($elements, function ($a, $b) use ($comparation, $params) {
		    return (call_user_func_array([$a, $comparation], $params) < call_user_func_array([$b, $comparation], $params)) ? 1 : -1;
		});

		foreach ($elements as $key => $currElement) {

			// 1st rank
			if($key === 0){
				$ranking->add(new Rank($currElement, 1));
				continue;
			}

			$prevElement = (isset($elements[$key-1])) ? $elements[$key-1] : $currElement;

			$prevValue = call_user_func_array([$prevElement, $comparation], $params);
			$currValue = call_user_func_array([$currElement, $comparation], $params);

			// if equal, current's element rank set same as previous element's rank
			if($prevValue == $currValue)
				$ranking->add(new Rank($currElement, $ranking->findRank($prevElement)));
			else
				$ranking->add(new Rank($currElement, $key+1));
		}

		return $ranking;
	}
}
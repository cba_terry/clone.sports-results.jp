<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 11/13/2015
 * Time: 7:07 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="single_total_score")
 * @Entity(repositoryClass="Entities\Repository\SingleTotalScoreRepository")
 **/
class SingleTotalScore
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $pass_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_rank;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $group_rank;
    /**
     * @Column(type="integer")
     **/
    protected $status = 0;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $total_score;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item1_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score1_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score1_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item2_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score2_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score2_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item3_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score3_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score3_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item4_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score4_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score4_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item5_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score5_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score5_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected $item6_score;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score6_id1;
    /**
     * @Column(type="integer",nullable=true)
     **/
    protected $single_item_score6_id2;
    /**
     * @Column(type="float",nullable=true)
     **/
    protected  $demerit_score;

    const UNPUBLISH = 0;
    const PUBLISHED = 2;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return SingleTotalScore
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    //////////
    /**
     * Set Single rank
     *
     * @param integer $rank
     * @return SingleTotalScore
     */
    public function setSingleRank($rank)
    {
        $this->single_rank = $rank;

        return $this;
    }

    /**
     * Get common rank
     *
     * @return integer 
     */
    public function getSingleRank()
    {
        return $this->single_rank;
    }

    /**
     * Set Group rank
     *
     * @param integer $rank
     * @return SingleTotalScore
     */
    public function setGroupRank($rank)
    {
        $this->group_rank = $rank;

        return $this;
    }

    /**
     * Get group rank
     *
     * @return integer 
     */
    public function getGroupRank()
    {
        return $this->group_rank;
    }

    ///////

    /**
     * Set pass_rank
     *
     * @param integer $passRank
     * @return SingleTotalScore
     */
    public function setPassRank($passRank)
    {
        $this->pass_rank = $passRank;

        return $this;
    }

    /**
     * Get pass_rank
     *
     * @return integer 
     */
    public function getPassRank()
    {
        return $this->pass_rank;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SingleTotalScore
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set total_score
     *
     * @param float $totalScore
     * @return SingleTotalScore
     */
    public function setTotalScore($totalScore = null)
    {
        if($totalScore) $this->total_score = $totalScore;

        else $this->total_score = $this->item1_score 
                                        + $this->item2_score
                                        + $this->item3_score
                                        + $this->item4_score
                                        + $this->item5_score
                                        + $this->item6_score
                                        ;

        return $this;
    }

    /**
     * Get total_score
     *
     * @return float 
     */
    public function getTotalScore()
    {
        return $this->total_score;
    }

    /**
     * Set item1_score
     *
     * @param float $item1Score
     * @return SingleTotalScore
     */
    public function setItem1Score($item1Score)
    {
        $this->item1_score = $item1Score;

        return $this;
    }

    /**
     * Get item1_score
     *
     * @return float 
     */
    public function getItem1Score()
    {
        return $this->item1_score;
    }

    /**
     * Set single_item_score1_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore1Id1($id)
    {
        $this->single_item_score1_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score1_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore1Id2($id)
    {
        $this->single_item_score1_id2 = $id;

        return $this;
    }

    /**
     * Set item2_score
     *
     * @param float $item2Score
     * @return SingleTotalScore
     */
    public function setItem2Score($item2Score)
    {
        $this->item2_score = $item2Score;

        return $this;
    }

    /**
     * Get item2_score
     *
     * @return float 
     */
    public function getItem2Score()
    {
        return $this->item2_score;
    }

    /**
     * Set single_item_score2_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore2Id1($id)
    {
        $this->single_item_score2_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score2_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore2Id2($id)
    {
        $this->single_item_score2_id2 = $id;

        return $this;
    }

    /**
     * Set item3_score
     *
     * @param float $item3Score
     * @return SingleTotalScore
     */
    public function setItem3Score($item3Score)
    {
        $this->item3_score = $item3Score;

        return $this;
    }

    /**
     * Get item3_score
     *
     * @return float 
     */
    public function getItem3Score()
    {
        return $this->item3_score;
    }

    /**
     * Set single_item_score3_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore3Id1($id)
    {
        $this->single_item_score3_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score3_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore3Id2($id)
    {
        $this->single_item_score3_id2 = $id;

        return $this;
    }

    /**
     * Set item4_score
     *
     * @param float $item4Score
     * @return SingleTotalScore
     */
    public function setItem4Score($item4Score)
    {
        $this->item4_score = $item4Score;

        return $this;
    }

    /**
     * Set single_item_score4_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore4Id1($id)
    {
        $this->single_item_score4_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score4_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore4Id2($id)
    {
        $this->single_item_score4_id2 = $id;

        return $this;
    }

    /**
     * Set item5_score
     *
     * @param float $item5Score
     * @return SingleTotalScore
     */
    public function setItem5Score($item5Score)
    {
        $this->item5_score = $item5Score;

        return $this;
    }

    /**
     * Get item5_score
     *
     * @return float 
     */
    public function getItem5Score()
    {
        return $this->item5_score;
    }

    /**
     * Set single_item_score5_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore5Id1($id)
    {
        $this->single_item_score5_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score5_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore5Id2($id)
    {
        $this->single_item_score5_id2 = $id;

        return $this;
    }

    /**
     * Set item6_score
     *
     * @param float $item6Score
     * @return SingleTotalScore
     */
    public function setItem6Score($item6Score)
    {
        $this->item6_score = $item6Score;

        return $this;
    }

    /**
     * Get item6_score
     *
     * @return float 
     */
    public function getItem6Score()
    {
        return $this->item6_score;
    }

    /**
     * Set single_item_score6_id1
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore6Id1($id)
    {
        $this->single_item_score6_id1 = $id;

        return $this;
    }

    /**
     * Set single_item_score5_id2
     *
     * @param integer $id
     * @return SingleTotalScore
     */
    public function setSingleItemScore6Id2($id)
    {
        $this->single_item_score6_id2 = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDemeritScore()
    {
        return $this->demerit_score;
    }

    /**
     * @param mixed $demerit_score
     */
    public function setDemeritScore($demerit_score)
    {
        $this->demerit_score = $demerit_score;
    }
}

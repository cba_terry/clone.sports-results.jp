<?php 

function admin_pagination_config()
{
	$config['full_tag_open'] = '<ul class="pageLink clearfix">';
	$config['full_tag_close'] = '</ul>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	$config['next_tag_open'] = '<li class="next">';
	$config['next_tag_close'] = '<li>';
	$config['prev_tag_open'] = '<li class="prev">';
	$config['prev_tag_close'] = '<li>';

	$config['first_tag_open'] = '<li class="first">';
	$config['first_tag_close'] = '</li>';
	$config['last_tag_open'] = '<li class="last">';
	$config['last_tag_close'] = '<li>';

	$config['next_link'] = '<img src="'.base_url('/img/admin/icon_arrow02.png').'" alt="">';
	$config['prev_link'] = '<img src="'.base_url('/img/admin/icon_arrow01.png').'" alt="">';

	$config['last_link'] = '最後';
	$config['first_link'] = '先頭';

	$config['page_query_string'] = TRUE;
	$config['per_page'] = 10;

	$config['base_url'] = preg_replace('/(\?|&)per_page=\d+/', '', current_full_url());

	return $config;
}

/**
 * This pagination is currently not in use.
 * I keep this pagination incase client wants the pagination exactly the same as design. If no reqired, anyone can delete this.
 * Pagination method: query.
 * @return pagination like design
 * @author Thom
 **/
function admin_paging_links($total, $cur_page, $per_page) 
{ 
	$last_page = ceil($total/$per_page);
	//Incase current page greater than last page, last page will be active
	if ($cur_page > $last_page){
		$cur_page = $last_page;
	}
	if ($cur_page > 1 && $cur_page < $last_page){
		$page_pre = $cur_page -1;
		$page_next = $cur_page +1;
	}
	if ($cur_page > 1 && $cur_page == $last_page){
		$page_pre = $cur_page -1;
		$page_next = $cur_page;
	}
	if ($cur_page == 1 && $cur_page < $last_page){
		$page_pre = $cur_page;
		$page_next = $cur_page + 1;
	}
	$query_string =  urldecode($_SERVER['QUERY_STRING']);
	if ($query_string && !preg_match('/^p=\d+/', $query_string)){
		$query_string = '?'.preg_replace('/&p=\d+/', '', $query_string).'&';
	}else{
		$query_string = '?';
	}

	$number_links = '';
	if ($last_page > 4){
		if ($last_page - $cur_page >= 4){
			$number_links = '<li class="active"><a href="'.$query_string.'p='.$cur_page.'">'.$cur_page.'</a></li>
							<li><a href="'.$query_string.'p='.($cur_page+1).'">'.($cur_page+1).'</a></li>
							<li><a href="javascript:void(0)">...</a></li>
							<li><a href="'.$query_string.'p='.($last_page-1).'">'.($last_page-1).'</a></li>
							<li><a href="'.$query_string.'p='.$last_page.'">'.$last_page.'</a></li>';
		}elseif ($last_page - $cur_page <=1){
			$number_links = '<li><a href="'.$query_string.'p=1">1</a></li>
							<li><a href="'.$query_string.'p=2">2</a></li>
							<li><a href="javascript:void(0)">...</a></li>
							<li'.($last_page-1 == $cur_page?' class="active"':'').'><a href="'.$query_string.'p='.($last_page-1).'">'.($last_page-1).'</a></li>
							<li'.($last_page == $cur_page?' class="active"':'').'><a href="'.$query_string.'p='.$last_page.'">'.$last_page.'</a></li>';
		}else{
			$number_links = '<li'.($last_page-3 == $cur_page?' class="active"':'').'><a href="'.$query_string.'p='.($last_page-3).'">'.($last_page-3).'</a></li>
							<li'.($last_page-2 == $cur_page?' class="active"':'').'><a href="'.$query_string.'p='.($last_page-2).'">'.($last_page-2).'</a></li>
							<li><a href="'.$query_string.'p='.($last_page-1).'">'.($last_page-1).'</a></li>
							<li><a href="'.$query_string.'p='.$last_page.'">'.$last_page.'</a></li>';
		}
	}else{
		for ($i=1; $i <= $last_page; $i++) { 
			$number_links .= '<li'.($i == $cur_page?' class="active"':'').'><a href="'.$query_string.'p='.$i.'">'.$i.'</a></li>';
		}
	}
	if($last_page >1){
		$page= '<div class="pageLinkWrapper">
					<ul class="pageLink clearfix">
						<li class="first"><a href="'.$query_string.'p=1">先頭</a></li>
						<li class="prev"><a href="'.$query_string.'p='.$page_pre.'"><img src="/img/admin/icon_arrow01.png" alt="" /></a></li>
						'.$number_links.'
						<li class="next"><a href="'.$query_string.'p='.$page_next.'"><img src="/img/admin/icon_arrow02.png" alt="" /></a></li>
						<li class="last"><a href="'.$query_string.'p='.$last_page.'">最後</a></li>
					</ul>
				</div>';
		return $page;
	}
	return '';
}

/**
 * Get gender string in Japanese
 *
 * @return string
 * @author Thom
 **/
function gender($sex)
{
	if ($sex == 1){
		return '男子';
	}elseif ($sex == 0){
		return '女子';
	}
	return '';
}

function formatScore($score = 0, $decimals = 3, $dec_point = '.', $thousands_sep = '') {
	if ($score === null) {
		return null;
	}
	return number_format((float) $score, $decimals, $dec_point, $thousands_sep);
}

function formatScore2($score = 0, $decimals = 3, $dec_point = '.', $thousands_sep = '') {
	if ($score === null) {
		return '-';
	}
	return number_format((float) $score, $decimals, $dec_point, $thousands_sep);
}

function userFormatScore($score, $str_pattern = '', $str_empty = '&nbsp;')
{
	if($score === null) return $str_empty;

	else return $str_pattern . formatScore($score);
}

function current_full_url()
{
	$CI =& get_instance();

	$url = $CI->config->site_url($CI->uri->uri_string());
	return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

function convert_gender ($gender)
{
	if ($gender == '男子'){
		return 1;
	}elseif ($gender == '女子'){
		return 0;
	}

	return $gender;
}

function showButton($status = 0)
{
	if($status == 0) {
		return ' againGray';
	} else {
		return ' btn_deny';
	}
}

function convert_utf8($str)
{
	return mb_convert_encoding($str, "UTF-8", mb_detect_encoding($str,"auto, JIS,SJIS,eucjp-win"));
}

function convert_game_type($type)
{
	if($type)
		return '団体';
	else
		return '個人';
}

function convertGameTypeStringToBool($str)
{
	if($str == '団体')
		return 1;
	else
		return 0;
}

function detectClass($str)
{
	preg_match_all('/(男子|女子)(予選A|予選B|決勝)/', $str, $matches);

	return $matches[2][0];
}

function detectSex($str, $bool = false)
{
	preg_match_all('/(男子|女子)(予選A|予選B|決勝)/', $str, $matches);

	if($bool && $matches[1][0]=='男子') return 1;
	if($bool && $matches[1][0]=='女子') return 0;

	return $matches[1][0];
}

/**
 * [rotateItem description]
 * @param  array   $items [description]
 * @param  integer $n     [description]
 * @return [type]         [description]
 */
function rotateItem($items = [], $n = 0)
{
    if($n) {
    
        for($i = 1; $i < $n; $i++)
            array_push($items, array_shift($items));
    }

    return array_values($items);
}
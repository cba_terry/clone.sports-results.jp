<?php
/**
 * On/Off box on view
 * @param [string] $string
 * @return boolean
 */
if ( ! function_exists('anable_box')) {
	function anable_box($type = null, $compare_l = null, $compare_r = null, $operator = '==') {
		$CI =& get_instance();
		switch ($type) {
			case 'navigation':
				return ($CI->router->fetch_class() == 'login');
				break;
			case 'menu-header':
				return ($CI->router->fetch_class() != 'edit_score');
				break;
			case 'tag-box':
				return ($CI->router->fetch_class() == 'edit_score' && $CI->input->get('tag') == 'on');
				break;
			default:
				return compare_2_value($compare_l, $compare_r, $operator);
				break;
		}
		return false;
	}
}

/**
 * Compare 2 value
 * @param [string|number] $value_1
 * @param [string|number] $value_2
 * @param [string]        $operator
 * @return boolean
 */
if ( ! function_exists('compare_2_value')) {
	function compare_2_value($value_1 = null, $value_2 = null, $operator = '==') {
		switch ($operator) {
			case '==':
				return ($value_1 == $value_2);
				break;
			case '!=':
				return ($value_1 != $value_2);
				break;
			case '===':
				return ($value_1 === $value_2);
				break;
			case '!==':
				return ($value_1 !== $value_2);
				break;
			case '>':
				return ($value_1 > $value_2);
				break;
			case '>=':
				return ($value_1 >= $value_2);
				break;
			case '<':
				return ($value_1 < $value_2);
				break;
			case '<=':
				return ($value_1 <= $value_2);
				break;
			default:
				return ($value_1 === $value_2);
				break;
		}
	}
}

/**
 * Register sourece css style or javascript
 * @param [string] $str_source [name1|name2|...]
 * @param [string] $type       [style or script]
 * @return string|boolean
 */
if ( ! function_exists('register_sources')) {
	function register_sources($str_source = null, $type = 'style', $device = '') {
		if (is_string($str_source) && ! empty($str_source)) {
			$device = base_url($device);
			switch ($type) {
				case 'script':
					$pattern = "<script type=\"text/javascript\" src=\"{$device}js/%s.js\"></script>\n";
					break;
				default:
					$pattern = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$device}css/%s.css\" media=\"all\" />\n";
					break;
			}
			$arr_sources = explode('|', $str_source);
			foreach ($arr_sources as $key => $source) {
				printf($pattern, $source);
			}
			return;
		}
		return false;
	}
}

/**
 * Callback sprintf by parame array
 * @param [string] $format [patern string]
 * @param [array]  $arr    [parame for patern]
 * @return string
 */
if ( ! function_exists('sprintf_arr')) {
	function sprintf_arr($format, $arr) {
		return call_user_func_array('sprintf', array_merge((array)$format, $arr));
	}
}

/**
 * Get value
 * @param [array|string]    $input_arr
 * @param [string|interger] $key
 * @param [string]          $string [partern for string display]
 * @return string
 */
if ( ! function_exists('get_value')) {
	function get_value($input_arr = array(), $string = '%s', $key = null, $empty = false) {
		$value = null;
		if (isset($input_arr[$key]) && is_array($input_arr)) {
			$value = $input_arr[$key];
		} elseif (is_string($input_arr) || is_numeric($input_arr)) {
			$value = $input_arr;
		}

		if ($value != null) {
			if (is_array($value) || is_bool($value) || is_object($value)) {
				return $value;
			} else {
				return sprintf_arr($string, array($value));
			}
		}
		return ($empty === false) ? null : $empty;
	}
}

/**
 * sksort function
 * sort by field in array
 * @see http://php.net/manual/en/function.ksort.php
 * @param $array array
 * @param $subkey string
 * @param $sort_ascending boolean
 * @return array;
 */
if ( !function_exists("sksort")) {
	function sksort(&$array, $subkey = "id", $sort_ascending = false) {
		$temp_array = array();
		if (count($array)) {
			$temp_array[key($array)] = array_shift($array);
		}
		foreach ($array as $key => $val) {
			$offset = 0;
			$found  = false;
			foreach ($temp_array as $tmp_key => $tmp_val) {
				if ( !$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
					$temp_array = array_merge(
						(array)array_slice($temp_array,0,$offset),
						array($key => $val),
						array_slice($temp_array,$offset)
					);
					$found = true;
				}
				$offset++;
			}
			if ( !$found) {
				$temp_array = array_merge(
					$temp_array,
					array($key => $val)
				);
			}
		}

		if ($sort_ascending) {
			$array = array_reverse($temp_array);
		} else {
			$array = $temp_array;
		}
		return $array;
	}
}

function cmp_sortflag($a, $b)
{
	if ($a->getFlag() == $b->getFlag()) {
		return 0;
	}
	return ($a->getFlag() > $b->getFlag()) ? -1 : 1;
}

function sort_result_list($arr_result = array(), $count_flag = 0, $flag = false)
{
	if ( ! empty($arr_result)) {
		$arr_flag   = array();
		$arr_single = array();
		if ($count_flag >= 3) {
			foreach ($arr_result as $key => $result) {
				if ($result['flag'] == 1) {
					$arr_flag[$key] = $result;
				} else {
					$arr_single[$key] = $result;
				}
			}
			if ($flag) {
				sksort($arr_flag, 'total_decision_point');
			}
			if ( ! empty($arr_flag) && ! $flag) {
				sksort($arr_flag, 'order', true);
			}
			if ( ! empty($arr_single)) {
				sksort($arr_single, 'order', true);
			}
			return array_merge($arr_flag, $arr_single);
		} else {
			sksort($arr_result, 'order', true);
			return $arr_result;
		}
	}
	return false;
}

/**
 * convert_date function
 * Convert Japanese date
 * @param $str string
 * @return string
 */
if( !function_exists("convert_date")) {
	function convert_date($str = null, $allow = 9) {
		if( !empty($str)) {
			// format date
			$Y = date("Y", strtotime($str));
			$m = date("m", strtotime($str));
			$d = date("d", strtotime($str));
			$w = date("w", strtotime($str));
			if( !checkdate($m, $d, $Y)) {
				return "";
			}
			$days = array("日","月","火","水","木","金","土");
			switch ($allow) {
				case 1:
					return $Y . "年";
					break;
				case 2:
					return $m . "月";
					break;
				case 3:
					return $d . "日";
					break;
				case 4:
					return $d . "日(" . $days[$w] . ")";
					break;
				case 5:
					return $m . "月" . $d . "日(" . $days[$w] . ")";
					break;
				case 6:
					return $Y . "年" . $m . "月" . $d . "日(" . $days[$w] . ")";
					break;
				case 7:
					return $Y;
					break;
				case 8:
					return $m . "月" . $d . "日";
					break;
				default:
					return $Y . "年" . $m . "月" . $d . "日";
					break;
			}
		}
		return $str;
	}
}

/**
 * convert_gtjdate function
 * @param $year string
 * @param $allow boolean
 * @return string
 */
if( !function_exists("convert_gtjdate")) {
	function convert_gtjdate($year, $allow = true) {
		if ($year < 1912 || strlen($year) !== 4) {
			return false;
		}
		$gengo  = "";
		$wayear = 0;
		if ($year <= 1912) {
			$gengo = "明治";
			$wayear = ($year - 1868)+1;
		} elseif ($year > 1912 && $year <= 1926) {
			$gengo = "大正";
			$wayear = ($year - 1912)+1;
		} elseif ($year > 1926 && $year <= 1989) {
			$gengo = "昭和";
			$wayear = ($year - 1926)+1;
		} else {
			$gengo  = "平成";
			$wayear = ($year-1989) + 1;
		}
		if ($allow) {
			$year = "年度";
		} else {
			$year = "年";
		}
		return $gengo . sprintf("%02d", $wayear) . $year;
	}
}

/**
 * compare datetime function
 * @param $time1
 * @param $time2
 * @return boolean
 */
if( !function_exists("compare_datetime")) {
	function compare_datetime($time1, $time2) {
		if (strtotime($time1) <= strtotime($time2)){
			return true;
		}else{
			return false;
		}
	}
}

/**
 * Compare datetime to get STT day
 */
if ( ! function_exists('get_stt_day')) {
	function get_stt_day($datetime = null) {
		$datetime = ( ! empty($datetime)) ? $datetime : new DateTime('now');
		$to_day   = new DateTime(TO_DAY);
		$obj_day  = $datetime->diff($to_day);
		// Get STT day is playing;
		return $obj_day->d + 1;
	}
}

/**
 * Convert time to second or millisecond
 * 00:05 => 5 second
 * 00:05 => 5000 millisecond
 * Exponential expression
 */
if ( ! function_exists('convert_2_second')) {
	function convert_2_second($time = null, $millisecond = true) {
		if ( ! empty($time)) {
			$arr_time = array_reverse(explode(':', $time));
			$second  = 0;
			foreach ($arr_time as $key => $value) {
				// Only get second, minutes
				if ($key > 2)
					break;
				$second += pow(60, $key) * $value;
			}
			return ($millisecond) ? $second * 1000 : $second;
		}
		return 0;
	}
}
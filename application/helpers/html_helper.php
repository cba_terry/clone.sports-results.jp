<?php

function setSelectFinalSetting($check, $value)
{
	if($check == $value)
		return 'selected="selected"';

	return '';
}

function reInput($scoreId = null, $scoreStatus = null, $scoreType = null, $referee = null)
{
	if($referee && $referee->getRefereeType() == 2)
		return;
	
	if(!$scoreId)
		return '<a class="againButton againGray" data-id="">再</a>';
	else if(!$scoreStatus || $scoreStatus == 2)
		return '<a class="againButton againGray" hover="" data-id="'.$scoreId.'" data-type="'.$scoreType.'">再</a>';
	else
		return '<a class="againButton" hover="" data-id="'.$scoreId.'" data-type="'.$scoreType.'">再</a>';

	return false;
}

function detectScoreType ($scoreType)
{
	if(in_array($scoreType, array(3,4,5,6,7)))
	{
		return 'Dスコア';
	}
	elseif (in_array($scoreType, array(8,9,10,11,12))) {
		return 'Eスコア';
	}
	elseif (in_array($scoreType, array(13,14)))
	{
		return 'タイム';
	}
	elseif (in_array($scoreType, array(15,16)))
	{
		return 'ライン';
	}
}

function detectScoreTypeFormatNumber ($scoreType)
{
	if(in_array($scoreType, array(3,4,5,6,7)))
	{
		return '00.00';
	}
	elseif (in_array($scoreType, array(8,9,10,11,12))) {
		return '00.00';
	}
	elseif (in_array($scoreType, array(13,14)))
	{
		return '00:00';
	}
	elseif (in_array($scoreType, array(15,16)))
	{
		return '-0.00';
	}
}

function detectScoreTypeDevideChar ($scoreType)
{
	if (in_array($scoreType, array(13,14)))
	{
		return ':';
	}else {
		return '.';
	}
}

function referee_edit_score_button($scoreStatus, $params = array(), $player_cancle = false)
{
	$uri = http_build_query($params);

	if($player_cancle)

		echo '<p class="button textButton"><a>棄権</a></p>';

	else if(!isset($scoreStatus))

		echo '<p class="button yetButton"><a href="/gymnastics/admin/referee/score?'.$uri.'">未</a></p>';

	else if($scoreStatus == 0)

		echo '<p class="button"><a href="/gymnastics/admin/referee/score?'.$uri.'">入力可</a></p>';

	else if($scoreStatus == 1 || $scoreStatus == 11)

		echo '<p class="button correctButton"><a>修正</a></p>';

	else
		echo '<p class="button yetButton"><a>済</a></p>';
}

function heat_select($name = '', $options = array(), $checked = null)
{
	if($checked == null) $checked = 1;
	
	foreach ($options as $key => $value) {
		
		$selected = (isset($checked) && $checked == $key) ? 'checked="checked"' : '';

		echo '<li class="radio heat"><input id="heat'.$key.'" type="radio" name="'.$name.'" '.$selected.' value="'.$key.'">
			<label for="heat'.$key.'">'.$key.'組</label></li>';
	}
}

function group_select($name = '', $options = array(), $select = null)
{
	$html = '<li class="select size01"><select name="'.$name.'" id="group_search">';

	foreach ($options as $key => $value) {
		
		$selected = (isset($select) && $select == $key) ? 'selected="selected"' : '';

		$html .= '<option value="'.$key.'" '.$selected.'>'.$key.'班</option>';
	}

	$html .= '</select></li>';

	$html .= '
					<script>
						var groups = ' . json_encode($options) . ';
						$(\'#group_search\').change(function(){
							var group = $(this).val();
							var heats = groups[group];

							$(this).parents(\'.groupList\').find(\'.radio\').remove();

							var html = \' \';
							var checked = \'\';
							var i = 0;

							$.each(heats, function(k,v){
								checked = (i==0) ? \'checked="checked"\' : \'\';
							    html += \'<li class="radio"><input id="heat\'+k+\'" type="radio" name="heat" \'+checked+\' value="\'+k+\'"><label for="heat\'+k+\'">\'+k+\'組</label></li>\';
								i++;
							});

							$(this).parent().after(html);
						});
					</script>
		';

	return $html;
}
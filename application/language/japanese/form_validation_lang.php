<?php

$lang['required'] 			= "%sは必須です。";
$lang['isset']				= "%s欄は空欄にできません。";
$lang['valid_email']		= "正しい%sを入力してください。";//"%s欄には正しいEmailアドレスを入力する必要があります。";
$lang['valid_url'] 			= "正しいURLを入力してください。";//"%s欄には正しいURLを入力する必要があります。";
$lang['valid_ip'] 			= "%s欄には正しいIPアドレスを入力する必要があります。";
$lang['min_length']			= "%sは%s文字以上で入力してください。";//"%s欄は最低 %s 文字以上でなければなりません。";
$lang['max_length']			= "%sは%s文字以内で入力してください。";//"%s欄は %s 文字を超えてはいけません。";
$lang['exact_length']		= "%s欄は %s 文字でなければなりません。";
$lang['alpha']				= "%s欄には、半角アルファベット以外は入力できません。";
$lang['alpha_numeric']		= "%sは半角英数字で入力してください。";//"%s欄には、半角英数字以外は入力できません。";
$lang['alpha_dash']			= "%s欄には、半角英数字、アンダースコア(_)、ハイフン(-)以外は入力できません。";
$lang['numeric']			= "%s欄には、数字以外は入力できません。";
$lang['is_numeric']			= "%s欄には、数値以外は入力できません。";
$lang['integer']			= "%s欄には、整数以外は入力できません。";
$lang['regex_match']		= "%s欄は、正しい形式ではありません。";
$lang['matches']			= "%sと%sが一致しません。";//"%sが%sと一致しません。";//"%s欄が %s 欄と一致しません。";
$lang['is_unique'] 			= "この%sは既に使用されています。";//"%sの入力内容は既に使用されています";
$lang['is_natural']			= "%s欄には、正の整数以外は入力できません。";
$lang['is_natural_no_zero']	= "%sは半角数字（整数）で入力して下さい。";
$lang['decimal']			= "%s欄は10進数しか入力できません。";
$lang['less_than']			= "%s欄は %s より小さい数しか入力できません。";
$lang['greater_than']		= "%s欄は %s より大きな数しか入力できません。";
$lang['valid_phone'] 		= "正しい形式で%sを入力してください。";//"%sは無効です。";
$lang['valid_zip'] 			= "正しい形式で%sを入力してください。";//"%sは無効です。";
$lang['_check_email_exist'] = "%s他の会社が既に使用されています。";// is used already by other company。";
$lang['valid_password'] 	= "%sは半角英数字で入力してください。";// "%sパスワード欄には、半角英数字以外は入力できません。";//"%s only alpha-numeric and special characters  !\"#$%&'()*+,\-./:;<=>?@[]^_`{|}。";
$lang['valid_loginId'] 	= "%sは無効です。";//"%s only alpha-numeric and special characters _-@。";
$lang['valid_htmlspecialchars'] 	= "%sの内容ではなく、特殊キャラクター<>/。";//"%s not content special characters <>/";
$lang['valid_date']			= "％sは間違った構造の日付YYYY-MM-DD。";//"%s wrong structute date yyyy-mm-dd";
$lang['valid_areacode']		= "%sは無効です。";
$lang['valid_kana']		    = "%sは全角カタカナで入力してください。";//"%s is invalid。";
$lang['valid_hafsize']		= "%sは無効です。";//"%s is invalid。";
$lang['valid_name']		    = "%sは無効です。";//"%s is invalid。";
$lang['not_space_full'] 	= "%sは無効です。";
/* End of file form_validation_lang.php */
/* Location: ./system/language/japanese/form_validation_lang.php */
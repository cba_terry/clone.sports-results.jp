<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<title><?php echo get_value($title); ?></title>
<link rel="stylesheet" href="<?=base_url('/css/common.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/css/style.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/css/monitor.css')?>" media="all">
<?php register_sources($css); ?>
<link rel="index contents" href="/" title="ホーム">
<script src="<?=base_url('/js/jquery-1.8.3.min.js')?>"></script>
<script src="<?=base_url('/js/script.js')?>"></script>
<?php register_sources($js, 'script'); ?>
<!--[if lt IE 9]>
<script src="<?=base_url('/js/html5shiv-printshiv.js')?>"></script>
<script src="<?=base_url('/js/css3-mediaqueries.js')?>"></script>
<![endif]-->
</head>
<body<?php echo get_value($pageId, ' id="%s"'); ?>>
	<div id="wrapper">
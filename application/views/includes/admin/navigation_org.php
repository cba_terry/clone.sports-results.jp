<div id="header" class="clearfix">
	<div class="headerInner clearfix">
		<h1 class="textHeader"><?=$this->session->userdata('association_name')?></h1><span class="tag">管理者画面</span>
		<div class="hBlock">
			<?php if (anable_box('menu-header')) : ?>
			<ul class="hList">
				<li><a href="/gymnastics/admin/organization/tournament">大会一覧</a></li>
			</ul>
			<?php endif; ?>
			<p class="rightBox">
				<span>アカウント名</span>
				<a class="hover" href="/gymnastics/admin/organization/logout"><img src="<?=base_url('/img/admin/icon_logout.gif')?>" alt="" /></a>
			</p>
		</div>
	</div>
</div>
<!-- /#header -->
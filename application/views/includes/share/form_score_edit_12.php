		<form action="" class="userForm" method="post">
			<input	type="hidden" name="tab" id="tab" value="1" >
			<div class="tabArea">
				<ul class="tab clearfix">
					<li class="active"><a href="#tab01">1回目</a></li>
					<li><a href="#tab02">2回目</a></li>
				</ul>
				<!-- /.tabList -->
				<div class="tabContents">
					<?php for ($j=0; $j <= 1 ; $j++) { //start for tab?>
					<div id="tab0<?=$j+1?>" class="tabBox">
						<div class="tableStyle tableStyle01">
							<table>
								<tr>
									<th><span>D</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeD() ; $i++) {?>
											<li>
												<label for="d<?=$i?>_score<?=$j?>">D<?=$i?></label>
												<span class="size07">
												<input type="text" name="d<?=$i?>_score[]" class="size07 d_score_<?=$j?>" id="d<?=$i?>_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'D'.$i, $j+1))?>" />
												</span>
											</li>
										<?php } ?>
										</ul>
									</td>
								</tr>
								<tr>
									<th><span>E</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeE() ; $i++) {?>
											<li>
												<label for="e<?=$i?>_score<?=$j?>">E<?=$i?></label>
												<span class="size07">
												<input type="text" name="e<?=$i?>_score[]" class="size07 e_score_<?=$j?>" id="e<?=$i?>_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'E'.$i, $j+1)) ?>" />
												</span>
											</li>
										<?php } ?>

											<li>
												<label for="e_demerit_score<?=$j?>">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="e_demerit_score[]" class="size07" id="e_demerit_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'EDemerit', $j+1)) ?>" />
												</span></li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>時間</th>
									<td><ul class="scoreList scoreList01">
												<?php $time1 = ($player->getScoreValue($item, 'Time1', $j+1))?explode(':', $player->getScoreValue($item, 'Time1', $j+1)):[0,0];
													  $time2 = ($player->getScoreValue($item, 'Time2', $j+1))?explode(':', $player->getScoreValue($item, 'Time2', $j+1)):[0,0]; ?>
											<li>
												<label for="time0101">タイム1</label>
												<span class="size08">
												<input type="text" name="time1_score1[]" class="size08" id="time1_score1<?=$j?>" value="<?php echo $time1[0]; ?>" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time1_score2[]" class="size08" id="time1_score2<?=$j?>" value="<?php echo $time1[1]; ?>" />
												</span></li>
											<li>
												<label for="time0201">タイム2</label>
												<span class="size08">
												<input type="text" name="time2_score1[]" class="size08" id="time2_score1<?=$j?>" value="<?php echo $time2[0]; ?>" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time2_score2[]" class="size08" id="time2_score2<?=$j?>" value="<?php echo $time2[1]; ?>" />
												</span>
											</li>
											<li>
												<label for="time_demerit_score<?=$j?>">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="time_demerit_score[]" class="size07" id="time_demerit_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'TimeDemerit', $j+1))?>" />
												</span>
											</li>

										</ul></td>
								</tr>
								<tr>
									<th>ライン</th>
									<td><ul class="scoreList">
											<li>
												<label for="line0101">ライン1</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line1_score[]" class="size07" id="line1_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'Line1', $j+1))?>" />
												</span></li>
											<li>
												<label for="line0201">ライン2</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line2_score[]" class="size07" id="line2_score<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'Line2', $j+1))?>" />
												</span>
											</li>
											<li>
												<label for="line_demerit_score<?=$j?>">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line_demerit_score[]" class="size07" id="line_demerit_score<?=$j?>" value="<?php if ($player->getScoreValue($item, 'LineDemerit', $j+1)) {echo $player->getScoreValue($item, 'LineDemerit', $j+1);} else if (!$player->getScoreValue($item, 'LineDemerit', $j+1)){echo $player->getScoreValue($item, 'Line1', $j+1)+$player->getScoreValue($item, 'Line2', $j+1);} ?>" />
												</span>
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>その他減点</th>
									<td><em class="other">-</em><span class="size07">
										<input type="text" name="other_demerit[]" class="size07" id="other_demerit<?=$j?>" value="<?=formatScore($player->getScoreValue($item, 'OtherDemerit', $j+1))?>" />
										</span></td>
								</tr>
							</table>
						</div>
						<div class="tableInfo">
							<table class="tableGeneral">
								<tr>
									<th>D合計</th>
									<th>E合計</th>
									<th>減点</th>
									<th>合計</th>
								</tr>
								<tr>
									<td id="d_score<?=$j?>">00.00</td>
									<td id="e_score<?=$j?>">00.00</td>
									<td id="demerit_score<?=$j?>">-00.0</td>
									<td id="final_score<?=$j?>">000.00</td>
								</tr>
							</table>
						</div>
					</div>
					<?php } // end for tab ?> 
				</div>
			</div>
			<ul class="buttonList clearfix">
				<?php if($this->input->get('ref') == 'all'){ ?>
				<li><a href="/gymnastics/admin/<?=(isset($referee))?'referee':'organization'?>/game/all?gid=<?=$game->getId()?>" class="buttonStyle hover">戻る</a></li>
				<?php }else { ?>
				<li><a href="/gymnastics/admin/<?=(isset($referee))?'referee':'organization'?>/game/item?gid=<?=$game->getId()?>&item=<?=$item?>" class="buttonStyle hover">戻る</a></li>
				<?php } ?>
				<li class="submitButton">
					<input type="hidden" value="1" name="tab" id="hiddentab"></input>
					<input type="submit" value="更新する" class="buttonGeneral hover" name="submit[confirm]" id="changeBtn" />
				</li>
			</ul>
		</form>
		<script type="text/javascript">
			$('.tab a').click(function(e){
				var tabId = $(this).text().substring(0,1);
				$('#hiddentab').val(tabId);
			})
		</script>
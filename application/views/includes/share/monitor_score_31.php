				<tr><?php 
				$higher = ($player->getItemFinalScore($citem->getName()) > $player->getItemFinalScore($citem->getName(),2))  ? 1:2;
				$lower = ($player->getItemFinalScore($citem->getName()) <= $player->getItemFinalScore($citem->getName(),2))  ? 1:2;
				?>
		              <td class="center first"><?=$player->getOrder()?></td>
		              <td class="center"><?=$player->getPlayerNo()?></td>
		              <td class="left"><?=$player->getPlayerName()?></td>
		              <td class="left"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
		              <td><span class="number"><?=$player->getItemDScore($citem->getName(),$lower)?></span><?=$player->getItemDScore($citem->getName(), $higher)?></td>
		              <?php for($i=1; $i<=$citem->getNumberRefereeE(); $i++) { $function = 'getItemE'.$i.'Score';?>
					  <td><span class="number"><?=$player->$function($citem->getName(),$lower)?></span><?=$player->$function($citem->getName(), $higher)?></td>
					  <?php } ?>
		              <td><span class="number"><?=formatScore($player->getItemEScore($citem->getName(), $lower)+$player->getItemEDemeritScore($citem->getName(), $lower))?></span><?=formatScore($player->getItemEScore($citem->getName(),$higher)+$player->getItemEDemeritScore($citem->getName(),$higher))?></td>
		              <td><span class="number">-<?=$player->getItemEDemeritScore($citem->getName(),$lower)?></span>-<?=$player->getItemEDemeritScore($citem->getName(),$higher)?></td>
		              <td><span class="number"><?=$player->getItemEScore($citem->getName(),$lower)?></span><?=$player->getItemEScore($citem->getName(),$higher)?></td>
		              <td><span class="number"><?=formatScore($player->getItemEScore($citem->getName(),$lower)+$player->getItemDScore($citem->getName(),$lower))?></span><?=formatScore($player->getItemEScore($citem->getName(),$higher)+$player->getItemDScore($citem->getName(),$higher))?></td>
		              <td><span class="number">-<?=formatScore($player->getItemTimeDemeritScore($citem->getName(),$lower)+$player->getItemLineDemeritScore($citem->getName(),$lower))?></span>-<?=formatScore($player->getItemTimeDemeritScore($citem->getName(),$higher)+$player->getItemLineDemeritScore($citem->getName(),$higher))?></td>
		              <td><span class="number"><?=$player->getItemFinalScore($citem->getName(),$lower)?></span><?=$player->getItemFinalScore($citem->getName(),$higher)?></td>
		              <td class="center"><?php if($pHeat->isOneOfThreeBest($player, $citem)) { ?><img alt="" src="<?=base_url('/img/monitor/icon_dot_white.gif')?>"><?php } ?></td>
		            </tr>
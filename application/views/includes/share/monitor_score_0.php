				<tr>
		              <td class="center first"><?=$player->getOrder($citem)?></td>
		              <td class="center"><?=$player->getPlayerNo()?></td>
		              <td class="left"><?=$player->getPlayerName()?></td>
		              <td class="left"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
		              <td><?=formatScore2($player->getItemScoreValue($citem, 'DScore'))?></td>
		              <?php for($i=1; $i<=$citem->getNumberRefereeE(); $i++) {?>
					  <td><?=formatScore2($player->getItemScoreValue($citem, 'E'.$i.'Score'))?></td>
					  <?php } ?>
		              <td><?=formatScore2($player->getItemScoreValue($citem, 'EScore')+$player->getItemScoreValue($citem, 'EDemeritScore'))?></td>
		              <td>-<?=formatScore($player->getItemScoreValue($citem, 'EDemeritScore'))?></td>
		              <td><?=formatScore2($player->getItemScoreValue($citem, 'EScore'))?></td>
		              <td><?=formatScore2($player->getItemScoreValue($citem, 'EScore')+$player->getItemScoreValue($citem, 'DScore'))?></td>
		              <td>-<?=formatScore($player->getItemScoreValue($citem, 'TimeDemeritScore')+$player->getItemScoreValue($citem, 'LineDemeritScore'))?></td>
		              <td><?=formatScore2($player->getItemScoreValue($citem, 'FinalScore'))?></td>
		              <td class="center"><?php if($pHeat->isOneOfThreeBest($player, $citem)) { ?><img alt="" src="<?=base_url('/img/monitor/icon_dot_white.gif')?>"><?php } ?></td>
		            </tr>
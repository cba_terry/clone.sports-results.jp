			<form action="" class="userForm" method="post">
				<div class="tabArea">
					<div class="tabContents">
						<div id="tab01" class="tabBox">
							<div class="tableStyle tableStyle01">
								<table>
									<tr>
										<th><span>D</span></th>
										<td><ul class="scoreList">
											<?php for ($i=1; $i <= $item->getNumberRefereeD() ; $i++) {
												$getDScore = 'getD'.$i.'Score';
											?>
												<li>
													<label>D1</label>
													<span class="size07"><?php echo $post['d'.$i.'_score']; ?></span>
													<input type="hidden" name="d<?=$i?>_score" id ="d<?=$i?>_score" class="d_score" value="<?php echo $post['d'.$i.'_score']; ?>">
												</li>
											<?php } ?>
											</ul></td>
									</tr>
									<tr>
										<th><span>E</span></th>
										<td><ul class="scoreList">
											<?php for ($i=1; $i <= $item->getNumberRefereeE() ; $i++) {
												$getEScore = 'getE'.$i.'Score';
											?>
												<li>
													<label>E<?=$i?></label>
													<span class="size07"><?php echo $post['e'.$i.'_score']; ?></span>
													<input type="hidden" name="e<?=$i?>_score" id ="e<?=$i?>_score" class="e_score" value="<?php echo $post['e'.$i.'_score']; ?>">
												</li>
											<?php } ?>
											<?php if(isset($post['e_demerit_score'])) {?>
												<li>
													<label>減点</label>
													<em class="line">-</em> <span class="size07"><?php echo $post['e_demerit_score']; ?></span>
													<input type="hidden" name="e_demerit_score" id ="e_demerit_score" value="<?php echo $post['e_demerit_score']; ?>"> </li>
											<?php } ?>
											</ul></td>
									</tr>
									<tr>
										<th>時間</th>
										<td><ul class="scoreList scoreList01">
												<li>
													<label>タイム1</label>
													<span class="size08"><?php echo $post['time1_score1']; ?></span> <em class="dot">:</em> <span class="size08"><?php echo $post['time1_score2']; ?></span>
													<input type="hidden" name="time1_score1" id ="time1_score1" value="<?php echo $post['time1_score1']; ?>">
													<input type="hidden" name="time1_score2" id ="time1_score2" value="<?php echo $post['time1_score2']; ?>">
												</li>
												<li>
													<label>タイム2</label>
													<span class="size08"><?php echo $post['time2_score1']; ?></span> <em class="dot">:</em> <span class="size08"><?php echo $post['time2_score2']; ?></span>
													<input type="hidden" name="time2_score1" id ="time2_score1" value="<?php echo $post['time2_score1']; ?>">
													<input type="hidden" name="time2_score2" id ="time2_score2" value="<?php echo $post['time2_score2']; ?>">
												</li>
												<li>
													<label>減点</label>
													<em class="line">-</em> <span class="size07"><?php echo $post['time_demerit_score']; ?></span>
													<input type="hidden" name="time_demerit_score" id ="time_demerit_score" value="<?php echo $post['time_demerit_score']; ?>">
												</li>
											</ul></td>
									</tr>
									<tr>
										<th>ライン</th>
										<td><ul class="scoreList scoreList01">
												<li>
													<label>ライン1</label>
													<em class="line">-</em> <span class="size07"><?php echo $post['line1_score']; ?></span>
													<input type="hidden" name="line1_score" id ="line1_score" value="<?php echo $post['line1_score']; ?>">
												</li>
												<li>
													<label>ライン2</label>
													<em class="line">-</em> <span class="size07"><?php echo $post['line2_score']; ?></span>
													<input type="hidden" name="line2_score" id ="line2_score" value="<?php echo $post['line2_score']; ?>">
												</li>
												<li>
													<label>減点</label>
													<em class="line">-</em> <span class="size07"><?php echo $post['line_demerit_score']; ?></span>
													<input type="hidden" name="line_demerit_score" id ="line_demerit_score" value="<?php echo $post['line_demerit_score']; ?>">
												</li>
											</ul></td>
									</tr>
									<tr>
										<th>その他減点</th>
										<td>
											<em class="other">-</em><span class="size07"><?php echo $post['other_demerit']; ?></span>
											<input type="hidden" name="other_demerit" id ="other_demerit" value="<?php echo $post['other_demerit']; ?>">
										</td>
									</tr>
								</table>
							</div>
							<div class="tableInfo">
								<table class="tableGeneral">
									<tr>
										<th>D合計</th>
										<th>E合計</th>
										<th>減点</th>
										<th>合計</th>
									</tr>
									<tr>
										<td id="d_score"></td>
										<td id="e_score"></td>
										<td id="demerit_score"></td>
										<td id="final_score"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<ul class="buttonList clearfix">
					<li><a href="javascript:void(0)"  onclick="window.history.back()" class="buttonStyle hover">戻る</a></li>
					<li class="editBtn">
						<input type="submit" value="登録する" class="buttonGeneral hover" id="EditBtn" name="submit[complete]" />
					</li>
					<?php if($referee->getRefereeType()) {?>
					<li class="submitButton">
						<input type="submit" value="公開する" class="buttonGeneral hover" id="publishBtn" name="submit[complete]" />
					</li>
					<?php }?>
				</ul>
			</form>
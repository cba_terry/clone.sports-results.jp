				<tr><?php 
				$higher = ($player->getItemScoreValue($citem, 'FinalScore') > $player->getItemScoreValue($citem, 'FinalScore', 2))  ? 1:2;
				$lower = ($player->getItemScoreValue($citem, 'FinalScore') <= $player->getItemScoreValue($citem, 'FinalScore', 2))  ? 1:2;
				?>
		              <td class="center first"><?=$player->getOrder($citem)?></td>
		              <td class="center"><?=$player->getPlayerNo()?></td>
		              <td class="left"><?=$player->getPlayerName()?></td>
		              <td class="left"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
		              <td><span class="number"><?=formatScore2($player->getItemScoreValue($citem, 'DScore', $lower))?></span><?=formatScore2($player->getItemScoreValue($citem, 'DScore', $higher))?></td>
		              <?php for($i=1; $i<=$citem->getNumberRefereeE(); $i++) { ?>
					  <td><span class="number"><?=formatScore2($player->getItemScoreValue($citem, 'E'.$i.'Score', $lower))?></span><?=formatScore2($player->getItemScoreValue($citem, 'E'.$i.'Score', $higher))?></td>
					  <?php } ?>
		              <td><span class="number"><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $lower)+$player->getItemScoreValue($citem, 'EDemeritScore', $lower))?></span><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $higher)+$player->getItemScoreValue($citem, 'EDemeritScore', $higher))?></td>
		              <td><span class="number">-<?=formatScore($player->getItemScoreValue($citem, 'EDemeritScore',$lower))?></span>-<?=formatScore($player->getItemScoreValue($citem, 'EDemeritScore', $higher))?></td>
		              <td><span class="number"><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $lower))?></span><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $higher))?></td>
		              <td><span class="number"><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $lower)+$player->getItemScoreValue($citem, 'DScore', $lower))?></span><?=formatScore2($player->getItemScoreValue($citem, 'EScore', $higher)+$player->getItemScoreValue($citem, 'DScore', $higher))?></td>
		              <td><span class="number">-<?=formatScore2($player->getItemScoreValue($citem, 'TimeDemeritScore', $lower)+$player->getItemScoreValue($citem, 'LineDemeritScore', $lower))?></span>-<?=formatScore2($player->getItemScoreValue($citem, 'TimeDemeritScore', $higher)+$player->getItemScoreValue($citem, 'LineDemeritScore', $higher))?></td>
		              <td><span class="number"><?=$player->getItemScoreValue($citem, 'FinalScore', $lower)?></span><?=$player->getItemScoreValue($citem, 'FinalScore', $higher)?></td>
		              <td class="center"><?php if($pHeat->isOneOfThreeBest($player, $citem)) { ?><img alt="" src="<?=base_url('/img/monitor/icon_dot_white.gif')?>"><?php } ?></td>
		            </tr>
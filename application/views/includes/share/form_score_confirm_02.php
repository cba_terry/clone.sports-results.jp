		<form action="#" class="userForm" method="post">
			<div class="tabArea">
				<ul class="tab clearfix">
					<li <?php if($this->input->post('tab') == 1) echo 'class="active"';?>><a href="#tab01">1回目</a></li>
					<li <?php if($this->input->post('tab') == 2) echo 'class="active"';?>><a href="#tab02">2回目</a></li>
				</ul>
				<!-- /.tabList -->
				<div class="tabContents">
				<?php for ($j=0; $j <= 1 ; $j++) { //start for tab?>
					<div id="tab0<?=$j+1?>" class="tabBox" <?php if($this->input->post('tab') == ($j+1)) echo 'style="display:block"';else echo 'style="display:none"'?>>
						<div class="tableStyle tableStyle01">
							<table>
								<tr>
									<th><span>D</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeD() ; $i++) {
											$dScore = 'd'.$i.'_score';
										?>
											<li>
												<label>D<?=$i?></label>
												<span class="size07"><?php echo $post[$dScore][$j]; ?></span>
												<input type="hidden" name="d<?=$i?>_score[]" class="d_score_<?=$j?>" id="d<?=$i?>_score<?=$j?>" value="<?php echo $post[$dScore][$j]; ?>">
											</li>
										<?php } ?>
										</ul></td>
								</tr>
								<tr>
									<th><span>E</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeE() ; $i++) {
											$eScore = 'e'.$i.'_score';
										?>
											<li>
												<label>E<?=$i?></label>
												<span class="size07"><?php echo $post[$eScore][$j]; ?></span>
												<input type="hidden" name="e<?=$i?>_score[]" class="e_score_<?=$j?>" id="e<?=$i?>_score<?=$j?>" value="<?php echo $post[$eScore][$j]; ?>">
											</li>
										<?php } ?>
											<li>
												<label>減点</label>
												<em class="line">-</em> <span class="size07"><?php echo $post['e_demerit_score'][$j]; ?></span>
												<input type="hidden" name="e_demerit_score[]" id="e_demerit_score<?=$j?>" value="<?php echo $post['e_demerit_score'][$j]; ?>">
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>時間</th>
									<td><ul class="scoreList scoreList01">
											<li>
												<label>タイム1</label>
												<span class="size08"><?php echo $post['time1_score1'][$j]; ?><input type="hidden" name="time1_score1[]" id="time1_score1<?=$j?>" value="<?php echo $post['time1_score1'][$j]; ?>"></span>
												<em class="dot">:</em>
												<span class="size08"><?php echo $post['time1_score2'][$j]; ?><input type="hidden" name="time1_score2[]" id="time1_score2<?=$j?>" value="<?php echo $post['time1_score2'][$j]; ?>"></span>
											</li>
											<li>
												<label>タイム2</label>
												<span class="size08"><?php echo $post['time2_score1'][$j]; ?><input type="hidden" name="time2_score1[]" id="time2_score1<?=$j?>" value="<?php echo $post['time2_score1'][$j]; ?>"></span>
												<em class="dot">:</em>
												<span class="size08"><?php echo $post['time2_score2'][$j]; ?><input type="hidden" name="time2_score2[]" id="time2_score2<?=$j?>" value="<?php echo $post['time2_score2'][$j]; ?>"></span>
											</li>
											<li>
												<label>減点</label>
												<em class="line">-</em> <span class="size07"><?php echo $post['time_demerit_score'][$j]; ?></span>
												<input type="hidden" name="time_demerit_score[]" id="time_demerit_score<?=$j?>" value="<?php echo $post['time_demerit_score'][$j]; ?>">
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>ライン</th>
									<td><ul class="scoreList scoreList01">
											<li>
												<label>ライン1</label>
												<em class="line">-</em> <span class="size07"><?php echo $post['line1_score'][$j]; ?></span>
												<input type="hidden" name="line1_score[]" id="line1_score<?=$j?>" value="<?php echo $post['line1_score'][$j]; ?>">
											</li>
											<li>
												<label>ライン2</label>
												<em class="line">-</em> <span class="size07"><?php echo $post['line2_score'][$j]; ?></span>
												<input type="hidden" name="line2_score[]" id="line2_score<?=$j?>" value="<?php echo $post['line2_score'][$j]; ?>">
											</li>
											<li>
												<label>減点</label>
												<em class="line">-</em> <span class="size07"><?php echo $post['line_demerit_score'][$j]; ?></span>
												<input type="hidden" name="line_demerit_score[]" id="line_demerit_score<?=$j?>" value="<?php echo $post['line_demerit_score'][$j]; ?>">
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>その他減点</th>
									<td><em class="other">-</em><span class="size07"><?php echo $post['other_demerit'][$j]; ?></span>
										<input type="hidden" name="other_demerit[]" id="other_demerit<?=$j?>" value="<?php echo $post['other_demerit'][$j]; ?>">
									</td>
								</tr>
							</table>
						</div>
						<div class="tableInfo">
							<table class="tableGeneral">
								<tr>
									<th>D合計</th>
									<th>E合計</th>
									<th>減点</th>
									<th>合計</th>
								</tr>
								<tr>
									<td id="d_score<?=$j?>">00.00</td>
									<td id="e_score<?=$j?>">00.00</td>
									<td id="demerit_score<?=$j?>">-00.0</td>
									<td id="final_score<?=$j?>">000.00</td>
								</tr>
							</table>
						</div>
					</div>
				<?php } // end for tab ?> 
				</div>
			</div>
			<ul class="buttonList clearfix">
				<li><a href="javascript:void(0)" onclick="window.history.back()" class="buttonStyle hover">戻る</a></li>
				<li class="editBtn">
					<input type="submit" value="登録する" class="buttonGeneral hover" id="EditBtn" name="submit[complete]" />
				</li>
				<?php if(isset($referee) && !$referee->isNormalReferee()) {?>
				<li class="submitButton">
					<input type="submit" value="公開する" class="buttonGeneral hover" id="publishBtn" name="submit[complete]" />
				</li>
				<?php }?>
			</ul>
		</form>
		<form action="" class="userForm" method="post">
			<div class="tabArea">
				<div class="tabContents">
					<div id="tab01" class="tabBox">
						<div class="tableStyle tableStyle01">
							<table>
								<tr>
									<th><span>D</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeD() ; $i++) {
											$getDScore = 'getD'.$i.'Score';
										?>
											<li>
												<label for="d1_score">D<?=$i?></label>
												<span class="size07">
												<input type="text" name="d<?=$i?>_score" class="size07 d_score" id="d<?=$i?>_score" value="<?=formatScore($player->getScoreValue($item, 'D'.$i)) ?>" />
												</span>
											</li>
										<?php } ?>
										</ul></td>
								</tr>
								<tr>
									<th><span>E</span></th>
									<td><ul class="scoreList">
										<?php for ($i=1; $i <= $item->getNumberRefereeE() ; $i++) {
											$getEScore = 'getE'.$i.'Score';
										?>
											<li>
												<label for="e1_score">E<?=$i?></label>
												<span class="size07">
												<input type="text" name="e<?=$i?>_score" class="size07 e_score" id="e<?=$i?>_score" value="<?=formatScore($player->getScoreValue($item, 'E'.$i)) ?>" />
												</span>
											</li>
										<?php } ?>
											<li>
												<label for="e_demerit_score">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="e_demerit_score" class="size07" id="e_demerit_score" value="<?=formatScore($player->getScoreValue($item, 'EDemerit')) ?>" />
												</span>
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>時間</th>
									<td><ul class="scoreList scoreList01">
											<li>
												<label for="time1_score1">タイム1</label>
												<span class="size08">
												<?php $time1 = ($player->getScoreValue($item, 'Time1'))?explode(':', $player->getScoreValue($item, 'Time1')):[0,0];
													  $time2 = ($player->getScoreValue($item, 'Time2'))?explode(':', $player->getScoreValue($item, 'Time2')):[0,0]; ?>
												<input type="text" name="time1_score1" class="size08" id="time1_score1" value="<?php echo $time1[0]; ?>" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time1_score2" class="size08" id="time1_score2" value="<?php echo $time1[1]; ?>" />
												</span></li>
											<li>
												<label for="time2_score1">タイム2</label>
												<span class="size08">
												<input type="text" name="time2_score1" class="size08" id="time2_score1" value="<?php echo $time2[0]; ?>" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time2_score2" class="size08" id="time2_score2" value="<?php echo $time2[1]; ?>" />
												</span>
											</li>
											<li>
												<label for="e_demerit_score">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="time_demerit_score" class="size07" id="time_demerit_score" value="<?=formatScore($player->getScoreValue($item, 'TimeDemerit'))?>" />
												</span>
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>ライン</th>
									<td><ul class="scoreList">
											<li>
												<label for="line1_score">ライン1</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line1_score" class="size07" id="line1_score" value="<?=formatScore($player->getScoreValue($item, 'Line1'))?>" />
												</span></li>
											<li>
												<label for="line2_score">ライン2</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line2_score" class="size07" id="line2_score" value="<?=formatScore($player->getScoreValue($item, 'Line2'))?>" />
												</span>
											</li>
											<li>
												<label for="e_demerit_score">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line_demerit_score" class="size07" id="line_demerit_score" value="<?php if ($player->getScoreValue($item, 'LineDemerit')) {echo $player->getScoreValue($item, 'LineDemerit');} else if (!$player->getScoreValue($item, 'LineDemerit')){echo $player->getScoreValue($item, 'Line1')+$player->getScoreValue($item, 'Line2');} ?>" />
												</span>
											</li>
										</ul></td>
								</tr>
								<tr>
									<th>その他減点</th>
									<td><em class="other">-</em><span class="size07">
										<input type="text" name="other_demerit" id="other_demerit" class="size07" id="other_demerit" value="<?=formatScore($player->getScoreValue($item, 'OtherDemerit'))?>" />
										</span></td>
								</tr>
							</table>
						</div>
						<div class="tableInfo">
							<table class="tableGeneral">
								<tr>
									<th>D合計</th>
									<th>E合計</th>
									<th>減点</th>
									<th>合計</th>
								</tr>
								<tr>
									<td id="d_score"></td>
									<td id="e_score"></td>
									<td id="demerit_score"></td>
									<td id="final_score"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<ul class="buttonList clearfix">
				<?php if($this->input->get('ref') == 'all'){ ?>
				<li><a href="/gymnastics/admin/<?=(isset($referee))?'referee':'organization'?>/game/all?gid=<?=$game->getId()?>" class="buttonStyle hover">戻る</a></li>
				<?php }else { ?>
				<li><a href="/gymnastics/admin/<?=(isset($referee))?'referee':'organization'?>/game/item?gid=<?=$game->getId()?>&item=<?=$item?>" class="buttonStyle hover">戻る</a></li>
				<?php } ?>
				<li class="submitButton">
					<input type="submit" value="更新する" class="buttonGeneral hover" name="submit[confirm]" id="changeBtn" />
				</li>
			</ul>
		</form>
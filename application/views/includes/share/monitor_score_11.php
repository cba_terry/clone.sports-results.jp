				<tr><?php 
				$higher = ($player->getFinalScore($citem) > $player->getFinalScore($citem, 2))  ? 1:2;
				$lower = ($player->getFinalScore($citem) <= $player->getFinalScore($citem, 2))  ? 1:2;
				?>
		              <td class="center first"><?=$player->getOrder($citem)?></td>
		              <td class="center"><?=$player->getPlayerNo()?></td>
		              <td class="left"><?=$player->getPlayerName()?></td>
		              <td class="left"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
		              <td><span class="number"><?=formatScore2($player->getDScore($citem, $lower))?></span><?=formatScore2($player->getDScore($citem, $higher))?></td>
		              <?php for($i=1; $i<=$citem->getNumberRefereeE(); $i++) { ?>
					  <td><span class="number"><?=formatScore2($player->getScoreValue($citem, 'E'.$i, $lower))?></span><?=formatScore2($player->getScoreValue($citem, 'E'.$i, $higher))?></td>
					  <?php } ?>
		              <td><span class="number"><?=formatScore2($player->getEScore($citem, $lower)+$player->getScoreValue($citem, 'EDemerit', $lower))?></span><?=formatScore2($player->getEScore($citem, $higher)+$player->getScoreValue($citem, 'EDemerit', $higher))?></td>
		              <td><span class="number">-<?=formatScore($player->getScoreValue($citem, 'EDemerit',$lower))?></span>-<?=formatScore($player->getScoreValue($citem, 'EDemerit', $higher))?></td>
		              <td><span class="number"><?=formatScore2($player->getEScore($citem, $lower))?></span><?=formatScore2($player->getEScore($citem, $higher))?></td>
		              <td><span class="number"><?=formatScore2($player->getEScore($citem, $lower)+$player->getDScore($citem, $lower))?></span><?=formatScore2($player->getEScore($citem, $higher)+$player->getDScore($citem, $higher))?></td>
		              <td><span class="number">-<?=formatScore($player->getScoreValue($citem, 'TimeDemerit', $lower)+$player->getScoreValue($citem, 'LineDemerit', $lower))?></span>-<?=formatScore($player->getScoreValue($citem, 'TimeDemerit', $higher)+$player->getScoreValue($citem, 'LineDemerit', $higher))?></td>
		              <td><span class="number"><?=$player->getFinalScore($citem, $lower)?></span><?=$player->getFinalScore($citem, $higher)?></td>
		              <td class="center"><?php if($pHeat->isOneOfThreeBest($player, $citem)) { ?><img alt="" src="<?=base_url('/img/monitor/icon_dot_white.gif')?>"><?php } ?></td>
		            </tr>
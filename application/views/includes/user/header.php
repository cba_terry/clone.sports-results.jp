<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title><?php echo get_value($title); ?></title>
<link rel="stylesheet" type="text/css" href="<?=base_url('/css/common.css')?>" media="all" />
<link rel="stylesheet" type="text/css" href="<?=base_url('/css/style.css')?>" media="all" />
<link rel="stylesheet" type="text/css" href="<?=base_url('/css/user.css')?>" media="all" />
<?php register_sources($css); ?>
<link rel="index contents" href="/" title="ホーム" />
<script type="text/javascript" src="<?=base_url('/js/jquery-1.8.3.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('/js/script.js')?>"></script>
<?php register_sources($js, 'script'); ?>
</head>
<body<?php echo get_value($pageId, ' id="%s"'); ?>>
	<div id="wrapper">
<ul class="tabLink clearfix">
<?php $publishSetting = $tournament->getPublicSetting(); ?>
<?php if($publishSetting->getNewsTimingPublish()) {?>
	<li<?php if ($page == 'result') { echo ' class="active"'; } ?>>
		<a href="/gymnastics/user/result/<?=$game->getId()?>">班別速報</a>
	</li>
<?php } ?>
<?php if($publishSetting->getGroupTimingPublish()) {?>
	<li<?php if ($page == 'group') { echo ' class="active"'; } ?>>
		<a href="/gymnastics/user/ranking/group/<?=$game->getId()?>">団体順位</a>
	</li>
<?php } // endif $flag_group ?>
<?php if($publishSetting->getSingleTimingPublish()) {?>
	<li<?php if ($page == 'single') { echo ' class="active"'; } ?>>
		<a href="/gymnastics/user/ranking/single/<?=$game->getId()?>">個人総合順位</a>
	</li>
<?php } ?>
<?php if($publishSetting->getItemTimingPublish()) {?>
	<li<?php if ($page == 'event') { echo ' class="active"'; } ?>>
		<a href="/gymnastics/user/ranking/event/<?=$game->getId()?>">種目別順位</a>
	</li>
<?php } ?>
</ul>
<!-- /.tabLink -->
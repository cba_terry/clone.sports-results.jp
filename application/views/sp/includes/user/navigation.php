<form method="post" action="">
<?php $publishSetting = $tournament->getPublicSetting(); ?>
	<select class="menu-tag">
	<?php if($publishSetting->getNewsTimingPublish()) {?>
		<option<?php if ($page == 'result') { echo ' selected'; } ?> value="/gymnastics/user/result/<?=$game->getId()?>">班別速報</option>
	<?php } ?>
	<?php if($publishSetting->getGroupTimingPublish()) {?>
		<option<?php if ($page == 'group') { echo ' selected'; } ?> value="/gymnastics/user/ranking/group/<?=$game->getId()?>">団体順位</a></option>
	<?php } ?>
	<?php if($publishSetting->getSingleTimingPublish()) {?>
		<option<?php if ($page == 'single') { echo ' selected'; } ?> value="/gymnastics/user/ranking/single/<?=$game->getId()?>">個人総合順位</option>
	<?php } ?>
	<?php if($publishSetting->getItemTimingPublish()) {?>
		<option<?php if ($page == 'event') { echo ' selected'; } ?> value="/gymnastics/user/ranking/event/<?=$game->getId()?>">種目別順位</option>
	<?php } ?>
	</select>
</form>
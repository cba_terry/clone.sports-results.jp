<?php
	$this->load->view("includes/user/header", array(
		'title'  => '大会一覧',
		'css'    => '',
		'js'     => 'biggerlink',
		'pageId' => 'pageTournamentList'
	));
?>
<div id="contents">
	<h1 class="headline1"><?php if ($association) echo $association->getName()?></h1>
	<div class="contentsBlock">
		<ul class="tournamentList">
		<?php
			if( ! empty($tournaments)) {
				foreach ($tournaments as $key => $tournament) {
		?>
			<li>
				<p><?=$tournament->getStartTime()->format('Y.m.d')?> - <?=$tournament->getEndTime()->format('Y.m.d')?></p>
				<p class="link"><a href="/gymnastics/user/game_list/<?=$tournament->getId()?>"><?=$tournament->getName()?></a></p>
				<p><?=$tournament->getPlace()?></p>
			</li>
		<?php
				} // endforeach
			} // endif
		?>
		</ul>
	</div>
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
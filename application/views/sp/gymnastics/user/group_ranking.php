<?php
	$this->load->view("includes/user/header", array(
		'title'  => '団体ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGroupRanking'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getGroupTimingPublishScore();
?>
<div id="contents">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="clearfix">
				<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
				<p class="tag"><?=($publicSetting->getGroupTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
			</div>
			<?php $this->load->view("includes/user/navigation", ['page' => 'group'])?>
		</div>
		<div class="section">
			<div class="tableResult">
				<table>
					<tr>
						<th class="col01" rowspan="2">順位</th>
						<th>学校名 (県)</th>
						<th class="col02" rowspan="2">合計<br>
(点差)</th>
					</tr>
					<tr>
						<th>種目/得点<?php if ($anableOptions) { echo ' (順位)'; } ?></th>
					</tr>
				<?php 
				$lastRank = $resultGroupRanking->first();
				foreach ($resultGroupRanking as $rank) { 
					$groupScore = $rank->getElement();
					$school     = $groupScore->getSchoolGroup();
				?>
					<tr>
						<td class="active01" rowspan="<?=($game->isMale()) ? 4 : 3;?>"><?=$rank->getRank()?></td>
						<td class="col01">
							<?=$school->getSchoolName()?>
							<?=($school->getSchoolPrefecture()) ? '('.$school->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point" rowspan="<?=($game->isMale()) ? 4 : 3;?>">
							<?=formatScore($groupScore->getTotalScore())?>
							<em class="block">(<?=$rank->distanceAnotherRank($lastRank, 'getTotalScore')?>)</em>
						</td>
					</tr>
					<tr>
						<td class="left">
						<?php
						$index     = 1;
						$int_items = count($items);
						foreach ($items as $item) {
						?>
						<span class="size05"><?=$item->getName()?></span>
						<span class="size06">
							<?=formatScore(call_user_func([$groupScore, 'getItem'.$item->getNo().'Score']))?>
						<?php if ($anableOptions) { ?>
							<em>(<?=$resultItemRanking[$item->getName()]->findRank($groupScore)?>)</em>
						<?php } // endif $anableOptions ?>
						</span>
						<?php
							if ( ! ($index % 2) && $index > 1 && $index < $int_items) {
								echo "</td></tr>";
								echo "<tr><td class=\"left\">";
							}
							$index++;
						}
						?>
						</td>
					</tr>
				<?php
				$lastRank = $rank;
				} // endforeach
				?>
				</table>
			</div>
		</div>
		<p class="buttonBack mt30"><a href="javascript:history.back()">戻る</a></p>
	</div>
	<!-- /.contentsBlock -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
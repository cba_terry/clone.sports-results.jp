<?php
	$this->load->view("includes/user/header", array(
		'title'  => '種目別ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageEventRanking'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getItemTimingPublishScore();
?>
<div id="contents">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="clearfix">
				<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
				<p class="tag"><?=($publicSetting->getItemTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
			</div>
			<?php $this->load->view("includes/user/navigation", ['page' => 'event'])?>
		</div>
		<ul class="eventsList">
		<?php foreach ($items as $it) { ?>
			<li><a href="/gymnastics/user/ranking/event/<?=$game->getId()?>?event=<?=$it->getNo()?>"<?php if($it->getNo() === $item->getNo()){echo ' class="active"';} ?>><?=$it?></a></li>
		<?php } ?>
		</ul>
		<div class="section">
			<div class="headSection">
				<h3 class="headline2"><?=$item?></h3>
			</div>
			<div class="tableResult">
				<table>
					<tr>
						<th class="col01">順位</th>
						<th>選手名 [学年]</th>
						<th>学校名 (県)</th>
						<th class="col02">合計</th>
					</tr>
				<?php
					$lastRank = $resultItemRanking->first();
				if ( ! $resultItemRanking->isEmpty()) {
					foreach ($resultItemRanking as $rank) {
						$player              = $rank->getElement();
						$distanceAnotherRank = $rank->distanceAnotherRank($lastRank, 'getItemBestScoreValue', [$item, 'FinalScore']);
				?>
					<tr>
						<td class="active01"<?=($item->isPlayTwice()) ? ' rowspan="2"' : '';?>><?=$rank->getRank()?></td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point"<?=($item->isPlayTwice()) ? ' rowspan="2"' : '';?>><?=formatScore($player->getItemBestScoreValue($item, 'FinalScore'))?></td>
					</tr>
				<?php if ($item->isPlayTwice()) { ?>
					<tr>
						<td class="left" colspan="2">
							<span class="size05">1本目</span>
							<span class="size06"><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 1))?></span>
							<span class="size05">2本目</span>
							<span class="size06"><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 2))?></span>
						</td>
					</tr>
				<?php } // endif $item->isPlayTwice() ?>
				<?php
						$lastRank = $rank;
					} // endforeach $resultItemRanking
				} // endif $resultItemRanking

				if ( ! $resultItemCancle->isEmpty()) {
					foreach ($resultItemCancle as $rank) {
						$player = $rank->getElement();
				?>
					<tr>
						<td class="active01"<?=($item->isPlayTwice()) ? ' rowspan="2"' : '';?>><?=$rank->getRank()?></td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point"<?=($item->isPlayTwice()) ? ' rowspan="2"' : '';?>>-</td>
					</tr>
				<?php if ($item->isPlayTwice()) { ?>
					<tr>
						<td class="left" colspan="2">
							<span class="size05">1本目</span>
							<span class="size06">-</span>
							<span class="size05">2本目</span>
							<span class="size06">-</span>
						</td>
					</tr>
				<?php } // endif $item->isPlayTwice() ?>
				<?php
					} // endforeach $resultItemCancle
				} // endif $resultItemCancle
				?>
				</table>
			</div>
		</div>
		<p class="buttonBack mt30"><a href="javascript:history.back()">戻る</a></p>
	</div>
	<!-- /.contentsBlock -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
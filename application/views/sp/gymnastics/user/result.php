<?php
	$this->load->view("includes/user/header", array(
		'title'  => '成績速報',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageResult'
	));

	$publicSetting   = $tournament->getPublicSetting();
	$anableOptions   = $publicSetting->getNewsTimingPublishScore();
	$rowspan         = $rowspanGroup = count($items) + 1;

	// Update rowspan
	foreach ($items as $item) {
		if ($item->isPlayTwice())
			$rowspan += 1;
	}
?>
<div id="contents">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="clearfix">
				<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
				<p class="tag"><?=($publicSetting->getNewsTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
			</div>
			<?php $this->load->view("includes/user/navigation", ['page' => 'result'])?>
		</div>
		<?php
			foreach ($groupHeatPlayers as $group) {
				foreach ($group->getHeats() as $heat) {
					$heatRotate = $heat->getRotations();
					// tach players trong head thanh 2 loai, choi cho group va choi cho head
					$playsAsGroup  = $heat->getPlayersPlayAsGroup();
					$playsAsSingle = $heat->getPlayersPlayAsSingle();
		?>
		<div class="section">
			<div class="headSection accordion">
				<h3 class="headline2"><span><?=$group->getId()?>班<?=$heat->getId()?>組</span></h3>
			</div>
			<div class="accordionBox tableResult">
				<table>
					<tr>
						<th class="col01" rowspan="2">No</th>
						<th>選手名 [学年]</th>
						<th>学校名 (県)</th>
						<th class="col02" rowspan="2">合計</th>
						<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<th class="col03" rowspan="<?=$rowspan?>">順位</th>
						<?php }?>
					</tr>
					<tr>
						<th colspan="2">
							種目/得点<?php if ($anableOptions) { echo ' (D,E,減点)';} ?>
						</th>
					</tr>
				<?php foreach ($playsAsGroup as $player) { ?>
				<?php $currentItemPlay = $player->getCurrentPlay($heatRotate, $items);?>
					<tr>
						<td rowspan="<?=$rowspan?>"><?=$player->getPlayerNo()?></td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							[<?=$player->getGrade()?>年]
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point" rowspan="<?=$rowspan?>"><?=userFormatScore($player->getTotalFinalScore())?></td>
						<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<td><?=$resultRanking->findRank($player)?></td>
						<?php }?>
					</tr>
					<?php foreach ($items as $item) { ?>
					<tr>
						<?php
							if(!$player->hasItemScorePublished($item)
								&& $currentItemPlay->getName()==$item->getName()
								&& $group->getId()==$group_current
							) {
						?>
						<td class="col02" colspan="2"<?=($item->isPlayTwice())?' rowspan="2"':'';?>>競技中</td>
						<?php } else { ?>
						<td class="left" colspan="2">
							<span class="size01"><?=$item->getName()?><?=($item->isPlayTwice())?'1本目':'';?></span>
							<span class="size02">
								<?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',1))?>
							<?php if ($anableOptions) { ?>
								<?php
									if(!$player->hasItemScorePublished($item)
										&& $player->getFlagCancle()
									) {
								?>
								<em>(D -, E -, 減 -)</em>
								<?php } else { ?>
								<em>(D <?=userFormatScore($player->getItemScoreValue($item, 'DScore',1))?>,
								E <?=userFormatScore($player->getItemScoreValue($item, 'EScore',1))?>,
								減 <?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',1), '-')?>)</em>
								<?php } ?>
							<?php } // endif $anableOptions ?>
							</span>
						</td>
						<?php } // endif ?>
					</tr>
					<?php if($item->isPlayTwice()){ ?>
					<tr>
						<td class="left" colspan="2">
							<span class="size01"><?=$item->getName()?>2本目</span>
							<span class="size02">
								<?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',2))?>
							<?php if ($anableOptions) { ?>
								<?php
									if(!$player->hasItemScorePublished($item)
										&& $player->getFlagCancle()
									) {
								?>
								<em>(D -, E -, 減 -)</em>
								<?php } else { ?>
								<em>(D <?=userFormatScore($player->getItemScoreValue($item, 'DScore',2))?>,
								E <?=userFormatScore($player->getItemScoreValue($item, 'EScore',2))?>,
								減 <?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',2), '-')?>)</em>
								<?php } ?>
							<?php } // endif $anableOptions ?>
							</span>
						</td>
					</tr>
					<?php } // endif $item->isPlayTwice() ?>
					<?php } // endforeach $items ?>
				<?php } // endforeach $playsAsGroup ?>
				<?php if ($heat->hasGroupSchoolPlay()){ ?>
				<tr class="active02">
					<td rowspan="<?=$rowspanGroup?>">&nbsp;</td>
					<td class="col01" colspan="2">チームベスト3</td>
					<td class="point" rowspan="<?=$rowspanGroup?>"><?=userFormatScore($heat->getSchoolTotalItem())?></td>
					<?php if($publicSetting->getNewsTimingPublishRank()) {?>
					<td rowspan="<?=$rowspanGroup?>">&nbsp;</td>
					<?php } ?>
				</tr>
				<?php foreach ($items as $item) { ?>
				<tr>
					<td class="left active02" colspan="2">
						<span class="size01"><?=$item->getName()?></span>
						<span class="size02"><?=userFormatScore($heat->getSchoolScoreItem($item))?></span>
					</td>
				</tr>
				<?php } // endforeach $items ?>
				<?php } // endif $heat->hasGroupSchoolPlay() ?>
				<?php
				if ($playsAsSingle) {
					foreach ($playsAsSingle as $player) {
				?>
				<?php $currentItemPlay = $player->getCurrentPlay($heatRotate, $items);?>
					<tr>
						<td rowspan="<?=$rowspan?>"><?=$player->getPlayerNo()?></td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							[<?=$player->getGrade()?>年]
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point" rowspan="<?=$rowspan?>"><?=userFormatScore($player->getTotalFinalScore())?></td>
						<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<td><?=$resultRanking->findRank($player)?></td>
						<?php }?>
					</tr>
					<?php foreach ($items as $item) { ?>
					<tr>
						<?php
							if(!$player->hasItemScorePublished($item)
								&& $currentItemPlay->getName()==$item->getName()
								&& $group->getId()==$group_current
							) {
						?>
						<td class="col02" colspan="2"<?=($item->isPlayTwice())?' rowspan="2"':'';?>>競技中</td>
						<?php } else { ?>
						<td class="left" colspan="2">
							<span class="size01"><?=$item->getName()?><?=($item->isPlayTwice())?'1本目':'';?></span>
							<span class="size02">
								<?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',1))?>
							<?php if ($anableOptions) { ?>
								<?php
									if(!$player->hasItemScorePublished($item)
										&& $player->getFlagCancle()
									) {
								?>
								<em>(D -, E -, 減 -)</em>
								<?php } else { ?>
								<em>(D <?=userFormatScore($player->getItemScoreValue($item, 'DScore',1))?>,
								E <?=userFormatScore($player->getItemScoreValue($item, 'EScore',1))?>,
								減 <?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',1), '-')?>)</em>
								<?php } ?>
							<?php } // endif $anableOptions ?>
							</span>
						</td>
						<?php } // endif ?>
					</tr>
					<?php if($item->isPlayTwice()){ ?>
					<tr>
						<td class="left" colspan="2">
							<span class="size01"><?=$item->getName()?>2本目</span>
							<span class="size02">
								<?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',2))?>
							<?php if ($anableOptions) { ?>
								<?php
									if(!$player->hasItemScorePublished($item)
										&& $player->getFlagCancle()
									) {
								?>
								<em>(D -, E -, 減 -)</em>
								<?php } else { ?>
								<em>(D <?=userFormatScore($player->getItemScoreValue($item, 'DScore',2))?>,
								E <?=userFormatScore($player->getItemScoreValue($item, 'EScore',2))?>,
								減 <?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',2), '-')?>)</em>
								<?php } ?>
							<?php } // endif $anableOptions ?>
							</span>
						</td>
					</tr>
					<?php } // endif $item->isPlayTwice() ?>
					<?php } // endforeach $items ?>
				<?php
					} // endforeach $playsAsGroup 
				} // endif $playsAsSingle
				?>
				</table>
			</div>
		</div>
		<?php } // endforeach heat
		} // endforeach group
		?>
		<p class="buttonBack mt30"><a href="javascript:history.back()">戻る</a></p>
	</div>
	<!-- /.contentsBlock -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
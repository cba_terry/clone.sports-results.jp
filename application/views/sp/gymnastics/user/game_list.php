<?php
	$this->load->view("includes/user/header", array(
		'title'  => '試合一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGameList'
	));
?>
<div id="contents">
	<h1 class="headline1">
	<?php
		if ( ! empty($tournament)) {
			echo $tournament->getName();
		};
	?>
	</h1>
	<?php
		if ( ! empty($game)) {
			$item_allow = $tournament->getClass();
			foreach ($game as $gender => $list_game) {
				$str_gender = gender($gender);
				if ($gender) {
					$info_box = array(
						'class' => 'contentsBlock',
					);
				} else {
					$info_box = array(
						'class' => 'contentsBlock01',
					);
				}

				$publishSetting = $tournament->getPublicSetting();

				if($publishSetting->getNewsTimingPublish()) {
					$info_box['url'] = '/gymnastics/user/result/';
				}
				elseif($publishSetting->getGroupTimingPublish()) {
					$info_box['url'] = '/gymnastics/user/ranking/group/';
				}
				elseif($publishSetting->getSingleTimingPublish()) {
					$info_box['url'] = '/gymnastics/user/ranking/single/';
				}
				elseif($publishSetting->getItemTimingPublish()) {
					$info_box['url'] = '/gymnastics/user/ranking/event/';
				}
	?>
	<div<?php echo get_value($info_box['class'], ' class="%s"'); ?>>
		<h2 class="headline2"><?php echo $str_gender; ?></h2>
		<div class="tableList">
			<table>
			<?php
				if ( ! empty($list_game)) {
						foreach ($list_game as $key => $info_game) {
							$str_gender_class = $str_gender . $info_game->getClass();
							if (in_array($str_gender_class, $item_allow)) {
			?>
				<tr>
					<td><?php echo $info_game->getClass(); ?></td>
					<td class="col01"><a href="<?php echo $info_box['url'] . $info_game->getId(); ?>" class="buttonActive">一覧</a></td>
				</tr>
			<?php
						} // endif
					} // endforeach
				} // endif;
			?>
			</table>
		</div>
	</div>
	<?php
			} // endforeach
		} // endif
	?>
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
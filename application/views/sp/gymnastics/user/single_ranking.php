<?php
	$this->load->view("includes/user/header", array(
		'title'  => '個人総合ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageSingleRanking'
	));
	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getSingleTimingPublishScore();
?>
<div id="contents">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="clearfix">
				<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
				<p class="tag"><?=($publicSetting->getSingleTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
			</div>
			<?php $this->load->view("includes/user/navigation", ['page' => 'single'])?>
		</div>
		<div class="section">
			<div class="tableResult">
				<table>
					<tr>
						<th class="col01" rowspan="2">No</th>
						<th>選手名 [学年]</th>
						<th>学校名 (県)</th>
						<th class="col02" rowspan="2">合計<br>
(点差)</th>
					</tr>
					<tr>
						<th colspan="2">種目/得点<?php if ($anableOptions) { echo ' (順位)'; } ?></th>
					</tr>
				<?php
					$rowspan  = ($game->isMale()) ? 4 : 3;
					$lastRank = $resultSingleRanking->first();
					foreach ($resultSingleRanking as $rank) {
						$player = $rank->getElement();
				?>
					<tr>
						<td class="active01" rowspan="<?=$rowspan?>"><?=$rank->getRank()?></td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point" rowspan="<?=$rowspan?>">
							<?=$player->getTotalFinalScore()?>
							<em class="block">(<?=$rank->distanceAnotherRank($lastRank, 'getTotalFinalScore')?>)</em>
						</td>
					</tr>
					<tr>
						<td class="left" colspan="2">
						<?php
						$index     = 1;
						$int_items = count($items);
						foreach ($items as $item) {
						?>
							<span class="size05"><?=$item->getName();?></span>
							<span class="size06"><?=formatScore($player->getItemBestScoreValue($item, 'FinalScore'),2)?>
							<?php if ($anableOptions) { ?>
								<em>(<?=$resultItemRanking[$item->getName()]->findRank($player)?>)</em>
							<?php } // endif $anableOptions ?>
							</span>
						<?php
							if ( ! ($index % 2) && $index > 1 && $index < $int_items) {
								echo "</td></tr>";
								echo "<tr><td class=\"left\" colspan=\"2\">";
							}
							$index++;
						}
						?>
						</td>
					</tr>
				<?php 
					$lastRank = $rank;
					} // endforeach $resultSingleRanking

				if ( ! $resultSingleCancle->isEmpty()) {
					foreach ($resultSingleCancle as $rank) {
						$player = $rank->getElement();
				?>
					<tr>
						<td class="active01" rowspan="<?=$rowspan?>">&nbsp;</td>
						<td class="col01">
							<?=$player->getPlayerName()?>
							<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
						</td>
						<td class="col01">
							<?=$player->getSchoolNameAb()?>
							<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
						</td>
						<td class="point" rowspan="<?=$rowspan?>"><?=$player->getTotalFinalScore()?></td>
					</tr>
					<tr>
						<td class="left" colspan="2">
						<?php
						$index     = 1;
						$int_items = count($items);
						foreach ($items as $item) {
						?>
							<span class="size05"><?=$item->getName();?></span>
							<span class="size06">
							<?=($player->getBestItemScore($item)) ? formatScore($player->getItemBestScoreValue($item, 'FinalScore'),2) : '-';?>
							</span>
						<?php
							if ( ! ($index % 2) && $index > 1 && $index < $int_items) {
								echo "</td></tr>";
								echo "<tr><td class=\"left\" colspan=\"2\">";
							}
							$index++;
						}
						?>
						</td>
					</tr>
				<?php
					} // endforeach $resultSingleRanking
				} // endif $resultSingleRanking
				?>
				</table>
			</div>
		</div>
		<p class="buttonBack mt30"><a href="javascript:history.back()">戻る</a></p>
	</div>
	<!-- /.contentsBlock01 -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
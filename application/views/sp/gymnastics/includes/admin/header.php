<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<title><?php echo get_value($title); ?></title>
<link rel="stylesheet" href="<?=base_url('/sp/css/common.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/sp/css/style.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/sp/css/admin.css')?>" media="all">
<?php register_sources($css); ?>
<link rel="index contents" href="/" title="ホーム">
<script src="<?=base_url('/sp/js/jquery-1.8.3.min.js')?>"></script>
<script src="<?=base_url('/sp/js/script.js')?>"></script>
<?php register_sources($js, 'script'); ?>
<!--[if lt IE 9]>
<script src="<?=base_url('/js/html5shiv-printshiv.js')?>"></script>
<![endif]-->
</head>
<body id="<?php echo $pageId; ?>">
	<div id="wrapper">
		<header id="header" class="clearfix">
			<p class="backButton">
				<a href="/gymnastics/admin/referee/top"><img src="<?=base_url('/sp/img/gymnastics/admin/referee/btn_back.png')?>" alt=""></a>
			</p>
			<h1 class="headline1">日本体操協会</h1>
			<p class="logoutButton">
				<a href="/gymnastics/admin/referee/logout"><img src="<?=base_url('/sp/img/gymnastics/admin/referee/btn_logout.png')?>" alt=""></a>
			</p>
		</header>
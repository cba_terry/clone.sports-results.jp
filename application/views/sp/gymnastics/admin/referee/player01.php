<?php
	$this->load->view("gymnastics/includes/admin/header", array(
		'title'  => '組内選手一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pagePlayer'
	));
	$group = $this->input->get('group');
	$heat = $this->input->get('heat');
?>
<div id="contents" class="clearfix">
	<div id="main">
		<h2 class="headline2"><?=$tourInf->getName()?><br><span><?=$rfeGame->getStrSex() . ' ' . $rfeGame->getClass(); ?> <?=$item->getName()?></span><br>
		<?php echo $group . '班' . $heat . '組'; ?></h2>
		<ul class="itemText">
			<?php
				foreach ($players as $player) {
					$school_name = ($player->getSchool()) ? $player->getSchool()->getSchoolNameAb() : '';
			?>
			<li>
				<div>
					<p class="numberText"><?php echo $player->getPlayerNo(); ?></p>
					<p><?php echo $player->getPlayerName() . '(' . $school_name . ')'; ?></p>
				</div>
				<?=referee_edit_score_button($player->getScoreStatus($item, $referee->getScoreTypeMap(), 1),
											array(
												'player' => $player->getId(),
												'group'  => $group,
												'heat'   => $heat,
											),
											$player->getFlagCancle())?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("gymnastics/includes/admin/footer"); ?>

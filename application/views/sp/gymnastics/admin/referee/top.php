<?php
	$this->load->view("gymnastics/includes/admin/header", array(
		'title'  => '種目・班・組選択',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageTop'
	));
	$group = ($this->input->get('group')) ? $this->input->get('group') : '1';
	$heat = ($this->input->get('heat')) ? $this->input->get('heat') : '1';
?>
<div id="contents" class="clearfix">
	<div id="main">
		<h2 class="headline2">
		<?php
			if (isset($rfeGame)) {
				echo $tourInf->getName() . '<br>';
				echo '<span>' . $rfeGame->getStrSex() . ' ' . $rfeGame->getClass() . ' ' . $rfeItem. '</span>';
			}
		?>
		</h2>
		<form class="searchForm" method="get" action="/gymnastics/admin/referee/player">
			<h3 class="headline3">班</h3>
			<ul class="radioList">
			<?php foreach ($groups as $key => $value) { ?>
				<li class="radio">
					<?=form_radio(array(
						'name' => 'group', 
						'id' => 'group' . $key, 
						'value' => $key, 
						'checked' => ($group == $key)
					))?>
					<?=form_label($key.'班','group' . $key)?>
				</li>
			<?php } ?>
			</ul>
			<!-- /.radioList -->
			<h3 class="headline3">組</h3>
			<ul class="radioList" id="heatList">
			<?php foreach ($groups[$heat] as $key => $value) { ?>
				<li class="radio">
					<?=form_radio(array(
						'name' => 'heat', 
						'id' => 'heat' . $key, 
						'value' => $key, 
						'checked' => ($heat == $key)
					))?>
					<?=form_label($key.'組','heat' . $key)?>
				</li>
			<?php } ?>
			</ul>
			<!-- /.radioList -->
			<p class="buttonStyle"><input type="submit" id="searchButton" class="hover" value="検索する"></p>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<script type="text/javascript">
	
	var groups = <?=json_encode($groups)?>;

	$('input[name=group]').click(function(){
		var group = $(this).val();
		var curgroup = groups[group];
		
		var html = '';

		checked = 'checked="checked"';
		$.each(curgroup, function(k,v){
			html += '<li class="radio"><input type="radio" '+checked+' name="heat" value="'+v+'" id="heat'+v+'"><label for="heat'+v+'">'+v+'組</label></li>';
			checked = '';
		});
		
		$('#heatList').html(html);
	});
</script>
<?php $this->load->view("gymnastics/includes/admin/footer"); ?>

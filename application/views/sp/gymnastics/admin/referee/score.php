<?php
	$this->load->view("gymnastics/includes/admin/header", array(
		'title'  => '得点入力［D審判］',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageScoreD'
	));
	$group      = $this->input->get('group');
	$heat       = $this->input->get('heat');
	$title_head = ($player->getSchool()) ? '(' . $player->getSchool()->getSchoolNameAb() . ')' : '';
	$title_head = $player->getPlayerNo() . ' ' . $player->getPlayerName() . ' ' . $title_head;
?>
<div id="contents" class="clearfix">
	<div id="main">
		<h2 class="headline2"><?=$tourInf->getName()?><br><span><?=$rfeGame->getStrSex() . ' ' . $rfeGame->getClass(); ?> <?=$rfeItem?></span><br>
<span><?php echo $group . '班' . $heat . '組'; ?></span><br>
<?=$title_head?></h2>
		<form class="scoreForm" method="post" action="">
			<h3 class="headline3"><?=detectScoreType($scoreType)?></h3>
			<div class="scoreTable">
				<table>
					<tr>
						<td><a href="javascript:void(0)" class="key" data-key="1">1</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="2">2</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="3">3</a></td>
					</tr>
					<tr>
						<td><a href="javascript:void(0)" class="key" data-key="4">4</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="5">5</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="6">6</a></td>
					</tr>
					<tr>
						<td><a href="javascript:void(0)" class="key" data-key="7">7</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="8">8</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="9">9</a></td>
					</tr>
					<tr>
						<td><a href="javascript:void(0)" class="key" data-key="<?=detectScoreTypeDevideChar($scoreType)?>" class="darkButton"><?=detectScoreTypeDevideChar($scoreType)?></a></td>
						<td><a href="javascript:void(0)" class="key" data-key="0">0</a></td>
						<td><a href="javascript:void(0)" class="key" data-key="-1" class="darkButton"><img src="<?=base_url('/sp/img/gymnastics/admin/referee/btn_delete.png')?>" alt=""></a></td>
					</tr>
				</table>
			</div>
			<!-- /.scoreTable -->
			<div class="wrapScore clearfix">
				<p class="score"><?=detectScoreTypeFormatNumber($scoreType)?></p>
				<input type="hidden" name="score" id="score" value="">
				<p class="smallButton buttonStyle"><a href="javascript:void(0)" class="hover openModal">登録</a></p>
			</div>
		</form>
	</div>
	<!-- /#main -->
	<div class="modal">
		<form action="#" method="post">
			<p class="close hover"><a href="javascript:void(0)"><img src="<?=base_url('/sp/img/gymnastics/admin/referee/btn_close.png')?>" alt="X" /></a></p>
			<div class="modalContent">
				<p class="question">確定して宜しいですか？</p>
				<p class="score"></p>
				<p class="button mb20"><input type="submit" id="scoreSubmit" value="登録を確定する"></p>
				<p class="button button01"><input type="submit" value="修正する"></p>
			</div>
		</form>
	</div>
</div>
<!-- /#contents -->
<script type="text/javascript">

	var negative = ($('.score').text().match(/^-/)) ? '-' : '';
	var devide = ($('.score').text().match(/:/)) ? ':' : '.';

	$('.key').click(function(){
		var key = $(this).attr('data-key');
		var score = $('#score');

		if (key == '-1'){
			if (score.val()){
				if(score.val().length == 1 && devide == '.' && negative != '-') score.val('00.000');
				else if(score.val().length == 1 && devide == ':') score.val('00:000');
				else if(score.val().length == 1 && negative == '-') score.val('0.000');
				score.val(score.val().slice(0,-1));
			}
		}else{
			if(score.val().match(/^0+\.?:?0+$/)) score.val(key);
			else score.val(score.val()+key);
		}

		$('p.score').text(negative+score.val());
	});
	$('#scoreSubmit').click(function(e){
		e.preventDefault();
		$('.scoreForm').submit();
	})
</script>
<?php $this->load->view("gymnastics/includes/admin/footer"); ?>

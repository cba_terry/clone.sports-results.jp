<?php
	$this->load->view("gymnastics/includes/admin/header", array(
		'title'  => '組内選手一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pagePlayer'
	));
	$group = $this->input->get('group');
	$heat = $this->input->get('heat');
	$active = ($this->input->get('round')) ? $this->input->get('round') : 1;
?>
<div id="contents" class="clearfix">
	<div id="main">
		<h2 class="headline2"><?=$tourInf->getName()?><br><span><?=$rfeGame->getStrSex() . ' ' . $rfeGame->getClass(); ?> <?=$item->getName()?></span><br>
		<?php echo $group . '班' . $heat . '組'; ?></h2>
		<ul class="tab clearfix">
			<li <?php if($active == 1) echo 'class="active"';?>><a href="#tab01"><span>1回目</span></a></li>
			<li <?php if($active == 2) echo 'class="active"';?>><a href="#tab02"><span>2回目</span></a></li>
		</ul>
		<div class="tabContents">
			<div id="tab01" class="tabBox" <?php if($active == 1) echo 'style="display:block"';else echo 'style="display:none"'?>>
				<ul class="itemText">
				<?php
					$round = 1;
					foreach ($players as $player) { $school_name = ($player->getSchool()) ? $player->getSchool()->getSchoolNameAb() : '';
				?>
					<li>
						<div>
							<p class="numberText"><?php echo $player->getPlayerNo(); ?></p>
							<p><?php echo $player->getPlayerName() . '(' . $school_name . ')'; ?></p>
						</div>
						<?=referee_edit_score_button($player->getScoreStatus($item, $referee->getScoreTypeMap(), $round),
													array(
														'player' => $player->getId(),
														'group'  => $group,
														'heat' 	 => $heat,
														'round'  => $round
													),
													$player->getFlagCancle())?>
					</li>
				<?php } ?>
				</ul>
			</div>
			<div id="tab02" class="tabBox" <?php if($active == 2) echo 'style="display:block"';else echo 'style="display:none"'?>>
				<ul class="itemText">
				<?php
					$round = 2;
					foreach ($players as $player) { $school_name = ($player->getSchool()) ? $player->getSchool()->getSchoolNameAb() : '';
				?>
					<li>
						<div>
							<p class="numberText"><?php echo $player->getPlayerNo(); ?></p>
							<p><?php echo $player->getPlayerName() . '(' . $school_name . ')'; ?></p>
						</div>
						<?=referee_edit_score_button($player->getScoreStatus($item, $referee->getScoreTypeMap(), $round),
													array(
														'player' => $player->getId(),
														'group' => $group,
														'heat' 	=> $heat,
														'round' => $round
													),
													$player->getFlagCancle())?>
					</li>
				<?php } ?>
				</ul>
			</div>
		</div>
		<!-- /.tabContents -->
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("gymnastics/includes/admin/footer"); ?>

<?php
	$this->load->view("gymnastics/includes/admin/header", array(
		'title'  => 'ログイン',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageLogin'
	));
?>
<div class="loginBlock">
	<form action="" id="loginForm" class="transform loginForm" method="post">
		<h1 id="logo"><img src="<?=base_url('/sp/img/gymnastics/admin/referee/logo.gif')?>" alt="logo"></h1>
		<?php
			if ( ! empty(validation_errors()) || isset($error)) {
				echo '<div class="mt10">';
				echo validation_errors(); 
				echo @$error;
				echo '</div>';
			}
		?>
		<div class="boxLogin refereeLogin clearfix">
			<p class="textBox">
				<span id="username" class="select">
					<select name="username">
				<?php
					if ( ! empty($referees)) {
						foreach ($referees as $key => $referee) {
				?>
						<option value="<?php echo $referee['referee_name']; ?>"><?php echo $referee['referee_name']; ?></option>
				<?php
						} // endforeach
					} // endif
				?>
					</select>
				</span>
				<input type="password" placeholder="Password" id="password" name="password" />
			</p>
			<input type="submit" id="button" class="buttonCustom" value="Login">
			<p class="remember">
				<input type="checkbox" id="checkbox" name="remember-me" value="1" />
				<label for="checkbox">パスワードを記憶する</label>
			</p>
			<p class="forget"><a href="#">パスワードを忘れた</a></p>
		</div>
	</form>
</div>
<?php $this->load->view("gymnastics/includes/admin/footer"); ?>
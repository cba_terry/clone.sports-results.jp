<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '全班・組選手一覧',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageAllList'
	));
	$group = ($this->input->get('group')) ? $this->input->get('group') : '1';
	$heat = ($this->input->get('heat')) ? $this->input->get('heat') : '1';
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="" method="get">
			<div class="headBox headBox02 clearfix">
				<h2 class="headline1"><?php echo $game->getStrSex() . ' ' . $game->getClass() ?></h2>
				<ul class="groupList">
					<li class="select size01">
						<select name="group" id="group_search">
							<?php foreach ($groups as $key => $g) { ?>
							<option <?php if($g['group'] == $group){ ?>selected="selected"<?php } ?> value="<?=$g['group']?>"><?=$g['group']?>班</option>
							<?php } ?>
						</select>
					</li>
					<li><input type="submit" class="buttonCustom hover" value="切替" style="opacity: 1;"></li>
				</ul>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo tablePlayer">
				<table class="mb30">
					<tr>
						<th class="col03">試技順</th>
						<th class="col03">No</th>
						<th>選手名</th>
						<th class="col10">学校名</th>
						<?php
							foreach ($items as $item) {
								echo('<th class="col02">' . $item . '</th>');
							}
						?>
					</tr>
			<?php
				if ( ! empty($players)) {
					foreach ($players as $player) {
			?>
				<tr>
					<td><?php echo $player->getOrder($referee->getItem()); ?></td>
					<td><?php echo $player->getPlayerNo(); ?></td>
					<td class="col01"><?php echo $player->getPlayerName(); ?></td>
					<td class="col01"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
				<?php
					if ( ! empty($items)) {
						foreach ($items as $item) {
							$url_href   = '/gymnastics/admin/referee/score_edit?player_id=' . $player->getId() . '&item=' . $item;
							if($item == $rfeItem)
							{
								if (isset($player->getItemScores()[$item])) {
									if ($player->getItemScores()[$item]->getStatus() == 1) {
										echo '<td class="center"><span class="status">確認中</span></td>';
									} elseif($player->getItemScores()[$item]->getStatus() == 2) {
										echo '<td class="center">公開</td>';
									} elseif ($player->getFlagCancle()) {
										echo '<td class="center">棄権</td>';
									} else {
										echo '<td><a class="buttonCustom hover" href="' . $url_href . '">未入力</a></td>';
									} // endif getStatus
								} elseif ($player->getFlagCancle()) {
									echo '<td class="center">棄権</td>';
								} else {
									echo '<td><a class="buttonCustom hover" href="' . $url_href . '">未入力</a></td>';
								} // endif getItemScores

							}else {
								if (isset($player->getItemScores()[$item])) {
									if ($player->getItemScores()[$item]->getStatus() == 1) {
										echo '<td class="center"><span class="status">確認中</span></td>';
									} elseif($player->getItemScores()[$item]->getStatus() == 2) {
										echo '<td class="center">公開</td>';
									} elseif ($player->getFlagCancle()) {
										echo '<td class="center">棄権</td>';
									} else {
										echo '<td><span class="status">未入力</span></td>';
									} // endif getStatus
								} elseif ($player->getFlagCancle()) {
									echo '<td class="center">棄権</td>';
								} else {
									echo '<td><span class="status">未入力</a></span></td>';
								} // endif getItemScores
							}
						} // endforeach of items loop
					} // endif items
				?>
				</tr>
			<?php
					} //endforeach of player loop
				} else { ?>
					<tr>
						<td colspan="<?php echo (count($items) + 4); ?>">検索結果がありません。</td>
					</tr>
			<?php } //endif of player loop ?>
				</table>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<script type="text/javascript">
	$(document).ready(function(){

		var game = '<?=$game->getId()?>';
		var group = '<?=$group?>';
		get_heat_by_group(game, group);

		$('#group_search').change(function(){
			var game = '<?=$game->getId()?>';
			var group = $(this).val();
			get_heat_by_group(game, group);
		});
	});

	function get_heat_by_group(game, group)
	{
		var heatSelected = <?=$heat?>

		$.ajax({
			url:"/gymnastics/ajax/getheats",
			type:"POST",
			data:{game: game, group : group},
			dataType:'json',
			success:function(response){

				// remove old heat radio
				$('.groupList  li.heat').remove();

				var html = '';

				$(response.heats).each(function(k, v){
					checked = '';
					if(heatSelected == v) checked='checked="checked"';
					html += '<li class="radio heat"><input id="heat'+v+'" type="radio" name="heat"  value="'+v+'" '+checked+'><label for="heat'+v+'">'+v+'組</label></li>';
				});

				$('.groupList li.select').after(html);
			}
		});
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>

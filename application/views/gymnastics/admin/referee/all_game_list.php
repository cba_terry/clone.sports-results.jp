<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試合一覧',
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min|update_status',
		'pageId' => 'pageAllGameList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?php echo gender($sex), ' ', $class; ?> 全種目</h2>
		</div>
		<!-- /.headBox -->
		<form action="" method="get" class="searchForm" id="form_toolbar">
			<input type="hidden" name="orderby" value="<?php echo $this->input->get('orderby'); ?>">
			<div class="wrapGroupList clearfix">
				<ul class="groupList" id="wrap_heat">
					<li class="select size01">
						<select name="group" id="group_search">
							<option value="">班</option>
						<?php
							if ( ! empty($group_ids)) {
								foreach ($group_ids as $key => $group) {
									$is_selected = ($this->input->get('group') == $group) ? ' selected' : '';
						?>
							<option value="<?=$group;?>"<?=$is_selected;?>><?=$group;?>班</option>
						<?php
								} // endforeach
							} // endif
						?>
						</select>
					</li>
					<li class="radio elm-after">
						<input id="heatall" type="radio" name="heat" value="" checked/>
						<label for="heatall">全組</label>
					</li>
					<?php if ( ! empty($heat_default)) { echo $heat_default; } ?>
					<li><input type="submit" class="buttonCustom hover" value="切替" /></li>
				</ul>
				<div class="boxGroup">
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>検索する</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<input type="hidden" name="prefecture" value="<?php echo $this->input->get('prefecture'); ?>">
			<input type="hidden" name="school_name" value="<?php echo $this->input->get('school_name'); ?>">
			<input type="hidden" name="player_name" value="<?php echo $this->input->get('player_name'); ?>">
		</form>
		<div class="filterBox">
			<p>
				<span><label for="publish01">チェックを入れた試合の</label></span>
				<span class="select size01">
					<select name="all_status">
						<option value="ステータスを変更する">ステータスを変更する</option>
					<?php
						foreach ($score_status as $key => $status) {
							echo "<option value=\"{$key}\">{$status}</option>";
						}
					?>
					</select>
				</span>
				<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
			</p>
			<p>
				<span class="status"><label>表示順</label></span>
				<span class="select size02">
					<select name="orderby" class="elm-orderby">
						<option value="1"<?php if ($this->input->get('orderby') != 2) { echo ' selected'; } ?>>登録順</option>
						<option value="2"<?php if ($this->input->get('orderby') == 2) { echo ' selected'; } ?>>試技順</option>
					</select>
				</span>
			</p>
		</div>
		<div class="tableInfo">
			<table class="tableGame">
				<tr>
					<th class="col01">
						<span class="checkbox">
							<input type="checkbox" id="checkAll" name="checkAll" />
							<label for="checkAll">&nbsp;</label>
						</span>
					</th>
					<th class="col08">試技順</th>
					<th class="col08">No</th>
					<th>選手名</th>
					<th class="col04">学校名</th>
				<?php
					foreach ($items as $item) {
						echo "<th class=\"col06\">{$item}</th>";
					}
				?>
					<th class="col07">合計</th>
					<th class="col08">ステータス</th>
					<th class="col08">得点<br />
編集</th>
				</tr>
			<?php 
				if ( ! empty($groupheat_players)) {
					foreach ($groupheat_players as $gh => $players) {
						$displayed_group = false;
						foreach ($players as $player) {
							$scores             = $player->getItemScores();
							$player_id          = $player->getId();
							$total_scores       = $player->getSingleTotalScores();
							$total_score_id     = ($total_scores->first()) ? $total_scores->first()->getId() : 0;
							$total_score_status = ($total_scores->first()) ? $total_scores->first()->getStatus() : null;
							//Display group row
							if ($player->getFlag() == 0 && ! empty($group_scores[$gh]) && ! $displayed_group) {
			?>
				<tr class="active03">
					<td colspan="5">チームベスト3</td>
					<?php foreach ($items as $item) { ?>
					<td class="point">
						<span>
							<?php echo isset($group_scores[$gh][$item])?formatScore($group_scores[$gh][$item]):'0.000'; ?>
						</span>
					</td>
					<?php } // endforeach $items ?>
					<td class="point">
						<span>
							<?php echo isset($group_scores[$gh]['total'])?formatScore($group_scores[$gh]['total']):'0.000'; ?>
						</span>
					</td>
					<td class="point br0"><span>-0.00</span></td>
					<td class="col03 bl0"><a class="buttonCustom hover" href="#">編集</a></td>
				</tr>
						<?php
								$displayed_group = true;
							} //End of display group row
						?>
				<tr>
					<td class="col01">
						<span class="checkbox">
							<input type="checkbox" id="check01" name="check" value="<?php echo $total_score_id; ?>"/>
							<label for="check01">&nbsp;</label>
						</span>
					</td>
					<td><?php echo $player->getOrder(); ?></td>
					<td><?php echo $player->getPlayerNo(); ?></td>
					<td><?php echo $player->getPlayerName(); ?></td>
					<td><?php if ($player->getSchool() != null) { echo $player->getSchool()->getSchoolNameAb(); } ?></td>
					<?php
						foreach ($items as $item) {
							$score_edit     = ($item == '跳馬') ? 'score_edit02' : 'score_edit01';
							$url_score_edit = "/gymnastics/admin/referee/{$score_edit}/?player_id={$player_id}";
							if (isset($scores[$item])) {
					?>
					<td class="point">
						<span><?php echo formatScore($scores[$item]->getFinalScore()); ?></span>
						<em>D <?php echo formatScore($scores[$item]->getDScore()); ?><br />
E <?php echo formatScore($scores[$item]->getEScore()); ?><br />
減 -<?php echo formatScore($scores[$item]->getDemeritScore()); ?></em>
					</td>
					<?php } else { ?>
					<td class="point">
						<span>00.000</span>
						<em>D 00.000<br />E 00.000<br />減 -0.000</em>
					</td>
					<?php
							} // endif $scores
						} // endforeach $items
					?>
					<td class="point">
						<span><?php echo formatScore($player->getTotalFinalScore()); ?></span>
						<em>D <?php echo formatScore($player->getTotalDScore()); ?><br />
E <?php echo formatScore($player->getTotalEScore()); ?><br />
減 -<?php echo formatScore($player->getTotalDemeritScore()); ?></em>
					</td>
					<td class="col03 buttonGroup">
						<span class="radio">
							<input id="unpublic<?=$total_score_id;?>" type="radio" value="0" name="status[<?=$total_score_id;?>]" <?php if ($total_score_status == 0) echo 'checked'; ?> />
							<label for="unpublic<?=$total_score_id;?>">非公開</label>
						</span>
						<span class="radio">
							<input id="public<?=$total_score_id;?>" type="radio" value="2" name="status[<?php echo $total_score_id; ?>]" <?php if ($total_score_status == 2) echo 'checked'; ?>/>
							<label for="public<?=$total_score_id;?>">公開</label>
						</span>
						<a href="javascript:void(0);" class="buttonCustom hover update_status" data-id="<?php echo $total_score_id; ?>">
							更新
						</a>
					</td>
					<td class="col03"><a class="buttonCustom hover" href="<?php echo $url_score_edit; ?>">編集</a></td>
				</tr>
				<?php
						} // endforeach $players
					} // endforeach $groupheat_players
				?>
			<?php } else { ?>
				<tr>
					<td colspan="<?php if ($sex==1) echo 15; else echo 13; ?>">検索結果がありません。</td>
				</tr>
			<?php } // endif $groupheat_players ?>
			</table>
		</div>
		<div class="filterBox">
			<p>
				<span><label for="publish01">チェックを入れた試合の</label></span>
				<span class="select size01">
					<select name="all_status">
						<option value="ステータスを変更する">ステータスを変更する</option>
					<?php
						foreach ($score_status as $key => $status) {
							echo "<option value=\"{$key}\">{$status}</option>";
						}
					?>
					</select>
				</span>
				<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
			</p>
			<p>
				<span class="status"><label>表示順</label></span>
				<span class="select size02">
					<select name="orderby" class="elm-orderby">
						<option value="1"<?php if ($this->input->get('orderby') != 2) { echo ' selected'; } ?>>登録順</option>
						<option value="2"<?php if ($this->input->get('orderby') == 2) { echo ' selected'; } ?>>試技順</option>
					</select>
				</span>
			</p>
		</div>
		<p class="buttonBack"><a href="/gymnastics/admin/referee/game_class" class="buttonStyle hover">戻る</a></p>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="javascript:void(0);"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="" method="get">
				<input type="hidden" name="orderby" value="<?php echo $this->input->get('orderby'); ?>">
				<input type="hidden" name="group" value="<?php echo $this->input->get('group'); ?>">
				<input type="hidden" name="heat" value="<?php echo $this->input->get('heat'); ?>">
				<div class="sheet">
					<table>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="prefecture" class="labelCss">都道府県</label>
										<p class="select size02">
											<select name="prefecture" id="prefecture">
												<option value="">選択してください</option>
										<?php
											if ( ! empty($prefecture_default)) {
												foreach ($prefecture_default as $school_id => $str_prefecture) {
													$is_selected = ($this->input->get('prefecture') == $school_id) ? ' selected' : '';
													echo "<option value=\"{$school_id}\"{$is_selected}>{$str_prefecture}</option>";
												}
											}
										?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="school_name" class="labelCss">学校名</label>
										<p class="select size03">
											<select name="school_name" id="school_name">
												<option value="">選択してください</option>
										<?php
											if ( ! empty($school_default)) {
												foreach ($school_default as $school_id => $str_school) {
													$is_selected = ($this->input->get('school_name') == $school_id) ? ' selected' : '';
													echo "<option value=\"{$school_id}\"{$is_selected}>{$str_school}</option>";
												}
											}
										?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="player_name" class="labelCss">選手名</label>
										<p class="select size03">
											<select name="player_name" id="player_name">
												<option value="">選択してください</option>
										<?php
											if ( ! empty($player_default)) {
												foreach ($player_default as $str_player) {
													$is_selected = ($this->input->get('player_name') == $str_player) ? ' selected' : '';
													echo "<option value=\"{$str_player}\"{$is_selected}>{$str_player}</option>";
												}
											}
										?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="button btn-reset" value="リセット" class="hover" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	$(document).ready(function(){
		$('#group_search').change(function(){
			var group_id = $(this).val();
			get_heat_by_group(group_id);
		});
	});
	function get_heat_by_group(group_id){
		$.ajax({
			url:'/gymnastics/admin/referee/all_game_list/ajax_num_heat',
			type:'POST',
			data:{
				group_id: group_id,
				class: '<?php echo $class; ?>',
				sex: '<?php echo $sex; ?>'
			},
			dataType:'html',
			success:function(response){
				$('#wrap_heat li.radio').not('.elm-after').remove();
				$('#wrap_heat li.elm-after').find('input').prop('checked', true);
				$('#wrap_heat li.elm-after').after(response);
			}
		});
	}
	function update_status(score_id, status_update) {
		$.ajax({
			url:'/gymnastics/admin/referee/all_game_list/updateStatus',
			type:'POST',
			data:{
				score_id: score_id,
				status_update: status_update,
				class: '<?php echo $class; ?>',
				sex: '<?php echo $sex; ?>'
			},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					// Refresh page by window.location.href
					// because when location.reload(true/false), style layout displayn't right
					$(location).attr('href', window.location.href);
				}
			}
		});
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>

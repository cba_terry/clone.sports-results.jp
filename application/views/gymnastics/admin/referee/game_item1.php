<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '種目別得点一覧',
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min|update_status',
		'pageId' => 'pageEventGameList'
	));
	$group = ($this->input->get('group')) ? $this->input->get('group') : '1';
	$heat = ($this->input->get('heat')) ? $this->input->get('heat') : '1';
?>
		<!-- /#header -->
		<div id="contents" class="clearfix">
			<div id="main">
				<form action="" method="get" class="searchForm">
					<input type="hidden" name="gid" value="<?=$game->getId()?>"></input>
					<input type="hidden" name="item" value="<?=$item?>"></input>
					<div class="headBox clearfix">
						<h2 class="headline1"><?php echo $game->getStrSex(), ' ', $game->getClass(), ' ', $item; ?></h2>
						<?php if($referee->isChiefReferee()){?>
						<ul class="btnLinkList clearfix">
							<li><a href="/gymnastics/admin/referee/game/all?gid=<?=$game->getId()?>">全種目一覧</a></li>
							<?php foreach ($game->getItems() as $i) {?>
							<li><a 
							<?php if($i->getName() == $this->input->get('item')) echo 'class="active"';?>
							<?php if($i->getName()=='段違い平行棒') {echo 'style="width:180px"';}?> href="/gymnastics/admin/referee/game/item?gid=<?=$game->getId()?>&item=<?=$i->getName()?>"><?=$i?>一覧</a></li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
					<!-- /.headBox -->
					<div class="wrapGroupList clearfix">
						<ul class="groupList">
							<li class="select size01">
								<select name="group" id="group_search">
									<?php foreach ($grus as $key => $g) { ?>
									<option <?php if($g['group'] == $group){ ?>selected="selected"<?php } ?> value="<?=$g['group']?>"><?=$g['group']?>班</option>
									<?php } ?>
								</select>
							</li>
							<li><input type="submit" class="buttonCustom hover" value="切替" style="opacity: 1;"></li>
						</ul>
						<div class="boxGroup">
							<div class="leadBox">
								<ul class="clearfix">
									<li><a class="buttonOrder" href="/gymnastics/admin/referee/game/order_setting?item=<?=$item?>&group=<?=$group?>&heat=<?=$heat?>"><span>試技順を設定する</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</form>
				<form action="" method="post">
					<div class="tableInfo">
						<table class="tableEvent">
							<tr>
								<th class="col01">試技順</th>
								<th class="col02">No</th>
								<th class="col03">選手名</th>
								<th class="col04">学校名</th>
								<?php for($i=1; $i<=$nrfd; $i++) { ?>
								<th class="col05">D<?=$i?></th>
								<?php } ?>
								<?php for($i=1; $i<=$nrfe; $i++) { ?>
								<th class="col05">E<?=$i?></th>
								<?php } ?>
								<th class="col05">E得点</th>
								<th class="col05">E減点</th>
								<th class="col05">E得点<br>決定点</th>
								<th class="col05">D+E</th>
								<th class="col05">減点<span>タイムライン</span></th>
								<th class="col06">合計</th>
								<th class="col06">チーム<br>加算</th>
								<th class="col07">ステータス</th>
								<th class="col08">得点<br>編集</th>
							</tr>
							<?php if(!empty($groupHeatPlayers)): 
							foreach ($groupHeatPlayers as $pGroup){
							foreach ($pGroup->getHeats() as $pHeat){
							// tach players trong head thanh 2 loai, choi cho group va choi cho head
							$playsAsGroup  = $pHeat->getPlayersPlayAsGroup(true, $item);
							$playsAsSingle = $pHeat->getPlayersPlayAsSingle(true, $item);

							foreach ($playsAsGroup as $player){ ?>
							<tr>
								<td><?=$player->getOrder($item)?></td>
								<td><?=$player->getPlayerNo()?></td>
								<td class="col01"><?=$player->getPlayerName()?></td>
								<td class="col01"><?=$player->getSchoolNameAb()?></td>
								<?php for($i=1; $i<=$nrfd; $i++) {?>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item, 'D'.$i))?></span><?=reInput($player->getScoreId($item, 'D'.$i), $player->getScoreStatus($item, 'D'.$i), 'D'.$i, $referee)?></td>
								<?php } ?>
								<?php for($i=1; $i<=$nrfe; $i++) {?>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item, 'E'.$i))?></span><?=reInput($player->getScoreId($item, 'E'.$i), $player->getScoreStatus($item, 'E'.$i), 'E'.$i, $referee)?></td>
								<?php } ?>
								<td><?=formatScore($player->getEScore($item)+$player->getScoreValue($item, 'EDemerit'))?></td>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item,'EDemerit'))?></span></td>
								<td><span><?=formatScore($player->getEScore($item))?></span></td>
								<td><?=formatScore($player->getEScore($item)+$player->getDScore($item))?>
								<td>-<?=formatScore($player->getScoreValue($item, 'TimeDemerit')+$player->getScoreValue($item, 'LineDemerit')+$player->getScoreValue($item, 'OtherDemerit'))?>
								<?php if($referee->isMainReferee() && $player->timeAndLineAllowReInput($item)) {?>
								<a class="againButton againTimeLine" hover="" data-player="<?=$player->getId()?>" data-round="1">再</a>
								<?php }else if($referee->isMainReferee()) {?>
								<a class="againButton againGray" hover="">再</a>
								<?php } ?>
								</td>
								<td class="point"><span><?=formatScore($player->getFinalScore($item))?></td>
								<td><?php if($pHeat->isOneOfThreeBest($player, $item)) { ?><img src="<?=base_url('/img/admin/icon_dot.png')?>" alt=""><?php } ?></td>
								<td class="col03 buttonGroup">
									<p class="statusList">
										<span class="radio">
											<input type="radio" id="not<?=$player->getId()?>" class="not" name="status[<?=$player->getId()?>]" value="0" <?php if($player->isItemScoreEntry($item)) echo 'checked'; ?> />
											<label for="not<?=$player->getId()?>">未</label>
										</span>
										<span class="radio">
											<input type="radio" id="input<?=$player->getId()?>" class="" name="status[<?=$player->getId()?>]" value="1" <?php if($player->isItemScoreModify($item)) echo 'checked'; ?> />
											<label for="input<?=$player->getId()?>">入力</label>
										</span>
										<span class="radio">
											<input type="radio" id="public<?=$player->getId()?>" class="public" name="status[<?=$player->getId()?>]" value="2" <?php if($player->isItemScorePublish($item)) echo 'checked'; ?> />
											<label for="public<?=$player->getId()?>">公開</label>
										</span>
									</p>
									<a class="buttonCustom hover update_status" href="javascript:void(0)" data-id="<?=$player->getId()?>">更新</a>
								</td>
								<td class="col03"><a class="buttonCustom hover" href="/gymnastics/admin/referee/score_edit?player_id=<?=$player->getId()?>&item=<?=$item?>">編集</a></td>
							</tr>
							<?php }
							//Display group row
							if ($pHeat->hasGroupSchoolPlay()){ ?>
							<tr class="active03">
								<td colspan="4">チームベスト3</td>
								<td class="point" colspan="<?=$nrfd+$nrfe;?>"><span><?=formatScore($pHeat->getSchoolScoreItem($item),2)?></span></td>
								<td colspan="2">チーム減点</td>
								<td class="point point01" colspan="3"><span>-<?=formatScore($pHeat->getSchoolItemDemeritScore($item),2)?></span><a class="buttonCustom hover" href="/gymnastics/admin/referee/team_demerit_edit?school=<?=$pHeat->findSchoolGroup()->getId()?>&item=<?=$item?>&group=<?=$pGroup->getId()?>&heat=<?=$pHeat->getId()?>">編集</a></td>
								<td colspan="2">チーム合計</td>
								<td class="point" colspan="2"><span><?=formatScore($pHeat->getSchoolScoreItem($item)-$pHeat->getSchoolItemDemeritScore($item),2)?></span></td>
							</tr>
							<?php } // end loop play as group
							foreach ($playsAsSingle as $player){ ?>
							<tr>
								<td><?=$player->getOrder($item)?></td>
								<td><?=$player->getPlayerNo()?></td>
								<td class="col01"><?=$player->getPlayerName()?></td>
								<td class="col01"><?=$player->getSchoolNameAb()?></td>
								<?php for($i=1; $i<=$nrfd; $i++) {?>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item, 'D'.$i))?></span><?=reInput($player->getScoreId($item, 'D'.$i), $player->getScoreStatus($item, 'D'.$i), 'D'.$i, $referee)?></td>
								<?php } ?>
								<?php for($i=1; $i<=$nrfe; $i++) {?>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item, 'E'.$i))?></span><?=reInput($player->getScoreId($item, 'E'.$i), $player->getScoreStatus($item, 'E'.$i), 'E'.$i, $referee)?></td>
								<?php } ?>
								<td><?=formatScore($player->getEScore($item)+$player->getScoreValue($item, 'EDemerit'))?></td>
								<td class="col05"><span><?=formatScore($player->getScoreValue($item,'EDemerit'))?></span></td>
								<td><span><?=formatScore($player->getEScore($item))?></span></td>
								<td><?=formatScore($player->getEScore($item)+$player->getDScore($item))?>
								<td>-<?=formatScore($player->getScoreValue($item, 'TimeDemerit')+$player->getScoreValue($item, 'LineDemerit')+$player->getScoreValue($item, 'OtherDemerit'))?>
								<?php if($referee->isMainReferee() && $player->timeAndLineAllowReInput($item)) {?>
								<a class="againButton againTimeLine" hover="" data-player="<?=$player->getId()?>" data-round="1">再</a>
								<?php }else if($referee->isMainReferee()) {?>
								<a class="againButton againGray" hover="">再</a>
								<?php } ?>
								</td>
								<td class="point"><span><?=formatScore($player->getFinalScore($item))?></td>
								<td></td>
								<td class="col03 buttonGroup">
									<p class="statusList">
										<span class="radio">
											<input type="radio" id="not<?=$player->getId()?>" class="not" name="status[<?=$player->getId()?>]" value="0" <?php if($player->isItemScoreEntry($item)) echo 'checked'; ?> />
											<label for="not<?=$player->getId()?>">未</label>
										</span>
										<span class="radio">
											<input type="radio" id="input<?=$player->getId()?>" class="" name="status[<?=$player->getId()?>]" value="1" <?php if($player->isItemScoreModify($item)) echo 'checked'; ?> />
											<label for="input<?=$player->getId()?>">入力</label>
										</span>
										<span class="radio">
											<input type="radio" id="public<?=$player->getId()?>" class="public" name="status[<?=$player->getId()?>]" value="2" <?php if($player->isItemScorePublish($item)) echo 'checked'; ?> />
											<label for="public<?=$player->getId()?>">公開</label>
										</span>
									</p>
									<a class="buttonCustom hover update_status" href="javascript:void(0)" data-id="<?=$player->getId()?>">更新</a>
								</td>
								<td class="col03"><a class="buttonCustom hover" href="/gymnastics/admin/referee/score_edit?player_id=<?=$player->getId()?>&item=<?=$item?>">編集</a></td>
							</tr>
							<?php }
							// end loop play as single
							}} ?>
						<?php else: ?>
						<tr><td colspan="<?php if ($game->getSex()==1) echo 15; else echo 13; ?>">検索結果がありません。</td></tr>
						<?php endif; ?>
						</table>
					</div>
					<?php if($referee->isChiefReferee()) { ?>
					<p class="buttonBack"><a href="/gymnastics/admin/referee/game" class="buttonStyle hover">戻る</a></p>
					<?php } ?>
				</form>
			</div>
			<!-- /#main -->
		</div>
		<!-- /#contents -->
<script>
	function update_status(ids, status)
	{
		$.ajax({
			url:"/gymnastics/admin/referee/game/updateStatus?gid=<?=$game->getId()?>&item=<?=$item?>",
			type:"POST",
			data:{ids: ids, status: status},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					location.reload();
				}
			}
		});
	}

	$(document).ready(function(){

		var game = '<?=$game->getId()?>';
		var group = '<?=$group?>';
		get_heat_by_group(game, group);

		$('#group_search').change(function(){
			var game = '<?=$game->getId()?>';
			var group = $(this).val();
			get_heat_by_group(game, group);
		});
	});

	function get_heat_by_group(game, group)
	{
		var heatSelected = <?=$heat?>

		$.ajax({
			url:"/gymnastics/ajax/getheats",
			type:"POST",
			data:{game: game, group : group},
			dataType:'json',
			success:function(response){

				// remove old heat radio
				$('.groupList  li.heat').remove();

				var html = '';

				$(response.heats).each(function(k, v){
					checked = '';
					if(heatSelected == v) checked='checked="checked"';
					html += '<li class="radio heat"><input id="heat'+v+'" type="radio" name="heat"  value="'+v+'" '+checked+'><label for="heat'+v+'">'+v+'組</label></li>';
				});

				$('.groupList .select').after(html);
			}
		});
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>

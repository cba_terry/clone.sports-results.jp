<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '全審判得点確認',
		'css'    => '',
		'js'     => 'score01',
		'pageId' => 'pageScoreEdit'
	));
?>
<!-- /#header -->
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox headBox01 clearfix">
			<?php if($referee->getRefereeType() == 0) {?>
				<h2 class="headline2">
				<?php echo gender($game->getSex()).' '.$game->getClass(); ?>
				<?php echo $player->getGroup().'班'.$player->getHeat().'組 '.$player->getPlayerNo().' '.$player->getPlayerName().'('.$player->getSchoolNameAb().')'; ?>:
				<?php echo $item->getName(); ?>
				</h2>
				<?php }else{ ?>
				<h2 class="headline1">
				<?php echo gender($game->getSex()).' '.$game->getClass(); ?><br />
				<?php echo $player->getGroup().'班'.$player->getHeat().'組 '.$player->getPlayerNo().' '.$player->getPlayerName().'('.$player->getSchoolNameAb().')'; ?>:<br>
				<?php echo $item->getName(); ?>
				</h2>
			<?php } ?>
			<form action="" method="get">
				<input type="hidden" name="player_id" value="<?=$this->input->get('player_id')?>">
				<input type="hidden" name="ref" value="<?=$this->input->get('ref')?>">
				<?php if($referee->getRefereeType() == 2) { ?>
				<ul class="groupList">
					<?php foreach ($game->getItems() as $key => $oitem): ?>
					<li class="radio"><input id="event0<?=$key?>" type="radio" name="item" <?php if($item->getName() == $oitem->getName()){ ?>checked="checked"<?php } ?> value="<?=$oitem->getName()?>"><label for="event0<?=$key?>"><?=$oitem->getName()?></label></li>	
					<?php endforeach ?>
					<li><input type="submit" class="buttonCustom hover" value="切替" /></li>
				</ul>
				<?php } ?>
			</form>
		</div>
		<?php 
		if($game->isInputPaper()) 
			$this->load->view("includes/share/form_score_edit_01");
		else 
			$this->load->view("includes/share/form_score_edit_11"); 
		?>
	</div>
	<!-- /#main --> 
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>

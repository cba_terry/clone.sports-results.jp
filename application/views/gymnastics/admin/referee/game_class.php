<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試合区分一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGameClass'
	));
	$uri = '/gymnastics/admin/referee/game/';
	$items = $game->getItems();
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="tableInfo tableClass">
			<table>
				<tr class="headTitle">
					<th colspan="8"><?=$game->getStrSex()?>試合区分一覧</th>
				</tr>
				<tr>
					<td><?=$game->getClass();?></td>
					<?php foreach ($items as $item) { $url = $uri . 'item?gid=' . $game->getId() . '&item=' . $item->getName();?>
						<td class="col01" <?php if($item->getName()=='段違い平行棒') {echo 'style="width:180px"';}?>>
							<a class="buttonCustom hover" href="<?=$url?>"><?=$item->getName()?>一覧</a>
						</td>
					<?php } ?>
					<td class="col02"><a class="buttonCustom hover" href="<?=$uri?>all?gid=<?=$game->getId()?>">全種目一覧</a></td>
				</tr>
			</table>
		</div>
		<p class="headTitle mb20">モニター一覧</p>
		<ul class="infoList clearfix">
			<li class="fullWidth"><a href="/gymnastics/monitor/scores?gid=<?=$game->getId()?>" target="_blank">審判長用班別得点一覧</a></li>
			<li class="fullWidth"><a href="/gymnastics/monitor/ranking/single/<?=$game->getId()?>" target="_blank">順位</a></li>
			<li class="fullWidth"><a href="/gymnastics/monitor/rotation/<?=$game->getId()?>" target="_blank">ローテーション</a></li>
		<?php
			if ($items_default) {
				$class_other = ( ! empty($game) && $game->getSex()) ? 'otherWidth' : 'otherWidth2';
				foreach ($items_default as $item) {
					$rotateItem = $rotations->filterItem($item)->first();
					$href   = get_value($urls, "%s", 'result');
					$href   = ($rotateItem) ? $href . $rotateItem->getHeat() : null;
					$target = ($href != '#') ? ' target="_blank"' : '';
		?>
			<?php if($rotateItem) { ?>
			<li class="<?=$class_other?>"><a href="<?=$href?>"<?=$target?>>得点（<?=$item?>）</a></li>
			<?php }else { ?>
				<li class="<?=$class_other?>"><a>得点（<?=$item?>）</a></li>
			<?php } ?>
		<?php
				} // endforeach
			} // endif
		?>
	</div>
	<!-- /#main --> 
</div>
<?php $this->load->view("includes/admin/footer"); ?>

<?php
	$this->load->view("includes/admin/header", array(
		'title'  => $title,
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min',
		'pageId' => 'pageEventGameList'
	));
?>
		<!-- /#header -->
		<div id="contents" class="clearfix">
			<div id="main">
				<form action="#" method="post" class="searchForm">
					<div class="headBox clearfix">
						<h2 class="headline1"><?php echo gender($game['sex']), ' ', $game['class'], ' ', $game['item']; ?></h2>
						<div class="boxGroup">
							<div class="leadBox">
								<ul class="clearfix">
									<li><a class="buttonOrder" href="/gymnastics/admin/referee/order_setting"><span>試技順を設定する</span></a></li>
									<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>選手を絞り込む</span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /.headBox -->
					<div class="filterBox">
						<p>
							<span><label for="publish01">チェックを入れた試合の</label></span>
							<span class="select size01">
								<select name="publish01" id="publish01">
									<option value="ステータスを変更する">ステータスを変更する</option>
									<?php foreach ($score_status02 as $key => $status) {?>
									<option value="<?=$key?>"><?=$status?></option>
									<?php } ?>
								</select>
							</span>
							<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
						</p>
					</div>
					<div class="tableInfo">
						<table class="tableGame tableEvent">
							<tr>
								<th class="center">
									<span class="checkbox">
										<input type="checkbox" id="checkAll" name="checkAll" />
										<label for="checkAll" class="empty">&nbsp;</label>
									</span>
								</th>
								<th class="col03">班</th>
								<th class="col03">組</th>
								<th>選手名/学校名</th>
								<th>試技順</th>
								<th>D1</th>
								<th>D2</th>
								<th>D3</th>
								<th>D4</th>
								<th>D5</th>
								<th>E1</th>
								<th>E2</th>
								<th>E3</th>
								<th>E4</th>
								<th>E5</th>
								<th>E減点</th>
								<th>減点</th>
								<th>合計</th>
								<th class="col06">ステータス</th>
								<th class="col01">編集</th>
							</tr>
							<?php if(!empty($player_scores)): foreach ($player_scores as $key => $player_score): ?>
							<tr class="blue">
								<td rowspan="2" class="col01"><span class="checkbox"><input type="checkbox" id="check01" name="check" value="<?php echo isset($player_score['scores'][0])?$player_score['scores'][0]->getId():0; ?>_<?php echo isset($player_score['scores'][1])?$player_score['scores'][1]->getId():0; ?>"/><label for="check01">&nbsp;</label></span></td>
								<td rowspan="2"><?php echo $player_score['player']->getGroup(); ?></td>
								<td rowspan="2"><?php echo $player_score['player']->getHeat(); ?></td>
								<td rowspan="2" class="col04"><?php echo $player_score['player']->getPlayerNo(); ?><br /><?php echo $player_score['player']->getPlayerName(); ?><br /><?php echo $player_score['player']->getSchoolNameAb(); ?></td>
								<td rowspan="2"><span><?php echo $player_score['player']->getOrder(); ?></span></td>
								<?php if (count($player_score['scores']) > 1): ?>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getD1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getD2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getD3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getD4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getD5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getE1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getE2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getE3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getE4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getE5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][0]->getEDemeritScore(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col02">-<?php echo formatScore($player_score['scores'][0]->getDemeritScore()); ?></td>
								<td rowspan="2"><?php if($player_score['scores'][0]->getFinalScore() > $player_score['scores'][1]->getFinalScore()) {echo formatScore($player_score['scores'][0]->getFinalScore());} else { echo formatScore($player_score['scores'][1]->getFinalScore());}  ?></td>
								<td rowspan="2" class="col03">
									<p class="statusList">
										<?=showStatus($player_score['scores'][0]->getId(), $player_score['scores'][1]->getId(), $player_score['scores'][0]->getStatus(), $player_score['scores'][1]->getStatus())?>
									</p>
									<a class="buttonCustom hover update_status" href="#" data-id="<?php echo $player_score['scores'][0]->getId(); ?>_<?php echo $player_score['scores'][1]->getId(); ?>">更新</a>
								</td>
								<td rowspan="2" class="col03"><a class="buttonCustom hover " href="/gymnastics/admin/referee/score_edit02?player_id=<?php echo $player_score['player']->getId(); ?>">スコア<br />編集</a></td>
							</tr>
							<tr class="blue">
								<td class="col02"><span><?php echo $player_score['scores'][1]->getD1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getD2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getD3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getD4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getD5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getE1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getE2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getE3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getE4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getE5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02"><span><?php echo $player_score['scores'][1]->getEDemeritScore(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][1]->getStatus())?>" data-id="<?php echo $player_score['scores'][1]->getId(); ?>">再</a></td>
								<td class="col02">-<?php echo formatScore($player_score['scores'][1]->getDemeritScore()); ?></td>
							</tr>
							<?php else: ?>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td rowspan="2">-</td>
								<td rowspan="2"></td>
								<td rowspan="2" class="col03">
									<p class="statusList">
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>未</label>
										</span>
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>1回目</label>
										</span>
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>2回目</label>
										</span>
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>公開</label>
										</span>
									</p>
									<a class="buttonCustom hover update_status" href="javascript:void(0)" >更新</a>
								</td>
								<td rowspan="2" class="col03">
									<a class="buttonCustom hover" href="/gymnastics/admin/referee/score_edit02?player_id=<?php echo $player_score['player']->getId(); ?>">スコア<br />編集</a>
								</td>
							</tr>
							<tr class="blue">
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col02"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
							</tr>
							<?php endif;//end if count scores > 1 ?>
							<?php endforeach; else: ?>
							<tr><td colspan="19">検索結果がありません。</td></tr>
							<?php endif; ?>
						</table>
					</div>
					<p class="more"><img src="<?=base_url('/img/admin/icon_more.png')?>" alt="..." /></p>
					<div class="filterBox">
						<p>
							<span><label for="publish02">チェックを入れた試合の</label></span>
							<span class="select size01">
								<select name="publish02" id="publish02">
									<option value="ステータスを変更する">ステータスを変更する</option>
									<?php foreach ($score_status02 as $key => $status) {?>
									<option value="<?=$key?>"><?=$status?></option>
									<?php } ?>
								</select>
							</span>
							<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
						</p>

					</div>
					<p class="buttonBack"><a href="javascript:history.back()" class="buttonStyle hover">戻る</a></p>
				</form>
			</div>
			<!-- /#main -->
		</div>
		<!-- /#contents -->
		
		<!-- /#footer -->
		<div class="modal">
			<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
			<div class="modalContent">
				<h3 class="hModal">絞り込み検索</h3>
				<div class="searchBox">
					<form action="#" method="get">
						<div class="sheet">
							<table>
								<tr><th>&nbsp;</th></tr>
								<tr>
									<td>
										<ul class="selectList clearfix">
											<li class="mr20">
												<label for="team01" class="labelCss">班</label>
												<p class="select size04">
													<select name="group" id="team01">
														<option value="">班</option>
														<?php for ($i=1; $i < 11 ; $i++) { ?>
														<option value="<?=$i?>"<?php if($this->input->get('group') == $i) echo ' selected="selected"'; ?>><?=$i?>班</option>
														<?php } ?>
													</select>
												</p>
											</li>
											<li class="mr20">
												<label for="team02" class="labelCss">組</label>
												<p class="select size04">
													<select name="heat" id="team02">
														<option value="">班</option>
														<?php for ($i=1; $i < 9 ; $i++) { ?>
														<option value="<?=$i?>"<?php if($this->input->get('heat') == $i) echo ' selected="selected"'; ?>><?=$i?>班</option>
														<?php } ?>
													</select>
												</p>
											</li>
											<li>
												<label for="status" class="labelCss">ステータス</label>
												<p class="select size02">
													<select name="status" id="status">
														<option value="">ステータス</option>
														<?php foreach ($score_status as $key => $status) {?>
														<option value="<?=$key?>"<?php if($this->input->get('status') == $key) echo ' selected="selected"'; ?>><?=$status?></option>
														<?php } ?>
													</select>
												</p>
											</li>
										</ul>
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix">
							<p class="btnCancel"><input type="button" value="リセット" name="reset" class="hover" /></p>
							<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.modal -->
<script>
	$(document).ready(function() {
		$('.btn_deny').click(function (){
		   var score_id  = $(this).attr('data-id');
		   update_status(score_id, '0', 1);
		});
		$('.update_status').click(function (){
		   var score_id      = $(this).attr('data-id');
		   var name_radio    = "status_"+score_id;
		   var status_update = $('input[name="'+name_radio+'"]:checked').val();
		   update_status(score_id, status_update, 1);
		});
		$('.btn_update_all').click(function (){
		   var status = $(this).parents('p').find('select').val();
		   if(status == 'ステータスを変更する') {
		       alert("チェック入れていた試合のステータスを選択してください。");
		       return false;
		   }
		   var list_id = "";
		   $("input[name='check']").each(function(){
		       if (this.checked) list_id = list_id+","+this.value;
		   })
		   list_id = list_id.substr(1);     
		   if (list_id == "") {
		       alert("ステータスを変更したい試合にチェックを入れてください。");
		       return false;
		   }
		   if (confirm("全て試合のステータスを更新してもよろしいですか？")) {
		       update_status(list_id, status, 'all');
		   }
		});
	});

	function update_status(score_id, status_update, type_update)
	{
		if (score_id.match(/(^|,)(0_0|1_0|0_1)(,|$)/)){
			location.reload(0);
			return;
		}
		$.ajax({
			url:"/gymnastics/admin/referee/event_game_list/updateStatus02",
			type:"POST",
			data:{score_id: score_id,status_update: status_update, type: type_update},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					location.reload();
				}
			}
		});
	}
	
</script>
<?php $this->load->view("includes/admin/footer"); ?>

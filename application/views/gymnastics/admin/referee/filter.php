			<table class="tableGame">
				<tr>
					<th class="col01"><span class="checkbox"><input type="checkbox" id="checkAll" name="checkAll" /><label for="checkAll">&nbsp;</label></span></th>
					<th class="col02">試技順</th>
					<th class="col03">No</th>
					<th class="col04">選手名</th>
					<th class="col05">学校名</th>
					<?php foreach ($items as $item) {
					echo '<th class="col06">'.$item.'</th>';
					} ?>
					<th class="col07">合計</th>
					<th class="col08">ステータス</th>
					<th width="60px">得点<br>編集</th>
				</tr>
				<?php if(!empty($players)):

				foreach ($players as $player){ ?>
				<tr>
					<td class="col01">
						<span class="checkbox">
							<input type="checkbox" id="check01" name="check" value="<?=$player->getId()?>"/>
							<label for="check01">&nbsp;</label>
						</span>
					</td>
					<td></td>
					<td><?=$player->getPlayerNo()?></td>
					<td class="col02"><?=$player->getPlayerName()?></td>
					<td class="col02"><?=$player->getSchoolNameAb()?></td>
					<?php foreach ($items as $item):?>
					<td class="point"><span><?=formatScore($player->getItemScoreValue($item, 'FinalScore'))?></span>
						<em>D <?=formatScore($player->getItemScoreValue($item, 'DScore'))?><br />
						E <?=formatScore($player->getItemScoreValue($item, 'EScore'))?><br />
						減 -<?=formatScore($player->getItemScoreValue($item, 'DemeritScore'))?></em>
					</td>
					<?php endforeach ?>
					<td class="point"><span><?=formatScore($player->getTotalFinalScore())?></span><em>D <?=formatScore($player->getTotalDScore())?><br />E <?=formatScore($player->getTotalEScore())?>
						<br />減 -<?=formatScore($player->getTotalDemeritScore())?></em></td>
					<td class="col03 buttonGroup">
						<span class="radio">
							<input type="radio" id="not<?php echo $player->getId(); ?>" class="not" value="0" name="status[<?=$player->getId()?>]" <?php if ($player->isTotalScoreUnPublished()) echo 'checked'; ?> />
							<label for="not<?php echo $player->getId(); ?>">非公開</label>
						</span>
						<span class="radio">
							<input type="radio" id="public<?php echo $player->getId(); ?>" class="public" value="2" name="status[<?=$player->getId()?>]" <?php if ($player->isTotalScorePublished()) echo 'checked'; ?> />
							<label for="public<?php echo $player->getId(); ?>">公開</label>
						</span>
						<a class="buttonCustom hover update_status" data-id="<?=$player->getId()?>" href="javascript:void(0)">更新</a>
					</td>
					<td class="col03">
						<a class="buttonCustom hover" href="/gymnastics/admin/referee/score_edit?player_id=<?=$player->getId()?>">編集</a>
					</td>
				</tr>
				<?php } ?>
			<?php else: ?>
			<tr><td colspan="<?php if ($game->isMale()) echo 15; else echo 13; ?>">検索結果がありません。</td></tr>
			<?php endif; ?>
			</table>
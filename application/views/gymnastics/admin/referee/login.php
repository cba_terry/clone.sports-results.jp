<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'ログイン',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageLogin'
	));
?>
<div id="contents" class="clearfix">
	<div class="loginBlock">
		<form action="" id="loginForm" class="transform loginForm" method="post">
			<h1 class="headline1"><img src="<?=base_url('/img/admin/logo.gif')?>" alt="logo" /></h1>
			<?php
				$valids = validation_errors();
                if ( ! empty($valids) || isset($error)) {
					echo '<div class="mt10">';
					echo validation_errors(); 
					echo @$error;
					echo '</div>';
				}
			?>
			<div class="boxLogin refereeLogin clearfix">
				<p class="textBox">
					<span id="username" class="select">
						<select name="username">
					<?php
						if ( ! empty($referees)) {
							foreach ($referees as $key => $referee) {
					?>
							<option value="<?php echo $referee['referee_name']; ?>"><?php echo $referee['referee_name']; ?></option>
					<?php
							} // endforeach
						} // endif
					?>
						</select>
					</span>
					<input type="password" placeholder="Password" id="password" name="password" />
				</p>
				<input type="submit" id="button" class="buttonCustom hover" value="Login" />
				<p class="remember">
					<input type="checkbox" id="checkbox" name="remember-me" value="1" />
					<label for="checkbox">パスワードを記憶する</label>
				</p>
				<p class="forget"><a href="#">パスワードを忘れた</a></p>
			</div>
		</form>
	</div>
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
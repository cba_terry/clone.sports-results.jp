<?php
	$this->load->view("includes/admin/header", array(
		'title'  => $title,
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min|update_status',
		'pageId' => 'pageEventGameList'
	));
?>
		<!-- /#header -->
		<div id="contents" class="clearfix">
			<div id="main">
				<form action="#" method="post" class="searchForm">
					<div class="headBox clearfix">
						<h2 class="headline1"><?php echo gender($game['sex']), ' ', $game['class'], ' ', $game['item']; ?></h2>
						<div class="boxGroup">
							<div class="leadBox">
								<ul class="clearfix">
									<li><a class="buttonOrder" href="/gymnastics/admin/referee/order_setting"><span>試技順を設定する</span></a></li>
									<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>選手を絞り込む</span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /.headBox -->
					<div class="filterBox">
						<p>
							<span><label for="publish01">チェックを入れた試合の</label></span>
							<span class="select size01">
								<select name="all_status" id="publish01">
									<option value="ステータスを変更する">ステータスを変更する</option>
									<?php foreach ($score_status as $key => $status) {?>
									<option value="<?=$key?>"><?=$status?></option>
									<?php } ?>
								</select>
							</span>
							<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
						</p>
					</div>
					<div class="tableInfo">
						<table class="tableGame tableEvent">
							<tr>
								<th class="center">
									<span class="checkbox">
										<input type="checkbox" id="checkAll" name="checkAll" />
										<label for="checkAll" class="empty">&nbsp;</label>
									</span>
								</th>
								<th class="col03">班</th>
								<th class="col03">組</th>
								<th>選手名/学校名</th>
								<th>試技順</th>
								<th>D1</th>
								<th>D2</th>
								<th>D3</th>
								<th>D4</th>
								<th>D5</th>
								<th>E1</th>
								<th>E2</th>
								<th>E3</th>
								<th>E4</th>
								<th>E5</th>
								<th>E減点</th>
								<th>減点</th>
								<th>合計</th>
								<th class="col06">ステータス</th>
								<th class="col01">編集</th>
							</tr>
							<?php if (!empty($player_scores )): foreach ($player_scores as $player_score): ?>
							<tr class="blue">
								<td class="col01"><span class="checkbox"><input type="checkbox" id="check01" name="check" value="<?php echo (count($player_score['scores']))?$player_score['scores'][0]->getId():0; ?>" /><label for="check01">&nbsp;</label></span></td>
								<td><?php echo $player_score['player']->getGroup(); ?></td>
								<td><?php echo $player_score['player']->getHeat(); ?></td>
								<td class="col04"><?php echo $player_score['player']->getPlayerNo(); ?><br /><?php echo $player_score['player']->getPlayerName(); ?><br /><?php echo $player_score['player']->getSchoolNameAb(); ?></td>
								<td><span><?php echo $player_score['player']->getOrder(); ?></span></td>
								<?php if (count($player_score['scores'])): ?>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getD1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getD2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getD3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getD4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getD5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getE1Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getE2Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getE3Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getE4Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getE5Score(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td class="col05"><span><?php echo $player_score['scores'][0]->getEDemeritScore(); ?></span><a href="#" class="againButton hover<?=showButton($player_score['scores'][0]->getStatus())?>" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">再</a></td>
								<td>-<?php echo formatScore($player_score['scores'][0]->getDemeritScore(),3); ?></td>
								<td><?php echo formatScore($player_score['scores'][0]->getFinalScore(),3); ?></td>
								<td class="col03">
									<p class="statusList">
										<span class="radio">
											<input type="radio" id="not<?php echo $player_score['scores'][0]->getId(); ?>" class="not" name="status[<?php echo $player_score['scores'][0]->getId(); ?>]" value="0" <?php echo ($player_score['scores'][0]->getStatus()==0)?'checked':''; ?> />
											<label for="not<?php echo $player_score['scores'][0]->getId(); ?>">未</label>
										</span>
										<span class="radio">
											<input type="radio" id="input<?php echo $player_score['scores'][0]->getId(); ?>" class="input" name="status[<?php echo $player_score['scores'][0]->getId(); ?>]" value="1" <?php echo ($player_score['scores'][0]->getStatus()==1)?'checked':''; ?> />
											<label for="input<?php echo $player_score['scores'][0]->getId(); ?>">入力</label>
										</span>
										<span class="radio">
											<input type="radio" id="public<?php echo $player_score['scores'][0]->getId(); ?>" class="public" name="status[<?php echo $player_score['scores'][0]->getId(); ?>]" value="2" <?php echo ($player_score['scores'][0]->getStatus()==2)?'checked':''; ?> />
											<label for="public<?php echo $player_score['scores'][0]->getId(); ?>">公開</label>
										</span>
									</p>
									<a class="buttonCustom hover update_status" href="#" data-id="<?php echo $player_score['scores'][0]->getId(); ?>">更新</a>
								</td>
								<?php else: ?>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td class="col05"><span></span><a href="javascript:void(0)" class="againButton hover againGray">再</a></td>
								<td>-</td>
								<td></td>
								<td class="col03">
									<p class="statusList">
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>未</label>
										</span>
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>入力</label>
										</span>
										<span class="radio">
											<input type="radio" name="status" disabled />
											<label>公開</label>
										</span>
									</p>
									<a class="buttonCustom hover update_status" href="javascript:void(0)" >更新</a>
								</td>
								<?php endif; ?>
								<td class="col03"><a class="buttonCustom hover" href="/gymnastics/admin/referee/score_edit01?player_id=<?php echo $player_score['player']->getId(); ?>&item=<?=$game['item']?>">スコア<br />編集</a></td>
							</tr>
							<?php endforeach; else: ?>
							<tr><td colspan="19">検索結果がありません。</td></tr>
							<?php endif; ?>
						</table>
					</div>
					<div class="filterBox">
						<p>
							<span><label for="publish02">チェックを入れた試合の</label></span>
							<span class="select size01">
								<select name="all_status" id="publish02">
									<option value="ステータスを変更する">ステータスを変更する</option>
									<?php foreach ($score_status as $key => $status) {?>
									<option value="<?=$key?>"><?=$status?></option>
									<?php } ?>
								</select>
							</span>
							<span class="btnUpdate"><input class="buttonCustom btn_update_all hover" type="button" value="更新" /></span>
						</p>
					</div>
					<p class="buttonBack"><a href="javascript:history.back()" class="buttonStyle hover">戻る</a></p>
				</form>
			</div>
			<!-- /#main -->
		</div>
		<!-- /#contents -->
		<div class="modal">
			<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
			<div class="modalContent">
				<h3 class="hModal">絞り込み検索</h3>
				<div class="searchBox">
					<form action="" method="get">
						<div class="sheet">
							<table>
								<tr><th>&nbsp;</th></tr>
								<tr>
									<td>
										<ul class="selectList clearfix">
											<li class="mr20">
												<label for="team01" class="labelCss">班</label>
												<p class="select size04">
													<select name="group" id="team01">
														<option value="">班</option>
														<?php for ($i=1; $i < 11 ; $i++) { ?>
														<option value="<?=$i?>"<?php if($this->input->get('group') == $i) echo ' selected="selected"'; ?>><?=$i?>班</option>
														<?php } ?>
													</select>
												</p>
											</li>
											<li class="mr20">
												<label for="team02" class="labelCss">組</label>
												<p class="select size04">
													<select name="heat" id="team02">
														<option value="">組</option>
														<?php for ($i=1; $i < 9 ; $i++) { ?>
														<option value="<?=$i?>"<?php if($this->input->get('heat') == $i) echo ' selected="selected"'; ?>><?=$i?>班</option>
														<?php } ?>
													</select>
												</p>
											</li>
											<li>
												<label for="status" class="labelCss">ステータス</label>
												<p class="select size02">
													<select name="status" id="status">
														<option value="">ステータス</option>
														<?php foreach ($score_status as $key => $status) {?>
														<option value="<?=$key?>"<?php if($this->input->get('status') == $key) echo ' selected="selected"'; ?>><?=$status?></option>
														<?php } ?>
													</select>
												</p>
											</li>
										</ul>
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix">
							<p class="btnCancel"><input type="button" value="リセット" name="reset" onclick="window.location.href='/gymnastics/admin/referee/event_game_list/'" class="hover" /></p>
							<p class="btnSubmit"><input type="submit" value="検索" class="hover" /></p>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.modal -->
<script>
	function update_status(score_id, status_update, type_update)
	{
		if (score_id == 0){
			location.reload(true);
		}
		$.ajax({
			url:"/gymnastics/admin/referee/event_game_list/updateStatus",
			type:"POST",
			data:{score_id: score_id,status_update: status_update, type: type_update},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					location.reload();
				}
			}
		});
	}
	
</script>
<?php $this->load->view("includes/admin/footer"); ?>

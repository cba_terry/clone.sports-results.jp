<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'モニター一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageMonitorList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2">モニター一覧</h2>
		</div>
		<!-- /.headBox -->
		<ul class="infoList clearfix">
			<li class="fullWidth"><a href="<?=get_value($urls, '%s', 'score', '#')?>" target="_blank">審判長用班別得点一覧</a></li>
			<li class="fullWidth"><a href="<?=get_value($urls, '%s', 'ranking', '#')?>" target="_blank">順位</a></li>
			<li class="fullWidth"><a href="<?=get_value($urls, '%s', 'rotation', '#')?>" target="_blank">ローテーション</a></li>
		<?php
			if ($items_default) {
				$class_other = ( ! empty($game) && $game->getSex()) ? 'otherWidth' : 'otherWidth2';
				foreach ($items_default as $item) {
					$rotateItem = $rotations->filterItem($item)->first();
					$href   = get_value($urls, "%s", 'result');
					$href   = ($rotateItem) ? $href . $rotateItem->getHeat() : null;
					$target = ($href != '#') ? ' target="_blank"' : '';
		?>
				<?php if($rotateItem) { ?>
				<li class="<?=$class_other?>"><a href="<?=$href?>"<?=$target?>>得点（<?=$item?>）</a></li>
				<?php }else { ?>
					<li class="<?=$class_other?>"><a>得点（<?=$item?>）</a></li>
				<?php } ?>
		<?php
				} // endforeach
			} // endif
		?>
		</ul>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
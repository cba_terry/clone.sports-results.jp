<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'ローテーション設定',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageRotation'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?=$game->getStrSex()?> <?=$game->getClass()?> ローテーション設定</h2>
		</div>
		<form action="" class="userForm" method="post">
			<div class="tableStyle">
				<table>
					<tr>
						<th><label for="tournament">試技順</label></th>
						<td><ul class="orderList">
							<?php foreach ($rotationSettings as $setting) {?>
								<li>
									<input type="text" name="setting[<?=$setting->getId()?>]" class="size06" id="order<?=$setting->getId()?>" value="<?=$setting->getOrder()?>" />
									<label for="order<?=$setting->getId()?>"><?=$setting->getName()?></label>
								</li>
							<?php } ?>
							</ul>
						</td>
					</tr>
 
					<?php if(count($rotations)) { ?>
						<?php 
						$i = 1;
						$rowspan = count($rotations);
						foreach ($rotations as $rotation) {
							$group = $rotation->getGroup();
							$heats = $gameInfo['groupheats'][$group];
							$firstHeat = reset($heats);
						?>
						<tr>
							<?php if($i == 1 ){?>
							<th rowspan="<?=$rowspan?>">1競技目</th>
							<?php } ?>
							<?php $i++; ?>
							<td>
								<p class="rowList pb0"> <span class="titleRadio"><?=$rotation->getGroup()?>班<?=$firstHeat?>組</span>
									<?php foreach ($rotationSettings as $key => $item) { ?>
									<span class="radio">
										<input type="radio" <?php if($item->getName() == $rotation->getFirstItem()) echo 'checked="checked"'; ?> name="rotation[<?=$rotation->getGroup()?>]" id="pair<?=$rotation->getGroup()?><?=$item->getId()?>" value="<?=$item->getId()?>" />
										<label for="pair<?=$rotation->getGroup()?><?=$item->getId()?>"><?=$item->getName()?></label>
									</span>
									<?php //if($key == count($heats)-1) break; 
									} ?>
								</p>
							</td>
						</tr>
						<?php } //endforeach ?>
						
					<?php } else { ?>
						<?php 
						$i = 1;
						$rowspan = count($gameInfo['groupheats']);
						foreach ($gameInfo['groupheats'] as $group => $heats) {	?>
						<tr>
							<?php if($i == 1 ){?>
							<th rowspan="<?=$rowspan?>">1競技目</th>
							<?php } ?>
							<?php $i++; ?>
							<td>
								<p class="rowList pb0"> <span class="titleRadio"><?=$group?>班<?=reset($heats)?>組</span>
									<?php foreach ($rotationSettings as $key => $item) { ?>
									<span class="radio">
										<input type="radio" name="rotation[<?=$group?>]" id="pair<?=$group?><?=$item->getId()?>" value="<?=$item->getId()?>" />
										<label for="pair<?=$group?><?=$item->getId()?>"><?=$item->getName()?></label>
									</span>
									<?php //if($key == count($heats)-1) break; 
									} ?>
								</p>
							</td>
						</tr>
						<?php } //endforeach ?>
 
 					<?php } //endif?>
					
				</table>
				<ul class="buttonList clearfix">
					<li><a href="/gymnastics/admin/organization/tournament/<?=$game->getTournament()->getId()?>/game_class" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton">
						<input type="submit" value="設定する" class="buttonGeneral hover btn_submit" id="changeBtn" />
					</li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<script type="text/javascript">
	$(document).ready(function(){。
		$('.btn_submit').click(function(){
			var num_heats = '<?=$num_heats?>';
			for(i=1; i<num_heats; i++){
				var length = $("p.list_rotation span:nth-child("+i+") input[type=radio]:checked").length;
				if(length > 1){
					alert("重複に選択できません");
					return false;
				}
			}
		});
	})
</script>
<?php $this->load->view("includes/admin/footer"); ?>
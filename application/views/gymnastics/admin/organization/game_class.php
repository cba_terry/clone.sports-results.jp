<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試合区分一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGameClass'
	));

	$tourClass = $tournament->getClass();
	$tourGames = $tournament->getGamesActive();

	$maleGames = $tourGames->filter(function($v){
		return $v->isMale();
	})->toArray();

	$femaleGames = $tourGames->filter(function($v){
		return !$v->isMale();
	})->toArray();

	uasort($maleGames, function($a, $b){
		return $a->getClass() > $b->getClass() ? 1 : -1;
	});

	uasort($femaleGames, function($a, $b){
		return $a->getClass() > $b->getClass() ? 1 : -1;
	});
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline1">試合区分一覧</h2>
		</div>
		<!-- /.headBox -->
		<?php if(count($maleGames)) { ?>
		<div class="tableInfo tablePlayer">
			<table>
				<tr class="headTitle">
					<th colspan="10">男子</th>
				</tr>
				<tr>
					<th>試合区分</th>
					<th class="col01">選手一覧</th>
					<th class="col01">審判一覧</th>
					<th class="col01">学校一覧</th>
					<th class="col01">試合一覧</th>
					<th class="col01">ローテーション</th>
					<th class="col01">モニター一覧</th>
					<th class="col01">決勝進出者一覧</th>
					<th class="col02">Excel出力</th>
					<th class="col02">賞状出力</th>
				</tr>
				<?php foreach($maleGames as $key => $game) {
					$gameId = $game->getId();
					?>
				<tr>
					<td class="col01"><?=$game->getClass()?></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/player?gid=<?=$gameId?>">選手</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/referee?gid=<?=$gameId?>">審判</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/group?gid=<?=$gameId?>">学校</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/game?gid=<?=$gameId?>">試合</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/rotation?gid=<?=$gameId?>">ローテーション</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/monitor?gid=<?=$gameId?>">モニター一覧</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/finalist?gid=<?=$gameId?>">結果一覧</a></td>
					<td><a class="hover" href="/gymnastics/admin/organization/excel_output?gid=<?=$gameId?>"><img src="<?=base_url('/img/admin/icon_excel.png')?>" alt="" /></a></td>
					<td><?php if($game->getClass()=='決勝') { ?><a class="hover" href="/gymnastics/admin/organization/certificate_output?gid=<?=$gameId?>"><img src="<?=base_url('/img/admin/icon_print.png')?>" alt="" /></a><?php } ?></td>
				</tr>
				<?php } ?>
			</table>
		</div>
		<?php } ?>
		<?php if(count($femaleGames)) { ?>
		<div class="tableInfo tablePlayer">
			<table>
				<tr class="headTitle">
					<th colspan="10">女子</th>
				</tr>
				<tr>
					<th>試合区分</th>
					<th class="col01">選手一覧</th>
					<th class="col01">審判一覧</th>
					<th class="col01">学校一覧</th>
					<th class="col01">試合一覧</th>
					<th class="col01">ローテーション</th>
					<th class="col01">モニター一覧</th>
					<th class="col01">決勝進出者一覧</th>
					<th class="col02">Excel出力</th>
					<th class="col02">賞状出力</th>
				</tr>
				<?php foreach($femaleGames as $key => $game) {
					$gameId = $game->getId();
					?>
				<tr>
					<td class="col01"><?=$game->getClass()?></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/player?gid=<?=$gameId?>">選手</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/referee?gid=<?=$gameId?>">審判</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/group?gid=<?=$gameId?>">学校</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/game?gid=<?=$gameId?>">試合</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/rotation?gid=<?=$gameId?>">ローテーション</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/monitor?gid=<?=$gameId?>">モニター一覧</a></td>
					<td><a class="buttonCustom hover" href="/gymnastics/admin/organization/finalist?gid=<?=$gameId?>">結果一覧</a></td>
					<td><a class="hover" href="/gymnastics/admin/organization/excel_output?gid=<?=$gameId?>"><img src="<?=base_url('/img/admin/icon_excel.png')?>" alt="" /></a></td>
					<td><?php if($game->getClass()=='決勝') { ?><a class="hover" href="/gymnastics/admin/organization/certificate_output?gid=<?=$gameId?>"><img src="<?=base_url('/img/admin/icon_print.png')?>" alt="" /></a><?php } ?></td>
				</tr>
				<?php } ?>
			</table>
		</div>
		<?php } ?>
		<p class="buttonBack"><a href="javascript:history.back()" class="buttonStyle hover">戻る</a></p>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
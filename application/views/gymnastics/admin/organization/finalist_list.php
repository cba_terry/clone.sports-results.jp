<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '決勝進出者一覧',
		'css'    => 'jquery-ui',
		'js'     => 'finalist',
		'pageId' => 'pageFinalistSetting'
	));

	

	$orderby = $this->input->get('orderby');
	$orderItem = $this->input->get('item');
?>
		<div id="contents" class="clearfix">
			<div id="main">
				<div class="headBox clearfix">
					<h2 class="headline1">結果一覧</h2>
					<div class="boxGroup">
						<div class="leadBox">
							<ul class="clearfix">
								<!--<li><a class="buttonFinal openModal" href="javascript:void(0)"><span>決勝の枠組を設定する</span></a></li>-->
								<?php if($finalGame) { ?>
								<li><a class="buttonFinal" href="/gymnastics/admin/organization/finalist/setting?gid=<?=$finalGame->getId()?>"><span>決勝の枠組を設定する</span></a></li>
								<?php }?>
							</ul>
						</div>
					</div>
				</div>
				<!-- /.headBox -->
				<div class="filterBox">
					<form action="" method="get" id="sort_form">
						<input type="hidden" name="gid" value="<?=$game->getId()?>">
						<p>
							<span class="status"><label for="orderby">表示順</label></span>
							<span class="select size02">
								<select name="orderby" id="orderby">
									<option value="group" <?php if($orderby == 'group') echo 'selected'?>>団体</option>
									<option value="single" <?php if($orderby == 'single') echo 'selected'?>>個人総合</option>
									<?php foreach ($items as $item) { ?>
										<option value="item" <?php if($orderby == 'item' && $orderItem == $item->getName()) echo 'selected'?>><?=$item->getName()?></option>
									<?php } ?>
								</select>
							</span>
						</p>
					</form>
				</div>
				<form action="" method="post" class="searchForm">
					<div class="tableInfo mb10">
						<table class="tableFinalist mb10">
							<tr>
								<th class="col001" rowspan="3">No</th>
								<th class="col002" rowspan="3">選手名</th>
								<th class="col003" rowspan="3">学校名</th>
								<th class="col001" rowspan="3">団体<br />
個人</th>
								<th colspan="<?=3+count($items)?>">順位 (得点)</th>
								<th class="col005" rowspan="3">決勝<br />
進出権利</th>
								<th rowspan="3">通過種別</th>
							</tr>
							<tr>
								<th class="col004" rowspan="2">団体</th>
								<th class="col004" rowspan="2">個人<br />
総合</th>
								<th class="col004" rowspan="2"><span>団体除く</span><br />
個人<br />
総合</th>
								<th colspan="<?=count($items)?>">種目別</th>
							</tr>
							<tr>
								<?php foreach ($items as $item): ?>
								<th class="col004"><?=$item?></th>
								<?php endforeach; ?>
							</tr>
					<?php if(!empty($players)): 
							foreach ($players as $player): 
								$singleTotalScore = $player->getSingleTotalScore();
								$itemTotalScore = $player->getItemTotalScore();
							?>
							<tr data-player=<?=$player->getId()?>>
								<td><?=$player->getPlayerNo()?></td>
								<td class="col01"><?=$player->getPlayerName()?></td>
								<td class="col01"><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
								<td><?=convert_game_type($player->getFlag())?></td>
								<td><span class="score"><?=$rankGroup->findRank($player)?></span>
									<span class="input"><em>並び順</em><input type="text" name="group_rank" value="<?php if($singleTotalScore && $singleTotalScore->getGroupRank() != NULL) {?><?=$singleTotalScore->getGroupRank()?><?php }else{ ?><?=$rankGroup->findRank($player)?><?php } ?>" /></span></td>
								<td><span><?=$rankCommon->findRank($player)?><em>(<?=$player->getTotalFinalScore();?>)</em></span>
									<span class="input"><em>並び順</em><input type="text" name="common_rank"  value="<?php if($singleTotalScore && $singleTotalScore->getPassRank() != NULL) {?><?=$singleTotalScore->getPassRank()?><?php }else{ ?><?=$rankGroup->findRank($player)?><?php } ?>" /></span></td>
								<td><span class="score"><?=$rankSingle->findRank($player)?></span>
									<span class="input"><em>並び順</em><input type="text" name="single_rank" value="<?php if($singleTotalScore && $singleTotalScore->getSingleRank() != NULL) {?><?=$singleTotalScore->getSingleRank()?><?php }else{ ?><?=$rankSingle->findRank($player)?><?php } ?>" /></span></td>
								<?php foreach ($items as $item): ?>
									<?php if (isset($itemTotalScore) && call_user_func_array([$itemTotalScore, 'getItem'.$item->getNo().'Score'], []) != NULL): ?>
								<td><span><?=$rankItems[(string) $item]->findRank($player)?><em>(<?=formatScore(call_user_func_array([$itemTotalScore, 'getItem'.$item->getNo().'Score'], []),2); ?>)</em></span>
									<span class="input"><em>並び順</em><input name="item<?=$item->getNo()?>_rank" type="text" value="<?=call_user_func_array([$itemTotalScore, 'getItem'.$item->getNo().'Rank'], [])?>" /></span></td>
									<?php else: ?>
								<td><span>00<em>(00.00)</em></span>
									<span class="input"><em>並び順</em><input name="item<?=$item->getNo()?>_rank" type="text" /></span></td>
									<?php endif; ?>
								<?php endforeach ?>
								<td class="col02">
									<p class="select">
										<select name="pass_single_or_group" id="rights01" class="comefinal">
											<option <?php if($player->passAsGroup() || ($player->getPassPlayType() == NULL && $player->isPlayAsGroup())) echo 'selected="selected"'?> value="団体">団体</option>
											<option <?php if($player->passAsSingle() || ($player->getPassPlayType() == NULL && $player->isPlayAsSingle())) echo 'selected="selected"'?> value="個人">個人</option>
										</select>
									</p>
								</td>
								<td class="col02">
									<p class="select">
										<select name="pass_play_type" id="type01">
											<option></option>
											<option <?php if($chooseFinalFromGroup->findRank($player)) echo 'selected="selected"'; ?> value="団体">団体</option>
											<option <?php if($chooseFinalFromSingle->findRank($player)) echo 'selected="selected"'; ?> value="個人">個人総合</option>
											<option <?php if($chooseFinalFromItem->findRank($player)) echo 'selected="selected"'; ?> value="種目別">種目別</option>
										</select>
									</p>
								</td>
							</tr>
							<?php endforeach; ?>
					<?php endif; ?>
						</table>
					</div>
					<ul class="buttonList clearfix">
						<li><a href="javascript:history.back()" class="buttonStyle hover">戻る</a></li>
					<?php if ($game->getClass() != '決勝') { ?>
						<li class="submitButton"><input type="submit" value="更新する" class="buttonGeneral hover" id="outputBtn" /></li>
					<?php } // endif $final_game ?>
					</ul>
				</form>
			</div>
			<!-- /#main -->
		</div>
		<!-- /#contents -->
		<div id="footer">
			<p id="copyright">&copy; 全国高等学校体育連盟体操専門部</p>
		</div>
		<!-- /#footer -->
		<div class="modal">
			<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
			<div class="modalContent">
				<h3 class="hModal">決勝の枠組み設定</h3>
				<div class="searchBox">
					<form action="#" method="post">
						<div class="sheet">
							<table>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group01" />
												<label for="group01">1班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs01" />
												<label for="pairs01">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group02" />
												<label for="group02">2班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs02" />
												<label for="pairs02">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group03" />
												<label for="group03">3班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs03" />
												<label for="pairs03">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group04" />
												<label for="group04">4班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs04" />
												<label for="pairs04">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group05" />
												<label for="group05">5班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs05" />
												<label for="pairs05">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="inputRow">
											<span class="checkbox">
												<input type="checkbox" name="group" id="group06" />
												<label for="group06">6班</label>
											</span>
											<span class="with">X</span>
											<span class="input">
												<input type="text" name="pairs" id="pairs06" />
												<label for="pairs06">組で枠を作成</label>
											</span>
										</p>
									</td>
								</tr>
							</table>
						</div>
						<div class="mt20 clearfix">
							<p class="btnCancel"><input type="button" value="リセット" name="reset" class="hover" /></p>
							<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.modal -->
	</div>
	<!-- /#wrapper -->
<?php if ($game->getClass() != '決勝') { ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#outputBtn').click(function(e){
				e.preventDefault();

				var playerList = [];

				$('tr').each(function(){
					
					if($(this).data('player') != undefined){
						var player = {};

						player.id = $(this).data('player');
						player.common_rank = $(this).find('input[name=common_rank]').val();
						player.group_rank = $(this).find('input[name=group_rank]').val();
						player.single_rank = $(this).find('input[name=single_rank]').val();
						player.pass_single_or_group = $(this).find('select[name=pass_single_or_group]').val();
						player.pass_play_type = $(this).find('select[name=pass_play_type]').val();
						<?php foreach ($items as $item){ ?>
						player.item<?=$item->getNo()?>_rank = $(this).find('input[name=item<?=$item->getNo()?>_rank]').val();
						<?php } ?>
						playerList.push(player);
					}
				});

				$.ajax({
					url:"/gymnastics/admin/organization/finalist/accept?gid=<?=$game->getId()?>",
					type:"POST",
					data:{players: playerList},
					dataType:'json',
					success:function(response){
						if(response.status == 'success'){
							alert(response.message);
						}
					}
				});
			})

			$('#orderby').change(function(){

				var item = $('#orderby option:selected').text();

				if($(this).val() == 'item')
				{
					$('#sort_form').append('<input type="hidden" name="item" value="'+item+'"/>');
				}
				
				$('#sort_form').submit();
			})
		})
	</script>
<?php } // endif $final_game ?>
</body>
</html>
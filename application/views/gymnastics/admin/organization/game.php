<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試合一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageEventList'
	));

	$items = $game->getItems();
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?=$game->getStrSex()?> <?=$game->getClass()?></h2>
		</div>
		<!-- /.headBox -->
		<ul class="infoList clearfix">
			<li class="fullWidth"><a href="/gymnastics/admin/organization/game/all?gid=<?=$game->getId()?>">全種目一覧</a></li>
			<?php foreach ($items as $key => $item) { ?>
				<li><a href="/gymnastics/admin/organization/game/item?gid=<?=$game->getId()?>&item=<?=$item->getName()?>"><?=$item->getName()?>一覧</a></li>	
			<?php } ?>
		</ul>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
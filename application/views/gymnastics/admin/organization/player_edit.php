<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '選手編集',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pagePlayerEdit'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?=gender($player->getSex())?> <?=$player->getGame()->getClass()?> 選手編集</h2>
		</div>
		<form action="" class="userForm" method="post">
		<?php echo validation_errors(); ?>
			<div class="tableStyle">
				<table>
					<tr>
						<th>ID</th>
						<td><?php echo $player->getId(); ?></td>
					</tr>
					<tr>
						<th><label for="player01">選手No</label></th>
						<td><input class="size09" name="player_no" value="<?php echo set_value('player_no', $player->getPlayerNo()); ?>" id="player01" type="text" /></td>
					</tr>
					<tr>
						<th><label for="player01">組内表示順</label></th>
						<td><input type="text" class="size09" name="order" value="<?php echo set_value('order', $player->getOrder()); ?>"/></td>
					</tr>
					<tr>
						<th><label for="player02">選手名</label></th>
						<td><input class="size04" name="name" value="<?php echo set_value('name', $player->getPlayerName()); ?>" id="player02" type="text" /></td>
					</tr>
					<tr>
						<th><label for="school">学校名</label></th>
						<td>
							<p class="select size04">
								<select name="school_id" id="school_id">
							<?php
								if ($schools) {
									foreach($schools as $school) {
										if ($player->getSchool() !== null) {
											$is_selected = ($school->getId() == $player->getSchool()->getId()) ? ' selected="selected"' : '';
										} else {
											$is_selected = '';
										}
							?>
									<option data-prefecture="<?=$school->getSchoolPrefecture();?>" value="<?=$school->getId();?>"<?=$is_selected;?>><?=$school->getSchoolNameAb();?></option>
							<?php
									} // endforeach
								} // endif
							?>
								</select>
							</p>
						</td>
					</tr>
					<tr>
						<th><label for="school">都道府県</label></th>
						<td class="elm-prefecture">
							<?php if ($player->getSchool() !== null) echo $player->getSchool()->getSchoolPrefecture();?>
						</td>
					</tr>
					<tr>
						<th>学年</th>
						<td>
							<span class="radio">
								<input type="radio" name="grade" id="year01" value="1" <?=set_radio('grade', "1", $player->getGrade() == "1" ? true : false)?> />
								<label for="year01">1年</label>
							</span><span class="radio">
								<input type="radio" name="grade" id="year02" value="2" <?=set_radio('grade', "2", $player->getGrade() == "2" ? true : false)?> />
								<label for="year02">2年</label>
							</span><span class="radio">
								<input type="radio" name="grade" id="year03" value="3" <?=set_radio('grade', "3", $player->getGrade() == "3" ? true : false)?> />
								<label for="year03">3年</label>
							</span>
						</td>
					</tr>
					<tr>
						<th>性別</th>
						<td><?=gender($player->getSex())?></td>
					</tr>
					<tr>
						<th>個人・団体</th>
						<td>
							<span class="radio">
								<input type="radio" <?=set_radio('flag', "0", $player->getFlag() == 0 ? true : false)?> value="0" name="flag" id="individual" />
								<label for="individual">個人</label>
							</span><span class="radio">
								<input type="radio" <?=set_radio('flag', "1", $player->getFlag() == 1 ? true : false)?> value="1" name="flag" id="organization" />
								<label for="organization">団体</label>
							</span>
						</td>
					</tr>
					<tr>
						<th>班組</th>
						<td>
							<ul class="selectList clearfix">
								<li class="mr5">
									<span class="mr10">決勝</span>
									<p class="select size03">
										<select name="group" id="group_select">
											<?php if($groups) {foreach($groups as $group) { ?>
											<option <?php if($group->getGroup() == $player->getGroup()){ ?>selected="selected"<?php } ?> value="<?=$group->getGroup()?>"><?=$group->getGroup()?></option>
											<?php }}else { ?>
												<option value="">01</option>
											<?php } ?>
										</select>
									</p>
									<p><label class="labelCss" for="team">班</label></p>
								</li>
								<li>
									<p class="select size03">
										<select name="heat" id="heat_select">
											<?php 
											foreach ($heats as $heat) { ?>
											<option <?php echo ($player->getHeat() == $heat->getHeat())?'selected':''; ?> value="<?php echo $heat->getHeat(); ?>"><?php echo $heat->getHeat(); ?></option>
											<?php } ?>
										</select>
									</p>
									<p><label class="labelCss" for="group">組</label></p>
								</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>棄権</th>
						<td>
							<span class="checkbox">
								<input type="checkbox" <?php echo set_radio("flag_cancle", "1", $player->getFlagCancle() == 1 ? true : false)?> name="flag_cancle" value="1" id="flag_cancle"/>
								<label for="flag_cancle">棄権</label>
							</span>
						</td>
					</tr>
				</table>
				<ul class="buttonList clearfix">
					<li><a href="/gymnastics/admin/organization/player?gid=<?=$gameId?>" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton"><input type="submit" value="変更する" class="buttonGeneral hover" id="changeBtn" /></li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<script>
	$(document).ready(function() {
		$('#school_id').change(function(event) {
			var elmOption = $(this).find('option:selected');
			var prefecture = elmOption.data('prefecture');
			$('.elm-prefecture').html(prefecture);
		});
	});
</script>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
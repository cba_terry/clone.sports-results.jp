<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'チーム減点編集',
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min',
		'pageId' => 'pageGameList'
	));

	$group = $this->input->get('group');
	$heat = $this->input->get('heat');
?>
	<!-- /#header -->
	<div id="contents">
		<div id="main" class="clearfix">
			<div class="headBox clearfix">
				<h2 class="headline2">男子 予選A <?=$group?>班<?=$heat?>組 <?=$school->getSchoolNameAb()?></h2>
			</div>
			<form action="#" method="post" class="userForm">
				<div class="tableStyle">
					<table>
						<?php foreach ($items as $key => $item) { 
							$fnGetDeremitScore  = 'getItem' . ($key+1) . 'DemeritScore';
							$fnGetDeremitReason = 'getItem' . ($key+1) . 'DemeritReason';
						?>
						<tr>
							<th><?=$item->getName()?></th>
							<td>
								<ul class="listInput clearfix">
									<li><span class="w103">チーム減点 -</span><input type="text" id="item<?=$key+1?>_demerit_score" class="size07" name="item<?=$key+1?>_demerit_score" value="<?=@$groupScore->$fnGetDeremitScore()?>" /></li>
									<li><span>理由</span><input type="text" id="item<?=$key+1?>_demerit_reason" class="size10" name="item<?=$key+1?>_demerit_reason" value="<?=@$groupScore->$fnGetDeremitReason()?>" /></li>
								</ul>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<th>更新者</th>
							<td>大塚 幹雄</td>
						</tr>
					</table>
					<ul class="buttonList clearfix">
						<?php if($this->input->get('item')) { ?> 
						<li><a href="/gymnastics/admin/organization/game/item?gid=<?=$game->getId()?>&item=<?=$this->input->get('item')?>" class="buttonStyle hover">戻る</a></li>
						<?php }else{ ?>
						<li><a href="/gymnastics/admin/organization/game/all?gid=<?=$game->getId()?>" class="buttonStyle hover">戻る</a></li>
						<?php } ?>
						<li class="submitButton"><input type="submit" value="変更する" class="buttonGeneral hover" id="changeBtn" /></li>
					</ul>
				</div>
			</form>
		</div>
		<!-- /#main -->
	</div>
	<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'ユーザー登録',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageUserEdit'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2">ユーザー登録</h2>
		</div>
		<form action="" class="userForm" method="post">
			<?php
				if ( ! empty(validation_errors())) {
					echo '<div class="mb10">' . validation_errors() . '</div>';
				}
			?>
			<div class="tableStyle">
				<table>
					<tr>
						<th><label for="userID">ユーザーID</label></th>
						<td><input type="text" name="user_code" class="size01" value="" /></td>
					</tr>
					<tr>
						<th><label for="name">ユーザー名</label></th>
						<td><input type="text" name="user_name" class="size01" value="" /></td>
					</tr>
					<tr>
						<th><label for="password">パスワード</label></th>
						<td><input type="password" name="password" class="size04" id="password" value="" /></td>
					</tr>
					<tr>
						<th><label for="repassword">パスワード（確認）</label></th>
						<td><input type="password" name="repassword" class="size04" id="repassword" value="" /></td>
					</tr>
				</table>
				<ul class="buttonList clearfix">
					<li><a href="/gymnastics/admin/organization/user" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton"><input type="submit" value="変更する" class="buttonGeneral hover" id="changeBtn" /></li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '大会一覧',
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min|update_status',
		'pageId' => 'pageTournamentList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="#" method="post" class="searchForm">
			<div class="headBox clearfix">
				<h2 class="headline1">大会一覧</h2>
				<div class="boxGroup">
					<p class="result">全<?=$total_rows?>件</p>
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>大会を絞り込む</span></a></li>
							<li><a class="buttonAdd" href="/gymnastics/admin/organization/tournament/add"><span>大会を追加する</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo">
				<table class="tableTournament">
					<tr>
						<th class="col01">日付</th>
						<th>大会名</th>
						<th class="col03">大会区分</th>
						<th class="col04">試合区分一覧</th>
						<th class="col04">試合区分設定</th>
						<th class="col05">ステータス</th>
						<th class="col06">編集</th>
						<th class="col06">削除</th>
					</tr>
				<?php
					if ( ! empty($total_rows)) {
						foreach ($tournaments as $tournament) {
				?>
					<tr>
						<td><?=$tournament->getStartTime()->format('Y.m.d')?><br />
- <?=$tournament->getEndTime()->format('Y.m.d')?></td>
						<td><a href="/gymnastics/user/game_list/<?=$tournament->getId()?>" class="link"><?=$tournament->getName()?></a></td>
						<td><?=$tournament->getArea()?></td>
						<td class="col01">
							<a class="buttonCustom hover" href="/gymnastics/admin/organization/tournament/<?=$tournament->getId()?>/game_class">試合区分一覧</a>
						</td>
						<td class="col01">
							<a class="buttonCustom hover" href="/gymnastics/admin/organization/tournament/<?=$tournament->getId()?>/game_class/setting">試合区分設定</a>
						</td>
						<td class="col01 buttonGroup">
							<ul>
								<li class="radio">
									<input id="un_status_<?=$tournament->getId()?>" type="radio" name="status[<?=$tournament->getId()?>]" value="0" <?php if ($tournament->getStatus() == 0) echo 'checked'; ?> />
									<label for="un_status_<?=$tournament->getId()?>">非公開</label>
								</li>
								<li class="radio">
									<input id="status_<?=$tournament->getId()?>" type="radio" name="status[<?=$tournament->getId()?>]"  value="1" <?php if ($tournament->getStatus() == 1) echo 'checked'; ?>/>
									<label for="status_<?=$tournament->getId()?>">公開</label>
								</li>
							</ul>
							<p><span class="btnUpdate"><input type="button" class="buttonCustom hover update_status" data-id="<?=$tournament->getId()?>" value="更新" /></span></p>
						</td>
						<td class="col01">
							<a class="hover" href="/gymnastics/admin/organization/tournament/edit/<?=$tournament->getId()?>">
								<img src="<?=base_url('/img/admin/icon_edit.gif')?>" alt="" />
							</a>
						</td>
						<td class="col01">
							<a class="buttonRemove hover" href="javascript:void(0)" onclick="delete_tournament(<?=$tournament->getId()?>)">
								<img src="<?=base_url('/img/admin/icon_remove.gif')?>" alt="" />
							</a>
						</td>
					</tr>
				<?php 
						} // endforeach
					} else {
				?>
					<tr>
						<td colspan="9" class="center">検索結果がありません。</td>
					</tr>
				<?php } //endif ?>
				</table>
			</div>
			<div class="pageLinkWrapper">
				<?=$pagination->create_links();?>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="#" method="get" id="form_search">
				<div class="sheet">
					<table>
						<tr>
							<th><label for="date01" class="labelCss">日付</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('start_date', $this->input->get('start_date')); ?>" class="datetime datepicker size01" id="date01" name="start_date" /><span class="tilde">～</span><input type="text" value="<?php echo set_value('end_date', $this->input->get('end_date')); ?>" class="datetime datepicker size01" id="date02" name="end_date" /></td>
						</tr>
						<tr>
							<th><label for="select01" class="labelCss">大会名</label></th>
						</tr>
						<tr>
							<td>
								<div class="">
									<input type="text" value="<?php echo set_value('tour_name', $this->input->get('tour_name')); ?>" class="size01" id="findtourname" name="tour_name" />
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="select02" class="labelCss">大会区分</label></th>
						</tr>
						<tr>
							<td>
								<div class="select size02">
									<select name="tournament_area">
										<option value="">選択してください</option>
									<?php 
										$tournament_area = $this->config->item('property')['tournament_area']; 
										foreach ($tournament_area as $area) {
									?>
										<option value="<?=$area?>" <?php if($area == @$this->input->get('tournament_area')) echo 'selected="selected"';?>><?=$area?></option>
									<?php
										}
									?>
									</select>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover btn_clear" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	function delete_tournament(id)
	{
		if(confirm("この大会を削除してもよろしいですか？")) {
			$.post('/gymnastics/admin/organization/tournament/delete',{
				tournament_id: id,
			},
			function(data, status){
				if(status == 'success')
					location.reload();
			});
		}
	}
	function update_status(ids, status)
	{
		$.ajax({
			url:"/gymnastics/admin/organization/tournament/updateStatus",
			type:"POST",
			data:{ ids: ids, status: status},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					location.reload();
				}
			}
		});
	}

	$('.btn_clear').click(function(event) {
		event.preventDefault();
		$('.searchBox form input[type="text"]').val('');
		$('.searchBox form select').val('');
	});
</script>
<?php $this->load->view("includes/admin/footer"); ?>
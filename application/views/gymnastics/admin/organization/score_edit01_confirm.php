<?php
$this->load->view("includes/admin/header", array(
		'title'  => '全審判得点確認',
		'css'    => '',
		'js'     => 'score01',
		'pageId' => 'pageScoreEdit'
	));
?>
	<!-- /#header -->
	<div id="contents" class="clearfix">
		<div id="main">
			<div class="headBox clearfix">
				<h2 class="headline2"><?php echo gender($game->getSex()).' '.$game->getClass(); ?><br />
				<?php echo $player->getGroup().'班'.$player->getHeat().'組 '.$player->getPlayerNo().' '.$player->getPlayerName().'('.$player->getSchoolNameAb().')'; ?>:<br>
<?php echo $item->getName(); ?></h2>
			</div>
			<?php $this->load->view("includes/share/form_score_confirm_01"); ?>
		</div>
		<!-- /#main --> 
	</div>
	<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>

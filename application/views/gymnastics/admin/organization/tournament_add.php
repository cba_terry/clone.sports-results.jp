<?php
$this->load->view("includes/admin/header", array(
    'title'  => '大会登録',
    'css'    => '',
    'js'     => 'renewal',
    'pageId' => 'pageTournamentEdit'
));
?>
    <div id="contents" class="clearfix">
        <div id="main">
            <div class="headBox clearfix">
                <h2 class="headline2">大会登録</h2>
            </div>
            <form action="" class="userForm" method="post" enctype="multipart/form-data">
                <?php echo validation_errors(); ?>
                <div class="tableStyle">
                    <table>
                        <tr>
                            <th><label for="tournament">大会名</label></th>
                            <td><input class="size01" name="name" value="" id="tournament" type="text" /></td>
                        </tr>
                        <tr>
                            <th><label for="nation">大会区分</label></th>
                            <td><p class="select size02">
                                    <select name="area" id="nation" class="selectStyle">
                                        <?php foreach($area as $item) {?>
                                            <option value="<?=$item?>"><?=$item?></option>
                                        <?php }?>
                                    </select>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="year01">会期</label></th>
                            <td><ul class="selectList clearfix">
                                    <li>
                                        <p class="select size02">
                                            <select name="startTime[year]" id="year01" class="selectStyle">
                                                <?php for($i=0; $i<4; $i++) {
                                                    $year = date('Y') + $i;?>
                                                    <option value="<?=$year?>"><?=$year?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="year01">年</label>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="select size03">
                                            <select name="startTime[month]" id="month01" class="selectStyle">
                                                <?php for($i=1; $i<=12; $i++) {?>
                                                    <option value="<?=$i?>"><?=$i?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="month01">月</label>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="select size03">
                                            <select name="startTime[day]" id="day01" class="selectStyle">
                                                <?php for($i=1; $i<=31; $i++) {?>
                                                    <option value="<?=$i?>"><?=$i?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="day01">日</label>
                                        </p>
                                    </li>
                                    <li class="iconBg">
                                        <p class="select size02">
                                            <select name="endTime[year]" id="year02" class="selectStyle">
                                                <?php for($i=0; $i<4; $i++) {
                                                    $year = date('Y') + $i; ?>
                                                    <option value="<?=$year?>"><?=$year?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="year02">年</label>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="select size03">
                                            <select name="endTime[month]" id="month02" class="selectStyle">
                                                <?php for($i=1; $i<=12; $i++) {?>
                                                    <option value="<?=$i?>"><?=$i?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="month02">月</label>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="select size03">
                                            <select name="endTime[day]" id="day02" class="selectStyle">
                                                <?php for($i=1; $i<=31; $i++) {?>
                                                    <option value="<?=$i?>"><?=$i?></option>
                                                <?php }?>
                                            </select>
                                        </p>
                                        <p>
                                            <label class="labelCss" for="day02">日</label>
                                        </p>
                                    </li>
                                </ul></td>
                        </tr>
                        <tr>
                            <th><label for="location">場所</label></th>
                            <td><input class="size01" name="place" value="" id="location" type="text" /></td>
                        </tr>
                        <tr>
                            <th><label for="locationURL">場所URL</label></th>
                            <td><input class="size01" name="place_url" value="" id="locationURL" type="text" /></td>
                        </tr>
                        <tr>
                            <th>試合形式</th>
                            <td>
                                <div class="wrapRowList">
                                <?php $property = $this->config->item('property');?>
                                <p class="rowList w1"><span class="title">男子</span>
                                <?php foreach($property['tournament_class'] as $key => $class) { ?>
                                    <span class="checkbox">
        								<input type="checkbox" value="<?=$class?>" name="class[1][]" id="single_male_<?=$key?>" />
        								<label for="single_male_<?=$key?>"><?=$class?></label>
        							</span>
                                <?php } ?>
                                </p>
                                <p class="rowList w1"><span class="title">女子</span>
                                <?php foreach($property['tournament_class'] as $key => $class) { ?>
                                    <span class="checkbox">
                                        <input type="checkbox" value="<?=$class?>" name="class[0][]" id="single_female_<?=$key?>" />
                                        <label for="single_female_<?=$key?>"><?=$class?></label>
                                    </span>
                                <?php } ?>
                                </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>サイト公開</th>
                            <td>
                                <div class="wrapRowList">
                                <?php foreach($property['public_setting'] as $key => $item) { ?>
                                <p class="rowList w2"><span class="title"><?=$item?></span>
                                    <span class="checkbox">
                                        <input type="checkbox" name="<?=$key?>_publish" id="<?=$key?>_publish">
                                        <label for="<?=$key?>_publish">サイトに表示</label>
                                    </span>
                                    <?php foreach($property['public_setting_value'] as $k => $value) { ?>
                                    <span class="radio">
								    <input type="radio" <?php if(false) { ?>checked="checked"<?php } ?> name="<?=$key?>" id="report<?=$key?><?=$k?>" value="<?=$value?>"/>
								    <label for="report<?=$key?><?=$k?>"><?=$value?></label>
								    </span>
                                    <?php } ?>
                                    <span class="checkbox">
                                        <input type="checkbox" name="<?=$key?>_type" id="<?=$key?>_type">
                                        <label for="<?=$key?>_type">情報確定</label>
                                    </span>
                                </p>
                                <?php } ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>サイト表示項目</th>
                            <td>
                                <?php foreach($property['public_setting_score'] as $key => $public_item_score) { ?>
                                <p class="rowList w2"><span class="title"><?=$property['public_setting'][$key]?></span>
                                    <span class="checkbox">
                                    <input type="checkbox" name="<?=$key?>_publish_score" id="<?=$key?>_publish_score"/>
                                    <label for="<?=$key?>_publish_score"><?=$public_item_score?></label> 
                                    </span>
                                    <?php if($key=='news_timing') {?>
                                    <span class="checkbox">
                                    <input type="checkbox" name="news_timing_publish_rank" id="news_timing_publish_rank" value="1" <?php if(@$setting->news_timing_publish_rank) { ?>checked="checked"<?php } ?> />
                                    <label for="news_timing_publish_rank">順位</label>
                                    </span>
                                    <?php } ?>
                                </p>
                                <?php } ?>
                            </td> 
                        </tr>
                        <tr>
                            <th>大会ロゴイメージ</th>
                            <td>
                                <div class="imageUpload">
                                    <p class="image preview-image"><img src="<?=base_url('/img/admin/img_no_image.gif')?>" alt=""></p>
                                    <div class="groupFile">
                                        <p class="fileUpload hover" style="opacity: 1;">
                                            <span>ファイルを選択</span>
                                            <input name="tour_image" class="file" type="file">
                                        </p>
                                        <span class="uploadFile">未選択</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>賞状表示文言</th>
                            <td>
                                <p class="rowList w3"><span class="title">団体</span>
                                    <textarea class="size01 idleField input-textarea" name="text_display_group" id="text_display_group"></textarea>
                                </p>
                                <p class="rowList w3"><span class="title">個人</span>
                                    <textarea class="size01 idleField input-textarea" name="text_display_single" id="text_display_single"></textarea>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="name">賞状表示代表者名</label></th>
                            <td>
                                <textarea class="size01 idleField input-textarea" name="person_represent_name" id="person_represent_name"></textarea>
                            </td>
                        </tr>
                    </table>
                    <ul class="buttonList clearfix">
                        <li><a href="/gymnastics/admin/organization/tournament" class="buttonStyle hover">戻る</a></li>
                        <li class="submitButton"><input type="submit" value="登録する" class="buttonGeneral hover" id="changeBtn" /></li>
                    </ul>
                </div>
            </form>
        </div>
        <!-- /#main -->
    </div>
    <!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
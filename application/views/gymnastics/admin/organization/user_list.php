<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'ユーザー一覧',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageUserList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="" method="post" class="searchForm">
			<div class="headBox clearfix">
				<h2 class="headline1">ユーザー一覧</h2>
				<div class="boxGroup">
					<p class="result">全<?=$users->count()?>件</p>
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>ユーザーを絞り込む</span></a></li>
							<li><a class="buttonAdd" href="/gymnastics/admin/organization/user/add"><span>ユーザーを追加する</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo tableUser">
				<table>
					<tr>
						<th class="col02">登録日</th>
						<th class="col02">ユーザーID</th>
						<th>ユーザー名</th>
						<th class="col03">パスワード</th>
						<th class="col04">編集</th>
						<th class="col04">削除</th>
					</tr>
					<?php
						if ($users->count()) {
							foreach($users  as $user) {
					?>
					<tr>
						<td class="center"><?=$user->getCreated()->format('Y-m-d')?></td>
						<td><?=$user->getUserCode()?></td>
						<td><?=$user->getUserName()?></td>
						<td><?=$user->getPassword()?></td>
						<td class="col01"><a class="hover" href="/gymnastics/admin/organization/user/edit/<?=$user->getId()?>"><img src="<?=base_url('/img/admin/icon_edit.gif')?>" alt="" /></a></td>
						<td class="col01"><a class="buttonRemove hover" href="javascript:void(0)" onclick="delete_user(<?=$user->getId()?>)"><img src="<?=base_url('/img/admin/icon_remove.gif')?>" alt="" /></a></td>
					</tr>
					<?php
							} // endforeach
						} else {
							echo '<tr><td colspan="6" class="center">検索結果がありません。</td></tr>';
						} // endif
					?>
				</table>
			</div>
			<div class="pageLinkWrapper">
				<?=$pagination->create_links();?>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="#" method="get">
				<div class="sheet">
					<table>
						<tr>
							<th><label for="user" class="labelCss">ユーザーIDまたはユーザー名</label></th>
						</tr>
						<tr>
							<td><input type="text" value="" class="size03" id="user" name="keyword" /></td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	function delete_user(id)
	{
		if(confirm("この審判を削除してもよろしいですか？")) {
			$.post('/gymnastics/admin/organization/user/delete',{
				user_id: id,
			},
			function(data, status){
				if(status == 'success')
					location.reload();
			});
		}
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>
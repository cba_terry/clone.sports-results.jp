<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試合区分設定',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGameClassSetting'
	));

	$order = array(
		'男子予選A',
		'男子予選B',
		'男子決勝',
		'女子予選A',
		'女子予選B',
		'女子決勝'
	);

	$tmpOrder = [];

	foreach ($order as $o) {
		foreach ($games as $game) {
			if($o == (string) $game)
			{
				array_push($tmpOrder, $game); 
			}
		}
	}

	$games = $tmpOrder;

	$gamekeys = $tournament->getClass();
	$active = ($this->input->get('game')) ? $this->input->get('game') : $gamekeys[0];

?>

<div id="contents">
	<div id="main" class="clearfix">
		<div class="headBox clearfix">
			<h2 class="headline2">試合区分設定</h2>
		</div>
		<div class="tabArea">
			<ul class="tab tab01 clearfix">
				<?php $i=1;
				foreach ($games as $game) { ?>
					<li <?php if($active == (string) $game) { echo 'class="active"'; } ?>><a href="#tab0<?=$i?>"><?=$game?></a></li>
				<?php $i++;} ?>
			</ul>
			<!-- /.tabList -->
			<div class="tabContents">
			<?php
			$tabindex = 1;
			foreach($games as $gidx => $game) { 
				$displayBox = ($active == $game) ? 'style="display:block"' : 'style="display:none"';
				$items 	= $this->config->item('property')['item_'.$game->getStrSex()];
			?>
				<div id="tab0<?=$tabindex?>" class="tabBox" <?=$displayBox?> >
					<form action="" class="userForm" method="post" enctype="multipart/form-data">
						<?php echo validation_errors(); ?>
						<input type="hidden" name="game" value="<?=$game?>">
						<div class="tableStyle">
							<table>
								<tr>
									<th>CSVイン ポート</th>
									<td>
										<ul class="CSVList clearfix">
											<li><span class="text">選手</span>
												<p class="fileUpload hover"><span>ファイルを選択</span>
													<input type="file" name="file_player" class="file" />
												</p>
												<span class="uploadFile">未選択</span>
											</li>
											<li class="download">
												<a href="/gymnastics/admin/organization/tournament/<?=$tournament->getId()?>/game_class/download_csv?gid=<?=$game->getId()?>"><span>サンプルデータダウンロード</span></a>
											</li>
											<li class="checkbox">
												<input type="checkbox" <?php if(isset($input_final_round) && $input_final_round == '1'){ ?>checked="checked"<?php } ?> id="input_final_round[<?=$gidx?>]" name="input_final_round" value="1">
												<label for="input_final_round[<?=$gidx?>]">決勝進出者一覧の内容でインポート</label>
											</li>
										</ul>
									</td>
								</tr>
							</table>
						</div>
						<div class="tableStyle">
							<table>
								<?php foreach($items as $idx => $item) {
									$item = $game->findItem($item); 
									$number_referee_d = ($item) ? $item->getNumberRefereeD() : 1;
									$number_referee_e = ($item) ? $item->getNumberRefereeE() : 1;
								?>
								<tr>
									<?php if($idx==1) { ?>
									<th rowspan="<?=count($items)?>">実施種目/審判数</th> 
									<?php } ?>
									<td colspan="3">
										<ul class="listOption">
											<li>
												<p class="rowList w5">
												<span class="title">
													<label class="labelCss" for="item_name_<?=$idx?>">種目<?=$idx?></label>
												</span>
													<span class="select size05">
													<select name="item[<?=$idx?>][name]" id="item_name_<?=$idx?>" class="selectStyle">
														<?php foreach($items as $idx2 => $item2) {?>
														<option <?php if(($idx == $idx2 && !isset($game_items[$idx])) || (isset($game_items[$idx]) && ($game_items[$idx]['name'] == $item2))) {
															echo 'selected="selected"';
															} ?> 
														value="<?=$item2?>"><?=$item2?></option>
														<?php } ?>
													</select>
												</span>
												<span class="select size03 time">
													<select name="item[<?=$idx?>][flag_item]" id="item_time_<?=$idx?>" class="selectStyle">
														<?php $flag_items = $this->config->item('property')['flag_item']?>
														<?php foreach($flag_items as $kflag => $vflag) {?>
															<option <?php if($item && $item->isPlayTwice() == $kflag){echo 'selected="selected"';
															} ?> value="<?=$kflag?>"><?=$vflag?></option>
														<?php } ?>
													</select>
												</span>
												<span>
													<label class="labelCss" for="item_time_<?=$idx?>">回</label>
												</span>
											</li>
											<li>
												<p class="rowList w5 pb5">
													<span class="title">D審判</span>
													<?php for ($i = 1; $i <=5; $i++ ) { ?>
													<span class="radio">
														<input type="radio" id="item_<?=$gidx?>_<?=$i?>_<?=$idx?>_number_referee_d" name="item[<?=$idx?>][number_referee_d]" value="<?=$i?>" <?php if($i == $number_referee_d){ echo 'checked="checked"';}?>>
														<label for="item_<?=$gidx?>_<?=$i?>_<?=$idx?>_number_referee_d"><?=$i?>名</label>
													</span>
													<?php } ?>
												</p>
												<p class="rowList w5">
													<span class="title">E審判</span>
													<?php for ($i = 1; $i <=5; $i++ ) { ?>
													<span class="radio">
														<input type="radio" id="item_<?=$gidx?>_<?=$i?>_<?=$idx?>_number_referee_e" name="item[<?=$idx?>][number_referee_e]" value="<?=$i?>" <?php if($i == $number_referee_e){echo 'checked="checked"';
															} ?>>
														<label for="item_<?=$gidx?>_<?=$i?>_<?=$idx?>_number_referee_e"><?=$i?>名</label>
													</span>
													<?php } ?>
												</p>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
								<tr>
									<th>決勝進出数</th>
									<td colspan="3">
										<p class="pb10"><span class="checkbox">
											<input type="checkbox" <?php if($game->getPreliminary() == '1'){ ?>checked="checked"<?php } ?> name="preliminary" id="preliminary[<?=$gidx?>]" value="1" />
											<label for="preliminary[<?=$gidx?>]">予選なしの場合はチェック</label>
											</span>
										</p>
										<ul class="listInput">
											<?php $sex = $game->getSex();?>
											<li> <span>個人</span>
												<label class="labelCss" for="class_single_pass_number_<?=$sex?>">上位</label>
												<input class="size03" name="single_pass_number" value="<?php echo set_value('single_pass_number['.$sex.']', $game->getSinglePassNumber()); ?>" id="class_single_pass_number_<?=$sex?>" type="text" />
												<em>人</em></li>
											<li> <span>団体</span>
												<label class="labelCss" for="class_group_pass_number_<?=$sex?>">上位</label>
												<input class="size03" name="group_pass_number" value="<?php echo set_value('group_pass_number['.$sex.']', $game->getGroupPassNumber()); ?>" id="class_group_pass_number_<?=$sex?>" type="text" />
												<em>チーム</em></li>
											<li> <span>種目</span>
												<label class="labelCss" for="class_item_pass_number_<?=$sex?>">上位</label>
												<input class="size03" name="item_pass_number" value="<?php echo set_value('item_pass_number['.$sex.']', $game->getItemPassNumber()); ?>" id="class_item_pass_number_<?=$sex?>" type="text" />
												<em>人</em></li>
										</ul>
									</td>
								</tr>
								<tr>
									<th>結果入力方法</th>
									<td colspan="3"><span class="radio">
										<input type="radio" <?php if($game->isInputNormal()){ ?>checked="checked"<?php } ?> name="input_type" id="class_input_type_[<?=$gidx?>]_1" value="0" />
										<label for="class_input_type_[<?=$gidx?>]_1">各審判による入力</label>
										</span> <span class="radio">
										<input type="radio" <?php if($game->isInputPaper()){ ?>checked="checked"<?php } ?> name="input_type" id="class_input_type_[<?=$gidx?>]_2" value="1" />
										<label for="class_input_type_[<?=$gidx?>]_2">セクレタリー・CJによる入力</label>
										</span>
										<span class="checkbox">
											<input type="checkbox" name="input_line_score" id="input_line_score_<?=$gidx?>" <?php if($game->getInputLineScore()) {echo 'checked="checked"';}?>>
											<label for="input_line_score_<?=$gidx?>">ライン</label>
										</span>
										<span class="checkbox">
											<input type="checkbox" name="input_time_score" id="input_time_score_<?=$gidx?>" <?php if($game->getInputTimeScore()) {echo 'checked="checked"';}?>>
											<label for="input_time_score_<?=$gidx?>">タイム</label>
										</span>
									</td>
								</tr>
							</table>
						</div>
						<ul class="buttonList clearfix">
							<li><a href="/gymnastics/admin/organization/tournament" class="buttonStyle hover">戻る</a></li>
							<li class="submitButton">
								<input type="submit" value="選手・審判以外のデータをクリアする" data-gid="<?=$game->getId()?>" class="buttonGeneral buttonGeneral01 hover clear_score_data" style="opacity: 0.6;">
								<input type="submit" value="設定する" class="buttonGeneral hover" id="changeBtn" />
							</li>
						</ul>
					</form>
				</div>
	        <?php $tabindex++; } ?>
			</div>
			<!-- /.tabContents -->
		</div>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<script type="text/javascript">
// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {
  $(this).on('submit',function(e){
    var $form = $(this);

    if ($form.data('submitted') === true) {
      // Previously submitted - don't submit again
      e.preventDefault();
    } else {
      // Mark it so that the next submit can be ignored
      $form.data('submitted', true);
    }
  });

  $('.clear_score_data').click(function(e){
  	e.preventDefault();
  	var gameId = $(this).data('gid');
  	if(gameId)
  	{
  		// do post
  		$.ajax({
			url:"/gymnastics/admin/organization/clean_score_data",
			type:"POST",
			data:{ gid: gameId},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					alert(response.message);
				}
			}
		});
  	}
  })

  // Keep chainability
  return this;
};

<?php 
if($this->input->get('status') == 'success') echo 'alert("ファイルのインポートが完了しました。");';
if($this->input->get('status') == 'error') echo 'alert("ファイルのインポートが失敗しました。");';
?>

$('form').preventDoubleSubmission();
</script>
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '審判一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageRefereeEdit'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?=$referee->getGame()->getStrSex()?> <?=$referee->getGame()->getClass()?> 審判編集</h2>
		</div>
		<form action="" class="userForm" method="post">
			<?php echo validation_errors(); ?>
			<div class="tableStyle">
				<table>
					<tr>
						<th>ID</th>
						<td><?php echo $referee->getId(); ?></td>
					</tr>
					<tr>
						<th><label for="referee">審判名</label></th>
						<td><input type="text" id="referee" class="referee size04" name="referee_name" value="<?php echo set_value('referee_name', $referee->getRefereeName()); ?>" /></td>
					</tr>
					<tr>
						<th><label for="password">パスワード</label></th>
						<td><input type="text" id="password" class="password size04" name="password" value="<?php echo set_value('password', $referee->getPassword()); ?>" /></td>
					</tr>
					<tr>
						<th>試合区分</th>
						<td><?php echo $referee->getGame()->getClass();?></td>
					</tr>
					<tr>
						<th>種目</th>
						<td>
							<?php 
								$property = $this->config->item('property');
								if($referee->getGame()->getSex() == 1) {
									$list_item = $property['item_rotation_man'];
								}else{
									$list_item = $property['item_rotation_woman'];
								}
							?>
							<p class="rowList">
							<?php foreach($list_item as $key => $value) { ?>
							<span class="radio size02">
								<input type="radio" <?php if($referee->checkItem($value)) echo 'checked="checked"'?> value="<?=$value?>" name="item" class="event"  id="event<?=$key?>" />
								<label for="event<?=$key?>"><?=$value?></label>
							</span>
							<?php if($key == $num_heats) break; ?>
							<?php } ?>
							</p>
						</td>
					</tr>
					<tr>
						<th>記録種別</th>
						<td>
							<?php $property = $this->config->item('property');?>
							<p class="rowList">
							<?php foreach($property['score_type'] as $key => $value) { ?>
								<?php if(in_array($key, [3,9,13])){echo '</p><p class="rowList">';} ?>
								<span class="radio size02">
									<input type="radio" name="score_type" id="record<?=$key?>" <?php if($referee->checkScoreType($key)) echo 'checked="checked"'?> value="<?=$key?>"/>
									<label for="record<?=$key?>"><?=$value?></label>
								</span>
							<?php } ?>
							</p>
							
						</td>
					</tr>
				</table>
				<ul class="buttonList clearfix">
					<li><a href="/gymnastics/admin/organization/referee?gid=<?=$referee->getGame()->getId()?>" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton">
						<input type="submit" value="変更する" class="buttonGeneral hover" id="changeBtn" />
					</li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
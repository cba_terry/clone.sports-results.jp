<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '学校一覧',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageGroupList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="#" method="post">
			<div class="headBox clearfix">
				<h2 class="headline1"><?=$game->getStrSex()?> <?=$game->getClass()?> 学校一覧</h2>
				<div class="boxGroup">
					<p class="result">全<?=$schools->count()?>件</p>
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>学校を絞り込む</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo tableReferee">
				<table class="mb30">
					<tr>
						<th class="col01">学校No</th>
						<th>学校名（略称）</th>
						<th class="col05">都道府県</th>
						<th class="col03">団体</th>
						<th class="col03">棄権</th>
						<th class="col03">編集</th>
					</tr>
				<?php
					if ($schools->count()) {
						foreach($schools  as $school) {
							$schoolId = $school->getId();
				?>
					<tr>
						<td class="center"><?=@$school->getSchoolNo()?></td>
						<td><?=@$school->getSchoolNameAb()?></td>
						<td class="center"><?=@$school->getSchoolPrefecture()?></td>
						<td class="center"><?php if (isset($schoolsCheck['flag'][$schoolId])) { echo '&#10004;';} ?></td>
						<td class="center"><?php if ($school->getCancelFlag()) { echo '&#10004;';} ?></td>
						<td class="center">
							<a class="hover" href="/gymnastics/admin/organization/group/edit/<?=@$school->getId()?>?gid=<?=$game->getId()?>">
								<img src="<?=base_url('/img/admin/icon_edit.gif')?>" alt="" />
							</a>
						</td>
					</tr>
				<?php
						} // endforeach
					} else {
				?>
					<tr class="center">
						<td colspan="6">検索結果がありません。</td>
					</tr>
				<?php } // endif ?>
				</table>
			</div>
			<div class="pageLinkWrapper">
				<?=$pagination->create_links();?>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="" method="get">
				<input type="hidden" name="gid" value="<?=$this->input->get('gid')?>">
				<div class="sheet">
					<table>
						<tr>
							<th><label for="id" class="labelCss">ID</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('school_no', $this->input->get('school_no')); ?>" class="size01" id="school_no" name="school_no" /></td>
						</tr>
						<tr>
							<th><label for="referee" class="labelCss">学校名</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('school_name', $this->input->get('school_name')); ?>" class="size03" id="school_name" name="school_name" /></td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<?php $this->load->view("includes/admin/footer"); ?>
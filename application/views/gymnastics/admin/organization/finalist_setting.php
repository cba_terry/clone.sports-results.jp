<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '決勝枠組み設定',
		'css'    => 'jquery-ui',
		'js'     => 'finalist',
		'pageId' => 'pageFinalistSetting'
	));
?>
<!-- /#header -->
	<div id="contents" class="clearfix">
		<div id="main">
			<form action="" class="userForm" method="post">
				<div class="headBox headBox01 clearfix">
					<h2 class="headline1">決勝の枠組み設定</h2>
					<ul class="groupList mt0">
						<?php for($i = 1; $i <= $numberTeam; $i++) { ?>
						<li class="radio"><input id="group0<?=$i?>" type="radio" name="team" <?php if($team == $i) echo 'checked="checked"'?> value="<?=$i?>"><label for="group01"><?=$i?>班</label></li>
						<?php } ?>
						<li class="endRadio"><input type="submit" class="buttonCustom hover" id="selectTeam" value="切替"></li>
						<li><a href="javascript:void(0)" id="addTeam" class="addButton buttonCustom openModal hover" style="opacity: 1;"><span>班を追加</span></a></li>
					</ul>
				</div>
				<ul class="finalList clearfix">
					<?php if(count($setting)) {?>
					<?php foreach ($setting as $kgroup => $groups) {  ?>
					<li class="tableInfo">
						<table>
							<tr>
								<th colspan="2"><span class="checkbox"><input type="checkbox" class="removeSelect" id="group0<?=$kgroup?>" name="group"><label for="group0<?=$kgroup?>">&nbsp;</label></span><span class="textBox"><?=$kgroup?>組</span></th>
							</tr>
							<?php foreach($groups as $heat => $set) { ?>
							<tr>
								<td class="col01">
									<p class="select size01">
										<select name="data[<?=$kgroup.']['.$heat?>][type]" class="select_type" id="select_type_<?=$kgroup.'_'.$heat?>">
											<option <?=setSelectFinalSetting($set['type'], '団体')?> value="団体">団体</option>
											<option <?=setSelectFinalSetting($set['type'], '個人')?> value="個人">個人</option>
											<option <?=setSelectFinalSetting($set['type'], '種目別')?> value="種目別">種目</option>
										</select>
									</p>
								</td>
								<td>
									<ul class="selectList clearfix">
										<li><p class="select size02">
											<select name="data[<?=$kgroup.']['.$heat?>][class]" class="select_class" id="select_class_<?=$kgroup.'_'.$heat?>" data-value="<?=$set['class']?>">
											</select>
										</p></li>
										<li><p class="select size01">
											<select name="data[<?=$kgroup.']['.$heat?>][rank]" class="select_rank" id="select_rank_<?=$kgroup.'_'.$heat?>">
												<?php for($k=1; $k<=40; $k++){?>
												<option <?=setSelectFinalSetting($set['rank'], $k)?> value="<?=$k?>位"><?=$k?>位</option>
												<?php } ?>
											</select>
										</p></li>
									</ul>
								</td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan="2" class="addButton"><a class="hover"><span>セットを追加</span></a></td>
							</tr>
						</table>
					</li>
					<?php } ?>
					<?php }else { ?>
					<?php $this->load->view("includes/admin/final_setting")?>
					<?php } ?>
					<li class="addBox"><a href="javascript:void(0)"><span>組を追加</span></a></li>
					<li class="tableInfo dummyGroup">
						<table>
							<tr>
								<th colspan="2"><span class="checkbox"><input type="checkbox" class="removeSelect" id="group01" name="group"><label for="group01">&nbsp;</label></span><span class="textBox"></span></th>
							</tr>
							<tr class="dummyHeat">
								<td class="col01">
									<p class="select size01">
										<select name="" class="select_type" id="select_type_">
											<option value="団体">団体</option>
											<option value="個人">個人</option>
											<option value="種目別">種目</option>
										</select>
									</p>
								</td>
								<td>
									<ul class="selectList clearfix">
										<li><p class="select size02">
											<select name="" class="select_class" id="select_class_">
											</select>
										</p></li>
										<li><p class="select size01">
											<select name="" class="select_rank" id="select_rank_">
												<?php for($k=1; $k<=40; $k++){?>
												<option value="<?=$k?>位"><?=$k?>位</option>
												<?php } ?>
											</select>
										</p></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="addButton"><a class="hover"><span>セットを追加</span></a></td>
							</tr>
						</table>
					</li>
				</ul>
				<ul class="buttonList clearfix">
					<li><a href="javascript:history.back()" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton">
						<input type="submit" value="班を削除する" class="buttonGeneral buttonGeneral02 hover" id="deleteBtn" style="opacity: 1;">
						<input type="submit" value="チェックした組を削除する" class="buttonGeneral buttonGeneral02 ml5 hover" id="removeBtn">
						<input type="submit" value="設定する" class="buttonGeneral hover" id="changeBtn">
					</li>
				</ul>
			</form>
		</div>
		<!-- /#main --> 
	</div>
	<!-- /#contents -->
	<div id="footer">
		<p id="copyright">&copy; 全国高等学校体育連盟体操専門部</p>
	</div>
	<!-- /#footer --> 
</div>
<!-- /#wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		<?php 
		$items = [];
		foreach ($game->getItems() as $item) {
				$items[] = $item->getName();
			}
		?>

		<?php
			$games = $tournament->getGames()->filter(function($g) use ($game) {
				return $g->getSex() === $game->getSex() && ($g->getClass() == '予選A' || $g->getClass() == '予選B');
			});
		?>

		var game_type = ['団体','個人','種目別'];
		<?php if($games->count() == 2) {?>
		var class_type = ['予選A', '予選B'];
		<?php }else {?>
		var class_type = ['予選'];
		<?php } ?>
		var item_type = ['<?=implode('\',\'', $items)?>'];

		$('body').delegate('.select_type','change', function(e){
			var sval = $(this).val();

			var select_class = $(this).parents('td').next().find('.select_class');
			var options = '';

			if(sval == '団体' || sval == '個人'){

				$(class_type).each(function(k,v){
					options += '<option value="'+v+'_'+sval+'">'+v+sval+'</option>';
				});
			}
			else {
				$(class_type).each(function(k,v){

					$(item_type).each(function(k2,v2){

						options += '<option value="'+v+'_'+v2+'">'+v+' 種目別 '+v2+'</option>';
					});
				});
			}

			select_class.html(options);
		});

		$('.select_type').each(function(){

			var sval = $(this).val();

			var select_class = $(this).parents('td').next().find('.select_class');
			var options = '';

			if(sval == '団体' || sval == '個人'){

				$(class_type).each(function(k,v){

					var selected = '';

					if(select_class.data('value') == v) selected = 'selected="selected"';

					options += '<option value="'+v+'_'+sval+'" '+selected+'>'+v+sval+'</option>';
				});
			}
			else {
				$(class_type).each(function(k,v){

					$(item_type).each(function(k2,v2){

						options += '<option value="'+v+'_'+v2+'">'+v+' 種目別 '+v2+'</option>';
					});
				});
			}

			select_class.html(options);

			select_class.val(select_class.data('value'));
		});

		$('.addBox').click(function(e){
			var dummy = $('.dummyGroup').clone();

			var nextKey = $('.tableInfo').length;

			dummy.find('th .textBox').text(nextKey+'組');

			$(dummy.find('.select_type')).each(function(k,v){
				var orgId = $(this).attr('id');
				var newId = orgId + nextKey + '_' + (k+1);
				var iName = 'data['+nextKey+']['+(k+1)+'][type]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			$(dummy.find('.select_class')).each(function(k,v){
				var orgId = $(this).attr('id');
				var newId = orgId + nextKey + '_' + (k+1);
				var iName = 'data['+nextKey+']['+(k+1)+'][class]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			$(dummy.find('.select_rank')).each(function(k,v){
				var orgId = $(this).attr('id');
				var newId = orgId + nextKey + '_' + (k+1);
				var iName = 'data['+nextKey+']['+(k+1)+'][rank]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			dummy.removeClass('dummyGroup');
			dummy.find('.dummyHeat').removeClass('dummyHeat');
			
			dummy.insertBefore('.addBox');

		});

		$('#addTeam').click(function(e){

			var dummy = $($('.groupList .radio').get(0)).clone();
			var nextKey = $('.groupList .radio').length+1;

			dummy.find('label').text(nextKey+'組');
			dummy.find('label').attr('for', 'group0'+nextKey);
			dummy.find('input').attr('value', nextKey);
			dummy.find('input').attr('id','group0'+nextKey);
			dummy.find('input').attr('checked',false);

			dummy.insertBefore('.endRadio');
		});

		$('body').delegate('.addButton', 'click', function(e){
			var dummy = $('.dummyHeat').clone();

			var nextKey = $(this).parents('.tableInfo').find('tr').length-1;
			var currBoxIndex = $(this).parents('.tableInfo').find('th .textBox').text().substring(0, 1);
			
			dummy.removeClass('dummyHeat');

			$(dummy.find('.select_type')).each(function(k,v){
				var orgId = $(this).attr('id').substring(0,12);;
				var newId = orgId + currBoxIndex + '_' + nextKey;
				var iName = 'data['+currBoxIndex+']['+nextKey+'][type]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			$(dummy.find('.select_class')).each(function(k,v){
				var orgId = $(this).attr('id').substring(0,13);;
				var newId = orgId + currBoxIndex + '_' + nextKey;
				var iName = 'data['+currBoxIndex+']['+nextKey+'][class]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			$(dummy.find('.select_rank')).each(function(k,v){
				var orgId = $(this).attr('id').substring(0,12);;
				var newId = orgId + currBoxIndex + '_' + nextKey;
				var iName = 'data['+currBoxIndex+']['+nextKey+'][rank]';
				$(this).attr('id', newId);
				$(this).attr('name', iName);
			});

			//console.log(dummy);
			
			dummy.insertBefore($(this).parents('tr'));

		});

		$('body').delegate('#removeBtn','click',function(e){
			
			e.preventDefault();

			$('.removeSelect').each(function(e){

				if($(this).attr('checked') == 'checked')
				{
					$(this).parents('.tableInfo').remove();
				}
			});

			// reindex
			for (var i = 0; i < $('.tableInfo').length; i++) {

				var currentElm = $('.tableInfo').get(i);

				var nextKey = i+1;

				$(currentElm).find('th .textBox').text(nextKey+'組');

				$(currentElm).find('.select_type').each(function(k,v){
					var orgId = $(this).attr('id').substring(0,12);
					var newId = orgId + nextKey + '_' + (k+1);
					var iName = 'data['+nextKey+']['+(k+1)+'][type]';
					$(this).attr('id', newId);
					$(this).attr('name', iName);
				});

				$(currentElm).find('.select_class').each(function(k,v){
					var orgId = $(this).attr('id').substring(0,13);
					var newId = orgId + nextKey + '_' + (k+1);
					var iName = 'data['+nextKey+']['+(k+1)+'][class]';
					$(this).attr('id', newId);
					$(this).attr('name', iName);
				});

				$(currentElm).find('.select_rank').each(function(k,v){
					var orgId = $(this).attr('id').substring(0,12);
					var newId = orgId + nextKey + '_' + (k+1);
					var iName = 'data['+nextKey+']['+(k+1)+'][rank]';
					$(this).attr('id', newId);
					$(this).attr('name', iName);
				});
			}
		});

		$('#selectTeam').click(function(e){
			e.preventDefault();

			var uri = location.href;

			var team = $('.radio input:checked').val();

			uri = uri.replace(/&team=\d+/g, "");

			uri += '&team=' + team;

			window.location.href = uri;
		})
	});
</script>
<style type="text/css">
	.dummyGroup
	{
		display: none;
	}
</style>
</body>
</html>
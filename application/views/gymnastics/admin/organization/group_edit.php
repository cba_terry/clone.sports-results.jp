<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '学校編集',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageRefereeEdit'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2"><?=$game->getStrSex()?> <?=$game->getClass()?> 学校編集</h2>
		</div>
		<form action="" class="userForm" method="post">
			<div class="tableStyle">
				<table>
					<tr>
						<th>学校No</th>
						<td><?=$school->getSchoolNo();?></td>
					</tr>
					<tr>
						<th><label for="school_name">学校名</label></th>
						<td><input class="size04" name="school_name" value="<?=$school->getSchoolName();?>" type="text" /></td>
					</tr>
					<tr>
						<th><label for="school_name">学校名（略称）</label></th>
						<td><input class="size04" name="school_name_ab" value="<?=$school->getSchoolNameAb();?>" type="text" /></td>
					</tr>
					<tr>
						<th><label for="school_prefecture">都道府県</label></th>
						<td>
							<p class="select size02">
								<select name="school_prefecture" class="selectStyle">
								<?php
									if (isset($this->config->item('property')['prefectures'])) {
										foreach ($this->config->item('property')['prefectures'] as $prefecture) {
											$is_selected = preg_match('/'.$school->getSchoolPrefecture().'/', $prefecture) ? ' selected' : '';
											echo "<option value=\"{$prefecture}\"{$is_selected}>{$prefecture}</option>";
										}
									}
								?>
								</select>
							</p>
						</td>
					</tr>
					<tr>
						<th>団体</th>
						<td>
							<span class="checkbox">
								<label for="organization"><?=convert_game_type($checkFlag)?></label>
							</span>
						</td>
					</tr>
					<tr>
						<th>棄権	</th>
						<td>
							<span class="checkbox">
								<input type="checkbox" name="flag_cancel" id="abstention" <?php if($school->getCancelFlag()) {echo 'checked="checked"';} ?>/>
								<label for="abstention">棄権	</label>
							</span>
						</td>
					</tr>
				</table>
				<ul class="buttonList clearfix">
					<li><a href="/gymnastics/admin/organization/group?gid=<?=$game->getId()?>" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton"><input type="submit" value="変更する" class="buttonGeneral hover" id="changeBtn" /></li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => $title,
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageLogin'
	));
?>
<div id="contents" class="clearfix">
	<div class="loginBlock">
		<form action="" id="loginForm" class="transform loginForm" method="post">
			<h1 class="headline1"><img src="<?=base_url('/img/admin/logo.gif')?>" alt="logo" /></h1>
			<?php echo validation_errors(); ?>
			<?php echo @$error; ?>
			<div class="boxLogin clearfix">
				<p class="textBox">
					<input type="text" class="user" placeholder="Username" id="username" name="username" value="<?php echo set_value('username'); ?>" /><input type="password" placeholder="Password" id="password" name="password" />
				</p>
				<input type="submit" id="button" class="buttonCustom hover" value="Login" />
				<p class="remember"><input type="checkbox" id="checkbox" name="remember-me" value="1" /><label for="checkbox">パスワードを記憶する</label></p>
				<p class="forget"><a href="#">パスワードを忘れた</a></p>
			</div>
		</form>
	</div>
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
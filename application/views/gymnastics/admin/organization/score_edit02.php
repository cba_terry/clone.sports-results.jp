<?php
$this->load->view("includes/admin/header", array(
		'title'  => '全審判得点確認',
		'css'    => '',
		'js'     => 'score02',
		'pageId' => 'pageScoreEdit'
	));
	
	$items = $game->getItems();
?>
<!-- /#header -->
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox headBox01 clearfix">
			<h2 class="headline1"><?php echo gender($game->getSex()).' '.$game->getClass(); ?><br />
				<?php echo $player->getGroup().'班'.$player->getHeat().'組 '.$player->getPlayerNo().' '.$player->getPlayerName().'('.$player->getSchoolNameAb().')'; ?>:<br>
<?php echo $item->getName(); ?></h2>
			<form action="" method="get">
				<input type="hidden" name="player_id" value="<?=$player->getId()?>"></input>
				<input type="hidden" name="ref" value="<?=$this->input->get('ref')?>"></input>
				<ul class="groupList">
					<?php foreach ($items as $key => $oitem): ?>
					<li class="radio"><input id="event0<?=$key?>" type="radio" name="item" <?php if($item->getName() == $oitem->getName()){ ?>checked="checked"<?php } ?> value="<?=$oitem->getName()?>"><label for="event0<?=$key?>"><?=$oitem->getName()?></label></li>	
					<?php endforeach ?>
					<li><input type="submit" class="buttonCustom hover" value="切替"></li>
				</ul>
			</form>
		</div>
		<?php $this->load->view("includes/share/form_score_edit_02"); ?>
	</div>
	<!-- /#main --> 
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>

<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '全種目試合一覧',
		'css'    => 'jquery-ui',
		'js'     => 'modal|jquery-ui.min|jquery.ui.datepicker-ja.min|update_status|renewal',
		'pageId' => 'pageGameList'
	));
	$group = ($this->input->get('group')) ? $this->input->get('group') : '1';
	$heat = ($this->input->get('heat')) ? $this->input->get('heat') : -1;
?>
<!-- /#header -->
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline1"><?php echo $game->getStrSex(), ' ', $game->getClass(); ?></h2>
			<ul class="btnLinkList clearfix">
				<li><a href="javascript:void(0)" class="active">全種目一覧</a></li>
				<?php foreach ($items as $item) {?>
				<li><a <?php if($item->getName()=='段違い平行棒') {echo 'style="width:180px"';}?> href="/gymnastics/admin/organization/game/item?gid=<?=$game->getId()?>&item=<?=$item->getName()?>"><?=$item?>一覧</a></li>
				<?php } ?>
			</ul>
		</div>
		<!-- /.headBox -->
		<form action="" method="get" class="searchForm" id="form_toolbar">
			<input type="hidden" name="gid" value="<?=$game->getId()?>"></input>
			<input type="hidden" name="orderby" value="<?php echo $this->input->get('orderby'); ?>">
			<div class="wrapGroupList clearfix">
				<ul class="groupList">
					<li class="select size01">
						<select name="group" id="group_search">
							<?php foreach ($groups as $key => $g) { ?>
							<option <?php if($g['group'] == $group){ ?>selected="selected"<?php } ?> value="<?=$g['group']?>"><?=$g['group']?>班</option>
							<?php } ?>
						</select>
					</li>
					<li class="radio">
						<input id="heat_all" type="radio" name="heat" value="0" <?php if(!$this->input->get('heat')){echo 'checked="checked"';} ?>>
						<label for="heat_all">全組</label>
					</li>
					<li><input type="submit" class="buttonCustom hover" value="切替" style="opacity: 1;"></li>
				</ul>
				<div class="boxGroup">
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>検索する</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<input type="hidden" name="prefecture" value="<?php echo $this->input->get('prefecture'); ?>">
			<input type="hidden" name="school_name" value="<?php echo $this->input->get('school_name'); ?>">
			<input type="hidden" name="player_name" value="<?php echo $this->input->get('player_name'); ?>">
		</form>
		<div class="filterBox">
			<p>
				<span><label for="publish01">チェックを入れた試合の</label></span>
				<span class="select size01">
					<select name="all_status" id="publish01">
						<option value="ステータスを変更する">ステータスを変更する</option>
						<?php foreach ($score_status as $key => $status) {?>
						<option value="<?=$key?>"><?=$status?></option>
						<?php } ?>
					</select>
				</span>
				<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
			</p>
			<p>
				<span class="status"><label>表示順</label></span>
				<span class="select size02">
					<select name="orderby" class="elm-orderby">
						<option value="1"<?php if ($this->input->get('orderby') != 2) { echo ' selected'; } ?>>登録順</option>
						<option value="2"<?php if ($this->input->get('orderby') == 2) { echo ' selected'; } ?>>試技順</option>
					</select>
				</span>
			</p>
		</div>
		<div class="tableInfo">
			<table class="tableGame">
				<tr>
					<th class="col01"><span class="checkbox"><input type="checkbox" id="checkAll" name="checkAll" /><label for="checkAll">&nbsp;</label></span></th>
					<th class="col02">試技順</th>
					<th class="col03">No</th>
					<th class="col04">選手名</th>
					<th class="col05">学校名</th>
					<?php foreach ($items as $item) {
					echo '<th class="col06">'.$item.'</th>';
					} ?>
					<th class="col07">合計</th>
					<th class="col08">ステータス</th>
					<th width="60px">得点<br>編集</th>
				</tr>
				<?php if(!empty($groupHeatPlayers)):
				foreach ($groupHeatPlayers as $pGroup){
				foreach ($pGroup->getHeats() as $pHeat){

				// tach players trong head thanh 2 loai, choi cho group va choi cho head
				$playsAsGroup  = $pHeat->getPlayersPlayAsGroup();
				$playsAsSingle = $pHeat->getPlayersPlayAsSingle();

				foreach ($playsAsGroup as $player){ ?>
				<tr>
					<td class="col01">
						<span class="checkbox">
							<input type="checkbox" id="check01" name="check" value="<?=$player->getId()?>"/>
							<label for="check01">&nbsp;</label>
						</span>
					</td>
					<td></td>
					<td><?=$player->getPlayerNo()?></td>
					<td class="col02"><?=$player->getPlayerName()?></td>
					<td class="col02"><?=$player->getSchoolNameAb()?></td>
					<?php foreach ($items as $item):?>
					<td class="point"><span><?=formatScore($player->getItemScoreValue($item, 'FinalScore'))?></span>
						<em>D <?=formatScore($player->getItemScoreValue($item, 'DScore'))?><br />
						E <?=formatScore($player->getItemScoreValue($item, 'EScore'))?><br />
						減 -<?=formatScore($player->getItemScoreValue($item, 'DemeritScore'))?></em>
					</td>
					<?php endforeach ?>
					<td class="point"><span><?=$player->getTotalFinalScore()?></span><em>D <?=$player->getTotalDScore()?><br />E <?=$player->getTotalEScore()?>
						<br />減 -<?=$player->getTotalDemeritScore()?></em></td>
					<td class="col03 buttonGroup">
						<span class="radio">
							<input type="radio" id="not<?php echo $player->getId(); ?>" class="not" value="0" name="status[<?=$player->getId()?>]" <?php if (!$player->getSingleTotalScoreStatus()) echo 'checked'; ?> />
							<label for="not<?php echo $player->getId(); ?>">非公開</label>
						</span>
						<span class="radio">
							<input type="radio" id="public<?php echo $player->getId(); ?>" class="public" value="2" name="status[<?=$player->getId()?>]" <?php if ($player->getSingleTotalScoreStatus() == 2) echo 'checked'; ?> />
							<label for="public<?php echo $player->getId(); ?>">公開</label>
						</span>
						<a class="buttonCustom hover update_status" data-id="<?=$player->getId()?>" href="javascript:void(0)">更新</a>
					</td>
					<td class="col03">
						<a class="buttonCustom hover" href="/gymnastics/admin/organization/score_edit?player_id=<?=$player->getId()?>&ref=all">編集</a>
					</td>
				</tr>
				<?php } // end loop play as group
				//Display group row
				if ($pHeat->hasGroupSchoolPlay()){ ?>
				<tr class="active03">
					<td colspan="5">チーム得点<br>(上位3名計)</td>
					<?php foreach ($items as $item): ?>
					<td class="point"><span><?=formatScore($pHeat->getSchoolScoreItem($item, true),2)?></span></td>
					<?php endforeach; ?>
					<td class="point"><span><?=formatScore($pHeat->getSchoolScoreTotal(true),2)?></span></td>
					<td class="point br0"><span>-<?=formatScore($pHeat->getSchoolItemDemeritScore($item),2)?></span></td>
					<td class="col03 bl0"><a class="buttonCustom hover" href="/gymnastics/admin/organization/team_demerit_edit?school=<?=$pHeat->findSchoolGroup()->getId()?>&group=<?=$pGroup->getId()?>&heat=<?=$pHeat->getId()?>" style="opacity: 1;">編集</a></td>
				</tr>
				<?php } // end group row display
				foreach ($playsAsSingle as $player){ ?>
				<tr>
					<td class="col01">
						<span class="checkbox">
							<input type="checkbox" id="check01" name="check" value="<?=$player->getId()?>"/>
							<label for="check01">&nbsp;</label>
						</span>
					</td>
					<td></td>
					<td><?=$player->getPlayerNo()?></td>
					<td class="col02"><?=$player->getPlayerName()?></td>
					<td class="col02"><?=$player->getSchoolNameAb()?></td>
					<?php foreach ($items as $item):?>
					<td class="point"><span><?=formatScore($player->getItemScoreValue($item, 'FinalScore'))?></span>
						<em>D <?=formatScore($player->getItemScoreValue($item, 'DScore'))?><br />
						E <?=formatScore($player->getItemScoreValue($item, 'EScore'))?><br />
						減 -<?=formatScore($player->getItemScoreValue($item, 'DemeritScore'))?></em>
					</td>
					<?php endforeach ?>
					<td class="point"><span><?=$player->getTotalFinalScore()?></span><em>D <?=$player->getTotalDScore()?><br />E <?=$player->getTotalEScore()?>
						<br />減 -<?=$player->getTotalDemeritScore()?></em></td>
					<td class="col03 buttonGroup">
						<span class="radio">
							<input type="radio" id="not<?php echo $player->getId(); ?>" class="not" value="0" name="status[<?=$player->getId()?>]" <?php if ($player->isTotalScoreUnPublished()) echo 'checked'; ?> />
							<label for="not<?php echo $player->getId(); ?>">非公開</label>
						</span>
						<span class="radio">
							<input type="radio" id="public<?php echo $player->getId(); ?>" class="public" value="2" name="status[<?=$player->getId()?>]" <?php if ($player->isTotalScorePublished()) echo 'checked'; ?> />
							<label for="public<?php echo $player->getId(); ?>">公開</label>
						</span>
						<a class="buttonCustom hover update_status" data-id="<?=$player->getId()?>" href="javascript:void(0)">更新</a>
					</td>
					<td class="col03">
						<a class="buttonCustom hover" href="/gymnastics/admin/organization/score_edit?player_id=<?=$player->getId()?>&ref=all">編集</a>
					</td>
				</tr>
				<?php }
				// end loop play as single
				}} ?>
			<?php else: ?>
			<tr><td colspan="<?php if ($game->getSex()==1) echo 15; else echo 13; ?>">検索結果がありません。</td></tr>
			<?php endif; ?>
			</table>
		</div>
		<div class="filterBox">
			<p>
				<span><label for="publish01">チェックを入れた試合の</label></span>
				<span class="select size01">
					<select name="all_status" id="publish02">
						<option value="ステータスを変更する">ステータスを変更する</option>
						<?php foreach ($score_status as $key => $status) {?>
						<option value="<?=$key?>"><?=$status?></option>
						<?php } ?>
					</select>
				</span>
				<span class="btnUpdate"><input class="buttonCustom hover btn_update_all" type="button" value="更新" /></span>
			</p>
			<p>
				<span class="status"><label>表示順</label></span>
				<span class="select size02">
					<select name="orderby" class="elm-orderby">
						<option value="1"<?php if ($this->input->get('orderby') != 2) { echo ' selected'; } ?>>登録順</option>
						<option value="2"<?php if ($this->input->get('orderby') == 2) { echo ' selected'; } ?>>試技順</option>
					</select>
				</span>
			</p>
		</div>
		<p class="buttonBack"><a href="/gymnastics/admin/organization/game?gid=<?=$game->getId()?>" class="buttonStyle hover">戻る</a></p>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="" method="get" id="form_modal">
				<input type="hidden" name="gid" value="<?=$game->getId()?>"></input>
				<input type="hidden" name="orderby" value="<?php echo $this->input->get('orderby'); ?>">
				<input type="hidden" name="group" value="<?php echo $this->input->get('group'); ?>">
				<input type="hidden" name="heat" value="<?php echo $this->input->get('heat'); ?>">
				<div class="sheet">
					<table>
						<tbody><tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="prefectures" class="labelCss">都道府県</label>
										<p class="select size02">
											<select name="prefecture" id="prefectures">
												<option value="">選択</option>
												<?php
												array_map(function($name){echo '<option value="'.$name.'">'.$name.'</option>';}, $groupHeatPlayers->getListPrefectureName());
												?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="school_name" class="labelCss">学校名</label>
										<p class="select size03">
											<select name="school_name" id="school_name">
												<option value="">選択</option>
												<?php
												array_map(function($name){echo '<option value="'.$name.'">'.$name.'</option>';}, $groupHeatPlayers->getListSchoolName());
												?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li>
										<label for="player_name" class="labelCss">選手名</label>
										<p class="select size03">
											<select name="player_name" id="player_name">
												<option value="">選択</option>
												<?php
												array_map(function($name){echo '<option value="'.$name.'">'.$name.'</option>';}, $groupHeatPlayers->getListPlayerName());
												?>
											</select>
										</p>
									</li>
								</ul>
							</td>
						</tr>
					</tbody></table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="button" value="リセット" class="hover btn-reset" style="opacity: 1;"></p>
					<p class="btnSubmit"><input type="submit" value="検索" class="hover" style="opacity: 1;"></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	function update_status(ids, status)
	{
		$.ajax({
			url:"/gymnastics/admin/organization/game/update_status_total?gid=<?=$game->getId()?>",
			type:"POST",
			data:{ ids: ids, status: status},
			dataType:'json',
			success:function(response){
				if(response.status == 'success'){
					location.reload();
				}
			}
		});
	}

	$(document).ready(function(){

		var game = '<?=$game->getId()?>';
		var group = '<?=$group?>';
		get_heat_by_group(game, group);

		$('#group_search').change(function(){
			var game = <?=$this->input->get("gid")?>;
			var group = $(this).val();
			get_heat_by_group(game, group);
		});

		$('body').on('change', '#prefectures', function(e){
			$.ajax({
					url:"/gymnastics/admin/organization/game/filter_options?gid=<?=$game->getId()?>",
					type:"GET",
					data:{type:'pref', value:$(this).val(), group:'<?=$group?>', heat:'<?=$heat?>'},
					dataType:'json',
					success:function(response){
						var school_options = '<option value="">選択</option>';
						var player_options = '<option value="">選択</option>';

						$(response.schools).each(function(k,s){
							school_options += '<option value="'+s+'">'+s+'</option>';
						});

						$(response.players).each(function(k,p){
							player_options += '<option value="'+p+'">'+p+'</option>';
						});

						$('#school_name').html(school_options);
						$('#player_name').html(player_options);
					}
				});
		});

		$('body').on('change', '#school_name', function(e){
			$.ajax({
					url:"/gymnastics/admin/organization/game/filter_options?gid=<?=$game->getId()?>",
					type:"GET",
					data:{type:'school', value:$(this).val(), group:'<?=$group?>', heat:'<?=$heat?>'},
					dataType:'json',
					success:function(response){
						var player_options = '<option value="">選択</option>';

						$(response.players).each(function(k,p){
							player_options += '<option value="'+p+'">'+p+'</option>';
						});

						$('#player_name').html(player_options);
					}
				});
		});

		$('.btnSubmit input').click(function(e){

			e.preventDefault();

			if($('#prefectures').val() != '' || $('#school_name').val() != '' || $('#player_name').val() != '')
			{
				$.ajax({
					url:"/gymnastics/admin/organization/game/filter?" + $('#form_modal').serialize(),
					type:"GET",
					data:{},
					dataType:'html',
					success:function(response){
						$('.tableInfo').html(response);
					}
				});

				$('.modal, .overlay').fadeOut();
			}else{
				location.href = '/gymnastics/admin/organization/game/all?gid=<?=$game->getId()?>';
			}

			return false;
		})
	});

	function get_heat_by_group(game, group)
	{
		var heatSelected = <?=$heat?>

		$.ajax({
			url:"/gymnastics/ajax/getheats",
			type:"POST",
			data:{game: game, group : group},
			dataType:'json',
			success:function(response){

				// remove old heat radio
				$('.groupList  li.heat').remove();

				var html = '';

				$(response.heats).each(function(k, v){
					checked = '';
					if(heatSelected == v) checked='checked="checked"';
					html += '<li class="radio heat"><input id="heat'+v+'" type="radio" name="heat"  value="'+v+'" '+checked+'><label for="heat'+v+'">'+v+'組</label></li>';
				});

				$('.groupList li.radio').after(html);
			}
		});
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>

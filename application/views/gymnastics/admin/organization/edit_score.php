<?php
	$this->load->view("includes/admin/header", array(
		'title'  => $title,
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageEditScore'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline2">男子 予選A<br />
				<?php
					if (anable_box('tag-box')) {
						echo '1班1組 0000 斎藤真希(鶴岡東) : 跳馬';
					} else {
						echo '1班1組 0000 佐藤登(鶴岡東) : 種目1';
					}
				?>
			</h2>
		</div>
		<form action="#" class="userForm" method="post">
			<div class="tabArea">
				<?php if (anable_box('tag-box')) : ?>
				<ul class="tab clearfix tab01">
					<li class="active"><a href="#tab01">1回目</a></li>
					<li><a href="#tab02">2回目B</a></li>
				</ul>
				<?php endif; ?>
				<!-- /.tabList -->
				<div class="tabContents">
					<div id="tab01" class="tabBox">
						<div class="tableStyle tableStyle01">
							<table>
								<tr>
									<th><span>D</span></th>
									<td><ul class="scoreList">
											<li>
												<label for="scoreD01">D1</label>
												<span class="size07">
												<input type="text" name="scoreD01" class="size07" id="scoreD01" value="00.00" />
												</span> </li>
											<li>
												<label for="scoreD02">D2</label>
												<span class="size07">
												<input type="text" name="scoreD02" class="size07" id="scoreD02" value="15.00" />
												</span> </li>
											<li>
												<label for="scoreD03">D3</label>
												<span class="size07">
												<input type="text" name="scoreD03" class="size07" id="scoreD03" value="00.00" />
												</span> </li>
											<li>
												<label for="scoreD04">D4</label>
												<span class="size07">
												<input type="text" name="scoreD04" class="size07" id="scoreD04" value="00.00" />
												</span> </li>
										</ul></td>
								</tr>
								<tr>
									<th><span>E</span></th>
									<td><ul class="scoreList">
											<li>
												<label for="scoreE01">E1</label>
												<span class="size07">
												<input type="text" name="scoreE01" class="size07" id="scoreE01" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE02">E2</label>
												<span class="size07">
												<input type="text" name="scoreE02" class="size07" id="scoreE02" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE03">E3</label>
												<span class="size07">
												<input type="text" name="scoreE03" class="size07" id="scoreE03" value="10.00" />
												</span></li>
											<li>
												<label for="scoreE04">E4</label>
												<span class="size07">
												<input type="text" name="scoreE04" class="size07" id="scoreE04" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE05">E5</label>
												<span class="size07">
												<input type="text" name="scoreE05" class="size07" id="scoreE05" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE06">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="scoreE06" class="size07" id="scoreE06" value="0.00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>時間</th>
									<td><ul class="scoreList scoreList01">
											<li>
												<label for="time0101">タイム1</label>
												<span class="size08">
												<input type="text" name="time0101" class="size08" id="time0101" value="00" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time0102" class="size08" id="time0102" value="00" />
												</span></li>
											<li>
												<label for="time0201">タイム2</label>
												<span class="size08">
												<input type="text" name="time0201" class="size08" id="time0201" value="00" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time0202" class="size08" id="time0202" value="00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>ライン</th>
									<td><ul class="scoreList">
											<li>
												<label for="line0101">ライン1</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line0101" class="size07" id="line0101" value="0.00" />
												</span></li>
											<li>
												<label for="line0201">ライン2</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line0201" class="size07" id="line0201" value="0.00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>その他減点</th>
									<td><em class="other">-</em><span class="size07">
										<input type="text" name="other" class="size07" id="other" value="0.00" />
										</span></td>
								</tr>
							</table>
						</div>
						<div class="tableInfo">
							<table class="tableGeneral">
								<tr>
									<th>D合計</th>
									<th>E合計</th>
									<th>減点</th>
									<th>合計</th>
								</tr>
								<tr>
									<td>00.00</td>
									<td>00.00</td>
									<td>-00.0</td>
									<td>000.00</td>
								</tr>
							</table>
						</div>
					</div>
					<!-- tab01 -->
					<?php if (anable_box('tag-box')) : ?>
					<div id="tab02" class="tabBox">
						<div class="tableStyle tableStyle01">
							<table>
								<tr>
									<th><span>D</span></th>
									<td><ul class="scoreList">
											<li>
												<label for="scoreD01">D1</label>
												<span class="size07">
												<input type="text" name="scoreD0101" class="size07" id="scoreD0101" value="00.00" />
												</span> </li>
											<li>
												<label for="scoreD0201">D2</label>
												<span class="size07">
												<input type="text" name="scoreD0201" class="size07" id="scoreD0201" value="15.00" />
												</span> </li>
											<li>
												<label for="scoreD03">D3</label>
												<span class="size07">
												<input type="text" name="scoreD0301" class="size07" id="scoreD0301" value="00.00" />
												</span> </li>
											<li>
												<label for="scoreD04">D4</label>
												<span class="size07">
												<input type="text" name="scoreD0401" class="size07" id="scoreD0401" value="00.00" />
												</span> </li>
										</ul></td>
								</tr>
								<tr>
									<th><span>E</span></th>
									<td><ul class="scoreList">
											<li>
												<label for="scoreE01">E1</label>
												<span class="size07">
												<input type="text" name="scoreE0101" class="size07" id="scoreE0101" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE02">E2</label>
												<span class="size07">
												<input type="text" name="scoreE0201" class="size07" id="scoreE0201" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE03">E3</label>
												<span class="size07">
												<input type="text" name="scoreE0301" class="size07" id="scoreE0301" value="10.00" />
												</span></li>
											<li>
												<label for="scoreE04">E4</label>
												<span class="size07">
												<input type="text" name="scoreE0401" class="size07" id="scoreE0401" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE05">E5</label>
												<span class="size07">
												<input type="text" name="scoreE0501" class="size07" id="scoreE0501" value="00.00" />
												</span></li>
											<li>
												<label for="scoreE06">減点</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="scoreE0601" class="size07" id="scoreE0601" value="0.00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>時間</th>
									<td><ul class="scoreList scoreList01">
											<li>
												<label for="time0101">タイム1</label>
												<span class="size08">
												<input type="text" name="time010101" class="size08" id="time010101" value="00" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time010201" class="size08" id="time010201" value="00" />
												</span></li>
											<li>
												<label for="time0201">タイム2</label>
												<span class="size08">
												<input type="text" name="time020101" class="size08" id="time020101" value="00" />
												</span> <em class="dot">:</em> <span class="size08">
												<input type="text" name="time020201" class="size08" id="time020201" value="00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>ライン</th>
									<td><ul class="scoreList">
											<li>
												<label for="line0101">ライン1</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line010101" class="size07" id="line010101" value="0.00" />
												</span></li>
											<li>
												<label for="line0201">ライン2</label>
												<em class="line">-</em> <span class="size07">
												<input type="text" name="line020101" class="size07" id="line020101" value="0.00" />
												</span></li>
										</ul></td>
								</tr>
								<tr>
									<th>その他減点</th>
									<td><em class="other">-</em><span class="size07">
										<input type="text" name="other01" class="size07" id="other01" value="0.00" />
										</span></td>
								</tr>
							</table>
						</div>
						<div class="tableInfo">
							<table class="tableGeneral">
								<tr>
									<th>D合計</th>
									<th>E合計</th>
									<th>減点</th>
									<th>合計</th>
								</tr>
								<tr>
									<td>00.00</td>
									<td>00.00</td>
									<td>-00.0</td>
									<td>000.00</td>
								</tr>
							</table>
						</div>
					</div>
					<!-- #tag02 -->
					<?php endif; ?>
				</div>
			</div>
			<ul class="buttonList clearfix">
				<li><a href="#" class="buttonStyle hover">戻る</a></li>
				<li class="submitButton">
					<input type="submit" value="更新する" class="buttonGeneral hover" id="changeBtn" />
				</li>
			</ul>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '賞状出力',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageCertificateOutput'
	));
?>
	<div id="contents" class="clearfix">
		<div id="main">
			<div class="headBox clearfix">
				<h2 class="headline1"><?=$game->getStrSex()?> <?=$game->getClass()?> 賞状出力</h2>
			</div>
			<form action="" method="post" class="userForm">
				<div class="tableStyle">
					<table>
						<tr>
							<th>団体</th>
							<td>
								<ul class="selectList clearfix">
									<li>
										<p class="select size03">
											<select name="group[from]" id="organization_place01" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="organization_place01">位</label></p>
									</li>
									<li class="iconBg">
										<p class="select size03">
											<select name="group[to]" id="organization_place02" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="organization_place02">位まで</label></p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<th>個人総合</th>
							<td>
								<ul class="selectList clearfix">
									<li>
										<p class="select size03">
											<select name="single[from]" id="individual_place01" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="individual_place01">位</label></p>
									</li>
									<li class="iconBg">
										<p class="select size03">
											<select name="single[to]" id="individual_place02" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="individual_place02">位まで</label></p>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<th>種目別</th>
							<td>
								<ul class="selectList clearfix">
									<li>
										<p class="select size03">
											<select name="item[from]" id="event_place01" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="event_place01">位</label></p>
									</li>
									<li class="iconBg">
										<p class="select size03">
											<select name="item[to]" id="event_place02" class="selectStyle">
												<?php for($i=1;$i<20;$i++) {?>
												<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
											</select>
										</p>
										<p><label class="labelCss" for="event_place02">位まで</label></p>
									</li>
								</ul>
							</td>
						</tr>
					</table>
					<ul class="buttonList clearfix">
						<li><a href="javascript:void(0)" onclick="window.history.back()" class="buttonStyle hover">戻る</a></li>
						<li class="submitButton"><input type="submit" value="出力する" class="buttonGeneral hover" id="outputBtn" /></li>
					</ul>
				</div>
				<!-- / class section -->
			</form>
		</div>
		<!-- /#main -->
	</div>
	<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>
<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '選手一覧',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pagePlayerList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="#" method="post">
			<div class="headBox clearfix">
				<h2 class="headline1"><?=$game->getStrSex()?> <?=$game->getClass()?> 選手一覧</h2>
				<div class="boxGroup">
				<?php $total_result = count($players)?count($players):0; ?>
					<p class="result">全<?php echo $total_result; ?>件</p>
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>選手を絞り込む</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo tablePlayer">
				<table class="mb30">
					<tr>
						<th class="col04">ID</th>
						<th class="col04">選手No</th>
						<th class="col04">表示順</th>
						<th>選手名</th>
						<th>学校名</th>
						<th>都道府県</th>
						<th class="col04">学年</th>
						<th class="col04">性別</th>
						<th class="col04">個・団</th>
						<th>試合区分</th>
						<th class="col04">班</th>
						<th class="col04">組</th>
						<th class="col04">棄権</th>
						<th class="col04">編集</th>
						<th class="col04">削除</th>
					</tr>
					<?php
						if (count($players)) {
							foreach ($players as $player) {
					?>
					<tr>
						<td><?php echo $player->getId(); ?></td>
						<td><?php echo $player->getPlayerNo(); ?></td>
						<td><?php //echo $player->getOrder(); ?></td>
						<td><?php echo $player->getPlayerName(); ?></td>
						<td><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
						<td><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolPrefecture();?></td>
						<td><?php echo $player->getGrade(); ?>年</td>
						<td><?php echo gender($player->getSex()); ?></td>
						<td><?php echo convert_game_type($player->getFlag()) ?></td>
						<td><?php echo $game->getClass(); ?></td>
						<td><?php echo $player->getGroup(); ?></td>
						<td><?php echo $player->getHeat(); ?></td>
						<td><?php if($player->getFlagCancle() == 1){echo "&#10004;"; }?></td>
						<td><a class="hover" href="/gymnastics/admin/organization/player/edit/<?php echo $player->getId(); ?>"><img src="<?=base_url('/img/admin/icon_edit.gif')?>" alt="" /></a></td>
						<td><a class="buttonRemove hover" href="javascript:void(0)" onclick="delete_player(<?=$player->getId()?>)"><img src="<?=base_url('/img/admin/icon_remove.gif')?>" alt="" /></a></td>
					</tr>
					<?php
							} // endforeach
						} else {
					?>
					<tr>
						<td colspan="15">検索結果がありません。</td>
					</tr>
					<?php } // endif ?>
				</table>
			</div>
			<div class="pageLinkWrapper">
				<?=$pagination->create_links();?>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="" method="get">
				<div class="sheet">
					<input type="hidden" name="gid" value="<?=$game->getId()?>">
					<table>
						<tr><th>&nbsp;</th></tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li class="mr30">
										<label for="player_id" class="labelCss">ID</label>
										<input type="text" value="<?php echo set_value('player_id', $this->input->get('player_id')); ?>" class="size01" id="id" name="player_id" />
									</li>
									<li>
										<label for="player_no" class="labelCss">選手No</label>
										<input type="text" value="<?php echo set_value('player_no', $this->input->get('player_no')); ?>" class="size01" id="player01" name="player_no" />
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<th><label for="player_name" class="labelCss">選手名</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('player_name', $this->input->get('player_name')); ?>" class="size03" id="player02" name="player_name" /></td>
						</tr>
						<tr>
							<th><label for="school_name" class="labelCss">学校名</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('school_name', $this->input->get('school_name')); ?>" class="size03" id="school" name="school_name" /></td>
						</tr>
						<tr><th>&nbsp;</th></tr>
						<tr>
							<td>
								<ul class="selectList clearfix">
									<li class="mr30">
										<label for="year01" class="labelCss">学年</label>
										<div class="chooseBox">
											<p>
												<input type="radio" value="1" name="grade" id="year01" <?php if($this->input->get('grade') == '1') echo 'checked="checked"'; ?> />
												<input type="radio" value="2" name="grade" id="year02" <?php if($this->input->get('grade') == '2') echo 'checked="checked"'; ?> />
												<input type="radio" value="3" name="grade" id="year03" <?php if($this->input->get('grade') == '3') echo 'checked="checked"'; ?> />
											</p>
											<ul class="chooseList clearfix">
												<li><label for="year01" <?php if($this->input->get('grade') == 1) echo 'class="checked"'; ?>>1年</label></li>
												<li><label for="year02" <?php if($this->input->get('grade') == 2) echo 'class="checked"'; ?>>2年</label></li>
												<li class="last"><label for="year03" <?php if($this->input->get('grade') == 3) echo 'class="checked"'; ?>>3年</label></li>
											</ul>
										</div>
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover clearCondition" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	function delete_player(id)
	{
		if(confirm("この審判を削除してもよろしいですか？")) {
			$.post('/gymnastics/admin/organization/player/delete',{
				player_id: id,
			},
			function(data, status){
				if(status == 'success')
					location.reload();
			});
		}
	}
// $(document).ready(function() {
// 	$('.clearCondition').click(function(e) {
// 		e.preventDefault();
// 		$('input[type="text"]').val('');
// 		$('input[type="radio"]').removeAttr('checked');
// 		$('.chooseList li label').removeClass('checked');
// 		$(this).parents('form').submit();
// 	});
// });
</script>
<?php $this->load->view("includes/admin/footer"); ?>
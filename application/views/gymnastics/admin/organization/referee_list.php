<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '審判一覧',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageRefereeList'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<form action="" method="post" id="form_referee_list">
			<input type="hidden" name="is_post" value="1">
			<div class="headBox clearfix">
				<h2 class="headline1"><?=$game->getStrSex()?> <?=$game->getClass()?> 審判一覧</h2>
				<div class="boxGroup">
					<p class="result">全<?=$referees->count()?>件</p>
					<div class="leadBox">
						<ul class="clearfix">
							<li><a class="buttonSearch openModal" href="javascript:void(0)"><span>審判を絞り込む</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.headBox -->
			<div class="tableInfo tableReferee">
				<table class="mb30">
					<tr>
						<th class="col03">ID</th>
						<th>審判名</th>
						<th>パスワード</th>
						<th class="col07">試合区分</th>
						<th class="col04">種目</th>
						<th class="col07">記録種別</th>
						<th class="col03">利用</th>
						<th class="col03">編集</th>
						<th class="col03">削除</th>
					</tr>
				<?php
					if ( $referees->count() ) {
						foreach ($referees as $referee) {
							$name_checkbox  = 'flag_active[' . $referee->getId() . ']';
							$is_flag_active = ((int) $referee->getFlagActive() === 1) ? ' checked' : '';
				?>
					<tr>
						<td class="center"><?=$referee->getId()?></td>
						<td><?=$referee->getRefereeName()?></td>
						<td><?=$referee->getPassword()?></td>
						<td class="center"><?php echo $this->input->get('class')?></td>
						<td><?=$referee->getItem()?></td>
						<td class="center"><?php echo @$this->config->item('property')['score_type'][$referee->getScoreType()]?></td>
						<td class="center">
							<span class="checkbox">
								<input type="checkbox" name="<?=$name_checkbox;?>" value="1" id="<?=$name_checkbox;?>" <?=$is_flag_active;?>>
								<label class="empty" for="<?=$name_checkbox;?>">&nbsp;</label>
							</span>
						</td>
						<td class="center">
							<a class="hover" href="/gymnastics/admin/organization/referee/edit/<?=$referee->getId()?>">
								<img src="<?=base_url('/img/admin/icon_edit.gif')?>" alt="" />
							</a>
						</td>
						<td class="center">
							<a class="buttonRemove hover" href="javascript:void(0)" onclick="delete_referee(<?=$referee->getId()?>)">
								<img src="<?=base_url('/img/admin/icon_remove.gif')?>" alt="" />
							</a>
						</td>
					</tr>
				<?php
						} //endforeach
					} else {
				?>
					<tr>
						<td colspan="9" class="center">検索結果がありません。</td>
					</tr>
				<?php } //endif ?>
				</table>
			</div>
			<p class="buttonBack black"><a href="javascript:void(0);" class="buttonStyle hover btn-flag-active" style="opacity: 1;">チェックした審判を利用する</a></p>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<div class="modal">
	<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
	<div class="modalContent">
		<h3 class="hModal">絞り込み検索</h3>
		<div class="searchBox">
			<form action="" method="get">
				<input type="hidden" name="gid" value="<?=$game->getId()?>">
				<div class="sheet">
					<table>
						<tr>
							<th><label for="id" class="labelCss">ID</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('id', $this->input->get('id')); ?>" class="size01" id="id" name="id" /></td>
						</tr>
						<tr>
							<th><label for="referee" class="labelCss">審判名</label></th>
						</tr>
						<tr>
							<td><input type="text" value="<?php echo set_value('referee_name', $this->input->get('referee_name')); ?>" class="size03" id="referee_name" name="referee_name" /></td>
						</tr>
					</table>
				</div>
				<div class="clearfix">
					<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover" /></p>
					<p class="btnSubmit"><input type="submit" value="検索" class="hover" /></p>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /.modal -->
<script>
	function delete_referee(id)
	{
		if(confirm("この審判を削除してもよろしいですか？")) {
			$.post('/gymnastics/admin/organization/referee/delete',{
				referee_id: id,
			},
			function(data, status){
				if(status == 'success')
					location.reload();
			});
		}
	}
</script>
<?php $this->load->view("includes/admin/footer"); ?>
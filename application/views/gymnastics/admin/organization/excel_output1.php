<?php
	$this->load->view("includes/admin/header", array(
		'title'  => 'Excel出力',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageExcelOutput'
	));
?>
<div id="contents" class="clearfix">
	<div id="main">
		<div class="headBox clearfix">
			<h2 class="headline1"><?=$game->getStrSex()?> <?=$game->getClass()?> Excel出力</h2>
		</div>
		<form action="" class="userForm" method="post">
			<div class="tableStyle excelTable">
				<table>
					<tr>
						<th rowspan="10">出力データ</th>
						<td colspan="3">
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="choose_final" id="choose_final" />
									<label for="choose_final">決勝演技順</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="choose_player_list" id="member" />
									<label for="member">メンバー表</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td class="col01">
							<ul class="inputGroup clearfix">
								<li class="checkbox">
									<input type="checkbox" name="choose_group" id="choose_group" value="1" />
									<label for="choose_group">班別速報</label>
								</li>
								<li>
									<p class="select size11">
										<select name="select_group" id="select_group">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</p>
									<p><label for="number_team01" class="labelCss">班</label></p>
								</li>
							</ul>
						</td>
						<td colspan="2">
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="group_include_score" id="group_include_score" value="1" />
									<label for="group_include_score">D得点、E得点、減点</label>
								</span>
								<span class="checkbox">
									<input type="checkbox" name="group_include_rank" id="group_include_rank" value="1" />
									<label for="group_include_rank">順位</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="choose_school" id="organization01" />
									<label for="organization01">団体順位</label>
								</span>
							</p></div>
						</td>
						<td colspan="2">
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="school_include_rank" id="school_include_rank" />
									<label for="school_include_rank">順位</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="choose_single" id="choose_single" value="1" />
									<label for="choose_single">個人総合順位</label>
								</span>
							</p></div>
						</td>
						<td colspan="2">
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="single_include_2nd_score" id="single_include_2nd_score" value="1" />
									<label for="single_include_2nd_score">順位</label>
								</span>
							</p></div>
						</td>
					</tr>
					<!--
					<tr>
						<td>
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="data" id="event01" />
									<label for="event01">種目別順位</label>
								</span>
							</p></div>
						</td>
						<td colspan="2">
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="data" id="event02" />
									<label for="event02">D得点、E得点、減点</label>
								</span>
							</p></div>
						</td>
					</tr>-->
					<tr>
						<td colspan="3">
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="certificate" id="certificate" value="1" />
									<label for="certificate">賞状読み上げ用</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<ul class="inputGroup inputGroup01 clearfix">
								<li class="checkbox">
									<input type="checkbox" name="choose_rotation[1]" id="scoring_set01" />
									<label for="scoring_set01">得点集計表（組別）</label>
								</li>
								<li>
									<p class="select size11">
										<select name="choose_rotation_group[1]" id="number_team02">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</p>
									<p><label for="number_team02" class="labelCss">班</label></p>
								</li>
								<li>
									<p class="select size12">
										<select name="choose_rotation_event[1]" id="rotation01">
											<option value="">全ローテーション</option>
											<?php foreach ($rotationSettings as $key => $rst) { ?>
												<option value="<?=$key+1?>">全ローテーション0<?=$key+1?></option>
											<?php } ?>
										</select>
									</p>
								</li>
							</ul>
						</td>
						<td class="col02">
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="data" id="scoring_set02" />
									<label for="scoring_set02">E得点詳細</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<ul class="inputGroup inputGroup01 clearfix">
								<li class="checkbox">
									<input type="checkbox" name="choose_rotation[2]" id="scoring_school01" />
									<label for="scoring_school01">得点集計表（学校別）</label>
								</li>
								<li>
									<p class="select size11">
										<select name="choose_rotation_group[2]" id="number_team03">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</select>
									</p>
									<p><label for="number_team03" class="labelCss">班</label></p>
								</li>
								<li>
									<p class="select size12">
										<select name="choose_rotation_event[2]" id="rotation02">
											<option value="">全ローテーション</option>
											<?php foreach ($rotationSettings as $key => $rst) { ?>
												<option value="<?=$key+1?>">全ローテーション0<?=$key+1?></option>
											<?php } ?>
										</select>
									</p>
								</li>
							</ul>
						</td>
						<td>
							<div class="wrapRowList"><p class="rowList w4">
								<span class="title">表示項目</span>
								<span class="checkbox">
									<input type="checkbox" name="data" id="scoring_school02" />
									<label for="scoring_school02">E得点詳細</label>
								</span>
							</p></div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="wrapRowList"><p class="rowList">
								<span class="checkbox">
									<input type="checkbox" name="choose_detail" id="detail" />
									<label for="detail">詳細データ</label>
								</span>
							</p></div>
						</td>
					</tr>
				</table>
				<ul class="buttonList clearfix">
					<li><a href="javascript:void(0)" onclick="window.history.back()" class="buttonStyle hover">戻る</a></li>
					<li class="submitButton"><input type="submit" value="出力する" class="buttonGeneral hover" id="outputBtn" /></li>
				</ul>
			</div>
		</form>
	</div>
	<!-- /#main -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/admin/footer"); ?>

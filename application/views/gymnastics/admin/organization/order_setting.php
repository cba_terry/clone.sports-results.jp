<?php
	$this->load->view("includes/admin/header", array(
		'title'  => '試技順設定',
		'css'    => '',
		'js'     => 'modal',
		'pageId' => 'pageOrderSetting'
	));
	$group    = ($this->input->get('group')) ? $this->input->get('group') : '1';
	$heat     = ($this->input->get('heat')) ? $this->input->get('heat') : '1';
	$btn_back = ( ! empty($request_uri)) ? $request_uri : 'javascript:history.back()';
?>
		<div id="contents" class="clearfix">
			<div id="main">
				<form action="" method="post" class="searchForm">
					<div class="headBox clearfix">
						<h2 class="headline2"><?php echo $game->getStrSex(), ' ', $game->getClass(), ' ', $item; ?> 試技順設定</h2>
					</div>
					<!-- /.headBox -->
					<div class="tableInfo tableOrder">
						<table>
							<tr>
								<th class="col01">No</th>
								<th class="col02">選手名</th>
								<th class="col03">学校名</th>
								<th>試技順</th>
							</tr>
							<?php foreach ($players as $key => $player) { ?>
							<tr>
								<td class="col01"><?=$player->getPlayerNo()?></td>
								<td><?=$player->getPlayerName()?></td>
								<td><?php if ($player->getSchool()) echo $player->getSchool()->getSchoolNameAb();?></td>
								<td class="col04"><input type="text" value="<?=$player->getOrder($item)?>" id="order<?=$player->getId()?>" class="size01 idleField" name="order[<?=$player->getId()?>]" /></td>
							</tr>
							<?php } ?>
						</table>
					</div>
					<ul class="buttonList clearfix">
						<li><a href="/gymnastics/admin/organization/game/item?gid=<?=$game->getId()?>&item=<?=$item?>" class="buttonStyle hover">戻る</a></li>
						<li class="submitButton"><input type="submit" value="設定する" class="buttonGeneral hover" id="changeBtn" /></li>
					</ul>
				</form>
			</div>
			<!-- /#main -->
		</div>
		<!-- /#contents -->
		<div class="modal">
			<p class="close hover"><a href="#"><img src="<?=base_url('/img/admin/icon_close.png')?>" alt="X" /></a></p>
			<div class="modalContent">
				<h3 class="hModal">絞り込み検索</h3>
				<div class="searchBox">
					<form action="" method="get">
						<input type="hidden" name="group" value="<?=$group?>" />
						<input type="hidden" name="heat" value="<?=$heat?>" />
						<input type="hidden" name="item" value="<?=$item?>" />
						<div class="sheet">
							<table>
								<tr>
									<th><label for="user" class="labelCss">選手名</label></th>
								</tr>
								<tr>
									<td><input type="text" value="<?=$this->input->get('player_name')?>" class="size03" id="user" name="player_name" /></td>
								</tr>
							</table>
						</div>
						<div class="clearfix">
							<p class="btnCancel"><input type="reset" value="リセット" name="reset" class="hover" /></p>
							<p class="btnSubmit"><input type="submit" value="検索" name="search" class="hover" /></p>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.modal -->
<?php $this->load->view("includes/admin/footer"); ?>


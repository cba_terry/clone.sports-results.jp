<?php
	$title_head = $game.'団体ランキング';
	$this->load->view("includes/monitor/header", array(
		'title'  => $title_head,
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGroupRanking'
	));
?>
<div id="container">
	<header id="header" class="clearfix">
		<div class="headerInner">
			<p class="headText"><?=$title_head?></p>
			<p class="licence">Powerd by Datastudium Inc.</p>
		</div>
	</header>
	<!-- / #header -->
	<div id="contents">
		<div class="tableDetail wrapTable">
			<table>
				<tr>
					<th class="col01">順位</th>
					<th class="col02">番号</th>
					<th>名前/学校</th>
					<th class="col03">得点</th>
				</tr>
			<?php foreach ($resultGroup as $rank) { 
				$score = $rank->getElement();
				$school = $score->getSchoolGroup(); 
				?>
				<tr>
					<td><?=$rank->getRank()?></td>
					<td><?=$school->getSchoolNo()?></td>
					<td class="setCol"><?=$school->getSchoolNameAb()?></td>
					<td><?=formatScore($score->getTotalScore(), 3)?></td>
				</tr>
			<?php } ?>
			</table>
		</div>
	</div>
	<!-- / #contents -->
</div>
<!-- / #container -->
<?php if ( ! empty($game)) { ?>
<script>
	$(document).ready(function() {
		setTimeout(function(){
			window.location.href = '<?=$nextMonitor?>';
		}, <?=$timeMonitor?>);
	});
</script>
<?php } // endif ?>
<?php $this->load->view("includes/monitor/footer"); ?>
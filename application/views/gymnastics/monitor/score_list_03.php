<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<title>審判長用判別得点表</title>
<link rel="stylesheet" href="<?=base_url('/css/common.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/css/style.css')?>" media="all">
<link rel="stylesheet" href="<?=base_url('/css/monitor2.css')?>" media="all">
<link rel="index contents" href="/" title="ホーム">
<script src="<?=base_url('/js/jquery-1.8.3.min.js')?>"></script>
<script src="<?=base_url('/js/script.js')?>"></script>
<!--[if lt IE 9]>
<script src="<?=base_url('/js/html5shiv-printshiv.js')?>"></script>
<script src="<?=base_url('/js/css3-mediaqueries.js')?>"></script>
<![endif]-->
</head>
<body id="pageScoreList">
<div id="wrapper">
  <div id="container">
    <div id="contents">
      <div class="tableInfo">
        <div class="groupOut">
          <div class="groupIn">
            <p class="text"><?=$game->getStrSex()?><?=$game->getClass()?></p>
            <div class="groupElement">
            	<form action="" method="get">
		            <ul class="groupList">
						<li class="select size01">
							<input type="hidden" name="gid" value="<?=$this->input->get('gid')?>" />
							<input type="hidden" name="template" value="<?=$this->input->get('template')?>" />
							<select name="group" id="select_group">
								<?php foreach ($groups as $key => $group) { ?>
								<option <?php if($group['group'] == $int_group){ ?>selected="selected"<?php } ?> value="<?=$group['group']?>"><?=$group['group']?>班</option>
								<?php } ?>
							</select>
		                </li>
		                <?php for ($i=1; $i<=count($rotations); $i++) {?>
		                <li class="radio">
							<input id="rotate0<?=$i?>" type="radio" name="rotate" value="<?=$i?>" <?php if($i == $int_rotate){ ?>checked="checked"<?php } ?> />
							<label for="rotate0<?=$i?>">ローテーション<?=$i?></label>
		                </li>
		                <?php } ?>
		            </ul>
	            </form>
            </div>
            <p class="licenceTable">Powerd by Datastudium Inc.</p>
          </div>
        </div>
		<ul class="listTable clearfix">
		<?php foreach ($groupHeatPlayers as $group => $heats) { 
			foreach ($heats as $heat => $players) {
			$citem = $currentItemHeats[$heat-1];
			?>
			<?php if($citem){ ?>
			<li>
		        <table class="tableEvent">
		          <tbody>
		            <tr class="bgWhite">
		              <td colspan="4"><?=$heat?>組</td>
		              <td colspan="12" class="alignLeft"><span><?=$citem->getName()?></span></td>
		            </tr>
		            <tr class="rowTitle">
		              <td class="col01 first">試技順</td>
		              <td class="col02">No</td>
		              <td class="col03">選手名</td>
		              <td class="col04">学校名</td>
		              <td class="col05">D<?=implode('・D', range(1, $citem->getNumberRefereeD()))?></td>
		              <?php for($i=1; $i<=$citem->getNumberRefereeE(); $i++) { ?>
		              <td class="col0<?=$i+5?>">E<?=$i?></th>
		              <?php } ?>
		              <td class="col10">E得点</td>
		              <td class="col11">E減点</td>
		              <td class="col12">E得点<br>
		                決定点</td>
		              <td class="col13">D+E</td>
		              <td class="col14">減点<br>
		                タイム・ライン</td>
		              <td class="col15">合計</td>
		              <td class="col16">チーム<br>
		                加算</td>
		            </tr>
		            <?php
		            $displayed_group = false;
		            foreach ($players as $player) { 
					//Display group row
					if ($player->isPlayAsSingle() && $displayed_group){ ?>
		            <tr class="bgGray">
		              <td colspan="4">チームベスト3</td>
		              <td colspan="4"><?=formatScore($groupScore[$group][$heat][$citem->getName()],2)?></td>
		              <td colspan="2">チーム減点</td>
		              <td colspan="2">-<?=formatScore($groupScore[$group][$heat]['team_demerit_score'][$citem->getName()],2)?></td>
		              <td colspan="2">チーム合計</td>
		              <td colspan="2"><?=formatScore($groupScore[$group][$heat][$citem->getName()]-$groupScore[$group][$heat]['team_demerit_score'][$citem->getName()],2)?></td>
		            </tr>
		            <?php } 
					$displayed_group = $player->getFlag();

					if($citem->getFlagItem())
						$this->load->view("includes/share/monitor_score_31", array('player' => $player, 'citem' => $citem));
					else 
						$this->load->view("includes/share/monitor_score_30", array('player' => $player, 'citem' => $citem));
					?>
		            <?php } ?>
		          </tbody>
		        </table>
			</li>
			<?php }?>
		<?php }} ?>
        </ul>
      </div>
    </div>
    <!-- / #contents --> 
  </div>
  <!-- / #container --> 
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#select_group').change(function(){
			$('form').submit();
		});
		$('.radio input').click(function(){
			$('form').submit();
		})
	});
</script>
<!-- / #wrapper -->
</body>
</html>
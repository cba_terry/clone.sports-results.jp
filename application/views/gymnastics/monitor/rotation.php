<?php
	$title_head = $game->getStrSex() . $game->getClass();
	$this->load->view("includes/monitor/header", array(
		'title'  => $title_head,
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageRotation'
	));
?>
<div id="container">
	<header id="header" class="clearfix">
		<div class="headerInner">
			<p class="headText"><?=$title_head?><span><?=$group?>班<?=$rotate?>種目目</span></p>
			<p class="licence">Powerd by Datastudium Inc.</p>
		</div>
	</header>
	<!-- / #header -->
	<div id="contents">
		<div class="tableDetail rotationTable">
			<?php if($heatItemCurrent) { ?>
			<table>
				<tr>
					<th class="sets" colspan="2"><span class="text"><?=$heat?></span><span><?=$heatItemCurrent->getFirstItem()?></span></th>
					<th class="rest">
					<?php if (!empty($itemRelax)) { ?>
						<div class="clearfix">
							<ul class="clearfix">
							<?php foreach ($itemRelax as $heat => $relax) { ?>
								<li><span class="restCell01"><?=$relax?></span><span class="restCell02"><?=$heatItemCurrent->getHeat()?>組</span></li>
							<?php } ?>
							</ul>
						</div>
					<?php } ?>
					</th>
				</tr>
				<?php
					$hl_active = false;
					foreach ($players as $player) {
						$school    = ($player->getSchool() !== null) ? $player->getSchool()->getSchoolNameAb() : '';
						// Set highlight border to distinguish area between flag = 1, flag <> 1
						// Set only one highlight
						if ( ! $player->getFlag() && ! $hl_active) {
							$highlight = ' class="borderCol"';
							$hl_active = true;
						} else {
							$highlight = '';
						}
				?>
				<tr<?=$highlight?>>
					<td class="number"><?=$player->getPlayerNo()?></td>
					<td class="setCol"><?=$player->getPlayerName()?></td>
					<td class="place"><?=$school?></td>
				</tr>
				<?php } ?>
			</table>
			<?php } ?>
		</div>
	</div>
	<!-- / #contents -->
</div>
<!-- / #container -->
<?php if ( ! empty($game)) { ?>
<script>
	$(document).ready(function() {
		setTimeout(function(){
			window.location.href = '<?=$nextMonitor?>';
		}, <?=$timeMonitor?>);
	});
</script>
<?php } // endif ?>
<?php $this->load->view("includes/monitor/footer"); ?>
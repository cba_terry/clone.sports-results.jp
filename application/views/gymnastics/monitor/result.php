<?php
	$this->load->view("includes/monitor/header", array(
		'title'  => '得点結果',
		'css'    => '',
		'js'     => 'renewal',
		'pageId' => 'pageResult'
	));
	$tournament_name = ($tournament !== null) ? $tournament->getName() : '';
	$tournament_day  = get_value($stt_day, ' 第%s日目');
	$school = ($player)?$player->getSchool():null; 
?>
<div id="container">
	<header id="header" class="clearfix">
		<div class="headerInner">
			<p class="headText"><?=$tournament_name?><?=$tournament_day?></p>
			<p class="licence">Powerd by Datastudium Inc.</p>
		</div>
	</header>
	<!-- / #header -->
	<div id="contents" data-player="<?=($player)?$player->getId():''?>">
		<div class="infoText">
			<h2 class="headline1 player player-name"><?=($player)?$player->getPlayerName():''?></h2>
			<p class="txt01 player school-name"><?=($school)?$school->getSchoolNameAb():''?></p>
			<?php if($game->isInputNormal()){?>
			<p class="number01 player final-score"><?=($player)?$player->getFinalScore($item):''?></p>
			<?php }else{ ?>
			<p class="number01 player final-score"><?=($player)?$player->getItemScoreValue($item, 'FinalScore'):''?></p>
			<?php } ?>
		</div>
		<table class="tableStyle">
			<tr>
				<th><span class="font">D</span></th>
				<th><span class="font">E</span></th>
				<th>減点</th>
			</tr>
			<?php if($game->isInputNormal()){?>
			<tr>
				<td class="player d-score"><?=($player)?$player->getDScore($item):''?></td>
				<td class="player e-score"><?=($player)?$player->getEScore($item):''?></td>
				<td class="player demerit-score"><?=($player)?$player->getDemeritScore($item):''?></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td class="player d-score"><?=($player)?$player->getItemScoreValue($item, 'DScore'):''?></td>
				<td class="player e-score"><?=($player)?$player->getItemScoreValue($item, 'EScore'):''?></td>
				<td class="player demerit-score"><?=($player)?$player->getItemScoreValue($item, 'DemeritScore'):''?></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
<!-- / #container -->
<?php if ( ! empty($tournament)) { ?>
<script>
	$(document).ready(function() {
		$.monitorNext({
			url: $(location).attr('href'),
			time: <?=$monitor_time?>,
			request: 'ajax',
			callDone: function(res){
				var elmContents = $('#contents');
				var currentPlayer = elmContents.attr('data-player');
				$('.player').html('&nbsp');
				if (res != null && currentPlayer != res.player_id) {
					elmContents.attr('data-player', res.player_id);
					$('.player-name').html(res.player_name);
					$('.school-name').html(res.school_name);
					$('.final-score').html(res.final_score);
					$('.d-score').html(res.d_score);
					$('.e-score').html(res.e_score);
					$('.demerit-score').html(res.demerit_score);
				}
			},
			callFail: function(res){
				$('.player').html('&nbsp');
			}
		});
	});
</script>
<?php } // endif ?>
<?php $this->load->view("includes/monitor/footer"); ?>
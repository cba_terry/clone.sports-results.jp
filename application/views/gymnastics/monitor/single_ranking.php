<?php
	$title_head = $game->getStrSex() . $game->getClass() . '個人総合ランキング';
	$this->load->view("includes/monitor/header", array(
		'title'  => $title_head,
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageSingleRanking'
	));
?>
<div id="container">
	<header id="header" class="clearfix">
		<div class="headerInner">
			<p class="headText"><?=$title_head?></p>
			<p class="licence">Powerd by Datastudium Inc.</p>
		</div>
	</header>
	<!-- / #header -->
	<div id="contents">
		<div class="tableDetail wrapTable">
			<table>
				<tr>
					<th class="col01">順位</th>
					<th class="col02">番号</th>
					<th>名前/学校</th>
					<th class="col03">得点</th>
					<th class="col04">個/団</th>
				</tr>
			<?php foreach ($resultSingle as $rank) { 
				$player = $rank->getElement(); 
				$is_team = ($player->isPlayAsGroup()) ? 'icon_team' : 'icon_individual';
			?>
				<tr>
					<td><?=$rank->getRank()?></td>
					<td><?=$player->getPlayerNo()?></td>
					<td class="setCol"><?=$player->getPlayerName()?><em><?=$player->getSchool()->getSchoolName()?></em></td>
					<td><?=formatScore($player->getTotalFinalScore(), 3)?></td>
					<td class="icon"><img src="<?=base_url('/img/monitor/'.$is_team.'.png')?>" alt="個/団"></td>
				</tr>
			<?php } ?>
			</table>
		</div>
	</div>
	<!-- / #contents -->
</div>
<!-- / #container -->
<?php if ( ! empty($game)) { ?>
<script>
	$(document).ready(function() {
		setTimeout(function(){
			window.location.href = '<?=$nextMonitor?>';
		}, <?=$timeMonitor?>);
	});
</script>
<?php } // endif ?>
<?php $this->load->view("includes/monitor/footer"); ?>
<?php
	$this->load->view("includes/user/header", array(
		'title'  => '種目別ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageEventRanking'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getItemTimingPublishScore();
?>
<div id="contents" class="clearfix">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="viewInner">
				<div class="clearfix">
					<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
					<p class="tag"><?=($publicSetting->getItemTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
				</div>
				<?php $this->load->view("includes/user/navigation", ['page' => 'event'])?>
			</div>
		</div>
		<!-- /.headContents -->
		<div class="viewInner">
			<div class="section">
				<div class="headSection clearfix">
					<h3 class="headline2"><?=$item?></h3>
					<ul class="eventsList">
					<?php foreach ($items as $it) { ?>
						<li><a href="/gymnastics/user/ranking/event/<?=$game->getId()?>?event=<?=$it->getNo()?>"<?php if($it->getNo() === $item->getNo()){echo ' class="active"';} ?>><?=$it?></a></li>
					<?php } ?>
					</ul>
				</div>
				<div class="tableUser tableEvent">
					<table>
						<tr>
							<th class="col01"<?=($item->isPlayTwice() && $anableOptions) ? ' rowspan="2"' : '';?>>順位</th>
							<th<?=($item->isPlayTwice() && $anableOptions) ? ' rowspan="2"' : '';?>>選手名 [学年]</th>
							<th<?=($item->isPlayTwice() && $anableOptions) ? ' rowspan="2"' : '';?>>学校名 (県)</th>
						<?php if (!$item->isPlayTwice() && $anableOptions) { ?>
							<th class="col04">D得点</th>
							<th class="col04">E得点</th>
							<th class="col04">減点</th>
						<?php } elseif ($item->isPlayTwice()) { ?>
							<th<?=($anableOptions) ? ' colspan="3"' : '';?>>1本目</th>
							<th<?=($anableOptions) ? ' colspan="3"' : '';?>>2本目</th>
						<?php } // endif $item->isPlayTwice() ?>
							<th class="col04"<?=($item->isPlayTwice() && $anableOptions) ? ' rowspan="2"' : '';?>>合計</th>
							<th class="col04"<?=($item->isPlayTwice() && $anableOptions) ? ' rowspan="2"' : '';?>>点差</th>
						</tr>
						<?php if ($item->isPlayTwice() && $anableOptions) { ?>
						<tr>
							<th class="col04">D得点</th>
							<th class="col04">E得点</th>
							<th class="col04">減点</th>
							<th class="col04">D得点</th>
							<th class="col04">E得点</th>
							<th class="col04">減点</th>
						</tr>
						<?php } // endif $item->isPlayTwice() ?>
						<?php
							$lastRank = $resultItemRanking->first();
						if ( ! $resultItemRanking->isEmpty()) {
							foreach ($resultItemRanking as $rank) {
								$player              = $rank->getElement();
								$distanceAnotherRank = $rank->distanceAnotherRank($lastRank, 'getItemBestScoreValue', [$item, 'FinalScore']);
						?>
						<tr>
							<td class="active01"><?=$rank->getRank()?></td>
							<td class="col01">
								<?=$player->getPlayerName()?>
								<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
							</td>
							<td class="col01">
								<?=$player->getSchoolNameAb()?>
								<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
							</td>
						<?php if ($item->isPlayTwice() && $anableOptions) { ?>
							<td><?=formatScore($player->getItemScoreValue($item, 'DScore', 1))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'EScore', 1))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'DemeritScore', 1))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'DScore', 2))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'EScore', 2))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'DemeritScore', 2))?></td>
						<?php } elseif ($item->isPlayTwice() && !$anableOptions) { ?>
							<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 1))?></td>
							<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 2))?></td>
						<?php } elseif (!$item->isPlayTwice() && $anableOptions) { ?>
							<td><?=formatScore($player->getItemBestScoreValue($item, 'DScore'))?></td>
							<td><?=formatScore($player->getItemBestScoreValue($item, 'EScore'))?></td>
							<td><?=formatScore($player->getItemBestScoreValue($item, 'DemeritScore'))?></td>
						<?php } // endif ?>
							<td class="point"><?=formatScore($player->getItemBestScoreValue($item, 'FinalScore'))?></td>
							<td><?=($distanceAnotherRank != '-') ? formatScore($distanceAnotherRank) : $distanceAnotherRank;?></td>
						</tr>
						<?php
								$lastRank = $rank;
							} // endforeach $resultItemRanking
						} // endif $resultItemRanking

						if ( ! $resultItemCancle->isEmpty()) {
							foreach ($resultItemCancle as $rank) {
								$player = $rank->getElement();
						?>
						<tr>
							<td class="active01">&nbsp;</td>
							<td class="col01"><?=$player->getPlayerName()?></td>
							<td class="col01"><?=$player->getSchoolNameAb()?></td>
						<?php if ($item->isPlayTwice() && $anableOptions) { ?>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						<?php } elseif ($item->isPlayTwice() && !$anableOptions) { ?>
							<td>-</td>
							<td>-</td>
						<?php } elseif (!$item->isPlayTwice() && $anableOptions) { ?>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						<?php } // endif ?>
							<td class="point">-</td>
							<td>-</td>
						</tr>
						<?php
							} // endforeach $resultItemCancle
						} // endif $resultItemCancle
						?>
					</table>
				</div>
			</div>
			<!-- /.section -->
			<p class="buttonBack mt30"><a href="javascript:history.back()" class="hover">戻る</a></p>
		</div>
		<!-- /.viewInner -->
	</div>
	<!-- /.contentsBlock01 -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
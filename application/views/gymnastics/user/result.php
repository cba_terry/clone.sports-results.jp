<?php
	$this->load->view("includes/user/header", array(
		'title'  => '成績速報',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageResult'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getNewsTimingPublishScore();
	$rowspan       = ($anableOptions) ? ' rowspan="2"' : '';
?>
<div id="contents" class="clearfix">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="viewInner">
				<div class="clearfix">
					<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
					<p class="tag"><?=($publicSetting->getNewsTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
				</div>
				<?php $this->load->view("includes/user/navigation", ['page' => 'result'])?>
			</div>
		</div>
		<!-- /.headContents -->
		<div class="viewInner">
			<div class="section">
				<div class="tableUser tableResult">
					<table>
						<tr>
							<th class="col01"<?=$rowspan?>>班</th>
							<th class="col01"<?=$rowspan?>>組</th>
							<th class="col02"<?=$rowspan?>>No</th>
							<th class="col09"<?=$rowspan?>>選手名 [学年]</th>
							<th class="col10"<?=$rowspan?>>学校名 (県)</th>
							<?php foreach ($items as $item) { ?>
								<?php if($item->isPlayTwice()) { ?>
								<th colspan="3"><?=$item?>1本目</th>
								<th colspan="3"><?=$item?>2本目</th>
								<?php }else{ ?>
								<th colspan="3"><?=$item?></th>
								<?php }?>
							<?php } ?>
							<th<?=$rowspan?>>合計</th>
							<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<th class="col01"<?=$rowspan?>>順位</th>
							<?php }?>
						</tr>
					<?php if ($anableOptions) { ?>
						<tr>
						<?php foreach ($items as $item) {?>
							<?php if($item->isPlayTwice()){ ?>
							<th class="col02 pv0">D</th>
							<th class="col02 pv0">E</th>
							<th class="col02 pv0">減点</th>
							<th class="col02 pv0">D</th>
							<th class="col02 pv0">E</th>
							<th class="col02 pv0">減点</th>
							<?php }else { ?>
							<th class="col02 pv0">D</th>
							<th class="col02 pv0">E</th>
							<th class="col02 pv0">減点</th>
							<?php } ?>
						<?php } ?>
					<?php } // endif $anableOptions ?>
				<?php
				foreach ($groupHeatPlayers as $group) {
					foreach ($group->getHeats() as $heat) {
						$heatRotate = $heat->getRotations();
						// tach players trong head thanh 2 loai, choi cho group va choi cho head
						$playsAsGroup  = $heat->getPlayersPlayAsGroup();
						$playsAsSingle = $heat->getPlayersPlayAsSingle();
						// loop play group
						foreach ($playsAsGroup as $player) { ?>
						<?php $currentItemPlay = $player->getCurrentPlay($heatRotate, $items);?>
						<tr>
							<td<?=$rowspan?>><?=$group->getId()?></td>
							<td<?=$rowspan?>><?=$heat->getId()?></td>
							<td<?=$rowspan?>><?=$player->getPlayerNo()?></td>
							<td<?=$rowspan?> class="left pl20 col01">
								<?=$player->getPlayerName()?>
								[<?=$player->getGrade()?>年]
							</td>
							<td<?=$rowspan?> class="left pl20 col01">
								<?=$player->getSchoolNameAb()?>
								<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
							</td>
							<?php foreach ($items as $item) {  ?>
							<?php if(!$player->hasItemScorePublished($item) && $player->getFlagCancle()) {?>
							<td class="point" colspan="3">-</td>
							<?php }elseif(!$player->hasItemScorePublished($item) && $currentItemPlay->getFirstItem()==$item->getName() && $group->getId()==$group_current) {?>
							<td class="statusLive" colspan="3"<?=$rowspan?>><span>競技中</span></td>
							<?php }else { ?>
							<td class="point" colspan="3"><?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',1))?></td>
							<?php } ?>
							<?php if($item->isPlayTwice() && !$player->hasItemScorePublished($item) && $player->getFlagCancle()){?>
							<td class="point" colspan="3">-</td>
							<?php }elseif($item->isPlayTwice()){ ?>
							<td class="point" colspan="3"><?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',2))?></td>
							<?php } ?>
							<?php } ?>
							<td class="point"<?=$rowspan?>><?=userFormatScore($player->getTotalFinalScore())?></td>
							<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<td<?=$rowspan?>><?=$resultRanking->findRank($player)?></td>
							<?php } ?>
						</tr>
					<?php if ($anableOptions) { ?>
						<tr>
							<?php foreach ($items as $item) { ?>
							<?php if(!$player->hasItemScorePublished($item) && $player->getFlagCancle()) {?>
							<td class="point"><span>-</span></td>
							<td class="point"><span>-</span></td>
							<td class="point"><span>-</span></td>
							<?php }elseif(!$player->hasItemScorePublished($item) && $currentItemPlay->getFirstItem()==$item->getName() && $group->getId()==$group_current) {?>
							<?php }else { ?>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DScore',1))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'EScore',1))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',1), '-')?></span></td>
							<?php } ?>
							<?php if($item->isPlayTwice() && !$player->hasItemScorePublished($item) && $player->getFlagCancle()){?>
							<td class="point"><span>-</span></td>
							<td class="point"><span>-</span></td>
							<td class="point"><span>-</span></td>
							<?php }elseif($item->isPlayTwice()){ ?>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DScore',2))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'EScore',2))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',2), '-')?></span></td>
							<?php } ?>
							<?php } ?>
						</tr>
					<?php } // endif $anableOptions ?>
						<?php } #end loop play group
						//Display group row
						if ($heat->hasGroupSchoolPlay()){ ?>
						<tr class="active02">
							<td colspan="5">チームベスト3</td>
							<?php foreach ($items as $item): ?>
							<td class="point" colspan="<?=($item->isPlayTwice())?6:3?>"><span><?=userFormatScore($heat->getSchoolScoreItem($item, true))?></span></td>
							<?php endforeach; ?>
							<td><?=userFormatScore($heat->getSchoolTotalItem())?></td>
							<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<td></td>
							<?php } ?>
						</tr>
						<?php } // end group row display
						// loop play single
						foreach ($playsAsSingle as $player) { ?>
						<?php $currentItemPlay = $player->getCurrentPlay($heatRotate, $items);?>
						<tr>
							<td<?=$rowspan?>><?=$group->getId()?></td>
							<td<?=$rowspan?>><?=$heat->getId()?></td>
							<td<?=$rowspan?>><?=$player->getPlayerNo()?></td>
							<td<?=$rowspan?> class="left pl20 col01"><?=$player->getPlayerName()?> [<?=$player->getGrade()?>年]</td>
							<td<?=$rowspan?> class="left pl20 col01"><?=$player->getSchoolNameAb()?> (<?=$player->getPrefecture()?>)</td>
							<?php foreach ($items as $item) { ?>
							<?php
								if(!$player->hasItemScorePublished($item)
									&& $currentItemPlay->getFirstItem()==$item->getName()
									&& $group->getId()==$group_current
								) {
							?>
							<td class="statusLive" colspan="3"<?=$rowspan?>><span>競技中</span></td>
							<?php }else { ?>
							<td class="point" colspan="3"><?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',1))?></td>
							<?php } ?>
							<?php if($item->isPlayTwice()){ ?>							
							<td class="point" colspan="3"><?=userFormatScore($player->getItemScoreValue($item, 'FinalScore',2))?></td>
							<?php } ?>
							<?php } ?>
							<td class="point"<?=$rowspan?>><?=userFormatScore($player->getTotalFinalScore())?></td>
							<?php if($publicSetting->getNewsTimingPublishRank()) {?>
							<td<?=$rowspan?>><?=$resultRanking->findRank($player)?></td>
							<?php } ?>
						</tr>
					<?php if ($anableOptions) { ?>
						<tr>
							<?php foreach ($items as $item) { ?>
							<?php if(!$player->hasItemScorePublished($item) && $currentItemPlay->getFirstItem()==$item->getName() && $group->getId()==$group_current) {?>
							<?php }else { ?>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DScore',1))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'EScore',1))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',1), '-')?></span></td>
							<?php } ?>
							<?php if($item->isPlayTwice()){ ?>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DScore',2))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'EScore',2))?></span></td>
							<td class="point"><span><?=userFormatScore($player->getItemScoreValue($item, 'DemeritScore',2), '-')?></span></td>
							<?php } ?>
							<?php } ?>
						</tr>
					<?php } // endif $anableOptions ?>
						<?php } #end loop play single ?>
					<?php } // endforeach heat
				} // endforeach group
				?>
					</table>
				</div>
			</div>
			<!-- /.section -->
			<p class="buttonBack"><a href="javascript:history.back()" class="hover">戻る</a></p>
		</div>
		<!-- /.viewInner -->
	</div>
	<!-- /.contentsBlock01 -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>

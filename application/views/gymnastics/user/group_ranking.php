<?php
	$this->load->view("includes/user/header", array(
		'title'  => '団体ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageGroupRanking'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getGroupTimingPublishScore();
?>
<div id="contents" class="clearfix">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $tournament->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="viewInner">
				<div class="clearfix">
					<h2 class="headline3"><?=$game?></h2>
					<p class="tag"><?=($publicSetting->getGroupTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
				</div>
				<?php $this->load->view("includes/user/navigation", ['page' => 'group'])?>
			</div>
		</div>
		<!-- /.headContents -->
		<div class="viewInner">
			<div class="section mb10">
				<div class="tableUser tableGroup">
					<table>
						<tr>
							<th class="col01"<?=($anableOptions) ? ' rowspan="2"' : '';?>>順位</th>
							<th<?=($anableOptions) ? ' rowspan="2"' : '';?>>学校名</th>
							<?php foreach ($items as $item) { ?>
							<th<?=($anableOptions) ? ' colspan="2"' : '';?>><?=$item?></th>
							<?php } ?>
							<th class="col03"<?=($anableOptions) ? ' rowspan="2"' : '';?>>合計</th>
							<th class="col03"<?=($anableOptions) ? ' rowspan="2"' : '';?>>点差</th>
						</tr>
						<?php if ($anableOptions) { ?>
						<tr>
							<?php foreach ($items as $item) { ?>
							<th class="col04 pv0">得点</th>
							<th class="col05 pv0">順位</th>
							<?php } ?>
						</tr>
						<?php } //endif $anableOptions ?>
					<?php 
					$lastRank = $resultGroupRanking->first();
					foreach ($resultGroupRanking as $rank) { 
						$groupScore = $rank->getElement();
						$school = $groupScore->getSchoolGroup();
					?>
						<tr>
							<td class="active01"><?=$rank->getRank()?></td>
							<td class="col01">
								<?=$school->getSchoolNameAb()?>
								<?=($school->getSchoolPrefecture()) ? '('.$school->getSchoolPrefecture().')' : '';?>
							</td>
							<?php foreach ($items as $item) { ?>
							<td><?=formatScore(call_user_func([$groupScore, 'getItem'.$item->getNo().'Score']))?></th>
								<?php if ($anableOptions) { ?>
							<td><span><?=$resultItemRanking[$item->getName()]->findRank($groupScore)?></span></th>
								<?php } // endif $anableOptions ?>
							<?php } ?>
							<td class="point"><?=formatScore($groupScore->getTotalScore())?></td>
							<td><?=$rank->distanceAnotherRank($lastRank, 'getTotalScore')?></td>
						</tr>
					<?php
					$lastRank = $rank;
					} // endforeach
					?>
					</table>
				</div>
			</div>
			<!-- /.section -->
			<p class="buttonBack mt30"><a href="javascript:history.back()" class="hover">戻る</a></p>
		</div>
		<!-- /.viewInner -->
	</div>
	<!-- /.contentsBlock01 -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
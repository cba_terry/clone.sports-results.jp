<?php
	$this->load->view("includes/user/header", array(
		'title'  => '試合一覧',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageTournamentList'
	));
?>
<div id="contents" class="clearfix">
	<div class="viewInner">
		<h1 class="headline1"><?php if ($association) echo $association->getName()?></h1>
		<div class="contentsBlock">
			<div class="viewInner">
				<div class="section">
					<div class="tableUser tableTournament">
						<table>
							<tr>
								<th class="col01">場所</th>
								<th>大会名</th>
								<th class="col02">場所</th>
							</tr>
							<?php
								if( ! empty($tournaments)) {
									foreach ($tournaments as $key => $tournament) {
							?>
							<tr>
								<td><?=$tournament->getStartTime()->format('Y.m.d')?> - <?=$tournament->getEndTime()->format('Y.m.d')?></td>
								<td><a href="/gymnastics/user/game_list/<?=$tournament->getId()?>"><?=$tournament->getName()?></a></td>
								<td><?=$tournament->getPlace()?></td>
							</tr>
							<?php
									} // endforeach
								} // endif
							?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- / .viewInner -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
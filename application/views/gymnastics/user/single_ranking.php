<?php
	$this->load->view("includes/user/header", array(
		'title'  => '個人総合ランキング',
		'css'    => '',
		'js'     => '',
		'pageId' => 'pageSingleRanking'
	));

	$publicSetting = $tournament->getPublicSetting();
	$anableOptions = $publicSetting->getSingleTimingPublishScore();
?>
<div id="contents" class="clearfix">
	<div class="viewInner">
		<h1 class="headline1"><?php echo $game->getTournament()->getName(); ?></h1>
	</div>
	<div class="contentsBlock<?php if(!$game->isMale()) echo '01'?>">
		<div class="headContents">
			<div class="viewInner">
				<div class="clearfix">
					<h2 class="headline3"><?=$game->getStrSex()?><?=$game->getClass()?></h2>
					<p class="tag"><?=($publicSetting->getSingleTimingType()) ? STATUS_DEFINE: STATUS_BREAK?></p>
				</div>
				<?php $this->load->view("includes/user/navigation", ['page' => 'single'])?>
			</div>
		</div>
		<!-- /.headContents -->
		<div class="viewInner">
			<div class="section">
				<div class="tableUser tableSingle">
					<table>
						<tr>
							<th class="col01" rowspan="2">順位</th>
							<th rowspan="2">選手名 [学年]</th>
							<th rowspan="2">学校名 (県)</th>
							<?php foreach ($items as $item) { ?>
							<th colspan="<?=($item->isPlayTwice() ? 4:2)?>"<?php if(!$item->isPlayTwice() && !$anableOptions){echo ' rowspan="2"';}?>><?=$item?></th>
							<?php } ?>
							<th class="col03" rowspan="2">合計</th>
							<th class="col04" rowspan="2">点差</th>
						</tr>
						<tr>
							<?php 
							foreach ($items as $item) {
								if($item->isPlayTwice()) {
							?>
								<th class="col09 pv0">1本目</th>
								<th class="col09 pv0">2本目</th>
								<?php } ?>
								<?php if ($anableOptions) { ?>
								<th class="col04 pv0">得点</th>
								<th class="col05 pv0">順位</th>
								<?php } elseif ($item->isPlayTwice() && !$anableOptions) { ?>
								<th class="col04 pv0">得点</th>
								<?php } ?>
							<?php } ?>
						</tr>
					<?php 
					$lastRank = $resultSingleRanking->first();
					foreach ($resultSingleRanking as $rank) { $player = $rank->getElement(); ?>
						<tr>
							<td class="active01"><?=$rank->getRank()?></td>
							<td class="left pl20">
								<?=$player->getPlayerName()?>
								<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
							</td>
							<td class="left pl20">
								<?=$player->getSchoolNameAb()?>
								<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
							</td>
							<?php foreach ($items as $item) {
								if($item->isPlayTwice()) {?>
								<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 1))?></td>
								<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 2))?></td>
								<?php } ?>
								<td<?php if (!$anableOptions){ echo ' colspan="2"';} ?>><?=formatScore($player->getItemBestScoreValue($item, 'FinalScore'))?></td>
							<?php if ($anableOptions) { ?>
								<td><span><?=$resultItemRanking[$item->getName()]->findRank($player)?></span></td>
							<?php } // endif $anableOptions ?>
							<?php } ?>
							<td class="point"><?=$player->getTotalFinalScore()?></td>
							<td><?=$rank->distanceAnotherRank($lastRank, 'getTotalFinalScore')?></td>
						</tr>
					<?php 
					$lastRank = $rank;
					} // endforeach $resultSingleRanking

					if ( ! $resultSingleCancle->isEmpty()) {
						foreach ($resultSingleCancle as $rank) {
							$player = $rank->getElement();
					?>
						<tr>
							<td class="active01">&nbsp;</td>
							<td class="left pl20">
								<?=$player->getPlayerName()?>
								<?=($player->getGrade()) ? '['.$player->getGrade().'年]' : '';?>
							</td>
							<td class="left pl20">
								<?=$player->getSchoolNameAb()?>
								<?=($player->getSchool()) ? '('.$player->getSchool()->getSchoolPrefecture().')' : '';?>
							</td>
						<?php
						foreach ($items as $item) {
							if ($player->getBestItemScore($item)) {
								if($item->isPlayTwice()) {
						?>
								<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 1))?></td>
								<td><?=formatScore($player->getItemScoreValue($item, 'FinalScore', 2))?></td>
							<?php } // endif $item->isPlayTwice() ?>
								<td<?php if (!$anableOptions){ echo ' colspan="2"';} ?>><?=formatScore($player->getItemBestScoreValue($item, 'FinalScore'))?></td>
							<?php if ($anableOptions) { ?>
								<td><span><?=$resultItemRanking[$item->getName()]->findRank($player)?></span></td>
							<?php } ?>
						<?php
							} else {
								if($item->isPlayTwice()) {
						?>
							<td>-</td>
							<td>-</td>
							<?php } // endif $item->isPlayTwice() ?>
							<td<?php if (!$anableOptions){ echo ' colspan="2"';} ?>>-</td>
							<?php if ($anableOptions) { ?>
								<td><span>&nbsp;</span></td>
							<?php } ?>
						<?php
							} // endif $player->getBestItemScore()
						} // endforeach $items
						?>
							<td class="point"><?=$player->getTotalFinalScore()?></td>
							<td>&nbsp;</td>
						</tr>
					<?php 
						} // endforeach $resultSingleCancle
					} // endif $resultSingleCancle
					?>
					</table>
				</div>
			</div>
			<!-- /.section -->
			<p class="buttonBack mt30"><a href="javascript:history.back()" class="hover">戻る</a></p>
		</div>
		<!-- /.viewInner -->
	</div>
	<!-- /.contentsBlock01 -->
</div>
<!-- /#contents -->
<?php $this->load->view("includes/user/footer"); ?>
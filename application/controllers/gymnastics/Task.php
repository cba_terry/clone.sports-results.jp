<?php
use Entities\Collection\RankingCollection;

class Task extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function dotask()
	{
		$tasks = $this->getRepository('Task')->findAll();

		if(count($tasks)==0) exit('no work.'); 

		$gameBuillRank = [];

		foreach ($tasks as $task) {
			
			$scoreId = $task->getSingleItemScoreId();

			$singleItemScore = $this->getRepository('SingleItemScore')->find($scoreId);

			$player = $singleItemScore->getPlayer();

			$game = $player->getGame();

			$item = $game->findItem($singleItemScore->getItem());

			$singleTotalScore 	= $player->getSingleTotalScore();
			$itemTotalScore 	= $player->getItemTotalScore();

			if(!$singleTotalScore) $singleTotalScore = new \Entities\SingleTotalScore;
			if(!$itemTotalScore) $itemTotalScore = new \Entities\ItemTotalScore;

			$bestScore = $player->getItemBestScoreValue($item, 'FinalScore');
			$avgScore = $player->getItemEventFinalScore($item);

			$singleItemScore1 = $player->getItemScore($item, \Entities\SingleItemScore::ROUND1);
			$singleItemScore2 = $player->getItemScore($item, \Entities\SingleItemScore::ROUND2);

			if($singleItemScore1) 
			{
				call_user_func_array([$singleTotalScore, 'setSingleItemScore'.$item->getNo().'Id1'], [$singleItemScore1->getId()]);
				call_user_func_array([$itemTotalScore, 'setSingleItemScore'.$item->getNo().'Id1'], [$singleItemScore1->getId()]);
			}

			if($singleItemScore2)
			{
				call_user_func_array([$singleTotalScore, 'setSingleItemScore'.$item->getNo().'Id2'], [$singleItemScore2->getId()]);
				call_user_func_array([$itemTotalScore, 'setSingleItemScore'.$item->getNo().'Id2'], [$singleItemScore2->getId()]);
			}

			call_user_func_array([$singleTotalScore, 'setItem'.$item->getNo().'Score'], [$bestScore]);
			call_user_func_array([$itemTotalScore, 'setItem'.$item->getNo().'Score'], [$avgScore]);

			$singleTotalScore->setTotalScore();

			$player->addSingleTotalScore($singleTotalScore);
			$player->addItemTotalScore($itemTotalScore);

			$this->em->persist($player);

			//Insert and update to GroupScore
			$schoolGroup = $player->getSchool();
			$groupScore  = ($schoolGroup !== null) ? $schoolGroup->getGroupScore() : null;

			if(!$groupScore) $groupScore = new \Entities\GroupScore;

			// find 3 person (in same school) have highest score by this item
			$players = $this->getRepository('Player')->findBy([
				'school' => $schoolGroup,
				]);

			// sort player by score DESC
			uasort($players, function($a,$b) use ($item){
				return $a->getItemBestScoreValue($item, 'FinalScore') > $b->getItemBestScoreValue($item, 'FinalScore') ? -1 : 1;
			});

			$threeBestPlayer = array_slice($players, 0, 3);

			$groupItemScore = 0;

			foreach ($threeBestPlayer as $key => $player) {

				$bestScore = $player->getBestItemScore($item);

				if($bestScore) call_user_func_array([$groupScore, 'setItem'.$item->getNo().'SingleItemScoreId'.($key+1)], [$bestScore->getId()]);
				
				$groupItemScore += $player->getItemBestScoreValue($item, 'FinalScore');
			}

			$fn = 'setItem'.$item->getNo().'Score';

			$groupScore->$fn($groupItemScore);
			$groupScore->setTotalScore();
			$groupScore->setSchoolGroup($schoolGroup);

			$schoolGroup->setGroupScore($groupScore);

			$this->em->persist($groupScore);
			$this->em->persist($schoolGroup);
			// work seem complete
			$this->em->remove($task);

			$gameBuillRank[$game->getId()] = $game;
		}

		$this->em->flush();

		foreach ($gameBuillRank as $game) {

			$sql = "SELECT s.id FROM item_total_score s 
							LEFT JOIN player p ON p.item_total_score_id = s.id
							LEFT JOIN game g ON p.game_id = g.id WHERE g.id = " . $game->getId();

			$itemScoreTotalIds = $this->db->query($sql)->result_array();

			$itemScoresTotals = $this->getRepository('ItemTotalScore')->findBy([
				'id' => array_column($itemScoreTotalIds, 'id'),
				]);

			$items = $game->getItems();

			foreach ($items as $item)
			{
				$resultRank = RankingCollection::implementRanking($itemScoresTotals, 'getItem'.$item->getNo().'Score');

				foreach ($resultRank as $rank) {

					$itemTotalScore = $rank->getElement();
					call_user_func_array([$itemTotalScore, 'setItem'.$item->getNo().'Rank'], [$rank->getRank()]);

					$this->em->persist($itemTotalScore);
				}
			}

			$sql = "SELECT gs.id FROM group_score gs
							LEFT JOIN school_group s ON gs.id = s.group_score_id
							LEFT JOIN game g ON s.game_id = g.id WHERE g.id = " . $game->getId();

			$groupScoreIds = $this->db->query($sql)->result_array();

			$groupScores = $this->getRepository('GroupScore')->findBy([
				'id' => array_column($groupScoreIds, 'id'),
				]);

			$resultRank = RankingCollection::implementRanking($groupScores, 'getTotalScore');

			foreach ($resultRank as $rank) {

				$groupScore = $rank->getElement();
				
				call_user_func_array([$groupScore, 'setRank'], [$rank->getRank()]);

				if(!$groupScore->getPassRank())
				{
					call_user_func_array([$groupScore, 'setPassRank'], [$rank->getRank()]);
				}

				$this->em->persist($groupScore);
			}

		}

		$this->em->flush();

		exit();
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function getHeats()
	{
		$game  = $this->input->post('game');
		$group = $this->input->post('group');

		$heats = $this->getRepository('Player')->getGroupHeats($game, $group);

		echo json_encode(['heats'  => array_values($heats[$group])]);
		exit();
	}
}
<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Ranking extends MY_Controller {

	const DEFAULT_TIME = 10000;


	/**
	 * [single description]
	 * @param  integer $gameId [description]
	 * @return [type]          [description]
	 */
	public function single($gameId = 0)
	{
		$nextMonitor = null;

		$game     = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game.');
		
		$timeMonitor  	= $this->getTimeForMonitor($game);
		$players 		= $this->getRepository('Player')->getPlayersUserResults($game);

		$resultSingle 	= RankingCollection::implementRanking($players, 'getTotalFinalScore');

		$nextMonitor 	= '/gymnastics/monitor/ranking/group/' . $game->getId();
		
		$this->load->view('gymnastics/monitor/single_ranking', [
			'game' => $game,
			'resultSingle' => $resultSingle,
			'nextMonitor' => $nextMonitor,
			'timeMonitor' => $timeMonitor,
			]);
	}


	/**
	 * [group description]
	 * @param  integer $gameId [description]
	 * @return [type]          [description]
	 */
	public function group($gameId = 0)
	{
		$nextMonitor = null;

		$game     = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game.');

		$items 			= $game->getItems();
		$players 		= $this->getRepository('Player')->getPlayersUserResults($game);

		$groupScores 	= [];
		$schoolGroups 	= $game->getSchoolGroups();

		foreach ($schoolGroups as $schoolGroup) {

			$groupScore = new \Entities\GroupScore;

			// find persons in same school
			$schoolPlayers = array_filter($players, function($p) use ($schoolGroup) {
				return $p->getSchool() == $schoolGroup;
			});

			// calculate score for each item
			foreach ($items as $item) {

				$bestScores = [];

				foreach ($schoolPlayers as $player) {
					
					$score = $player->getItemBestScoreValue($item, 'FinalScore');
					
					if(!in_array($score, $bestScores)) $bestScores[] = $score;
				}

				arsort($bestScores);

				$threeBestScore = array_slice($bestScores, 0, 3);

				$groupItemScore = array_sum($threeBestScore);

				$fn = 'setItem'.$item->getNo().'Score';

				$groupScore->$fn($groupItemScore);
				$groupScore->setTotalScore();
				$groupScore->setSchoolGroup($schoolGroup);
			}

			$groupScores[] = $groupScore;
		}

		$resultGroup 	= RankingCollection::implementRanking($groupScores, 'getTotalScore');

		$timeMonitor  	= $this->getTimeForMonitor($game);

		$nextMonitor 	= '/gymnastics/monitor/ranking/single/' . $game->getId();
		
		$this->load->view('gymnastics/monitor/group_ranking', [
			'game' => $game,
			'resultGroup' => $resultGroup,
			'nextMonitor' => $nextMonitor,
			'timeMonitor' => $timeMonitor,
			]);
	}


	/**
	 * [getTimeForMonitor description]
	 * @param  [type] $game [description]
	 * @return [type]       [description]
	 */
	private function getTimeForMonitor($game)
	{
		$tournament = $game->getTournament();
		
		if ($tournament !== null && $tournament->getPublicSetting() !== null) {

			$timeMonitor = convert_2_second($tournament->getPublicSetting()->getMonitorRankingInterbal());
			$timeMonitor = ($timeMonitor > 0) ? $timeMonitor : self::DEFAULT_TIME;

			return $timeMonitor;
		}

		return self::DEFAULT_TIME;
	}
}

<?php
use Entities\Collection\GroupPlayCollection;
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Rotation extends MY_Controller {

	protected $monitor_time = 10000;

	public function index($gameId = 0)
	{
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game.');
		
		$group 		= $game->findGroupCurrentPlay();
		$rotate 	= $game->findRotateCurrentPlay();
		$heat 		= ($this->input->get('heat')) ? $this->input->get('heat'):null;

		$items 		= $game->getItems();	

		$itemRelax = $game->findHeatRelaxInRotation($group, $rotate);

		// get rotation by group
		$rotationSettings 	= $game->getRotationSettings();
		$groupRotations		= $game->findFirstRotationGroup($group);

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);

		$currentRotate = $rotateFirsts->doRotate($rotate-1);

		if(!$heat) $heat = $currentRotate->rotations->first()->getHeat();

		$heatItemCurrent 	= $currentRotate->filterHeat($heat)->first();

		// next heat
		$idxOfCurHeat = $currentRotate->rotations->indexOf($heatItemCurrent);

		$idxOfNextHeat = $idxOfCurHeat+1;

		if($idxOfNextHeat > $currentRotate->rotations->indexOf($currentRotate->rotations->last()))
			$idxOfNextHeat = $currentRotate->rotations->indexOf($currentRotate->rotations->first());

		$nextRotateHeat = $currentRotate->rotations->get($idxOfNextHeat);

		// main data 
		$players = $this->getRepository('Player')->getPlayersResults($game, $group, $heat);

		// Set link next, prev between the heat
		$nextMonitor 	= $nextRotateHeat->getHeat();
		$nextMonitor 	= '/gymnastics/monitor/rotation/' . $gameId . '?heat=' . $nextMonitor;

		$this->load->view('gymnastics/monitor/rotation', [
			'game'				=> $game,
			'items'            	=> $items,
			'group'            	=> $group,
			'heat'				=> $heat,
			'players' 			=> $players,
			'rotate'       		=> $rotate,
			'itemRelax' 		=> $itemRelax,
			'nextMonitor' 		=> $nextMonitor,
			'timeMonitor' 		=> $this->monitor_time,
			'heatItemCurrent' 	=> $heatItemCurrent,
		]);
	}
}

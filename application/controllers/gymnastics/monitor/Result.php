<?php
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends MY_Controller {

	const MONITOR_TIME = 10000;

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$gameId = $this->uri->segment(4);

		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game.');

		$heat = $this->uri->segment(6);

		$tournament = $game->getTournament();
		
		// Set data default
		$monitor_time   = 10000;

		// Return is Datetime
		$stt_day = get_stt_day($tournament->getStartTime());

		if ($tournament->getPublicSetting() !== null) {

			$monitor_time = convert_2_second($tournament->getPublicSetting()->getMonitorResultTime());

			if(!$monitor_time) $monitor_time = self::MONITOR_TIME;
		}

		//////////////////////////////////////////////
		
		$group  = $game->findGroupCurrentPlay();
		$rotate = $game->findRotateCurrentPlay();

		$rotationSettings 	= $game->getRotationSettings();
		$groupRotations		= $game->findFirstRotationGroup($group);

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);

		$heatItemCurrent 	= $rotateFirsts->doRotate($rotate-1)->filterHeat($heat)->first();

		if($game->isInputNormal())
		{
			$players = $this->getRepository('Player')->getPlayersScores($game, $group, $heat);
		}
		else
		{
			$players = $this->getRepository('Player')->getPlayersResults($game, $group, $heat);
		}

		$item = $game->findItem($heatItemCurrent->getFirstItem());

		// filter player has published score
		$players = array_filter($players, function($player) use ($item) {
			return $player->hasItemScorePublished($item);
		});

		$playerDisplay = (count($players)) ? end($players) : null;

		$currentItem = $game->findItem($heatItemCurrent->getFirstItem());

		if ($this->input->is_ajax_request()) {

			if($playerDisplay)
			{
				$dataPlayer = [
					'player_id' => $playerDisplay->getId(),
					'player_name' => $playerDisplay->getPlayerName(),
					'school_name' => $playerDisplay->getSchool()->getSchoolNameAb()
				];

				if($game->isInputNormal())
				{
					$dataPlayer['d_score'] 		= $playerDisplay->getDScore($item);
					$dataPlayer['e_score'] 		= $playerDisplay->getEScore($item);
					$dataPlayer['demerit_score'] = $playerDisplay->getDemeritScore($item);
					$dataPlayer['final_score']  = $playerDisplay->getFinalScore($item);
				}
				else
				{
					$dataPlayer['d_score'] 		= $playerDisplay->getItemScoreValue($item, 'DScore');
					$dataPlayer['e_score'] 		= $playerDisplay->getItemScoreValue($item, 'EScore');
					$dataPlayer['demerit_score'] = $playerDisplay->getItemScoreValue($item, 'DemeritScore');
					$dataPlayer['final_score']  = $playerDisplay->getItemScoreValue($item, 'FinalScore');
				}
			}

			echo (!$dataPlayer) ? '' : json_encode($dataPlayer);
			exit();
		}

		$this->load->view('gymnastics/monitor/result', [
			'game' 			=> $game,
			'stt_day' 		=> $stt_day,
			'player' 		=> $playerDisplay,
			'monitor_time' 	=> $monitor_time,
			'tournament' 	=> $tournament,
			'item' 			=> $currentItem
			]);
	}
}

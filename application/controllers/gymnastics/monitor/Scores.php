<?php
use Entities\Collection\GroupPlayCollection;
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Scores extends MY_Controller {

	public $game;

	public function __construct()
	{
		parent::__construct();

		$gameId 	= $this->input->get('gid');
		$this->game = $this->getRepository('Game')->find($gameId);

		if(!$this->game) throw new Exception('Please select a game.');
	}

	public function index()
	{
		$group 		= ($this->input->get('group')) ? $this->input->get('group') : 1;
		$rotate 	= ($this->input->get('rotate')) ? $this->input->get('rotate') : 1;

		$rotationSettings 	= $this->game->getRotationSettings();
		$groupRotations		= $this->game->findFirstRotationGroup($group);

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);
	
		$currentItemHeats 	= $rotateFirsts->doRotate($rotate-1);

		$items = $this->game->getItems();

		// main data 
		if($this->game->isInputPaper())
		{
			$players = $this->getRepository('Player')->getPlayersResults($this->game, $group);
		}
		else
		{
			$players = $this->getRepository('Player')->getPlayersScores($this->game, $group);	
		}

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		// other data
		$groups = $this->getRepository('Player')->getGroupHeats($this->game);

		$template = ($this->input->get('template') == 3) ? '_03' : '';

		$this->load->view('gymnastics/monitor/score_list' . $template, [
			'game'				=> $this->game,
			'items'            => $items,
			'group'            => $group,
			'groups'           => $groups,
			'currentItemHeats' => $currentItemHeats,
			'groupHeatPlayers' => $groupHeatPlayers,
			'rotationSettings' => $rotationSettings,
			'int_group'        => $group,
			'int_rotate'       => $rotate
		]);
	}
}

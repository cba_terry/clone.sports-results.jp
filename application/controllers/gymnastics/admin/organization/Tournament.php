<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tournament extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$page = $this->input->get('per_page');

		$conditions = $this->input->get();
		$conditions['tournament_id'] = $this->session->userdata('tournament_id');
		$conditions['orderby'] = array(
			't.id' => 'DESC'
		);

		$data['tournaments'] = $this->getRepository('Tournament')->getPagedTournaments($conditions, $page, 5);
		// Set config pagination
		$config = admin_pagination_config();
		$data['total_rows'] = $config['total_rows'] = count($data['tournaments']);
		$config['per_page'] = 5;
		$config['base_url'] = '/gymnastics/admin/organization/Tournament';

		$data['pagination'] = $this->pagination->initialize($config);
		$this->load->view('gymnastics/admin/organization/tournament_list', $data);
	}

	/**
	 *	edit action
	 *
	 * 	There are 4 things we have to do in this action
	 *	1: validation form
	 *	2: if form data was be ok, update the tournament
	 *	3: update tournament's public setting
	 *	4: update tournament's games data (delete all game's data if game has ticked out off the form)
	 */

	public function edit($id)
	{
		$data['title'] = '大会編集';

		$user_id = $this->session->userdata('user')->getId();
		$user = $this->getRepository('User')->find($user_id);
		$association_id = $user->getAssociation()->getId();
		$association = $this->getRepository('Association')->find($association_id);

		$data['tournament'] = $this->getRepository('Tournament')->find($id);
		$data['area'] = $this->config->item('property')['tournament_area'];
		
		$this->form_validation->set_rules('endTime', '会期', 'callback_valid_datetime');
		if ($this->form_validation->run('tournament') == FALSE)
		{
			$this->load->view('gymnastics/admin/organization/tournament_edit', $data);
		}
		else
		{
			$area           = $this->input->post('area');
			$startTime      = $this->input->post('startTime');
			$endTime        = $this->input->post('endTime');
			$class          = $this->input->post('class');

			$tournament = $data['tournament'];

			$tournament->setName($this->input->post('name'));
			$tournament->setArea($area);
			$tournament->setAssociation($association);
			$tournament->setPlace($this->input->post('place'));
			$tournament->setPlaceUrl($this->input->post('place_url'));
			$tournament->setStartTime(new \DateTime(implode("-", $startTime)));
			$tournament->setEndTime(new \DateTime(implode("-", $endTime)));
			$tournament->setTextDisplaySingle($this->input->post('text_display_single'));
			$tournament->setTextDisplayGroup($this->input->post('text_display_group'));
			$tournament->setPersonRepresentName($this->input->post('person_represent_name'));

			$tourClass = [];
			$gamesBeforeDelete = $tournament->getGames();

			foreach($this->input->post('class') as $sex => $class)
			{
				foreach ($class as $cl) {

					$game = $this->getRepository('Game')->getGameByClassAndSex($tournament, $cl, $sex);
					
					if(!$game) 
					{
						$game = new \Entities\Game();
						$game->setActive(false);
					}

					$game->setSex($sex);
					$game->setClass($cl);
					$game->setTournament($tournament);

					$tournament->addGame($game);
					$tourClass[] = gender($sex).$cl;
				}					
			}

			$tournament->setClass($tourClass);

			// delete
			foreach($gamesBeforeDelete as $game)
			{
				if(!in_array($game->getStrSex().$game->getClass(), $tourClass) && $game->getId())
				{
					$this->em->remove($game);
				}
			}

			$this->em->flush();

			// file upload
			// config upload
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('tour_image')) {
            	$upload_data = $this->upload->data();
            	$tournament->setImage($upload_data['file_name']);
            }

			// end file upload

			$public_setting = $this->getRepository('PublicSetting')->findOneBy(array('tournament' => $tournament));

			$public_setting = ($public_setting) ? $public_setting : new \Entities\PublicSetting();

			$public_setting->setNewsTiming($this->input->post('news_timing'));
			$public_setting->setSingleTiming($this->input->post('single_timing'));
			$public_setting->setItemTiming($this->input->post('item_timing'));
			$public_setting->setGroupTiming($this->input->post('group_timing'));
			$public_setting->setNewsTimingPublish($this->input->post('news_timing_publish'));
			$public_setting->setGroupTimingPublish($this->input->post('group_timing_publish'));
			$public_setting->setSingleTimingPublish($this->input->post('single_timing_publish'));
			$public_setting->setItemTimingPublish($this->input->post('item_timing_publish'));
			$public_setting->setNewsTimingType($this->input->post('news_timing_type'));
			$public_setting->setGroupTimingType($this->input->post('group_timing_type'));
			$public_setting->setSingleTimingType($this->input->post('single_timing_type'));
			$public_setting->setItemTimingType($this->input->post('item_timing_type'));
			$public_setting->setNewsTimingPublishScore($this->input->post('news_timing_publish_score'));
			$public_setting->setGroupTimingPublishScore($this->input->post('group_timing_publish_score'));
			$public_setting->setSingleTimingPublishScore($this->input->post('single_timing_publish_score'));
			$public_setting->setItemTimingPublishScore($this->input->post('item_timing_publish_score'));
			$public_setting->setNewsTimingPublishRank($this->input->post('news_timing_publish_rank'));
			$public_setting->setTournament($tournament);

			$this->em->persist($tournament);
			$this->em->persist($public_setting);
			$this->em->flush();

			redirect('/admin/organization/tournament', 'refresh');
		}
	}

	/**
	 *	add action
	 * 
	 *	There are 4 things have to do in this action
	 *	1: validation form
	 *	2: if form data ok init a tournament
	 *	3: set Tournament's Public setting after object tournament has created.
	 *	4: set Tournament's games (prepare)
	 */

	public function add()
	{
		$data['area'] = $this->config->item('property')['tournament_area'];

		// 1: validation form

		$this->form_validation->set_rules('endTime', '会期', 'callback_valid_datetime');
		if($this->form_validation->run('tournament') !== FALSE)
		{
			$startTime = $this->input->post('startTime');
			$endTime = $this->input->post('endTime');

			// 2: init a Tournament
			$tournament = new Entities\Tournament;
			$tournament->setName($this->input->post('name'));
			$tournament->setArea($this->input->post('area'));
			$tournament->setPlace($this->input->post('place'));
			$tournament->setPlaceUrl($this->input->post('place_url'));
			$tournament->setStartTime(new \DateTime(implode("-", $startTime)));
			$tournament->setEndTime(new \DateTime(implode("-", $endTime)));
			$tournament->setAssociation($this->association);
			$tournament->setStatus(1);

			// 3: set Public Setting
			$public_setting = new Entities\PublicSetting;
			$public_setting->setNewsTiming($this->input->post('news_timing'));
			$public_setting->setSingleTiming($this->input->post('single_timing'));
			$public_setting->setItemTiming($this->input->post('item_timing'));
			$public_setting->setGroupTiming($this->input->post('group_timing'));
			$public_setting->setNewsTimingPublish($this->input->post('news_timing_publish'));
			$public_setting->setGroupTimingPublish($this->input->post('group_timing_publish'));
			$public_setting->setSingleTimingPublish($this->input->post('single_timing_publish'));
			$public_setting->setItemTimingPublish($this->input->post('item_timing_publish'));
			$public_setting->setNewsTimingType($this->input->post('news_timing_type'));
			$public_setting->setGroupTimingType($this->input->post('group_timing_type'));
			$public_setting->setSingleTimingType($this->input->post('single_timing_type'));
			$public_setting->setItemTimingType($this->input->post('item_timing_type'));
			$public_setting->setNewsTimingPublishScore($this->input->post('news_timing_publish_score'));
			$public_setting->setGroupTimingPublishScore($this->input->post('group_timing_publish_score'));
			$public_setting->setSingleTimingPublishScore($this->input->post('single_timing_publish_score'));
			$public_setting->setItemTimingPublishScore($this->input->post('item_timing_publish_score'));
			$public_setting->setNewsTimingPublishRank($this->input->post('news_timing_publish_rank'));
			$public_setting->setTournament($tournament);

			$tournament->addPublicSetting($public_setting);

			$tournament->setTextDisplaySingle($this->input->post('text_display_single'));
			$tournament->setTextDisplayGroup($this->input->post('text_display_group'));
			$tournament->setPersonRepresentName($this->input->post('person_represent_name'));

			// file upload
			// config upload
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('tour_image')) {
            	$upload_data = $this->upload->data();
            	$tournament->setImage($upload_data['file_name']);
            }

			// 4: set Games
			$tourClass = [];
			foreach($this->input->post('class') as $sex => $class)
			{
				foreach ($class as $cl) {

					$game = new \Entities\Game();

					$game->setSex($sex);
					$game->setClass($cl);
					$game->setActive(false);
					$game->setTournament($tournament);

					$tournament->addGame($game);
					$tourClass[] = gender($sex).$cl;
				}					
			}

			$tournament->setClass($tourClass);

			$this->em->persist($tournament);
			$this->em->flush();

			redirect('/admin/organization/tournament', 'refresh');

		}else{
			$this->load->view('gymnastics/admin/organization/tournament_add', $data);
		}
	}

	public function delete ()
	{
		$id = $this->input->post('tournament_id');
		$tournament = $this->getRepository('Tournament')->find($id);

		if($tournament){
			$this->em->remove($tournament);
			$this->em->flush();

			redirect('/admin/organization/referee?delete=success');
		}

		echo json_encode(array('id' => $id));
		exit;

	}

	/**
	 * updateStatus
	 *
	 * using ajax for update status at tournament
	 * tournament screen
	 *
	 * @return json
	 */

	public function updateStatus()
	{
		try{
			// array of rows score id, 
			// which was selected from the list screen.
			$id 	= $this->input->post('ids')[0]; 
			$status = $this->input->post('status');
			$tournament = $this->getRepository('Tournament')->find($id);
			$tournament->setStatus($status);
			$this->em->persist($tournament);
			$this->em->flush();
			$response = ['status' => 'success'];

		} catch (Exception $e){
			$response = ['status' => 'failed', 'message' => $e->getMessage()];
		}

		echo json_encode($response);
		exit;
	}

	public function valid_datetime()
	{
		$startTime       = $this->input->post('startTime');
		$endTime         = $this->input->post('endTime');
		$start_date_time = implode("-", $startTime);
		$end_date_time   = implode("-", $endTime);
		if(!compare_datetime($start_date_time, $end_date_time)) {
			$this->form_validation->set_message('valid_datetime', 'The field end date must greater than start date');
			return FALSE;
		}
		return TRUE;
	}

}
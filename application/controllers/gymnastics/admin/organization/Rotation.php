<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rotation extends Organization_Controller {

	public $game;

	public function __construct()
	{
		parent::__construct();

		$gameId 	= $this->input->get('gid');
		$this->game = $this->getRepository('game')->find($gameId);

		if(!$this->game) throw new Exception('Please select a game.');
	}

	public function index()
	{
		$gameInfo 		= $this->game->getGameInfo(true);
		$config 		= ($this->game->isMale()) ? 'item_rotation_man' : 'item_rotation_woman';

		$rotationSettings = $this->game->getRotationSettings();
		$defaultRotations = $this->config->item('property')[$config];
		$itemRotation = $this->config->item('property')['item_'.$this->game->getStrSex()];

		// implement cut configRotation when number heat lower than config
		// for example: neu trong csv toi da chi co 4 heat, ma so default 
		// rotation danh cho nu la 6 bao gom 2 mon nghi, thi se loai 2 mon nghi
		// khoi default rotation
		$maxHeat = 0;

		$rotations = [];

		if(empty($gameInfo['groupheats'])) throw new Exception('選手のデータを先に登録してください。');

		foreach ($gameInfo['groupheats'] as $group => $heats) {
			if(count($heats) > $maxHeat) $maxHeat = count($heats);

			$rotation = $this->game->getRotations()->filter(function($rotation) use ($group, $heats){
				return $rotation->getHeat() == reset($heats) && $rotation->getGroup() == $group;
			})->first();

			if($rotation) $rotations[] = $rotation;
		}

		// cut it
		if(count($itemRotation) < $maxHeat && $maxHeat < count($defaultRotations))
			$defaultRotations = array_slice($defaultRotations, 0, $maxHeat);

		if($maxHeat < count($itemRotation))
			$defaultRotations = array_slice($defaultRotations, 0, count($itemRotation));

		// check order of item, if empty we will insert 
		// data with column order default is null
		if(!$rotationSettings->count()) {
			//insert data for table rotation_setting
			$this->insertItemForRotationSetting($defaultRotations);
		}

		if ($this->input->post()) {

			$postRotation = $this->input->post('rotation');
			$postSetting  = $this->input->post('setting');

			asort($postSetting);

			//update order of item
			if ( ! empty($postSetting) ) {

				foreach ($postSetting as $sid => $order) {

					$setting = $this->em->getRepository('Entities\RotationSetting')->find($sid);

					if(!$setting) throw new Exception('Did you post a valid setting?');
					
					$setting->setOrder($order);

					$this->em->persist($setting);
				}
			}

			$groupHeats = $this->game->getGroupsHeat();

			$items = $this->game->getItems();

			foreach ($postRotation as $groupId => $settingId) {

				$heats = array_values($groupHeats[$groupId]);

				$numberItemRelaxAllow = count($heats) - count($items);

				$idxCurrHeat = $idxFirstHeat = $postSetting[$settingId];

				foreach ($heats as $key => $heatId) {

					$oneRotation = $this->game->getRotations()->filter(function($r) use ($heatId, $groupId) {
						return $r->getHeat() == $heatId && $r->getGroup() == $groupId;
					})->first();

					if(!$oneRotation) $oneRotation = new \Entities\Rotation;

					$item = $this->findRotateItem($idxCurrHeat, $numberItemRelaxAllow, $rotationSettings);

					$oneRotation->setGame($this->game);
					$oneRotation->setFirstItem($item->getName());
					$oneRotation->setGroup($groupId);
					$oneRotation->setHeat($heatId);

					$this->em->persist($oneRotation);
				}
			}

			$this->em->flush();

			redirect('/admin/organization/rotation?gid='.$this->game->getId());
		}
		$this->load->view('gymnastics/admin/organization/rotation', array(
			'game' => $this->game,
			'gameInfo' => $gameInfo,
			'rotations' => $rotations,
			'rotationSettings' => $rotationSettings,
			'defaultRotations' => $defaultRotations
		));
	}

	private function insertItemForRotationSetting($itemRotation = array())
	{
		// Reset key index
		// Because $order first is 0|1
		$index = 1;

		foreach ($itemRotation as $item) {
			$rotationItem = new Entities\RotationSetting;
			$rotationItem->setName($item);
			$rotationItem->setGame($this->game);
			$rotationItem->setOrder($index);
			$this->em->persist($rotationItem);
			// Update key index
			$index++;
		}

		$this->em->flush();

		redirect('/admin/organization/rotation?gid='.$this->game->getId());
	}

	private function findRotateItem(&$idxCurrHeat, &$numberItemRelaxAllow, $rotationSettings)
	{
		if($idxCurrHeat > count($rotationSettings)) $idxCurrHeat = $idxCurrHeat - count($rotationSettings);

		$item = $rotationSettings->filter(function($rst) use ($idxCurrHeat){
			return $rst->getOrder() == $idxCurrHeat;
		})->first();

		$idxCurrHeat++;

		if(!$this->game->findItem($item->getName())  && $numberItemRelaxAllow==0)
		{
			$item = $this->findRotateItem($idxCurrHeat, $numberItemRelaxAllow, $rotationSettings);

			if($item) $numberItemRelaxAllow--;
		}

		return $item;

	}
 }
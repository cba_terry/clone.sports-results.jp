<?php
use Doctrine\Common\Collections\ArrayCollection;
use Entities\SingleItemScore;
use Entities\SingleTotalScore;

defined('BASEPATH') OR exit('No direct script access allowed');

class Score_edit extends Organization_Controller {

	public $player;
	public $item;
	public $playerGame;

	public function __construct()
	{
		parent::__construct();

		$playerId 		= $this->input->get('player_id');
		$this->player 	= $this->getRepository('Player')->find($playerId);
		// assert player
		if ( !$this->player ) throw new Exception('Player was not specified.');

		// get game, player played for
		$this->playerGame = $this->player->getGame();
		
		// specified default item
		$this->item 	= urldecode($this->input->get('item'));
		
		if( !$this->item ) $this->item = $this->playerGame->getDefaultItemName();
	}

	public function index()
	{
		$postData 	= $this->input->post();

		$item = $this->playerGame->findItem($this->item);

		$data = array(
			'game' => $this->playerGame,
			'player' => $this->player,
			'item' => $item,
			);

		$template    = ($item->isPlayTwice()) ? 'score_edit02' : 'score_edit01';
		$fnComplete  = ($item->isPlayTwice()) ? 'complete02' : 'complete01';

		if ($postData && isset($postData['submit'], $postData['submit']['confirm'])){
			
			$data['post'] = $postData;
			$this->load->view('gymnastics/admin/organization/'.$template.'_confirm', $data);

		}elseif($postData && isset($postData['submit'],$postData['submit']['complete'])){
			
			$this->$fnComplete($postData, $item);

			$ref = $this->input->get('ref');

			if ($ref == 'all') {
				redirect('/admin/organization/game/all?gid='.$this->playerGame->getId());
			} else {
				redirect('/admin/organization/game/item?gid='
					.$this->playerGame->getId()
					.'&item='.$item->getName()
					.'&group='.$this->player->getGroup()
					.'&heat='.$this->player->getHeat());
			}

		}else{
			$this->load->view('gymnastics/admin/organization/'.$template, $data);
		}
	}
	
	private function complete02($data, $item){

		$scores[0] = $this->player->getItemScore($item, Entities\SingleItemScore::ROUND1);
		$scores[1] = $this->player->getItemScore($item, Entities\SingleItemScore::ROUND2);

		if (!$scores[0]) $scores[0] = new Entities\SingleItemScore();
		if (!$scores[1]) $scores[1] = new Entities\SingleItemScore();

		for ($i=0; $i < 2; $i++) {

			$round = $i+1;

			$scores[$i]->setStatus(Entities\SingleItemScore::MODIFY);
			$scores[$i]->setRound($round);

			$numberReferreD = $item->getNumberRefereeD();
			$numberReferreE = $item->getNumberRefereeE();

			// Score calculation
			for ($j=1; $j <= $numberReferreD; $j++) { 
				
				if(isset($data['d'.$j.'_score'])) call_user_func_array([$scores[$i], 'setD'.$j.'Score'], [$data['d'.$j.'_score'][$i]]);

				if($data['submit']['complete'] == '公開する')
				{
					call_user_func_array([$scores[$i], 'setD'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
				}
			}

			for ($j=1; $j <= $numberReferreE; $j++) { 
				
				if(isset($data['e'.$j.'_score'])) {
					call_user_func_array([$scores[$i], 'setE'.$j.'Score'], [$data['e'.$j.'_score'][$i]]);
				}

				if($data['submit']['complete'] == '公開する')
				{
					call_user_func_array([$scores[$i], 'setE'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
				}
			}

			if(isset($data['time1_score1'])) $scores[$i]->setTime1Score($data['time1_score1'][$i].':'.$data['time1_score2'][$i]);
			if(isset($data['time2_score1'])) $scores[$i]->setTime2Score($data['time2_score1'][$i].':'.$data['time2_score2'][$i]);
			if(isset($data['line1_score'])) $scores[$i]->setLine1Score($data['line1_score'][$i]);
			if(isset($data['line2_score'])) $scores[$i]->setLine2Score($data['line2_score'][$i]);


			if(isset($data['time_demerit_score'])) 	$scores[$i]->setTimeDemeritScore($data['time_demerit_score'][$i]);
			if(isset($data['line_demerit_score'])) 	$scores[$i]->setLineDemeritScore($data['line_demerit_score'][$i]);
			if(isset($data['other_demerit'])) 		$scores[$i]->setOtherDemerit($data['other_demerit'][$i]);
			if(isset($data['e_demerit_score'])) 	$scores[$i]->setEDemeritScore($data['e_demerit_score'][$i]);

			
			// Notice: please keep order of set Score
			// If you change order, the Score's result can be wrong
			$scores[$i]->setDScore(null, $numberReferreD);
			$scores[$i]->setEScore(null, $numberReferreE);
			$scores[$i]->setDemeritScore();
			$scores[$i]->setFinalScore();

			$this->player->registerSingleItemScore($scores[$i], $item);
		}

		if($data['submit']['complete'] == '公開する')
		{
			$scores[0]->setStatus(SingleItemScore::PUBLISH);
			$scores[1]->setStatus(SingleItemScore::PUBLISH);

			$scores[0]->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setTime2ScoreStatus(SingleItemScore::PUBLISH);

			$scores[1]->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setTime2ScoreStatus(SingleItemScore::PUBLISH);

			$singleTotalScore->setStatus(SingleTotalScore::PUBLISHED);
		}

		$bestScore = $this->player->getBestItemScore($item)->getFinalScore();

		/////////////////////////////////////////
		//Insert and update to SingleTotalScore//
		/////////////////////////////////////////
		
		$singleTotalScore = $this->player->getSingleTotalScore();

		if(!$singleTotalScore) $singleTotalScore = new SingleTotalScore;

		call_user_func_array([$singleTotalScore, 'setItem'.$item->getNo().'Score'], [$bestScore]);

		$singleTotalScore->setTotalScore();

		if($data['submit']['complete'] == '公開する')
		{
			$singleTotalScore->setStatus(SingleTotalScore::PUBLISHED);
		}

		$this->player->addSingleTotalScore($singleTotalScore);

		$this->em->persist($this->player);

		///////////////////////////////////
		//Insert and update to GroupScore//
		///////////////////////////////////
		
		$schoolGroup = $this->player->getSchool();
		$groupScore = ($schoolGroup !== null) ? $schoolGroup->getGroupScore() : null;

		if(!$groupScore) $groupScore = new \Entities\GroupScore;

		// find 3 person (in same school) have highest score by this item
		$players = $this->getRepository('Player')->findBy([
			'school' => $schoolGroup,
			'flag' => true,
			]);

		$bestScores = [];

		foreach ($players as $player) {
			
			$score = $player->getItemScoreValue($item, 'FinalScore');

			if(!in_array($score, $bestScores))
				$bestScores[] = $score;
		}

		arsort($bestScores);

		$threeBestScore = array_slice($bestScores, 0, 3);

		$groupItemScore = array_sum($threeBestScore);

		$fn = 'setItem'.$item->getNo().'Score';

		$groupScore->$fn($groupItemScore);
		$groupScore->setTotalScore();
		$groupScore->setSchoolGroup($schoolGroup);

		$this->em->persist($groupScore);
		$this->em->flush();
	}


	private function complete01($data, $item){

		$singleItemScore = $this->player->getItemScore($item, SingleItemScore::ROUND1);

		if(!$singleItemScore) $singleItemScore = new SingleItemScore;

		$singleItemScore->setStatus(SingleItemScore::MODIFY);
		$singleItemScore->setRound(SingleItemScore::ROUND1);

		$numberReferreD = $item->getNumberRefereeD();
		$numberReferreE = $item->getNumberRefereeE();

		// Score calculation
		for ($j=1; $j <= $numberReferreD; $j++) { 
			
			if(isset($data['d'.$j.'_score'])) call_user_func_array([$singleItemScore, 'setD'.$j.'Score'], [$data['d'.$j.'_score']]);

			if($data['submit']['complete'] == '公開する')
			{
				call_user_func_array([$singleItemScore, 'setD'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
			}
		}

		for ($j=1; $j <= $numberReferreE; $j++) { 
			
			if(isset($data['e'.$j.'_score'])) {
				call_user_func_array([$singleItemScore, 'setE'.$j.'Score'], [$data['e'.$j.'_score']]);
			}

			if($data['submit']['complete'] == '公開する')
			{
				call_user_func_array([$singleItemScore, 'setE'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
			}
		}

		if(isset($data['time1_score1'])) $singleItemScore->setTime1Score($data['time1_score1'].':'.$data['time1_score2']);
		if(isset($data['time2_score1'])) $singleItemScore->setTime2Score($data['time2_score1'].':'.$data['time2_score2']);
		if(isset($data['line1_score'])) $singleItemScore->setLine1Score($data['line1_score']);
		if(isset($data['line2_score'])) $singleItemScore->setLine2Score($data['line2_score']);


		if(isset($data['time_demerit_score'])) 	$singleItemScore->setTimeDemeritScore($data['time_demerit_score']);
		if(isset($data['line_demerit_score'])) 	$singleItemScore->setLineDemeritScore($data['line_demerit_score']);
		if(isset($data['other_demerit'])) 		$singleItemScore->setOtherDemerit($data['other_demerit']);
		if(isset($data['e_demerit_score'])) 	$singleItemScore->setEDemeritScore($data['e_demerit_score']);

		
		// Notice: please keep order of set Score
		// If you change order, the Score's result can be wrong
		$singleItemScore->setDScore(null, $numberReferreD);
		$singleItemScore->setEScore(null, $numberReferreE);
		$singleItemScore->setDemeritScore();
		$singleItemScore->setFinalScore();
		$singleItemScore->setBestScoreFlag(true);

		if($data['submit']['complete'] == '公開する')
		{
			$singleItemScore->setStatus(SingleItemScore::PUBLISH);
			$singleItemScore->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemScore->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemScore->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemScore->setTime2ScoreStatus(SingleItemScore::PUBLISH);
		}

		$this->player->registerSingleItemScore($singleItemScore, $item);

		$this->em->persist($this->player);

		$this->em->flush();

		$singleItemScoreId = $singleItemScore->getId();

		$task = $this->getRepository('Task')->findOneBy([
							'single_item_score_id' => $singleItemScoreId
			]);

		if(!$task) $task = new \Entities\Task;

		$task->setSingleItemScoreId($singleItemScoreId);

		$this->em->persist($task);

		$this->em->flush();
	}
}

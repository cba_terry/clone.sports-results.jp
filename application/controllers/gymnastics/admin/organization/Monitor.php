<?php
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Monitor extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$gameId = $this->input->get('gid');
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game.');

		// Get list item
		$index_item    = ($game->getSex()) ? 'male' : 'female';
		$index_item    = 'item_' . $index_item;
		$items_default = $this->config->item('property')[ $index_item ];
		$items_default = array_values($items_default);

		$group 		= $game->findGroupCurrentPlay();
		$rotate 	= $game->findRotateCurrentPlay();

		$arr_url = [
			'result'   => '/gymnastics/monitor/result/game/' . $gameId . '/heat/',
			'ranking'  => '/gymnastics/monitor/ranking/single/' . $gameId,
			'rotation' => '/gymnastics/monitor/rotation/' . $gameId,
			'score'    => '/gymnastics/monitor/scores?gid=' . $gameId . '&group='.$group.'&rotate='.$rotate
		];

		$rotationSettings 	= $game->getRotationSettings();
		$groupRotations		= $game->findFirstRotationGroup($group);

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);
		$currentItemHeats 	= $rotateFirsts->doRotate($rotate-1);

		$data['urls']          = $arr_url;
		$data['items_default'] = $items_default;
		$data['game']          = $game;
		$data['rotations']     = $currentItemHeats;
		$this->load->view('gymnastics/admin/organization/monitor_list', $data);
	}
}

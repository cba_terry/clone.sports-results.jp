<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_demerit_edit extends Organization_Controller  {
	
	public function index()
	{
		$schoolId = $this->input->get('school');
		// find schoolgroup
		$schoolGroup = $this->getRepository('SchoolGroup')->find($schoolId);

		if(!$schoolGroup) throw new Exception('School group not available!');
		
		// find groupscore by schoolgroup
		$groupScore = $schoolGroup->getGroupScore();

		if(!$groupScore) $groupScore = new \Entities\GroupScore;

		// find game's items
		$items = $schoolGroup->getGame()->getItems();

		// handle update
		if($this->input->post())
		{
			foreach ($items as $key => $item) {
				
				$itemIndex = ($key+1);// start index at 1

				// make function name
				$fnSetDemeritScore = 'setItem' . $itemIndex . 'DemeritScore';
				$fnSetDemeritReason = 'setItem' . $itemIndex . 'DemeritReason';

				$groupScore->setSchoolGroup($schoolGroup);

				// do function
				$groupScore->$fnSetDemeritScore($this->input->post('item' . $itemIndex . '_demerit_score'));
				$groupScore->$fnSetDemeritReason($this->input->post('item' . $itemIndex . '_demerit_reason'));
			}

			// commit change
			$this->em->persist($groupScore);
			$this->em->flush();
		}

		$this->load->view('gymnastics/admin/organization/team_demerit_edit', array(
			'items' => $items, 
			'game' => $schoolGroup->getGame(),
			'groupScore' => $groupScore,
			'school' => $schoolGroup,
		));
	}
}
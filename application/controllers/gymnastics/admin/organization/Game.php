<?php
use Entities\Collection\GroupPlayCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();

		$gameId = $this->input->get('gid');

		$this->game = $this->getRepository('Game')->find($gameId);

		if(!$this->game) throw new Exception('Game not found!');
	}

	/**
	 * index
	 *
	 * navigation screen
	 * 
	 * @param
	 * @return
	 */
	
	public function index()
	{
		$this->load->view('gymnastics/admin/organization/game', array('game' => $this->game));
	}


	/**
	 * all
	 *
	 * return all game's result in tournament
	 * 
	 * @param
	 * @return
	 */

	public function all()
	{
		$group = $this->input->get('group');
		$heat  = $this->input->get('heat');

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersResults($this->game, $group, $heat);

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		// number group for select box search
		$groups = $this->getRepository('Player')->getGroupList($this->game);

		$items 			= $this->game->getItems();

		$scoreStatus 	= $this->config->item('property')['score_status'];

		$this->load->view('gymnastics/admin/organization/game_all', array(
			'game'              => $this->game,
			'items'             => $items,
			'groups'			=> $groups,
			'score_status'      => $scoreStatus,
			'groupHeatPlayers' 	=> $groupHeatPlayers,
		));
	}

	/**
	 * item
	 *
	 * return single item game's result in tournament
	 * 
	 * @param array
	 * @return array
	 */

	public function item()
	{
		$item = $this->input->get('item');
		// Set referer link
		$this->session->set_userdata('redirect', $this->input->server('REQUEST_URI'));
		// default group heat
		$group = $heat = 1;

		if($this->input->get('group')) $group = $this->input->get('group');
		if($this->input->get('heat'))  $heat  = $this->input->get('heat');

		$players = $this->getRepository('Player')->getPlayersResults($this->game, $group, $heat);

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$groups = $this->getRepository('Player')->getGroupList($this->game);

		// find object item
		$item = $this->game->getItems()->filter(function($i) use ($item) {
			return $i->getName() == $item;
		})->first();

		$template = ($item->isPlayTwice()) ? '2' : '1';

		$this->load->view('gymnastics/admin/organization/game_item'.$template, [
			'game'  => $this->game,
			'item' 	=> $item,
			'grus'	=> $groups,
			'nrfd'	=> $item->getNumberRefereeD(),
			'nrfe'	=> $item->getNumberRefereeE(),
			'groupHeatPlayers' 	=> $groupHeatPlayers
		]);
		
	}

	/**
	 * order_setting
	 *
	 * return single item game's result in tournament
	 * 
	 * @param array
	 * @return array
	 */

	public function order_setting ()
	{
		$item = $this->input->get('item');
		// default group heat
		$group = $heat = 1;

		if($this->input->get('group')) $group = $this->input->get('group');
		if($this->input->get('heat'))  $heat = $this->input->get('heat');

		$item 	= $this->input->get('item');

		$players = $this->getRepository('Player')->findBy([
			'game' 	=> $this->game,
			'group' => $group,
			'heat' 	=> $heat,
		]);

		$item = $this->game->findItem($item);

		if($this->input->post('order')) {
			//update order
			$data_order = $this->input->post('order');

			if($data_order) {
				foreach ($data_order as $player_id => $order) {

					$filter = array_filter($players, function($p) use ($player_id){
						return $p->getId() == $player_id;
					});

					$player = end($filter);
					
					$player->setOrder(new \Entities\Order($item, $order));
					$this->em->persist($player);
				}

				$this->em->flush();
			}
		}

		$this->load->view('gymnastics/admin/organization/order_setting', array(
			'players' => $players,
			'game' => $this->game,
			'item' => $item
			));

	}

	/**
	 * updateStatus
	 *
	 * using ajax for update score status at game_list (admin), 
	 * all_game_list (referee) screen
	 *
	 * @author Thom
	 * @return json
	 */

	public function updateStatus()
	{
		try{
			// array of rows score id, 
			// which was selected from the list screen.
			$ids 	= $this->input->post('ids'); 
			$status = $this->input->post('status');

			$item = $this->input->get('item');
			$item = $this->game->findItem($item);

			if(!$item) throw new Exception('You have to select a item');
			
			foreach ($ids as $id) {

				if($this->game->isInputNormal())
				{
					$player = $this->getRepository('Player')->getPlayerScores($id);

					$scores = $player->getScores()->filter(function($s) use ($item){
						return $s->getItem() == $item->getName();
					});

					foreach ($scores as $score) {

						$singleItemScore = $player->getSingleItemScores()->filter(function($s) use ($score, $item) {
							return $s->getRound() == $score->getRound() && $s->getItem() == $item->getName();
						})->first();

						if(!$singleItemScore) $singleItemScore = new \Entities\SingleItemScore;

						//persist score status
						$score->setStatus($status);
						$score->setUnPick();

						$this->em->persist($score);

						// persist singleItemscore
						$singleItemScore->setPlayer($player);
						$singleItemScore->setItem($score->getItem());
						$singleItemScore->setRound($score->getRound());

						$numberReferreD = $item->getNumberRefereeD();
						$numberReferreE = $item->getNumberRefereeE();

						$fnSetScore = 'set'.$score->getType().'Score';

						if(method_exists($singleItemScore, $fnSetScore)) 
						{
							$singleItemScore->$fnSetScore($score->getValue());
						}

						$fnSetScoreStatus = $fnSetScore.'Status';

						if(method_exists($singleItemScore, $fnSetScoreStatus))
						{
							$singleItemScore->$fnSetScoreStatus($status);
						}

						// Notice: please keep order of set Score
						// If you change order, the Score's result can be wrong
						$singleItemScore->setDScore(null, $numberReferreD);
						$singleItemScore->setEScore(null, $numberReferreE);
						$singleItemScore->setDemeritScore();
						$singleItemScore->setFinalScore();
						$singleItemScore->setStatus($status);

						$player->registerSingleItemScore($singleItemScore, $item);
						
						$this->em->persist($player);
					}

					if($scores->count()==0)
					{
						$singleItemScores = $player->getSingleItemScores()->filter(function($s) use ($item) {
							return $s->getItem() == $item->getName();
						});

						if($singleItemScores->count()==0) continue;

						foreach ($singleItemScores as $singleItemScore) {
							$singleItemScore->setStatus($status);

							$this->em->persist($singleItemScore);
						}
					}
				}
				else
				{
					$player = $this->getRepository('Player')->getPlayerResults($id, $item);

					$singleItemScores = $player->getSingleItemScores();

					$singleItemScores = $singleItemScores->filter(function($s) use ($item) {
						return $s->getItem() == $item->getName();
					});

					foreach ($singleItemScores as $singleItemScore) {

						$singleItemScore->setStatus($status);

						$numberReferreD = $item->getNumberRefereeD();
						$numberReferreE = $item->getNumberRefereeE();

						// Score calculation
						for ($i=1; $i <= $numberReferreD; $i++) { 
							// set status
							$function = 'setD'.$i.'ScoreStatus';
							$singleItemScore->$function($status);
						}

						for ($i=1; $i <= $numberReferreE; $i++) { 
							// set status
							$function = 'setE'.$i.'ScoreStatus';
							$singleItemScore->$function($status);
						}

						$singleItemScore->setTime1ScoreStatus($status);
						$singleItemScore->setTime2ScoreStatus($status);
						$singleItemScore->setLine1ScoreStatus($status);
						$singleItemScore->setLine2ScoreStatus($status);

						$player->registerSingleItemScore($singleItemScore, $item);

						$this->em->persist($player);
					}
				}

				$this->em->flush();

				// push task
				$singleItemScores = $player->getSingleItemScores()->filter(function($s) use ($item) {
					return $s->getItem() == $item->getName();
				});
				
				foreach ($singleItemScores as $singleItemScore) 
				{
					$singleItemScoreId = $singleItemScore->getId();

					$task = $this->getRepository('Task')->findOneBy([
										'single_item_score_id' => $singleItemScoreId
						]);

					if(!$task) $task = new \Entities\Task;

					$task->setSingleItemScoreId($singleItemScoreId);

					$this->em->persist($task);
				}
			}

			$this->em->flush();

			$response = ['status' => 'success'];

		} catch (Exception $e){
			$response = ['status' => 'failed', 'message' => $e->getMessage()];
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * update_status_total
	 * using ajax for update score status at single_total_score, 
	 */
	public function update_status_total()
	{
		try{
			// array of rows score id, 
			// which was selected from the list screen.
			$ids 	= $this->input->post('ids'); 
			$status = $this->input->post('status');

			$players = $this->getRepository('Player')->findBy(['id' => $ids]);

			foreach ($players as $player) {

				$singleTotalScore = $player->getSingleTotalScore();

				if(!$singleTotalScore)
				{
					$singleTotalScore = new \Entities\SingleTotalScore;
					$player->addSingleTotalScore($singleTotalScore);
				}
				
				$singleTotalScore->setStatus($status); 

				$this->em->persist($singleTotalScore);
				$this->em->persist($player);
			}

			$this->em->flush();

			if ( ! $status) throw new Exception('Update status single total score fail');

			$response = ['status' => 'success'];

		} catch (Exception $e){
			$response = ['status' => 'failed', 'message' => $e->getMessage()];
		}

		echo json_encode($response);
		exit;
	}

	public function filter()
	{
		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		$items 			= $this->game->getItems();

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersResults($this->game, $group, $heat);

		$playerName = $this->input->get('player_name');
		$schoolName = $this->input->get('school_name');
		$prefecture = $this->input->get('prefecture');

		if(strlen($playerName))
		{
			$players = array_filter($players, function($player) use ($playerName){
				return $player->getPlayerName() == $playerName;
			});
		}

		if(strlen($prefecture))
		{
			$players = array_filter($players, function($player) use ($prefecture){
				return $player->getPrefecture() == $prefecture;
			});
		}

		if(strlen($schoolName))
		{
			if(strlen($schoolName))
			{
				$players = array_filter($players, function($player) use ($schoolName){
					return $player->getSchool()->getSchoolNameAb() == $schoolName;
				});
			}

			$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		}

		if(strlen($playerName) || strlen($prefecture))
		{
			$this->load->view('gymnastics/admin/referee/filter', array(
				'players'     => $players,
				'game'        => $this->game,
				'items'        => $items,
			));
		}else
		{
			$this->load->view('gymnastics/admin/referee/filter1', array(
				'game'              => $this->game,
				'items'             => $items,
				'groupHeatPlayers' 	=> $groupHeatPlayers,
			));
		}
	}

	public function filter_options()
	{
		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		if($heat <= 0) $heat = null;

		$items 			= $this->game->getItems();

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersByGroupHeat($this->game, $group, $heat);

		$getType = $this->input->get('type');
		$getValue = $this->input->get('value');

		$schoolList = [];
		$playerList = [];
		
		if($getType == 'pref' && strlen($getValue))
		{
			foreach ($players as $player) {
				if($player->getSchool()->getSchoolPrefecture() == $getValue && 
					FALSE === array_search($player->getSchool()->getSchoolNameAb(), $schoolList))
				{
					$schoolList[] = $player->getSchool()->getSchoolNameAb();
				}
			}

			foreach ($players as $player) {
				if(FALSE !== array_search($player->getSchool()->getSchoolNameAb(), $schoolList)
					&& FALSE === array_search($player->getPlayerName(), $playerList))
				{
					$playerList[] = $player->getPlayerName();
				}
			}
		}

		else if($getType == 'pref' && !strlen($getValue))
		{
			foreach ($players as $player) {
				if(FALSE === array_search($player->getSchool()->getSchoolNameAb(), $schoolList))
				{
					$schoolList[] = $player->getSchool()->getSchoolNameAb();
				}
			}
		}

		if($getType == 'school' && strlen($getValue))
		{
			foreach ($players as $player) {
				if($player->getSchool()->getSchoolNameAb() == $getValue)
				{
					$playerList[] = $player->getPlayerName();
				}
			}
		}

		else if($getType == 'school' && !strlen($getValue))
		{
			foreach ($players as $player) {
				$playerList[] = $player->getPlayerName();
			}
		}

		$response = [
			'schools' => $schoolList,
			'players' => $playerList
		];

		echo json_encode($response);
		exit();
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/CSVReader.php';

class Game_class extends Organization_Controller
{

    public function index($tournamentId)
    {
        $tournament = $this->getRepository('Tournament')->find($tournamentId);

        if(!$tournament) throw new Exception('Tournament not found');

        $game_hierarchy = $tournament->getGamesActive();
        
        $this->load->view('gymnastics/admin/organization/game_class', [
            'tournament' => $tournament,
            'game_hierarchy' => $game_hierarchy
            ]);
    }

    public function setting($tournamentId)
    {
        $data['tournament'] = $tournament = $this->getRepository('Tournament')->find($tournamentId);

        if (!$tournament) throw new Exception("Tournament not found!");

        $data['games'] = $tournament->getGames();

        if ($this->input->post()) {

            // params post
            $post = $this->input->post();

            $data['class']  = $class = detectClass($post['game']);
            $data['sex']    = $sex   = detectSex($post['game'], true);
            // repository
            $gameRepository    = $this->getRepository('Game');
            $schoolRepostitory = $this->getRepository('SchoolGroup');

            // found a tournament's game, which matched with class and sex attribute.
            $game = $gameRepository->getGameByClassAndSex($tournament, $class, $sex);

            // config upload
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '100';

            $this->load->library('upload', $config);

            // init csvreader class
            $csvreader = new CSVReader();

            if ($this->upload->do_upload('file_player')) {

                // csvreader setting
                $csvreader->separator = ',';
                $csvreader->fields = array(
                    'player_no', 'player_name', 'grade',
                    'school_no', 'school_name_ab', 'school_name', 'pref',
                    'group', 'heat', 'flag'
                );

                $upload_player_data = $this->upload->data();
                $player_contents = $csvreader->parse_file($upload_player_data['full_path']);
                unset($player_contents[0]);

                ///////////////////////
                // Player  		     //
                ///////////////////////
                $player_contents = $this->filterPlayerList($player_contents);
                // Condition to import csv
                // school_name_ab IS NOT NULL
                // school_name, grade, prefecture, school_no ALLOW NULL
                $filter_players = array_filter($player_contents, function($player){
                    return (empty($player['school_name_ab']) || $player['school_name_ab'] == '');
                });
                // Check update player
                if (empty($filter_players) && count($player_contents)) {
                    // remove old data
                    $players = $this->getRepository('Player')->findBy([
                        'game' => $game
                    ]);
                    if ( ! empty($players)) {
                        foreach ($players as $key => $player) {
                            $this->em->remove($player);
                        }
                    }

                    $schoolGroups = $game->getSchoolGroups();

                    foreach ($schoolGroups as $school) {
                        $this->em->remove($school);
                    }

                    $rotationSettings = $game->getRotationSettings();

                    foreach ($rotationSettings as $rst) {
                        $this->em->remove($rst);
                    }

                    $rotations = $game->getRotations();

                    foreach ($rotations as $r) {
                        $this->em->remove($r);
                    }

                    $this->em->flush();

                    $groupHeats = [];

                    foreach ($player_contents as $line)
                    {
                        $player = new \Entities\Player();
                        $player->setPlayerName($line['player_name']);
                        $player->setSex($sex);
                        $player->setGrade($line['grade']);
                        $player->setPlayerNo($line['player_no']);
                        $player->setSchoolNo($line['school_no']);
                        $player->setSchoolName($line['school_name']);
                        $player->setSchoolNameAb($line['school_name_ab']);
                        $player->setPrefecture($line['pref']);
                        $player->setGame($game);
                        $player->setFlag($line['flag']);
                        $player->setGroup($line['group']);
                        $player->setHeat($line['heat']);
                        $player->setTournament($tournament);

                        ///////////////////////
                        // School Group      //
                        ///////////////////////

                        $school = $game->checkSchoolExist($line['school_name_ab']);

                        if (!$school) 
                        {
                            $school = new \Entities\SchoolGroup();

                            $school->setSchoolName($line['school_name']);
                            $school->setSchoolNameAb($line['school_name_ab']);
                            $school->setSchoolNo($line['school_no']);
                            $school->setSchoolPrefecture($line['pref']);
                            $school->setGame($game);

                            $game->addSchoolGroup($school);
                        }

                        $player->setSchool($school);

                        $game->addPlayer($player);

                        $groupHeats[$line['group']][$line['heat']] = $line['heat'];
                    }

                    $config = $this->config->item('property')['item_'.$game->getStrSex()];
                }
            }
            else
            {
                if($_FILES['file_player']['name'])
                redirect('/admin/organization/tournament/'.$tournament->getId().'/game_class/setting?game='
                    .urlencode($post['game']).'&status=error', 'refresh');
            }

            $items = $post['item'];
            
            if (isset($items)) {
                $itemorgs = $game->getItems();
                foreach ($items as $key => $dt) {
                    if(strlen($items[$key]['name']) ||  strlen($items[$key]['flag_item']) ) {
                        $item = isset($itemorgs[$key - 1]) ? $itemorgs[$key - 1] : new \Entities\Item();
                        $item->setName($items[$key]['name']);
                        $item->setFlagItem($items[$key]['flag_item']);
                        $item->setNo($key);
                        $item->setNumberRefereeD($items[$key]['number_referee_d']);
                        $item->setNumberRefereeE($items[$key]['number_referee_e']);
                        $item->setSex($game->getSex());
                        $item->setGame($game);
                        $game->addItem($item);
                    }
                }
            }

            // Referee Auto create
            if(!$game->getReferees()->count()){
                // tao trong trai truong
                // basic name
                $refereeName = $game->getStrSex().$game->getClass().'審判長';

                $chiefReferee = new \Entities\Referee;
                $chiefReferee->setRefereeName($refereeName);
                $chiefReferee->setPassword('999');
                $chiefReferee->setRefereeType(2);
                $chiefReferee->setScoreType(1);
                $chiefReferee->setItem('');
                $chiefReferee->setFlagActive(true);
                $chiefReferee->setGame($game);

                $game->addReferee($chiefReferee);

                // tao trong tai chinh (moi item 1 trong tai)
                $gameItems = $game->getItems();
                foreach ($gameItems as $key => $item) {

                    $refereeName = $game->getStrSex().$game->getClass().$item->getName().'主審';

                    $mainReferee = new \Entities\Referee;
                    $mainReferee->setRefereeName($refereeName);
                    $mainReferee->setPassword('11');
                    $mainReferee->setItem($item->getName());
                    $mainReferee->setScoreType(2);
                    $mainReferee->setRefereeType(1);
                    $mainReferee->setFlagActive(true);
                    $mainReferee->setGame($game);

                    $game->addReferee($mainReferee);
                }

                // tao trong tai thuong
                $scoreMap = array(
                    3  => 'D1Score',
                    4  => 'D2Score',
                    5  => 'D3Score',
                    6  => 'D4Score',
                    7  => 'D5Score',
                    8  => 'E1Score',
                    9  => 'E2Score',
                    10 => 'E3Score',
                    11 => 'E4Score',
                    12 => 'E5Score',
                    13 => 'Time1Score',
                    14 => 'Time2Score',
                    15 => 'Line1Score',
                    16 => 'Line2Score'
                    );


                foreach ($gameItems as $k => $item) {

                    $numberRefereeD = $item->getNumberRefereeD();
                    $numberRefereeE = $item->getNumberRefereeE();

                    for($i=1; $i <= $numberRefereeD; $i++)
                    {
                        $refereeName = $game->getStrSex().$game->getClass().$item->getName().'D'.$i;

                        $normalReferee = new \Entities\Referee;
                        $normalReferee->setRefereeName($refereeName);
                        $normalReferee->setScoreType($i+2);
                        $normalReferee->setItem($item->getName());
                        $normalReferee->setPassword('1');
                        $normalReferee->setRefereeType(0);
                        $normalReferee->setFlagActive(true);
                        $normalReferee->setGame($game);

                        $game->addReferee($normalReferee);
                    }

                    for($i=1; $i <= $numberRefereeE; $i++)
                    {
                        $refereeName = $game->getStrSex().$game->getClass().$item->getName().'E'.$i;

                        $normalReferee = new \Entities\Referee;
                        $normalReferee->setRefereeName($refereeName);
                        $normalReferee->setScoreType($i+7);
                        $normalReferee->setItem($item->getName());
                        $normalReferee->setPassword('1');
                        $normalReferee->setRefereeType(0);
                        $normalReferee->setFlagActive(true);
                        $normalReferee->setGame($game);

                        $game->addReferee($normalReferee);
                    }

                    for($i=1; $i <= 2; $i++)
                    {
                        // referee Time
                        $refereeName = $game->getStrSex().$game->getClass().$item->getName().'タイム'.$i;

                        $normalReferee = new \Entities\Referee;
                        $normalReferee->setRefereeName($refereeName);
                        $normalReferee->setScoreType($i+12);
                        $normalReferee->setItem($item->getName());
                        $normalReferee->setPassword('1');
                        $normalReferee->setRefereeType(0);
                        $normalReferee->setFlagActive(true);
                        $normalReferee->setGame($game);

                        $game->addReferee($normalReferee);

                        // referee Line
                        $refereeName = $game->getStrSex().$game->getClass().$item->getName().'ライン'.$i;

                        $normalReferee = new \Entities\Referee;
                        $normalReferee->setRefereeName($refereeName);
                        $normalReferee->setScoreType($i+14);
                        $normalReferee->setItem($item->getName());
                        $normalReferee->setPassword('1');
                        $normalReferee->setRefereeType(0);
                        $normalReferee->setFlagActive(true);
                        $normalReferee->setGame($game);

                        $game->addReferee($normalReferee);
                    }
                }

            }

            ////////////////////////
            // Game Class Setting //
            ////////////////////////

            $game->setSinglePassNumber($this->input->post('single_pass_number'));
            $game->setGroupPassNumber($this->input->post('group_pass_number'));
            $game->setItemPassNumber($this->input->post('item_pass_number'));
            $game->setInputType($this->input->post('input_type'));
            $game->setInputLineScore($this->input->post('input_line_score'));
            $game->setInputTimeScore($this->input->post('input_time_score'));
            $game->setInputFinalRound($this->input->post('input_final_round'));
            $game->setPreliminary($this->input->post('preliminary'));
            $game->setActive(true);

            $this->em->persist($game);
            $this->em->flush();

            redirect('/admin/organization/tournament/'.$tournament->getId().'/game_class/setting?status=success&game='.urlencode($post['game']), 'refresh');
        }

        $this->load->view('gymnastics/admin/organization/game_class_setting', $data);
    }

    public function download_csv()
    {
        $gameId = $this->input->get('gid');

        $game = $this->getRepository('Game')->find($gameId);

    	$players_csv = $this->getRepository('Player')->findBy(array('game' => $game));
        
		$list = array();
		$count = 0;

		$list[$count][0]  = "No";
		$list[$count][1]  = "選手名";
        $list[$count][2]  = "学年";
		$list[$count][3]  = "学校No";
		$list[$count][4]  = "学校名（略称）";
		$list[$count][5]  = "学校名";
		$list[$count][6]  = "都道府県";
        $list[$count][7]  = "班";
        $list[$count][8]  = "組";
		$list[$count][9] = "個or団";

		foreach ($players_csv as $player) {
			$count++;
			
			$list[$count][0]  = $player->getPlayerNo();
			$list[$count][1]  = $player->getPlayerName();
            $list[$count][2]  = $player->getGrade();
			$list[$count][3]  = $player->getSchoolNo();
			$list[$count][4]  = $player->getSchoolNameAb();
			$list[$count][5]  = $player->getSchoolName();
			$list[$count][6]  = $player->getPrefecture();
            $list[$count][7]  = $player->getGroup();
            $list[$count][8]  = $player->getHeat();
			$list[$count][9]  = convert_game_type($player->getFlag());
		}

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream'); 
		header("Content-Disposition: attachment; filename=players_" . date("Ymd") . ".csv");
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		echo "\xEF\xBB\xBF"; // UTF-8 BOM
		$fp = fopen('php://output', 'w');
		foreach ($list as $fields) {
		    fputs($fp, implode($fields, ',')."\n"); // prevent quote character
		}
		fclose($fp);
		exit();
    }

    /**
     * Sort data from csv to set order player for in group, heat;
     */
    private function filterPlayerList($list_player = [])
    {
        if ( ! empty($list_player)) {
            $tmp_players = [];
            sksort($list_player, 'flag');
            foreach ($list_player as $key => $player) {
                $group_heat                 = $player['group'] . $player['heat'];
                $player_no                  = $player['player_no'];
                $player['flag']             = convertGameTypeStringToBool($player['flag']);
                $player['group_heat']       = $group_heat;
                $tmp_players[$group_heat][$player_no] = $player;
            }

            // Set order player of group player
            foreach ($tmp_players as $key => $players) {
                $order = 1;
                foreach ($players as $player_no => $player) {
                    $player['order'] = $order;
                    $tmp_players[$key][$player_no] = $player;
                    $order++;
                }
            }
            // Order group, heat mix -> max
            ksort($tmp_players);

            // In this case player_no from file csv is unique
            // to not replace value
            $tmp_lists = [];
            foreach ($tmp_players as $players) {
                $tmp_lists = array_merge($tmp_lists, $players);
            }

            $list_player = $tmp_lists;
        }
        return $list_player;
    }
}

<?php
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Collection\GroupPlayCollection;
use Entities\Collection\RankingCollection;
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_output extends Organization_Controller {

	protected $players;

	public function __construct()
	{
		parent::__construct();

		$gameId = $this->input->get('gid');

		$this->game = $this->getRepository('Game')->find($gameId);

		if(!$this->game) throw new Exception('Game not found!');

		$this->excel  = new PHPExcel;
	}

	public function index()
	{
		if($this->input->post())
		{
			$this->players = $this->getRepository('Player')->getPlayersResults($this->game);

			$i = 0;
			
			if($this->input->post('choose_group') && $this->input->post('group_include_score'))
			{
				$this->drawsheet1($i);
				$i++;
			}
			if($this->input->post('choose_group') && !$this->input->post('group_include_score'))
			{
				$this->drawsheet3($i);
				$i++;
			}
			if($this->input->post('choose_single') && !$this->input->post('single_include_2nd_score'))
			{
				$this->drawsheet4($i);
				$i++;
			}
			if($this->input->post('choose_single') && $this->input->post('single_include_2nd_score'))
			{
				$this->drawsheet5($i);
				$i++;
			}
			if($this->input->post('certificate'))
			{
				$this->drawsheet6($i);
				$i++;
			}
			if($this->input->post('choose_final'))
			{
				$this->drawsheet7($i);
				$i++;
			}
			if($this->input->post('choose_detail'))
			{
				$this->drawsheet8($i);
				$i++;
			}
			if($this->input->post('choose_rotation'))
			{
				$types = $this->input->post('choose_rotation');

				foreach ($types as $key => $type) {

					if($type){

						$group = ($this->input->post('choose_rotation_group')[$key])?$this->input->post('choose_rotation_group')[$key]:1;
						$event = ($this->input->post('choose_rotation_event')[$key])?$this->input->post('choose_rotation_event')[$key]:null;

						if($event == NULL)
						{
							$groupRotations	= $this->game->findFirstRotationGroup($group);

							foreach ($groupRotations as $count => $rst) {
								$event = $count+1;
								$this->drawsheet9($i, $event, $group, $key);
							}
						}
						else{
							$this->drawsheet9($i, $event, $group, $key);
						}
					}
				}
			}
			if($this->input->post('choose_player_list'))
			{
				$this->drawsheet10($i);
				$i++;
			}
			if($this->input->post('choose_school') && !$this->input->post('school_include_rank'))
			{
				$this->drawsheet11($i);
				$i++;
			}

			if($this->input->post('choose_school') && $this->input->post('school_include_rank'))
			{
				$this->drawsheet12($i);
				$i++;
			}

			$this->excel->removeSheetByIndex($i);

			$this->export();
		}

		$template = ($this->game->getClass() == '決勝') ? '1' : '0';

		$rotationSettings = $this->game->getRotationSettings();

		$this->load->view('gymnastics/admin/organization/excel_output'.$template, [
			'game' => $this->game,
			'rotationSettings' => $rotationSettings
			]);
	}

	private function drawsheet1($activeSheetIndex = 0)
	{
		$group = ($this->input->post('select_group'))?$this->input->post('select_group'):1;

		// make ranking for all players
		if($this->input->post('group_include_rank')) {
			$resultRanking 	= RankingCollection::implementRanking($this->players, 'getTotalFinalScore');
		}

		// filter players play in group
		$players = array_filter($this->players, function($p) use ($group) {
			return $p->getGroup() == $group;
		});

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        /////////////////////////
        // start excel draw    //
        /////////////////////////

		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('班別速報'.$this->game->getStrSex());

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '班別速報　'.$this->game->getStrSex().'　【'.$group.'班】');

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', '組');
		$this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		$this->excel->getActiveSheet()->mergeCells('E4:E5');
		$this->excel->getActiveSheet()->mergeCells('F4:F5');
		$this->excel->getActiveSheet()->mergeCells('G4:G5');
		
		// head item score
		$itemNameHeatCol = 'H';
		$cr->curCol = 'G';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		foreach ($items as $item) {

			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'4', $itemName);
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'D');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'E');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '減点');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '合計');
			$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
			// get col for next item name
			$colidx = $cr->getIndexCol($itemNameHeatCol);
			$itemNameHeatCol = $cr->rgeCol[$colidx+4];
		}

		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合\n得点");
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);

		if($this->input->post('group_include_rank')) {
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '順位');
		}

		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;

        $heats = $groupHeatPlayers->first()->getHeats();

        foreach ($heats as $heat)
        {
    		$heatCol = 'B';
    		$heatRow = $cr->curRow();

    		$heatPlayers = $heat->getPlayers();

    		foreach ($heatPlayers as $player)
    		{
    			$cr->curCol = $heatCol;

    			// player info
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			
    			// item score
    			foreach ($items as $item) {
					$itemName  = $item->getName();
					$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

    				$this->excel->getActiveSheet()->SetCellValue(
    					$cr->incCol().$cr->curRow(),
    					($playerOff) ? '-' : $player->getItemBestScoreValue($item, 'DScore')
    				);
    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    				$this->excel->getActiveSheet()->SetCellValue(
    					$cr->incCol().$cr->curRow(),
    					($playerOff) ? '-' : $player->getItemBestScoreValue($item, 'EScore')
    				);
    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    				$this->excel->getActiveSheet()->SetCellValue(
    					$cr->incCol().$cr->curRow(),
    					($playerOff) ? '-' : $player->getItemBestScoreValue($item, 'DemeritScore')
    				);
    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    				$this->excel->getActiveSheet()->SetCellValue(
    					$cr->incCol().$cr->curRow(),
    					($playerOff) ? '-' : $player->getItemBestScoreValue($item, 'FinalScore')
    				);
    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    			}

    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

    			if($this->input->post('group_include_rank')) {
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $resultRanking->findRank($player));
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    		}

    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        ),
			                    ),
			                )
			            );

    			$cr->incRow();
    		}

    		// draw a border line when end of each heat
    		$this->excel->getActiveSheet()->getStyle($heatCol.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );


    		$heatRange = $heatCol.$heatRow.':'.$heatCol.$cr->getPrevRow();
    		
    		// merge range cell for heat
    		$this->excel->getActiveSheet()->mergeCells($heatRange);
    		$this->excel->getActiveSheet()->SetCellValue($heatCol.$heatRow, $heat->getId());

    		$this->excel->getActiveSheet()->getStyle($heatCol.$heatRow)->applyFromArray(array('font'  => array('name'  => 'Verdana')));

    		// set text align center for heat value cell
    		$this->excel->getActiveSheet()->getStyle($heatRange)->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
		            );
		}

		// draw right line seperate for each item
		$itemHeatCol = 'K';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+4];
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// draw right line seperate for total score and ranking
		$colidx = $cr->getIndexCol($itemHeatCol);
		$rankiHeatCol = $cr->rgeCol[$colidx-2]; // -2 because we've +4 at last foreach loop
		$totalHeatCol = $cr->rgeCol[$colidx-3];

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );



		if($this->input->post('group_include_rank')) {

			$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'right' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );
		}

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet2($activeSheetIndex = 0)
	{
		$group = ($this->input->post('select_group'))?$this->input->post('select_group'):1;
		
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);

		$groupHeatPlayers = $this->getRepository('Player')
        						->getPlayersInGroupHeat($this->game, $group);

        // tinh toan diem theo nhom group
		$groupScores = [];

		foreach ($groupHeatPlayers as $group => $heats) {
			foreach ($heats as $heat => $players) {
				$groupScores[$group][$heat] = $this->calculateGroupScore($players);
			}
		}

        $items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '班別速報　'.$this->game->getStrSex().'　【'.$group.'班】');

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', '組');
		$this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		$this->excel->getActiveSheet()->mergeCells('E4:E5');
		$this->excel->getActiveSheet()->mergeCells('F4:F5');
		$this->excel->getActiveSheet()->mergeCells('G4:G5');
		
		// head item score
		$itemNameHeatCol = 'H';
		$cr->curCol = 'G';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		foreach ($items as $item) {

			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'4', $itemName);
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'D');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'E');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '減点');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '合計');
			$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
			// get col for next item name
			$colidx = $cr->getIndexCol($itemNameHeatCol);
			$itemNameHeatCol = $cr->rgeCol[$colidx+4];
		}

		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合\n得点");
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '順位');
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;

        foreach ($groupHeatPlayers[$group] as $heat => $players)
        {
    		$heatCol = 'B';
    		$heatRow = $cr->curRow();

    		$displayed_group = false;

    		foreach ($players as $player)
    		{
    			if ($player->getFlag() == 0  && $displayed_group){
    			// 	// d($cr->curRow());
    			// 	// dd($cr->curCol());
    				$this->excel->getActiveSheet()->mergeCells('C'.$cr->curRow().':E'.$cr->curRow());
    				$this->excel->getActiveSheet()->mergeCells('F'.$cr->curRow().':G'.$cr->curRow());

    				$this->excel->getActiveSheet()->SetCellValue('C'.$cr->curRow(), 'ベスト３');

    				$cr->curCol  = 'G';

    				// item score
	    			foreach ($items as $item) {
	    				// $itemName = $item->getName();
	    				// $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $groupScores[$heat][$item->getName()]);
	    				// $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $groupScores[$heat][$item->getName()]);
	    				// $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $groupScores[$heat][$item->getName()]);
	    				// $cr->incCol();
	    			}

    				$cr->incRow();
    			}

    			$displayed_group = $player->getFlag();

    			$cr->curCol = $heatCol;

    			// player info
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
    			
    			// item score
    			foreach ($items as $item) {
    				$itemName = $item->getName();
    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getHigherItemDScore($itemName));
    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getHigherItemEScore($itemName));
    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getHigherItemDemeritScore($itemName));
    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getHigherItemFinalScore($itemName));
    			}

    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getRanking());

    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        ),
			                    ),
			                )
			            );

    			$cr->incRow();
    		}

    		// draw a border line when end of each heat
    		$this->excel->getActiveSheet()->getStyle($heatCol.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );


    		$heatRange = $heatCol.$heatRow.':'.$heatCol.$cr->getPrevRow();
    		
    		// merge range cell for heat
    		$this->excel->getActiveSheet()->mergeCells($heatRange);
    		$this->excel->getActiveSheet()->SetCellValue($heatCol.$heatRow, $heat);
    		// set text align center for heat value cell
    		$this->excel->getActiveSheet()->getStyle($heatRange)->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
		            );
		}

		// draw right line seperate for each item
		$itemHeatCol = 'K';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+4];
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// all cell in table font size 11
		$this->excel->getActiveSheet()->getStyle('B6:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                )
		            );


		// draw right line seperate for total score and ranking
		$colidx = $cr->getIndexCol($itemHeatCol);
		$rankiHeatCol = $cr->rgeCol[$colidx-2]; // -2 because we've +4 at last foreach loop
		$totalHeatCol = $cr->rgeCol[$colidx-3];

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
	}

	private function drawsheet3($activeSheetIndex = 0)
	{
		$group = ($this->input->post('select_group'))?$this->input->post('select_group'):1;
		
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('班別速報'.$this->game->getStrSex().' （詳細なし）');

		// make ranking for all players
		if($this->input->post('group_include_rank')) {
			$resultRanking 	= RankingCollection::implementRanking($this->players, 'getTotalFinalScore');
		}

		// filter players play in group
		$players = array_filter($this->players, function($p) use ($group) {
			return $p->getGroup() == $group;
		});

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

        $items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '班別速報　'.$this->game->getStrSex().'　【'.$group.'班】');

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', '組');
		$this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');
		
		// head item score
		$cr->curCol = 'G';
		foreach ($items as $item) {
			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', $itemName);
		}

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合");
		if($this->input->post('group_include_rank')) {
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '順位');
		}

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 5;

        $heats = $groupHeatPlayers->first()->getHeats();

        foreach ($heats as $heat)
        {
    		$heatCol = 'B';
    		$heatRow = $cr->curRow();

    		$heatPlayers = $heat->getPlayers();

    		foreach ($heatPlayers as $player)
    		{
    			$cr->curCol = $heatCol;

    			// player info
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
    			
    			// item score
    			foreach ($items as $item) {
					$itemName  = $item->getName();
					$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

    				$this->excel->getActiveSheet()->SetCellValue(
    					$cr->incCol().$cr->curRow(),
    					($playerOff) ? '-' : $player->getItemBestScoreValue($item, 'FinalScore')
    				);
    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    			}

    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
    			
    			if($this->input->post('group_include_rank')) {
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $resultRanking->findRank($player));
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    		}

    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        ),
			                    ),
			                )
			            );

    			$cr->incRow();
    		}

    		// draw a border line when end of each heat
    		$this->excel->getActiveSheet()->getStyle($heatCol.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );


    		$heatRange = $heatCol.$heatRow.':'.$heatCol.$cr->getPrevRow();
    		
    		// merge range cell for heat
    		$this->excel->getActiveSheet()->mergeCells($heatRange);
    		$this->excel->getActiveSheet()->SetCellValue($heatCol.$heatRow, $heat->getId());

    		$this->excel->getActiveSheet()->getStyle($heatCol.$heatRow)->applyFromArray(array('font'  => array('name'  => 'Verdana')));

    		// set text align center for heat value cell
    		$this->excel->getActiveSheet()->getStyle($heatRange)->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
		            );
		}

		// draw right line seperate for each item
		$itemHeatCol = 'H';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$itemHeatCol = $cr->getNextCol($itemHeatCol);
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );


		// draw right line seperate for total score and ranking
		$rankiHeatCol = $itemHeatCol;
		$totalHeatCol = $cr->getNextCol($rankiHeatCol);

		if($this->input->post('group_include_rank')) {
			$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'right' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );
		}


		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet4($activeSheetIndex = 0)
	{
		$group = ($this->input->post('select_group'))?$this->input->post('select_group'):1;
		
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('個人総合'.$this->game->getStrSex());

		//single ranking
		$singlePlayers = array_filter($this->players, function($player){
			return $player->isPlayAsSingle();
		});

		$resultSingleRanking = RankingCollection::implementRanking($singlePlayers, 'getTotalFinalScore');
		// Filter case player is cancle and not score
		$filterSingleRanking = $resultSingleRanking->partition(function($k, $rank) {
			return ($rank->getElement()->getFlagCancle() && !$rank->getElement()->hasItemScoreFull());
		});

		// Move down player is cancel
		// Update data
		$resultSingleRanking = $filterSingleRanking[1]->toArray() + $filterSingleRanking[0]->toArray();

        $items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        // make ranking by item
		$itemRanking = [];

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($this->players, 'getItemBestScoreValue', [$item, 'FinalScore']);
		}

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '個人総合順位表　'.$this->game->getStrSex().$this->game->getClass());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

        // head player info
		// $this->excel->getActiveSheet()->SetCellValue('B4', "順\n位");
		// $this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		// $this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		// $this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		// $this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		// $this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');

		// remove rank
		$this->excel->getActiveSheet()->SetCellValue('B4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('C4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('D4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('E4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('F4', '都道府県');
		$this->excel->getActiveSheet()->getStyle('F4')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 9,
			                        'name'  => 'Verdana'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		$this->excel->getActiveSheet()->mergeCells('E4:E5');
		$this->excel->getActiveSheet()->mergeCells('F4:F5');
		//$this->excel->getActiveSheet()->mergeCells('G4:G5');
		
		// head item score
		$itemNameHeatCol = 'G';
		$cr->curCol = 'F';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		foreach ($items as $item) {

			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'4', $itemName);
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '合計');
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '順位');
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(4);
			$this->excel->getActiveSheet()->getStyle($cr->curCol() .'5')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 8,
			                        'name'  => 'Verdana'
			                    ),
		                )
	            );
			$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
			// get col for next item name
			$colidx = $cr->getIndexCol($itemNameHeatCol);
			$itemNameHeatCol = $cr->rgeCol[$colidx+2];
		}

		$this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合点");
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '点差');
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(7.5);

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        // 'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;
    		
		$heatCol = 'A';
		$heatRow = $cr->curRow();

		foreach ($resultSingleRanking as $rank)
		{
			$player = $rank->getElement();
			$cr->curCol = $heatCol;
			// player info
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			
			// item score
			foreach ($items as $item) {
				$itemName  = $item->getName();
				$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

				if ($player->getFlagCancle() && !$player->hasItemScoreFull()) {
					$scoreItemBestValue = '-';
					$rankPlayer         = '';
				} else {
					$scoreItemBestValue = $player->getItemBestScoreValue($item, 'FinalScore');
					$rankPlayer         = $resultItemRanking[$item->getName()]->findRank($player);
				}
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $scoreItemBestValue);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					array(
						'font'  => array('name'  => 'Verdana'),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
							'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					)
				);
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rankPlayer);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			}

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			if ($player->getFlagCancle() && !$player->hasItemScoreFull()) {
				$compareScore = '';
			} elseif (isset($aboveScore)) {
				$compareScore = ($aboveScore - $player->getTotalFinalScore());
			} else {
				$compareScore = '-';
			}
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $compareScore);
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			if($compareScore === '-')
			{
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );
			}

			// su dung de so sanh 2 diem cua 2 player tren va duoi.
			$aboveScore = $player->getTotalFinalScore();

			$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        ),
		                    ),
		                )
		            );

			$cr->incRow();
		}

		// draw right line seperate for each item
		$itemHeatCol = 'G';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+2];
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw bottom line end table
		$this->excel->getActiveSheet()->getStyle('B'.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// draw right line seperate for total score and ranking
		$rankiHeatCol = $cr->getNextCol($itemHeatCol);
		$totalHeatCol = $cr->getNextCol($rankiHeatCol);

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8);

		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet5($activeSheetIndex = 0)
	{
		$group = ($this->input->post('select_group'))?$this->input->post('select_group'):1;
		
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('個人総合'.$this->game->getStrSex());

		//single ranking
		$singlePlayers = array_filter($this->players, function($player){
			return $player->isPlayAsSingle();
		});

		$resultSingleRanking = RankingCollection::implementRanking($singlePlayers, 'getTotalFinalScore');
		// Filter case player is cancle and not score
		$filterSingleRanking = $resultSingleRanking->partition(function($k, $rank) {
			return ($rank->getElement()->getFlagCancle() && !$rank->getElement()->hasItemScoreFull());
		});

		// Move down player is cancel
		// Update data
		$resultSingleRanking = $filterSingleRanking[1]->toArray() + $filterSingleRanking[0]->toArray();

        $items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        // make ranking by item
		$itemRanking = [];

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($this->players, 'getItemBestScoreValue', [$item, 'FinalScore']);
		}

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '個人総合順位表　'.$this->game->getStrSex().$this->game->getClass());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', "順\n位");
		$this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');
		$this->excel->getActiveSheet()->getStyle('G4')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 9,
			                        'name'  => 'Verdana'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		$this->excel->getActiveSheet()->mergeCells('E4:E5');
		$this->excel->getActiveSheet()->mergeCells('F4:F5');
		$this->excel->getActiveSheet()->mergeCells('G4:G5');
		
		// head item score
		$itemNameHeatCol = 'H';
		$cr->curCol = 'G';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		foreach ($items as $item) {

			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'4', $itemName);

			if(!$item->isPlayTwice()) {
	
                $this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '合計');
				$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '順位');
				$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(4);
				$this->excel->getActiveSheet()->getStyle($cr->curCol() .'5')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 8,
				                        'name'  => 'Verdana'
				                    ),
			                )
		            );

				$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
				// get col for next item name
				$colidx = $cr->getIndexCol($itemNameHeatCol);
				$itemNameHeatCol = $cr->rgeCol[$colidx+2];
	
            }else {
	
                $this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '1回目');
                $this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '2回目');
                $this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '得点');
                $this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '順位');
                $this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(4);
                $this->excel->getActiveSheet()->getStyle($cr->curCol() .'5')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 8,
				                        'name'  => 'Verdana'
				                    ),
			                )
		            );

                $this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
				// get col for next item name
				$colidx = $cr->getIndexCol($itemNameHeatCol);
				$itemNameHeatCol = $cr->rgeCol[$colidx+4];
            }

			
		}

		$this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合点");
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '点差');
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(7.5);

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        // 'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;
    		
		$heatCol = 'A';
		$heatRow = $cr->curRow();

		foreach ($resultSingleRanking as $rank)
		{
			$player = $rank->getElement();
			$cr->curCol = $heatCol;
			// player info
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			
			// item score
			foreach ($items as $item) {
				$itemName  = $item->getName();
				$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

				if ($player->getFlagCancle() && !$player->hasItemScoreFull()) {
					$scoreItemBestValue = '-';
					$rankPlayer         = '';
				} else {
					$scoreItemBestValue = $player->getItemBestScoreValue($item, 'FinalScore');
					$rankPlayer         = $resultItemRanking[$item->getName()]->findRank($player);
				}

				if(!$item->isPlayTwice()) {

					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $scoreItemBestValue);
					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
						array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
					);
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rankPlayer);
					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
				}
				else
				{
					if ($player->getFlagCancle() && !$player->hasItemScoreFull()) {
                        $scoreItemBestValue1 = '-';
                        $scoreItemBestValue2 = '-';
                        $rankPlayer          = '';
                    } else {
                        $scoreItemBestValue1 = $player->getItemScoreValue($item, 'FinalScore',1);
                        $scoreItemBestValue2 = $player->getItemScoreValue($item, 'FinalScore',2);
                        $rankPlayer          = $resultItemRanking[$itemName]->findRank($player);
                    }
	
                    $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $scoreItemBestValue1);
                    $this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	
                        array(
                            'font'  => array('name'  => 'Verdana'),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                            )
                        )
                    );
	
                    $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $scoreItemBestValue2);
                    $this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	
                        array(
                            'font'  => array('name'  => 'Verdana'),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	
                            )
                        )
                    );
	
                    $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'FinalScore'));
                    $this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
                    $this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rankPlayer);
                    $this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
				}
			}

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			if ($player->getFlagCancle() && !$player->hasItemScoreFull()) {
				$compareScore = '';
			} elseif (isset($aboveScore)) {
				$compareScore = ($aboveScore - $player->getTotalFinalScore());
			} else {
				$compareScore = '-';
			}
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $compareScore);
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			if($compareScore === '-')
			{
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );
			}

			// su dung de so sanh 2 diem cua 2 player tren va duoi.
			$aboveScore = $player->getTotalFinalScore();

			$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        ),
		                    ),
		                )
		            );

			$cr->incRow();
		}

		// draw right line seperate for each item
		$itemHeatCol = 'G';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = ($item->isPlayTwice()) ? $cr->rgeCol[$colidx+4] : $cr->rgeCol[$colidx+2];
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw bottom line end table
		$this->excel->getActiveSheet()->getStyle('B'.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// draw right line seperate for total score and ranking
		$rankiHeatCol = $cr->getNextCol($itemHeatCol);
		$totalHeatCol = $cr->getNextCol($rankiHeatCol);

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8);

		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet6($activeSheetIndex = 0)
	{
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('入賞者（団体なし）');

		$items = $this->game->getItems();
        $tournament = $this->game->getTournament();

		//single ranking
		$singlePlayers = array_filter($this->players, function($player){
			return $player->isPlayAsSingle();
		});

		$resultSingleRanking = RankingCollection::implementRanking($singlePlayers, 'getTotalFinalScore')->slice(0,10);

        // make ranking by item
		$itemRanking = [];

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($this->players, 'getItemBestScoreValue', [$item, 'FinalScore'])->slice(0,10);
		}

		// start draw
        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '入賞者一覧　'.$this->game->getStrSex().$this->game->getClass());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		// draw table single ranking data
        $startRow = $cr->curRow = 7;
    		
		$startCol = 'A';
		$startRow = $cr->curRow();

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// single ranking table head
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow(), "順\n位");
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+2].$cr->getPrevRow(), 'No.');
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+3].$cr->getPrevRow(), '氏名');
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+4].$cr->getPrevRow(), "学\n年");
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+5].$cr->getPrevRow(), '学校');
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+6].$cr->getPrevRow(), '都道府県');
		$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow(), '得点');

		// item title
		$cell = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->curRow-2);
		$this->excel->getActiveSheet()->SetCellValue($cell, '個人総合');

		$range = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->curRow-2).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+2].($cr->curRow-2);
		$this->excel->getActiveSheet()->mergeCells($range);

		// style title
		$this->excel->getActiveSheet()->getStyle($range)->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => true,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		// style table head
		$this->excel->getActiveSheet()->getStyle($cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow().':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow())->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => $tableBgColor)
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => 'FFFFFF'),
				                        'size'  => 11,
				                        'name'  => 'ＭＳ ゴシック'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
			                )
		            );

		foreach ($resultSingleRanking as $rank)
		{
			$player = $rank->getElement();

			$cr->curCol = $startCol;
			// player info
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        ),
		                    ),
		                )
		            );

			$cr->incRow();
		}

		// style (color brow) for 2 last row
		$range = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->getPrevRow()-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
		$this->excel->getActiveSheet()->getStyle($range)->applyFromArray(
	                array(
	                    'fill' => array(
	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                        'color' => array('rgb' => 'D8D8D8')
		                ),
	                )
	            );

		// table border
		$rangeTop = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].($startRow-1);
		$this->excel->getActiveSheet()->getStyle($rangeTop)->applyFromArray(
	                array(
	                    'borders' => array(
	                        'top' => array(
	                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                            'color' => array('argb' => '000000'),
	                        )
	                    ),
	                )
	            );

		$rangLeft = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow();
		$this->excel->getActiveSheet()->getStyle($rangLeft)->applyFromArray(
	                array(
	                    'borders' => array(
	                        'left' => array(
	                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                            'color' => array('argb' => '000000'),
	                        )
	                    ),
	                )
	            );

		$rangRight = $cr->rgeCol[$cr->getIndexCol($startCol)+7].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
		$this->excel->getActiveSheet()->getStyle($rangRight)->applyFromArray(
	                array(
	                    'borders' => array(
	                        'right' => array(
	                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                            'color' => array('argb' => '000000'),
	                        )
	                    ),
	                )
	            );

		$rangBottom = $cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow().':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
		$this->excel->getActiveSheet()->getStyle($rangBottom)->applyFromArray(
	                array(
	                    'borders' => array(
	                        'bottom' => array(
	                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                            'color' => array('argb' => '000000'),
	                        )
	                    ),
	                )
	            );

		
		//==============================================================//
		//==================== draw item ===============================//
		//==============================================================//

		for($i=0; $i<$items->count(); $i++)
		{
			$startCol = 'I';

			if($i%2==0) $startCol = 'A';

			$cr->curRow = $startRow = 20 + (13 * floor(($i)/2));

			// item title
			$cell = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->curRow-2);
			$this->excel->getActiveSheet()->SetCellValue($cell, $items[$i]->getName());

			$range = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->curRow-2).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+2].($cr->curRow-2);
			$this->excel->getActiveSheet()->mergeCells($range);

			// style title item
			$this->excel->getActiveSheet()->getStyle($range)->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => $tableBgColor)
				                    ),
				                    'font'  => array(
				                        'bold'  => true,
				                        'color' => array('rgb' => 'FFFFFF'),
				                        'size'  => 11,
				                        'name'  => 'ＭＳ ゴシック'
				                    ),
			                )
		            );



			// item table head
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow(), "順\n位");
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+2].$cr->getPrevRow(), 'No.');
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+3].$cr->getPrevRow(), '氏名');
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+4].$cr->getPrevRow(), "学\n年");
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+5].$cr->getPrevRow(), '学校');
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+6].$cr->getPrevRow(), '都道府県');
			$this->excel->getActiveSheet()->SetCellValue($cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow(), '得点');

			// style table head
			$this->excel->getActiveSheet()->getStyle($cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow().':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow())->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => $tableBgColor)
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => 'FFFFFF'),
				                        'size'  => 11,
				                        'name'  => 'ＭＳ ゴシック'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
			                )
		            );


			foreach ($resultItemRanking[$items[$i]->getName()] as $rank) {

				$player = $rank->getElement();

				$cr->curCol = $startCol;
				// player info
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($items[$i], 'FinalScore'));
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

				$this->excel->getActiveSheet()->getStyle($cr->getNextCol($startCol).$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        ),
			                    ),
			                )
			            );

				$cr->incRow();
			}

			// style (color brow) for 2 last row
			$range = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($cr->getPrevRow()-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
			$this->excel->getActiveSheet()->getStyle($range)->applyFromArray(
		                array(
		                    'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'D8D8D8')
			                ),
		                )
		            );


			// table border
			$rangeTop = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].($startRow-1);
			$this->excel->getActiveSheet()->getStyle($rangeTop)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			$rangLeft = $cr->rgeCol[$cr->getIndexCol($startCol)+1].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow();
			$this->excel->getActiveSheet()->getStyle($rangLeft)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			$rangRight = $cr->rgeCol[$cr->getIndexCol($startCol)+7].($startRow-1).':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
			$this->excel->getActiveSheet()->getStyle($rangRight)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			$rangBottom = $cr->rgeCol[$cr->getIndexCol($startCol)+1].$cr->getPrevRow().':'.$cr->rgeCol[$cr->getIndexCol($startCol)+7].$cr->getPrevRow();
			$this->excel->getActiveSheet()->getStyle($rangBottom)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );
		}

		$this->excel->getActiveSheet()->getStyle('A5:'.$this->excel->getActiveSheet()->getHighestColumn().$this->excel->getActiveSheet()->getHighestRow())->applyFromArray(array(
				'alignment' => array(
	                'shrinkToFit' => true
	            )
			));

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(12);

		// draw  sheet bottom  line
		$this->excel->getActiveSheet()->getStyle('B45:I45')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B45', '審判長');

		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet7($activeSheetIndex = 0)
	{
		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($this->players);

		$finalSettings = $this->getRepository('FinalSetting')->findBy(['tournament' => $this->game->getTournament()]);

		if(!$finalSettings) throw new Exception("Final Setting did not exist.");

		// start draw

		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('決勝演技日程');

        $tournament = $this->game->getTournament();

        $games = $tournament->getGames();

        $items = $this->game->getItems();

        $rotationSettings = $this->game->getRotationSettings();
		$rotations = $this->game->getRotations();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '決勝演技日程　'.$this->game->getStrSex());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B5', '班');
		$this->excel->getActiveSheet()->SetCellValue('C5', '組');
		$this->excel->getActiveSheet()->SetCellValue('D5', '競技');
		$this->excel->getActiveSheet()->SetCellValue('E5', "予選\n順位");
		$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue('F5', "学校");
		$this->excel->getActiveSheet()->SetCellValue('G5', "都道\n府県");
		$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue('H5', 'BIB');
		$this->excel->getActiveSheet()->SetCellValue('I5', '選手名');
		$this->excel->getActiveSheet()->SetCellValue('J5', "学\n年");
		$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue('K5', "時間");

		$cr = new ColRow;

		$cr->curCol = 'K';

		foreach ($rotationSettings as $item) {

			$itemName  = $item->getName();

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', $itemName);

			$this->excel->getActiveSheet()->getStyle($cr->curCol().'5')->applyFromArray(
				array(
					'font'  => array('name'  => 'Verdana'),
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
						'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					)
				)
			);

			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(7.5);
		}

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getRowDimension('5')->setRowHeight(35);

		$settings = [];

		foreach ($finalSettings as $kgroup => $setting) {
			
			$groupSetting = unserialize($setting->getSetting());
			$settings[$setting->getTeam()] = $groupSetting;
		}
		
		$cr = new ColRow;
		$cr->curRow = 6;

		foreach ($groupHeatPlayers as $group) {

			$beginGroup = $cr->curRow();

			$heats = $group->getHeats();

			$this->excel->getActiveSheet()->SetCellValue('B'.$cr->curRow(), $group->getId());

			foreach ($heats as $heat)
	        {

	        	$heatFirstItem = $rotations->filter(function($r) use ($heat){
					return $r->getHeat() == $heat->getId();
				})->first();

				$arrayRotateSettings = $rotationSettings->toArray();

				$n = 2;

				$dumpRotationSettings = $arrayRotateSettings;

				while ($dumpRotationSettings[0]->getName() != $heatFirstItem->getFirstItem() && $n <= count($arrayRotateSettings)) {
					$dumpRotationSettings = rotateItem($arrayRotateSettings, $n);
					$n++;
				}

				$itemOrder = range(1, count($dumpRotationSettings));

				$itemOrder= rotateItem($itemOrder, $n-1);
		
	    		$heatCol = 'C';
	    		$heatRow = $cr->curRow();

	    		$heatPlayers = $heat->getPlayers();

	    		$singlePlayers = $heat->getPlayersPlayAsSingle();

	    		$numberRow = 0;

	    		if($heat->hasGroupSchoolPlay())
	    		{
	    			// find school rank
	    			$groupPlayers = $heat->getPlayersPlayAsGroup();

	    			$indexOfFirst = $groupPlayers->indexOf($groupPlayers->first());

	    			$pSchool = $groupPlayers->first()->getSchool();

	    			$player = $settings[$group->getId()][$heat->getId()][$indexOfFirst+1];

	    			$sex = $this->game->getSex();
	    			$class = explode('_', $player['class']);

	    			$roundGame = $games->filter(function($g) use ($class, $sex) {
	    				return $g->getSex() == $sex && $g->getClass() == $class[0];
	    			})->first();

	    			if(!$roundGame) throw new Exception("Round Game was lost");
	    			
	    			$school = $roundGame->getSchoolGroups()->filter(function($s) use ($pSchool){
	    				return $s->getSchoolNameAb() == $pSchool->getSchoolNameAb();
	    			})->first();

	    			$groupScore = $school->getGroupScore();

	    			$schoolRank = ($groupScore) ? $groupScore->getPassRank() . '位' : null;

	    			$cr->curCol = $heatCol;

	    			// player info
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), 'チーム');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $schoolRank);
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolNameAb());
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolPrefecture());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));

	    			$cr->incCol();

	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    			

	    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        ),
				                    ),
				                )
				            );

	    			$cr->incRow();

	    			$numberRow++;
	    		}

	    		foreach ($singlePlayers as $n => $player)
	    		{
	    			$cr->curCol = 'C';

	    			$playerRank = $settings[$group->getId()][$heat->getId()][$n+1];

	    			$playerRankItem = explode('_', $playerRank['class'])[1];

	    			// player info
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), ($playerRank['type'] == '種目別') ? $playerRankItem : $playerRank['type']);
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $playerRank['rank']);
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchool()->getSchoolNameAb());
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchool()->getSchoolPrefecture());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '222');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));

	    			$cr->incCol();

	    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        ),
				                    ),
				                )
				            );

	    			$cr->incRow();

	    			$numberRow++;
	    		}

	    		// item score
    			foreach ($itemOrder as $order) {

    				$cr->incCol();

    				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$heatRow.":".$cr->curCol().($heatRow+$numberRow-1));

    				$this->excel->getActiveSheet()->SetCellValue($cr->curCol().$heatRow, $order);

    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$heatRow)->applyFromArray(
    					array(
							'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
						)
    				);
    			}

	    		// draw a border line when end of each heat
	    		$this->excel->getActiveSheet()->getStyle($heatCol.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'bottom' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );


	    		$heatRange = $heatCol.$heatRow.':'.$heatCol.$cr->getPrevRow();
	    		
	    		// merge range cell for heat
	    		$this->excel->getActiveSheet()->mergeCells($heatRange);
	    		$this->excel->getActiveSheet()->SetCellValue($heatCol.$heatRow, $heat->getId());

	    		$this->excel->getActiveSheet()->getStyle($heatCol.$heatRow)->applyFromArray(array('font'  => array('name'  => 'Verdana')));

	    		// set text align center for heat value cell
	    		$this->excel->getActiveSheet()->getStyle($heatRange)->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
			            );
			}

			$this->excel->getActiveSheet()->mergeCells('B'.$beginGroup.':B'.$cr->getPrevRow());
			$this->excel->getActiveSheet()->mergeCells('K'.$beginGroup.':K'.$cr->getPrevRow());
		}

		$this->excel->getActiveSheet()->getStyle('B6:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Verdana'
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B6')->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('E6:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('K6:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
		                )
	            );

		// table border top
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
			                array(
			                    'borders' => array(
				                        'top' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				                            'color' => array('argb' => '000000'),
				                        ),
				                        'bottom' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		// table border right
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'5:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
				                        'right' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		// table border bottom
		$this->excel->getActiveSheet()->getStyle('B'.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
				                        'bottom' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		// table border left
		$this->excel->getActiveSheet()->getStyle('B5'.':B'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
				                        'left' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(1.5);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4.1);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(8.2);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(6.3);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8.6);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(6.5);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15.5);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(3.7);

		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	public function drawsheet8($activeSheetIndex = 0)
	{
		$groupsHeats = $this->getRepository('Player')->getGroupHeats($this->game);
		$maxHeat = 0;
		
		$rotateFirsts = [];

		foreach ($groupsHeats as $groupId => $group) {
			if(count($group) > $maxHeat) $maxHeat = count($group);

			$rotationSettings 	= $this->game->getRotationSettings();
			$groupRotations		= $this->game->findFirstRotationGroup($groupId);

			$rotateFirsts[$groupId] = new RotationCollection($groupRotations, $rotationSettings);
		}

		$this->players = $this->getRepository('Player')->getPlayersResults($this->game);

		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('個人総合'.$this->game->getStrSex());

		$resultSingleRanking = RankingCollection::implementRanking($this->players, 'getTotalFinalScore');

        $items = $this->game->getItems();

        $cr = new ColRow();

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B2', 'No');
		$this->excel->getActiveSheet()->SetCellValue('C2', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('D2', '学年');
		$this->excel->getActiveSheet()->SetCellValue('E2', '学校');
		$this->excel->getActiveSheet()->SetCellValue('F2', '都道府県');
		$this->excel->getActiveSheet()->SetCellValue('G2', '団or個');
		$this->excel->getActiveSheet()->SetCellValue('H2', '班');
		$this->excel->getActiveSheet()->SetCellValue('I2', '組');
		
		// head item score
		$itemNameHeatCol = 'J';
		$cr->curCol = 'I';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		for ($event = 1; $event <= $maxHeat; $event++)
		{
			$item = $items->first();

			$numberDScore = $item->getNumberRefereeD();
			$numberEScore = $item->getNumberRefereeE();

			for($i = 1; $i <= $numberDScore; $i++){
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', 'D'.$i);
			}

			for($i = 1; $i <= $numberEScore; $i++){
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', 'E'.$i);
			}

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', 'ライン');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', 'タイム');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', '減点');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'2', '合計');

			$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'1:'.$cr->curCol().'1');
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'1', '種目'.$event);
			// get col for next item name
			$colidx = $cr->getIndexCol($itemNameHeatCol);
			$itemNameHeatCol = $cr->rgeCol[$colidx+$numberDScore+$numberEScore+4];
		}

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'2', "個人総合");
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'2', '順位');
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'2', '通過順位');

		$this->excel->getActiveSheet()->getStyle('J1:'.$cr->curCol().'1')->applyFromArray(
		                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => '000000')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle('B2:'.$cr->curCol().'2')->applyFromArray(
		                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => '000000')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
		                )
		            );

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

        // draw table data
        $startRow = $cr->curRow = 3;
    		
		$heatCol = 'A';
		$heatRow = $cr->curRow();

		foreach ($this->players as $player)
		{
			$cr->curCol = $heatCol;
			// player info
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), convert_game_type($player->getFlag()));
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGroup());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getHeat());
			
			// item score
			$playerFirstItemHeat = $rotateFirsts[$player->getGroup()];

			for ($event = 1; $event <= $maxHeat; $event++)
			{

				if($event > count($groupsHeats[$player->getGroup()])) {
					
					$item = $items->first();
					
					$numberDScore = $item->getNumberRefereeD();
					$numberEScore = $item->getNumberRefereeE();

					for($i = 1; $i <= $numberDScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					}

					for($i = 1; $i <= $numberEScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					}

					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');

					continue;
				}

				$playerCurrentItemHeat = $playerFirstItemHeat->doRotate($event-1);

				$currentItem = $playerCurrentItemHeat->filterHeat($player->getHeat())->first();

				$currentItem = ($currentItem) ? $currentItem->getFirstItem() : null;

				$item = $this->game->findItem($currentItem);

				if($item)
				{
					$itemName  = $item->getName();
					$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

					$numberDScore = $item->getNumberRefereeD();
					$numberEScore = $item->getNumberRefereeE();

					for($i = 1; $i <= $numberDScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'D'.$i.'Score'));
					}

					for($i = 1; $i <= $numberEScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'E'.$i.'Score'));
					}

					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'TimeDemeritScore'));
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'LineDemeritScore'));
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'DemeritScore'));
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getItemBestScoreValue($item, 'FinalScore'));
				}
				else 
				{
					$item = $items->first();
					
					$numberDScore = $item->getNumberRefereeD();
					$numberEScore = $item->getNumberRefereeE();

					for($i = 1; $i <= $numberDScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					}

					for($i = 1; $i <= $numberEScore; $i++){
						$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					}

					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
				}
			}

			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getTotalFinalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $resultSingleRanking->findRank($player));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), ''); # place holder for pass rank


			$this->excel->getActiveSheet()->getStyle('H'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'font'  => array('name'  => 'Verdana'),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
								'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							)
		                )
		            );

			$cr->incRow();
		}

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E2:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(4);

		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet9(&$activeSheetIndex = 0, $event = 1, $group = 1, $tkey = 1)
	{
		$rotationSettings 	= $this->game->getRotationSettings();
		$groupRotations		= $this->game->findFirstRotationGroup($group);

		$rotateFirsts = new RotationCollection($groupRotations, $rotationSettings);

		$rotationCurrents = $rotateFirsts->doRotate($event-1);

		// filter players play in group
		$players = array_filter($this->players, function($p) use ($group) {
			return $p->getGroup() == $group;
		});

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        /////////////////////////
        // start excel draw    //
        /////////////////////////

        $heats = $groupHeatPlayers->first()->getHeats();
        
        foreach ($heats as $heat) {

        	$currentItem = $rotationCurrents->filterHeat($heat->getId())->first();

        	$item = $this->game->findItem($currentItem->getFirstItem());

        	if(!$item) continue;

        	$numberDScore = $item->getNumberRefereeD();
			$numberEScore = $item->getNumberRefereeE();

        	$this->excel->createSheet();
			$this->excel->setActiveSheetIndex($activeSheetIndex);
			$activeSheetIndex++;
			$this->excel->getActiveSheet()->setTitle('得点表1試技');

	        $cr = new ColRow();

	        // draw table head
	        // sheet description
	        $this->excel->getActiveSheet()->SetCellValue('A1', $tournament->getName());
	        $this->excel->getActiveSheet()->SetCellValue('A3', '体操競技得点表（Gymmastics Scorebook）');

	        if($tkey == 2)
	        	$this->excel->getActiveSheet()->SetCellValue('H3', '【学校別】');
	    	else
	    		$this->excel->getActiveSheet()->SetCellValue('H3', '【組別】');

	        // head player info
	        $this->excel->getActiveSheet()->SetCellValue('A4', 'チーム名');

	        $this->excel->getActiveSheet()->getStyle('A3:Q3')->applyFromArray(
				                array(
				                    'alignment' => array(
				                        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
			                )
		            );

	        $this->excel->getActiveSheet()->getStyle('A4')->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => 'F2F2F2')
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 11,
				                        'name'  => 'ＭＳ ゴシック'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
			                )
		            );

	        $school = $heat->findSchoolGroup();

	        $schoolName = ($school) ? $school->getSchoolNameAb() . '高校' : '';
	        $schoolPref = ($school) ? $school->getSchoolPrefecture() : '';

	        $this->excel->getActiveSheet()->mergeCells('A4:B5');
	        $this->excel->getActiveSheet()->SetCellValue('C4', $schoolName);
	        $this->excel->getActiveSheet()->mergeCells('C4:F5');
	        $this->excel->getActiveSheet()->mergeCells('G4:G5');
	        $this->excel->getActiveSheet()->SetCellValue('G4', $schoolPref);

	        $this->excel->getActiveSheet()->mergeCells('H4:H5');
	        $this->excel->getActiveSheet()->SetCellValue('H4', '種目');

	        $this->excel->getActiveSheet()->mergeCells('I4:I5');
	        $this->excel->getActiveSheet()->SetCellValue('I4', $this->game->getStrSex());
	        $this->excel->getActiveSheet()->mergeCells('J4:L5');
	        $this->excel->getActiveSheet()->SetCellValue('J4', $item->getName());
	        
	        $this->excel->getActiveSheet()->mergeCells('M4:M5');
	        $this->excel->getActiveSheet()->SetCellValue('M4', '班');

	        $this->excel->getActiveSheet()->getStyle('M4')->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => 'F2F2F2')
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
			                )
		            );

	        $markCol = 'M';
	        $this->excel->getActiveSheet()->mergeCells('N4:N5');
	        $this->excel->getActiveSheet()->SetCellValue('N4', $heat->getGroup());
	        $this->excel->getActiveSheet()->getStyle('N4')->applyFromArray(
				                array('font'  => array('name'  => 'Verdana', 'size' => 14)));
	        $this->excel->getActiveSheet()->mergeCells('O4:O5');
	        $this->excel->getActiveSheet()->SetCellValue('O4', '組');
	        $this->excel->getActiveSheet()->getStyle('O4:O5')->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => 'F2F2F2')
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
				                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
			                )
		            );
	        
	        $this->excel->getActiveSheet()->mergeCells('P4:Q5');
	        $this->excel->getActiveSheet()->SetCellValue('P4', $heat->getId());
	        $this->excel->getActiveSheet()->getStyle('P4')->applyFromArray(
				                array('font'  => array('name'  => 'Verdana', 'size' => 14)));

	        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 16,
				                        'name'  => 'Calibri'
				                    ),
				                )
				            );

	        $this->excel->getActiveSheet()->getStyle('A3:P3')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 16,
				                        'name'  => 'Calibri'
				                    ),
				                )
				            );

	        $this->excel->getActiveSheet()->getStyle('A4:' .$markCol.'5')->applyFromArray(
				                array(
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                    'font'  => array(
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 14,
				                        'name'  => 'Calibri'
				                    ),
				                )
				            );

	        $this->excel->getActiveSheet()->getStyle('M4:M5')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'size'  => 12
				                    ),
				                )
				            );

	        $this->excel->getActiveSheet()->getStyle('G4')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'size'  => 12,
				                    ),
			                )
		            );

	        $this->excel->getActiveSheet()->getStyle('H4')->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => 'F2F2F2')
				                    ),
				                    'font'  => array(
				                        'size'  => 12,
				                    ),
			                )
		            );

	        $this->excel->getActiveSheet()->getStyle('A4:Q4')->applyFromArray(
				                array(
				                    'borders' => array(
					                        'top' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );

	        $this->excel->getActiveSheet()->getStyle('C4:G5')->applyFromArray(
				                array(
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_NONE,
					                        ),
					                        'top' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					                            'color' => array('argb' => '000000'),
					                        )
					                    )
				                )
				            );

	        $this->excel->getActiveSheet()->mergeCells('O3:Q3');

	        $formatter = new IntlDateFormatter(
			    'ja_JP@calendar=japanese',
			    IntlDateFormatter::FULL,
			    IntlDateFormatter::FULL,
			    'Asia/Tokyo',
			    IntlDateFormatter::TRADITIONAL,
			    'Gy' //Age and year (regarding the age)
			);
			$r = $formatter->format(strtotime(date('Y-m-d')));

	        $this->excel->getActiveSheet()->SetCellValue('O3', $r.date('年m月d日'));
	        $this->excel->getActiveSheet()->getStyle('O3')->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
			                )
		            );

	        // draw table data
	        $startRow = $cr->curRow = 6;
	        $startCol = $cr->curCol = 'A';

	        $this->excel->getActiveSheet()->getRowDimension(3)->setRowHeight(25);
	        $this->excel->getActiveSheet()->getRowDimension(4)->setRowHeight(18);
    		$this->excel->getActiveSheet()->getRowDimension(5)->setRowHeight(18);

    		if($heat->hasGroupSchoolPlay())
    		{
    			$this->excel->getActiveSheet()->getRowDimension(6)->setRowHeight(16);
    			$this->excel->getActiveSheet()->getRowDimension(7)->setRowHeight(16);

    			$this->excel->getActiveSheet()->SetCellValue('B6', "選手番号\nNumber");
    			$this->excel->getActiveSheet()->getStyle('B6')->getAlignment()->setWrapText(true);
    			$this->excel->getActiveSheet()->getStyle('B6')->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
				$this->excel->getActiveSheet()->SetCellValue('C6', "選手名\nPlayer");
				$this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->SetCellValue('D6', "演技順\nOrder");
				$this->excel->getActiveSheet()->getStyle('D6')->getAlignment()->setWrapText(true);

				$this->excel->getActiveSheet()->mergeCells('B6:B7');
				$this->excel->getActiveSheet()->mergeCells('C6:C7');
				$this->excel->getActiveSheet()->mergeCells('D6:D7');

				$cr->curCol = 'D';
				
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', 'D' . implode('･D', range(1, $numberEScore)));
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');

				for ($i = 1; $i <= 4; $i++)
				{
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', 'E'.$i);
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				}

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', "E\nスコア");
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6'.':'.$cr->curCol().'7')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6')->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
		            );

				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', 'E減点');
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', "Eスコア\n決定点");
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', 'D＋E');
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');

				$this->excel->getActiveSheet()->getStyle('E'.$startRow.':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

				$objRichText = new PHPExcel_RichText();
				$objfont10 = $objRichText->createTextRun("減点\n");
				$objfont10->getFont()->setSize(10);
				$objfont6 = $objRichText->createTextRun('タイム・ライン');
				$objfont6->getFont()->setSize(6);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', $objRichText);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				
				$markCol = $cr->curCol();

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', "得点\nScore");
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->incCol().'7');
				$this->excel->getActiveSheet()->getStyle($cr->getPrevCol().'6:'.$cr->curCol().'7')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle($cr->getPrevCol().'6')->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
			            );
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6')->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'6', "チーム\n加算");

				$this->excel->getActiveSheet()->mergeCells($cr->curCol().'6:'.$cr->curCol().'7');
				$this->excel->getActiveSheet()->getStyle($cr->curCol().'6')->getAlignment()->setWrapText(true);

				$this->excel->getActiveSheet()->getStyle($markCol.'6:'.$cr->curCol().'7')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ Ｐゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

	            $this->excel->getActiveSheet()->getStyle('B6')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 10,
			                        'name'  => 'ＭＳ Ｐゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );


	            $this->excel->getActiveSheet()->getStyle('C6')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

	            $this->excel->getActiveSheet()->getStyle('D6')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 10,
			                        'name'  => 'ＭＳ Ｐゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$startRow.':'.$cr->curCol().$cr->getNextRow())
														->applyFromArray(array('font'  => array('size'  => 8)));

				$this->excel->getActiveSheet()->getStyle('B6')->applyFromArray(array('font'  => array('size'  => 10)));
				$this->excel->getActiveSheet()->getStyle('D6')->applyFromArray(array('font'  => array('size'  => 10)));

				$this->excel->getActiveSheet()->getStyle(($cr->getPrevCol($cr->getPrevCol($cr->getPrevCol()))).'6')->applyFromArray(
				                array('font'  => array('size' => 10)));

    			$groupPlayers = $heat->getPlayersPlayAsGroup();

    			$cr->curRow = $startTableData = 8;

	    		foreach ($groupPlayers as $player)
	    		{
	    			$cr->curCol = 'A';

	    			// player info
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana', 'size' => 12)));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ Ｐゴシック', 'size' => 12)));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), ($player->getOrder($item)) ? $player->getOrder($item) : '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana', 'size' => 12)));
	    			
	    			// item score
					$itemName  = $item->getName();
					$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'DScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );


					for($i = 1; $i <= 4; $i++)
					{
						$itemScore = '';
				
						if($this->input->post('choose_rotation_event_detail')[$tkey])
						{
							$itemScore = $player->getItemScoreValue($item, 'E'.$i.'Score');
						}

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->curRow(),
							($playerOff) ? '-' : $itemScore
						);	

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
					}

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore') + $player->getItemScoreValue($item, 'EDemeritScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
					
					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EDemeritScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore') + $player->getItemScoreValue($item, 'DScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'DemeritScore')
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'FinalScore')
					);

					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->incCol().$cr->curRow());

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
	    			
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), ($heat->isOneOfThreeBest($player, $item)) ? '○' : '');

	    			if($item->isPlayTwice())
	    				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());

	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	    											array(
	    												'font'  => array('name'  => 'ＭＳ Ｐゴシック'),
	    												'alignment' => array(
									                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
									                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
									                    ),
	    												)
	    											);

	    			if($item->isPlayTwice())
					{
	    				$cr->curCol = 'A';

						$this->excel->getActiveSheet()->mergeCells($cr->incCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
		    			$this->excel->getActiveSheet()->mergeCells($cr->incCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
		    			$this->excel->getActiveSheet()->mergeCells($cr->incCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
		    			
		    			// item score
						$itemName  = $item->getName();
						$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'DScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

						for($i = 1; $i <= 4; $i++)
						{
							$itemScore = '';
					
							if($this->input->post('choose_rotation_event_detail')[$tkey])
							{
								$itemScore = $player->getItemScoreValue($item, 'E'.$i.'Score', 2);
							}

							$this->excel->getActiveSheet()->SetCellValue(
								$cr->incCol().$cr->getNextRow(),
								($playerOff) ? '-' : $itemScore
							);

							$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );	
						}

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2) + $player->getItemScoreValue($item, 'EDemeritScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
						
						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'EDemeritScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2) + $player->getItemScoreValue($item, 'DScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'DemeritScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $player->getItemScoreValue($item, 'FinalScore', 2)
						);

						$this->excel->getActiveSheet()->getStyle($cr->getNextCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
		    			
		    			$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->getNextRow().':'.$cr->incCol().$cr->getNextRow());

		    			$this->excel->getActiveSheet()->getStyle('E'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'bottom' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_DOTTED,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

		    			$this->excel->getActiveSheet()->getRowDimension($cr->getNextRow())->setRowHeight(22);
		    			$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(22);

		    			$cr->incRow();
		    			$cr->incCol();
					}
					else
					{
						$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(40);
					}

					$this->excel->getActiveSheet()->getStyle('A'.$cr->curRow().':' .$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
					                        'bottom' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

					$cr->incRow();
	    		}

    			$this->excel->getActiveSheet()->mergeCells('A6'. ':A'.$cr->curRow());

    			$this->excel->getActiveSheet()->getStyle('A6'. ':A'.$cr->curRow())->applyFromArray(
				                array(
				                    'fill' => array(
			                        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        	'color' => array('rgb' => 'F2F2F2')
				                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->mergeCells('B'. $cr->curRow(). ':' .'D'.$cr->curRow());
    			$this->excel->getActiveSheet()->mergeCells('H'. $cr->curRow(). ':' .'I'.$cr->curRow());
    			$this->excel->getActiveSheet()->mergeCells('L'. $cr->curRow(). ':N'.$cr->curRow());
    			$this->excel->getActiveSheet()->mergeCells('E'. $cr->curRow(). ':G'.$cr->curRow());
    			$this->excel->getActiveSheet()->mergeCells('J'. $cr->curRow(). ':K'.$cr->curRow());
    			$this->excel->getActiveSheet()->mergeCells('O'. $cr->curRow(). ':Q'.$cr->curRow());
    			$this->excel->getActiveSheet()->SetCellValue('H'. $cr->curRow(), 'チーム減点');
    			$this->excel->getActiveSheet()->SetCellValue('L'. $cr->curRow(), 'チーム決定点');

    			$this->excel->getActiveSheet()->getStyle('L'. $cr->curRow(). ':N' .$cr->curRow())->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => true,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('H'. $cr->curRow(). ':I' .$cr->curRow())->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => true,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('B'. $cr->curRow(). ':' .'D'.$cr->curRow())->applyFromArray(
				                array(
				                    'fill' => array(
			                        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        	'color' => array('rgb' => 'D8D8D8')
				                    ),
				                    'font'  => array(
				                        'bold'  => true,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Calibri'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('D'. $startTableData. ':' .$cr->getPrevCol().$cr->getPrevRow())->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Verdana'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(40);

    			$this->excel->getActiveSheet()->getStyle('B'. $cr->curRow(). ':' .$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'fill' => array(
			                        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        	'color' => array('rgb' => 'D8D8D8')
				                    ),
				                )
				            );
				
				$this->excel->getActiveSheet()->SetCellValue('B'. $cr->curRow(), 'チームベスト３');    			

    			$this->excel->getActiveSheet()->SetCellValue('A'.$startRow, 'チーム');
    			$this->excel->getActiveSheet()->getStyle('A'.$startRow)->applyFromArray(
				                array(
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'font'  => array(
				                        'size'  => 14,
				                    ),
			                )
		            );

    			$this->excel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->setWrapText(true);

    			$this->excel->getActiveSheet()->getStyle('A'.$startTableData.':' .'D'.$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('A'. $cr->curRow(). ':' .$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
					                        'bottom' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('A6:' .$cr->curCol().'6')->applyFromArray(
				                array(
				                    'borders' => array(
					                        'top' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->getStyle('B'. $startRow. ':D'.$cr->getPrevRow())->applyFromArray(
				                array(
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );

	    		$this->excel->getActiveSheet()->getStyle('B'.($startRow+1). ':B'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );

	    		$this->excel->getActiveSheet()->getStyle('D'.($startRow+1). ':D'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );

    			$cr->incRow();
    		}

			$singlePlayers = $heat->getPlayersPlayAsSingle();

			$startRow = $cr->curRow();

			if($singlePlayers->count())
			{
				$cr->curCol = 'A';

				$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(16);
				$this->excel->getActiveSheet()->getRowDimension($cr->getNextRow())->setRowHeight(16);

				$this->excel->getActiveSheet()->SetCellValue('B'.$cr->curRow(), "選手番号\nNumber");
				$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow())->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
				$this->excel->getActiveSheet()->SetCellValue('C'.$cr->curRow(), "選手名・学校名\nPlayer");
				$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow())->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Calibri')));
				$this->excel->getActiveSheet()->SetCellValue('D'.$cr->curRow(), "演技順\nOrder");
				$this->excel->getActiveSheet()->getStyle('D'.$cr->curRow())->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('D'.$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));

				$this->excel->getActiveSheet()->mergeCells('B'.$cr->curRow().':B'.$cr->getNextRow());
				$this->excel->getActiveSheet()->mergeCells('C'.$cr->curRow().':C'.$cr->getNextRow());
				$this->excel->getActiveSheet()->mergeCells('D'.$cr->curRow().':D'.$cr->getNextRow());

				$cr->curCol = 'D';
				
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), 'D' . implode('･D', range(1, $numberEScore)));
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());


				for ($i = 1; $i <= 4; $i++)
				{
					$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), 'E'.$i);
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}
				

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), "E\nスコア");
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
		            );
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), 'E減点');
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), "Eスコア\n決定点");
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->getAlignment()->setWrapText(true);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), 'D＋E');
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());

				$this->excel->getActiveSheet()->getStyle('E'.$startRow.':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

				$objRichText = new PHPExcel_RichText();
				$objfont10 = $objRichText->createTextRun("減点\n");
				$objfont10->getFont()->setSize(10);
				$objfont6 = $objRichText->createTextRun('タイム・ライン');
				$objfont6->getFont()->setSize(6);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $objRichText);

				$markCol = $cr->curCol();

				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->getAlignment()->setWrapText(true);
				
				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), "得点\nScore");
				$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->getNextCol($cr->getNextCol()).$cr->getNextRow());
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->getAlignment()->setWrapText(true);

				$this->excel->getActiveSheet()->getStyle($markCol.$startRow.':'.$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
					                'fill' => array(
			                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        'color' => array('rgb' => 'F2F2F2')
				                    ),
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'ＭＳ Ｐゴシック'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
			                )
		            );

				$this->excel->getActiveSheet()->getStyle('B'.$startRow)->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 10,
			                        'name'  => 'ＭＳ Ｐゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );


	            $this->excel->getActiveSheet()->getStyle('C'.$startRow)->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

	            $this->excel->getActiveSheet()->getStyle('D'.$startRow)->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => 'F2F2F2')
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 10,
			                        'name'  => 'ＭＳ Ｐゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

				$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow())->applyFromArray(array('font'  => array('size'  => 10)));
				$this->excel->getActiveSheet()->getStyle('D'.$cr->curRow())->applyFromArray(array('font'  => array('size'  => 10)));

				$this->excel->getActiveSheet()->getStyle(($cr->getPrevCol()).$cr->curRow())->applyFromArray(
				                array('font'  => array('size' => 10)));

				$cr->incRow();
				$cr->incRow();
			}

			$startTableData = $cr->curRow();
			
    		foreach ($singlePlayers as $player)
    		{
    			$cr->curCol = 'A';

    			// player info
    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana', 'size' => 12)));
    			$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());

    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ Ｐゴシック', 'size' => 12)));

    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

    			$this->excel->getActiveSheet()->SetCellValue($cr->curCol().$cr->getNextRow(), $player->getSchool()->getSchoolNameAb());
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ Ｐゴシック', 'size' => 12)));


    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), ($player->getOrder($item)) ? $player->getOrder($item) : '');
    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(array('font'  => array('name'  => 'Verdana', 'size' => 12)));
    			$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
    			
    			// item score
				$itemName  = $item->getName();
				$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'DScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

				
				for($i = 1; $i <= 4; $i++)
				{
					$itemScore = '';

					if($this->input->post('choose_rotation_event_detail')[$tkey])
					{
						$itemScore = $player->getItemScoreValue($item, 'E'.$i.'Score');
					}

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->curRow(),
						($playerOff) ? '-' : $itemScore
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					if(!$item->isPlayTwice())	
					{
						$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
					}
				}
			

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore') + $player->getItemScoreValue($item, 'EDemeritScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
				
				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'EDemeritScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore') + $player->getItemScoreValue($item, 'DScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'DemeritScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

				$this->excel->getActiveSheet()->SetCellValue(
					$cr->incCol().$cr->curRow(),
					($playerOff) ? '-' : $player->getItemScoreValue($item, 'FinalScore')
				);

				if(!$item->isPlayTwice())	
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->getNextCol($cr->getNextCol()).$cr->getNextRow());
				}
				else
				{
					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->curRow().':'.$cr->getNextCol($cr->getNextCol()).$cr->curRow());
				}

				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
	    			
    			if($item->isPlayTwice())
				{
    				$cr->curCol = 'D';
	    			
	    			// item score
					$itemName  = $item->getName();
					$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'DScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					
					for($i = 1; $i <= 4; $i++)
					{
						$itemScore = '';

						if($this->input->post('choose_rotation_event_detail')[$tkey])
						{
							$itemScore = $player->getItemScoreValue($item, 'E'.$i.'Score', 2);
						}

						$this->excel->getActiveSheet()->SetCellValue(
							$cr->incCol().$cr->getNextRow(),
							($playerOff) ? '-' : $itemScore
						);

						$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );	
					}
					

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2) + $player->getItemScoreValue($item, 'EDemeritScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );
					
					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EDemeritScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'EScore', 2) + $player->getItemScoreValue($item, 'DScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'DemeritScore', 2)
					);

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->getNextRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

					$this->excel->getActiveSheet()->SetCellValue(
						$cr->incCol().$cr->getNextRow(),
						($playerOff) ? '-' : $player->getItemScoreValue($item, 'FinalScore', 2)
					);

					$this->excel->getActiveSheet()->mergeCells($cr->curCol().$cr->getNextRow().':'.$cr->getNextCol($cr->getNextCol()).$cr->getNextRow());

					$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					                array(
					                    'borders' => array(
					                        'right' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        ),
					                    ),
					                )
					            );

	    			$this->excel->getActiveSheet()->getStyle('E'.$cr->getNextRow().':'.$cr->getNextCol($cr->getNextCol()).$cr->getNextRow())->applyFromArray(
				                array(
				                    'borders' => array(
				                        'top' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_DOTTED,
				                            'color' => array('argb' => '000000'),
				                        ),
				                    ),
				                )
				            );
				}

				$cr->incCol();
				$cr->incCol();
				
				$this->excel->getActiveSheet()->getRowDimension($cr->getNextRow())->setRowHeight(22);
				$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(22);

				$this->excel->getActiveSheet()->getStyle('E'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
				                        'top' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        ),
				                    ),
				                )
				            );
    		}

    		$cr->incRow();
    		$cr->incRow();

    		if($singlePlayers->count())
    		{
    			$this->excel->getActiveSheet()->getStyle('B'.$startTableData. ':B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

	    		$this->excel->getActiveSheet()->getStyle('D'.$startTableData. ':D'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'allborders' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_THIN,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );

    			$this->excel->getActiveSheet()->mergeCells('A'.$startRow. ':A'.$cr->getPrevRow());

    			$this->excel->getActiveSheet()->getStyle('A'.$startRow. ':A'.$cr->getPrevRow())->applyFromArray(
				                array(
				                    'fill' => array(
			                        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
			                        	'color' => array('rgb' => 'F2F2F2')
				                    ),
				                )
				            );

    			$this->excel->getActiveSheet()->SetCellValue('A'.$startRow, '個人');
				$this->excel->getActiveSheet()->getStyle('A'.$startRow)->applyFromArray(
				                array(
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                    'font'  => array(
				                        'size'  => 14,
				                    ),
			                )
		            );

				$this->excel->getActiveSheet()->getStyle('A'.$startRow)->getAlignment()->setWrapText(true);

				$this->excel->getActiveSheet()->getStyle('A'.$startRow.':' .$cr->curCol().($startRow+1))->applyFromArray(
				                array(
				                    'borders' => array(
					                        'allborders' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_THIN,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

				$this->excel->getActiveSheet()->getStyle('A'. $cr->getPrevRow(). ':' .$cr->curCol().$cr->getPrevRow())->applyFromArray(
				                array(
				                    'borders' => array(
					                        'bottom' => array(
					                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					                            'color' => array('argb' => '000000'),
					                        )
					                    ),
				                )
				            );

				$this->excel->getActiveSheet()->getStyle('D'. $startTableData. ':' .$cr->getPrevCol().$cr->curRow())->applyFromArray(
				                array(
				                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 12,
				                        'name'  => 'Verdana'
				                    ),
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );
    		}

    		$this->excel->getActiveSheet()->getStyle('B'. $startTableData. ':D'.$cr->getPrevRow())->applyFromArray(
				                array(
				                    'alignment' => array(
				                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				                    ),
				                )
				            );


    		// draw border right table
    		$this->excel->getActiveSheet()->getStyle('A4:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );
    		// border left
    		$this->excel->getActiveSheet()->getStyle('A4:A'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// set with for column heat
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(3.75);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(9);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(18.7);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(6.5);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(9.6);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(9.6);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(9.7);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(9.6);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(9.6);

			$this->excel->getActiveSheet()->getColumnDimension($cr->getPrevCol())->setWidth(8);
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(6);

			$cr->incRow();

			$curColIndex = $cr->getIndexCol($cr->curCol());
			$firstTableColIndex = $curColIndex - 8;
			$firstTableCol = $cr->rgeCol[$firstTableColIndex];
		

			$this->excel->getActiveSheet()->mergeCells($firstTableCol.$cr->curRow().':'.$firstTableCol.$cr->getNextRow());
			$this->excel->getActiveSheet()->SetCellValue($firstTableCol.$cr->curRow(), "署\n名");
			$this->excel->getActiveSheet()->getStyle($firstTableCol.$cr->curRow().':'.$firstTableCol.$cr->getNextRow())->getAlignment()->setWrapText(true);

			$nextColIndex = $firstTableColIndex+4;
			$nextCol = $cr->rgeCol[$nextColIndex];
			$this->excel->getActiveSheet()->mergeCells($cr->getNextCol($firstTableCol).$cr->curRow().':'.$nextCol.$cr->curRow());
			$this->excel->getActiveSheet()->SetCellValue($cr->getNextCol($firstTableCol).$cr->curRow(), '主任審判'); 
			$this->excel->getActiveSheet()->mergeCells($cr->getNextCol($firstTableCol).$cr->getNextRow().':'.$nextCol.$cr->getNextRow());
			$this->excel->getActiveSheet()->mergeCells($cr->getNextCol($nextCol).$cr->curRow().':'.$cr->curCol().$cr->curRow());
			$this->excel->getActiveSheet()->SetCellValue($cr->getNextCol($nextCol).$cr->curRow(), '会場記録');
			$this->excel->getActiveSheet()->mergeCells($cr->getNextCol($nextCol).$cr->getNextRow().':'.$cr->curCol().$cr->getNextRow());

			//top line
			$this->excel->getActiveSheet()->getStyle($firstTableCol.$cr->curRow().':'.$cr->curCol().$cr->getNextRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                    'font'  => array(
		                        'size'  => 12,
		                    ),
		                )
		            );

			$this->excel->getActiveSheet()->getStyle($firstTableCol.$cr->curRow())->applyFromArray(
		                array(
		                    'font'  => array(
				                        'bold'  => false,
				                        'color' => array('rgb' => '000000'),
				                        'size'  => 14,
				                        'name'  => 'ＭＳ Ｐゴシック'
				                    ),
		                )
		            );

			$this->excel->getActiveSheet()->getRowDimension($cr->getPrevRow())->setRowHeight(8);
			$this->excel->getActiveSheet()->getRowDimension($cr->curRow())->setRowHeight(20);
			$this->excel->getActiveSheet()->getRowDimension($cr->getNextRow())->setRowHeight(30);

			$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
			//$this->excel->getActiveSheet()->getPageSetup()->setScale(88);
        }
	}

	private function drawsheet10($activeSheetIndex = 0)
	{
		// make ranking for all players
		if($this->input->post('group_include_rank')) {
			$resultRanking 	= RankingCollection::implementRanking($this->players, 'getTotalFinalScore');
		}

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($this->players);

		$items = $this->game->getItems();
        $tournament = $this->game->getTournament();

        /////////////////////////
        // start excel draw    //
        /////////////////////////

		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('班別速報'.$this->game->getStrSex());

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '班別速報　'.$this->game->getStrSex());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', '組');
		$this->excel->getActiveSheet()->SetCellValue('C4', 'No.');
		$this->excel->getActiveSheet()->SetCellValue('D4', '氏名');
		$this->excel->getActiveSheet()->SetCellValue('E4', "学\n年");
		$this->excel->getActiveSheet()->SetCellValue('F4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('G4', '都道府県');

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		$this->excel->getActiveSheet()->mergeCells('E4:E5');
		$this->excel->getActiveSheet()->mergeCells('F4:F5');
		$this->excel->getActiveSheet()->mergeCells('G4:G5');
		
		// head item score
		$itemNameHeatCol = 'H';
		$cr->curCol = 'G';
		$colidx = $cr->getIndexCol($itemNameHeatCol);

		foreach ($items as $item) {

			$itemName = $item->getName();
			$this->excel->getActiveSheet()->SetCellValue($itemNameHeatCol .'4', $itemName);
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'D');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', 'E');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '減点');
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'5', '合計');
			$this->excel->getActiveSheet()->mergeCells($itemNameHeatCol .'4:'.$cr->curCol().'4');
			// get col for next item name
			$colidx = $cr->getIndexCol($itemNameHeatCol);
			$itemNameHeatCol = $cr->rgeCol[$colidx+4];
		}

		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合\n得点");
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        'size'  => 11,
			                        'name'  => 'ＭＳ ゴシック'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;

        foreach ($groupHeatPlayers as $group) {

        	$heats = $group->getHeats();

	        foreach ($heats as $heat)
	        {
	    		$heatCol = 'B';
	    		$heatRow = $cr->curRow();

	    		$heatPlayers = $heat->getPlayers();

	    		foreach ($heatPlayers as $player)
	    		{
	    			$cr->curCol = $heatCol;

	    			// player info
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerNo());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPlayerName());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getGrade());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getSchoolNameAb());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $player->getPrefecture());
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
	    			
	    			// item score
	    			foreach ($items as $item) {
						$itemName  = $item->getName();
						$playerOff = (!$player->hasItemScorePublished($item) && $player->getFlagCancle()) ? true : false;

	    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(),'');
	    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	    					array(
								'font'  => array('name'  => 'Verdana'),
								'alignment' => array(
									'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
									'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
								)
							)
	    				);
	    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(),'');
	    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	    					array(
								'font'  => array('name'  => 'Verdana'),
								'alignment' => array(
									'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
									'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
								)
							)
	    				);
	    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(),'');
	    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	    					array(
								'font'  => array('name'  => 'Verdana'),
								'alignment' => array(
									'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
									'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
								)
							)
	    				);
	    				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(),'');
	    				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
	    					array(
								'font'  => array('name'  => 'Verdana'),
								'alignment' => array(
									'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
									'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
								)
							)
	    				);
	    			}

	    			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), '');
	    			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

	    			$this->excel->getActiveSheet()->getStyle('C'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
				                array(
				                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        ),
				                    ),
				                )
				            );

	    			$cr->incRow();
	    		}

	    		// draw a border line when end of each heat
	    		$this->excel->getActiveSheet()->getStyle($heatCol.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'bottom' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );


	    		$heatRange = $heatCol.$heatRow.':'.$heatCol.$cr->getPrevRow();
	    		
	    		// merge range cell for heat
	    		$this->excel->getActiveSheet()->mergeCells($heatRange);
	    		$this->excel->getActiveSheet()->SetCellValue($heatCol.$heatRow, $heat->getId());

	    		$this->excel->getActiveSheet()->getStyle($heatCol.$heatRow)->applyFromArray(array('font'  => array('name'  => 'Verdana')));

	    		// set text align center for heat value cell
	    		$this->excel->getActiveSheet()->getStyle($heatRange)->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
			            );
			}
        }

		// draw right line seperate for each item
		$itemHeatCol = 'K';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+4];
		}

		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('G4:G'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// pref column align center
		$this->excel->getActiveSheet()->getStyle('E4:E'.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                )
		            );

		// draw right line seperate for total score and ranking
		$colidx = $cr->getIndexCol($itemHeatCol);
		$rankiHeatCol = $cr->rgeCol[$colidx-2]; // -2 because we've +4 at last foreach loop
		$totalHeatCol = $cr->rgeCol[$colidx-3];

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );



		if($this->input->post('group_include_rank')) {

			$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'right' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );
		}

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet11($activeSheetIndex = 0)
	{
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('団体順位'.$this->game->getStrSex());

		$groupScores 	= [];
		$schoolGroups 	= $this->game->getSchoolGroups();

		$items = $this->game->getItems();
		$tournament = $this->game->getTournament();

		foreach ($schoolGroups as $schoolGroup) {

			$groupScore = new \Entities\GroupScore;

			// find persons in same school
			$schoolPlayers = array_filter($this->players, function($p) use ($schoolGroup) {
				return $p->getSchool() == $schoolGroup;
			});

			// calculate score for each item
			foreach ($items as $item) {

				$bestScores = [];

				foreach ($schoolPlayers as $player) {
					
					$score = $player->getItemBestScoreValue($item, 'FinalScore');
					
					if(!in_array($score, $bestScores)) $bestScores[] = $score;
				}

				arsort($bestScores);

				$threeBestScore = array_slice($bestScores, 0, 3);

				$groupItemScore = array_sum($threeBestScore);

				$fn = 'setItem'.$item->getNo().'Score';

				$groupScore->$fn($groupItemScore);
				$groupScore->setTotalScore();
				$groupScore->setSchoolGroup($schoolGroup);
			}

			$groupScores[] = $groupScore;
		}

		$resultGroupRanking = RankingCollection::implementRanking($groupScores, 'getTotalScore');

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($groupScores, 'getItem'.$item->getNo().'Score');
		}

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '団体順位 '.$this->game->getStrSex().$this->game->getClass());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', "順\n位");
		$this->excel->getActiveSheet()->SetCellValue('C4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('D4', '都道府県');
		$this->excel->getActiveSheet()->getStyle('D4')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 9,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		
		// head item score
		$cr->curCol = 'D';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'4', $item->getName());
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
			$this->excel->getActiveSheet()->mergeCells($cr->curCol() . '4:'.$cr->incCol().'4');
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(4);
			$this->excel->getActiveSheet()->SetCellValue($cr->getPrevCol() .'5', '合計');
			$this->excel->getActiveSheet()->SetCellValue($cr->curCol() .'5', '順位');
			$this->excel->getActiveSheet()->getStyle($cr->curCol() .'5')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 8,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );
		}

		$this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合点");
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(10);
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '点差');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(7.5);
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        // 'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;
    		
		$heatCol = 'A';
		$heatRow = $cr->curRow();

		$lastRank = $resultGroupRanking->first();

		foreach ($resultGroupRanking as $rank)
		{
			$groupScore = $rank->getElement();
			$school = $groupScore->getSchoolGroup();
			$cr->curCol = $heatCol;
			// player info
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			
			// item score
			foreach ($items as $item) {
				$itemName  = $item->getName();

				$itemScore  = call_user_func_array([$groupScore, 'getItem'.$item->getNo().'Score'], []);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $itemScore);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					array(
						'font'  => array('name'  => 'Verdana'),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
							'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					)
				);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			}

			// draw line seperate score and total
			$this->excel->getActiveSheet()->getStyle($cr->curCol().'4:'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'right' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );


			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $groupScore->getTotalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->distanceAnotherRank($lastRank, 'getTotalScore'));
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			$lastRank = $rank;

			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
	            );
			

			// su dung de so sanh 2 diem cua 2 player tren va duoi.
			$aboveScore = $player->getTotalFinalScore();

			$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        ),
		                    ),
		                )
		            );

			$cr->incRow();
		}

		$this->excel->getActiveSheet()->getStyle('B6:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
		                )
		            );

		// draw right line seperate for each item
		$itemHeatCol = 'D';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+2];
		}


		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('D4:D'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw bottom line end table
		$this->excel->getActiveSheet()->getStyle('B'.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw right line seperate for total score and ranking
		$rankiHeatCol = $cr->getNextCol($itemHeatCol);
		$totalHeatCol = $cr->getNextCol($rankiHeatCol);

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25.5);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(8);

		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function drawsheet12($activeSheetIndex = 0)
	{
		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex($activeSheetIndex);
		$this->excel->getActiveSheet()->setTitle('団体順位'.$this->game->getStrSex());

		$groupScores 	= [];
		$schoolGroups 	= $this->game->getSchoolGroups();

		$items = $this->game->getItems();
		$tournament = $this->game->getTournament();

		foreach ($schoolGroups as $schoolGroup) {

			$groupScore = new \Entities\GroupScore;

			// find persons in same school
			$schoolPlayers = array_filter($this->players, function($p) use ($schoolGroup) {
				return $p->getSchool() == $schoolGroup;
			});

			// calculate score for each item
			foreach ($items as $item) {

				$bestScores = [];

				foreach ($schoolPlayers as $player) {
					
					$score = $player->getItemBestScoreValue($item, 'FinalScore');
					
					if(!in_array($score, $bestScores)) $bestScores[] = $score;
				}

				arsort($bestScores);

				$threeBestScore = array_slice($bestScores, 0, 3);

				$groupItemScore = array_sum($threeBestScore);

				$fn = 'setItem'.$item->getNo().'Score';

				$groupScore->$fn($groupItemScore);
				$groupScore->setTotalScore();
				$groupScore->setSchoolGroup($schoolGroup);
			}

			$groupScores[] = $groupScore;
		}

		$resultGroupRanking = RankingCollection::implementRanking($groupScores, 'getTotalScore');

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($groupScores, 'getItem'.$item->getNo().'Score');
		}

        $cr = new ColRow();

        // draw table head
        // sheet description
        $this->excel->getActiveSheet()->SetCellValue('B1', $tournament->getName());
        $this->excel->getActiveSheet()->SetCellValue('B2', '場所：'.$tournament->getPlace().' 日時：平成'.$tournament->getStartTime()->format('Y年m月d日').'～平成'.$tournament->getEndTime()->format('Y年m月d日'));
        $this->excel->getActiveSheet()->SetCellValue('B3', '団体順位 '.$this->game->getStrSex().$this->game->getClass());

        // font size sheet description
		$this->excel->getActiveSheet()->getStyle('B1')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 12,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->getStyle('B3')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 16,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

        // head player info
		$this->excel->getActiveSheet()->SetCellValue('B4', "順\n位");
		$this->excel->getActiveSheet()->SetCellValue('C4', '学校');
		$this->excel->getActiveSheet()->SetCellValue('D4', '都道府県');
		$this->excel->getActiveSheet()->getStyle('D4')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 9,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );

		$this->excel->getActiveSheet()->mergeCells('B4:B5');
		$this->excel->getActiveSheet()->mergeCells('C4:C5');
		$this->excel->getActiveSheet()->mergeCells('D4:D5');
		
		// head item score
		$cr->curCol = 'D';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol() .'4', $item->getName());
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(9.5);
			$this->excel->getActiveSheet()->mergeCells($cr->curCol() . '4:'.$cr->incCol().'4');
			$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(4);
			$this->excel->getActiveSheet()->SetCellValue($cr->getPrevCol() .'5', '合計');
			$this->excel->getActiveSheet()->SetCellValue($cr->curCol() .'5', '順位');
			$this->excel->getActiveSheet()->getStyle($cr->curCol() .'5')->applyFromArray(
			                array(
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 8,
			                        'name'  => 'Calibri'
			                    ),
		                )
	            );
		}

		$this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);

		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', "総合点");
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(10);
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');
		$this->excel->getActiveSheet()->getStyle($cr->curCol().'4')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->SetCellValue($cr->incCol().'4', '点差');
		$this->excel->getActiveSheet()->getColumnDimension($cr->curCol())->setWidth(7.5);
		$this->excel->getActiveSheet()->mergeCells($cr->curCol().'4:'.$cr->curCol().'5');

		$tableBgColor = ($this->game->isMale()) ? '1976D2' : 'F06292';

		// draw a border line when end of each heat
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'5')->applyFromArray(
			                array(
				                'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => $tableBgColor)
			                    ),
			                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => 'FFFFFF'),
			                        // 'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    ),
			                    'borders' => array(
				                        'allborders' => array(
				                            'style' => PHPExcel_Style_Border::BORDER_THIN,
				                            'color' => array('argb' => '000000'),
				                        )
				                    ),
		                )
	            );

        // draw table data
        $startRow = $cr->curRow = 6;
    		
		$heatCol = 'A';
		$heatRow = $cr->curRow();

		$lastRank = $resultGroupRanking->first();

		foreach ($resultGroupRanking as $rank)
		{
			$groupScore = $rank->getElement();
			$school = $groupScore->getSchoolGroup();
			$cr->curCol = $heatCol;
			// player info
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolNameAb());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $school->getSchoolPrefecture());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'ＭＳ ゴシック')));
			
			// item score
			foreach ($items as $item) {
				$itemName  = $item->getName();

				$itemScore  = call_user_func_array([$groupScore, 'getItem'.$item->getNo().'Score'], []);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $itemScore);
				$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
					array(
						'font'  => array('name'  => 'Verdana'),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
							'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					)
				);

				$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->getRank());
			}

			// draw line seperate score and total
			$this->excel->getActiveSheet()->getStyle($cr->curCol().'4:'.$cr->curCol().$cr->curRow())->applyFromArray(
			                array(
			                    'borders' => array(
			                        'right' => array(
			                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
			                            'color' => array('argb' => '000000'),
			                        )
			                    ),
			                )
			            );


			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $groupScore->getTotalScore());
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));
			
			$this->excel->getActiveSheet()->SetCellValue($cr->incCol().$cr->curRow(), $rank->distanceAnotherRank($lastRank, 'getTotalScore'));
			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(array('font'  => array('name'  => 'Verdana')));

			$lastRank = $rank;

			$this->excel->getActiveSheet()->getStyle($cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'alignment' => array(
		                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                    ),
		                )
	            );
			

			// su dung de so sanh 2 diem cua 2 player tren va duoi.
			$aboveScore = $player->getTotalFinalScore();

			$this->excel->getActiveSheet()->getStyle('B'.$cr->curRow().':'.$cr->curCol().$cr->curRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'allborders' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_THIN,
		                            'color' => array('argb' => '000000'),
		                        ),
		                    ),
		                )
		            );

			$cr->incRow();
		}

		$this->excel->getActiveSheet()->getStyle('B6:'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'font'  => array(
			                        'bold'  => false,
			                        'color' => array('rgb' => '000000'),
			                        'size'  => 11,
			                        'name'  => 'Verdana'
			                    ),
		                )
		            );

		// draw right line seperate for each item
		$itemHeatCol = 'D';

		foreach ($items as $item) {
			$this->excel->getActiveSheet()->getStyle($itemHeatCol.'4:'.$itemHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

			// get col for next item name
			$colidx = $cr->getIndexCol($itemHeatCol);
			$itemHeatCol = $cr->rgeCol[$colidx+2];
		}


		// draw right line seperate between pref and score
		$this->excel->getActiveSheet()->getStyle('D4:D'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw left line begin table
		$this->excel->getActiveSheet()->getStyle('B4:B'.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'left' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B4:'.$cr->curCol().'4')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'top' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw bottom line end table
		$this->excel->getActiveSheet()->getStyle('B'.$cr->getPrevRow().':'.$cr->curCol().$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw top line table
		$this->excel->getActiveSheet()->getStyle('B5:'.$cr->curCol().'5')->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw right line seperate for total score and ranking
		$rankiHeatCol = $cr->getNextCol($itemHeatCol);
		$totalHeatCol = $cr->getNextCol($rankiHeatCol);

		$this->excel->getActiveSheet()->getStyle($totalHeatCol.'4:'.$totalHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->getStyle($rankiHeatCol.'4:'.$rankiHeatCol.$cr->getPrevRow())->applyFromArray(
		                array(
		                    'borders' => array(
		                        'right' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		// draw  sheet bottom  line
		$lineBottomRow  = 'B'.($cr->curRow()+1).':K'.($cr->curRow()+1);

		$this->excel->getActiveSheet()->getStyle($lineBottomRow)->applyFromArray(
		                array(
		                    'borders' => array(
		                        'bottom' => array(
		                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
		                            'color' => array('argb' => '000000'),
		                        )
		                    ),
		                )
		            );

		$this->excel->getActiveSheet()->SetCellValue('B'.($cr->curRow()+1), '審判長');

		// set with for column heat
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25.5);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(8);

		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->excel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
	}

	private function export()
	{
		// save to file

		$file_name_excel_output = 'gymnastics_excel_' . date('Y-m-d') . '.xlsx';
		$tmp_upload_excel       = APPPATH . '../uploads/' . $file_name_excel_output;

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save($tmp_upload_excel);
		

		if (headers_sent()) {
			throw new Exception('HTTP header already sent');
		} else {
			if ( ! is_file($tmp_upload_excel)) {
				header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
				throw new Exception('File not found or did not have data');
			} else if (!is_readable($tmp_upload_excel)) {
				header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
				throw new Exception('File not readable');
			} else {
				header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="' . basename($file_name_excel_output) .'"');
				header('Cache-Control: max-age=0');
				header('Cache-Control: max-age=1');

				header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
				header ('Cache-Control: cache, must-revalidate');
				header ('Pragma: public');

				readfile($tmp_upload_excel);
				shell_exec('sudo rm -rf ' . $tmp_upload_excel);
				exit;
			}
		}
	}
}

class ColRow
{
	public $curCol;
	public $curRow;

	public $rgeCol;
	public $rgeRow;


	public function __construct($col = null, $row = null)
	{
		$this->rgeCol = [];

		foreach (range('A', 'Z') as $lv1)
		{
			array_push($this->rgeCol, $lv1);
		}

		foreach (range('A', 'Z') as $lv1)
		{
			foreach (range('A', 'Z') as $lv2)
			{
				array_push($this->rgeCol, $lv1.$lv2);
			}
		}

		$this->rgeRow = range(1, 1000);

		$this->setPosition($col, $row);
	}


	public function setPosition($col, $row)
	{
		if($col) $this->curCol = $col;
		if($row) $this->curRow = $row;
	}

	public function incCol()
	{
		$key = array_search($this->curCol, $this->rgeCol);

		if(isset($this->rgeCol[$key+1]))
		{
			return $this->curCol = $this->rgeCol[$key+1];
		}
		else
			throw new Exception('Out off range column.');

		$this->curCol = $this->getNextCol();
	}

	public function incRow()
	{
		if(in_array($this->curRow, $this->rgeRow))
		{
			return $this->curRow = $this->curRow+1;
		}
		else
			throw new Exception('Out off range row');
	}

	public function curRow()
	{
		return $this->curRow;
	}

	public function curCol()
	{
		return $this->curCol;
	}

	public function getNextCol($col = null)
	{
		if(!$col) $col = $this->curCol;

		$key = array_search($col, $this->rgeCol);

		if(isset($this->rgeCol[$key+1]))
		{
			return $this->rgeCol[$key+1];
		}
		else
			throw new Exception('Out off range column.');
	}

	public function getPrevCol($col = null)
	{
		if(!$col) $col = $this->curCol;

		$key = array_search($col, $this->rgeCol);

		if(isset($this->rgeCol[$key-1]))
		{
			return $this->rgeCol[$key-1];
		}
		else
			throw new Exception('Out off range column.');
	}

	public function getNextRow($row = null)
	{
		if(!$row) $row = $this->curRow;

		if(in_array($row+1, $this->rgeRow))
		{
			return $this->curRow+1;
		}
		else
			throw new Exception('Out off range row');
	}

	public function getPrevRow($row = null)
	{
		if(!$row) $row = $this->curRow;

		if(in_array($row-1, $this->rgeRow))
		{
			return $this->curRow-1;
		}
		else
			throw new Exception('Out off range row');
	}

	public function getIndexCol($col)
	{
		if(!$col) $col = $this->curCol;

		$key = array_search($col, $this->rgeCol);

		if($key !== false)
		{
			return $key;
		}
		else
			throw new Exception('Out off range column.');
	}
}

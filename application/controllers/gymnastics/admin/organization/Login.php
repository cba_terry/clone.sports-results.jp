<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Organization_Controller {

	public function index()
	{
		if ($this->session->userdata('user')){
			redirect('/admin/organization/tournament', 'refesh');
		}
		$data['title'] = 'ログイン';
		$post = $this->input->post();
		if ($post && $this->form_validation->run('admin_login')){
			$remember_me = isset($post['remember-me'])?$post['remember-me']:NULL;
			$login = $this->do_login($post['username'], $post['password'], $remember_me);

			if ($login){
				redirect('/admin/organization/tournament', 'refesh');
			}else{
				$data['error'] = 'Username or password is not correct.';
			}
		}
		$this->load->view('gymnastics/admin/organization/login', $data);
	}

	private function do_login($username, $password, $remember_me){
		$user = $this->getRepository('User')->findOneBy(array('user_name' => $username, 'password' => $password));
		if ($user){
			$this->session->set_userdata('user', $user);
			//Remember me functionality
			if ($remember_me){
				$hash = md5(time());
				$user->setHash($hash);
				$this->em->persist($user);
				$this->em->flush();
				$this->load->helper('cookie');
				set_cookie('user', $username.'_'.$hash, 604800, '', '/', '', false, true);
			}
			return true;
		}
		return false;
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referee extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$gameId = $this->input->get('gid');
		$post   = $this->input->post();

		$game = $this->getRepository('game')->find((int)$gameId);

		if(!$game) throw new Exception('Game not found!');

		$conditions = [
			'game'         => $game,
			'id'           => $this->input->get('id'),
			'referee_name' => $this->input->get('referee_name'),
			'all'          => true
		];

		$referees = $this->getRepository('Referee')->getPagedReferees($conditions);

		// update flag_active
		if (isset($post['is_post']) && ! empty($referees)) {
			$arr_referee = array();
			// filter array referee
			foreach ($referees as $key => $referee) {
				$arr_referee[] = $referee->getId();
			}
			// reset flag active of list referee is 0
			$this->getRepository('Referee')->updateFlagActive($arr_referee);

			if (isset($post['flag_active'])) {
				// get and filter data from checkbox input
				// update flag active for referee is 1
				$arr_referee = array_keys($post['flag_active']);
				$this->getRepository('Referee')->updateFlagActive($arr_referee, 1);
			}
			redirect('/admin/organization/referee?gid='.$game->getId());
		}

		$this->load->view('gymnastics/admin/organization/referee_list', array(
			'referees'   => $referees,
			'conditions' => $conditions,
			'game'       => $game
		));
	}

	public function edit($id)
	{
		$referee = $this->getRepository('Referee')->find($id);
		if(!$referee) throw new Exception('Referee not found!');
		$gameId = $referee->getGame()->getId();

		$players = $this->getRepository('Player')->getResultList(['game' => $referee->getGame(), 'group_by' => 'heat']);

		if ($this->form_validation->run('referee') !== FALSE)
		{
			$referee->setRefereeName($this->input->post('referee_name'));
			$referee->setPassword($this->input->post('password'));
			$referee->setItem($this->input->post('item'));
			$referee->setScoreType($this->input->post('score_type'));

			$this->em->persist($referee);
			$this->em->flush();

			redirect('/admin/organization/referee?gid='.$gameId);
		}
		else
		{
			$this->load->view('gymnastics/admin/organization/referee_edit', array(
				'referee' => $referee,
				'num_heats' => count($players)
				));
		}
	}

	public function delete ()
	{
		$id = $this->input->post('referee_id');
		$referee = $this->getRepository('Referee')->find($id);

		if($referee){
			$this->em->remove($referee);
			$this->em->flush();

			redirect('/admin/organization/referee'.$this->url.'?delete=success');
		}

		echo json_encode(array('id' => $id));
		exit;
	}
}

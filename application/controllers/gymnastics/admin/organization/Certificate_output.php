<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/libraries/mpdf60/mpdf.php';

class Certificate_output extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$game = $this->getRepository('Game')->find($this->input->get('gid'));
		
		if(!$game) throw new Exception('Please select a game');

		$items = $game->getItems();


		if($this->input->post()) {

			$postGroup = $this->input->post('group');
			$postSingle = $this->input->post('single');
			$postItem = $this->input->post('item');
			
			$players = $this->getRepository('Player')->getPlayersResults($game);

			if(!$players) throw new Exception('決勝データがありません。');		

			// ranking
			// $resultRanking 	= RankingCollection::implementRanking($players, 'getTotalFinalScore');

			//single ranking
			$singlePlayers = array_filter($players, function($player){
				return $player->isPlayAsSingle();
			});

			$resultSingleRanking = RankingCollection::implementRanking($singlePlayers, 'getTotalFinalScore')
															->from($postSingle['from'])
															->to($postSingle['to']);

			//group ranking
			$groupScores 	= [];
			$schoolGroups 	= $game->getSchoolGroups();

			foreach ($schoolGroups as $schoolGroup) {

				$groupScore = new \Entities\GroupScore;

				// find persons in same school
				$schoolPlayers = array_filter($players, function($p) use ($schoolGroup) {
					return $p->getSchool() == $schoolGroup;
				});

				// calculate score for each item
				foreach ($items as $item) {

					$bestScores = [];

					foreach ($schoolPlayers as $player) {
						
						$score = $player->getItemBestScoreValue($item, 'FinalScore');
						
						if(!in_array($score, $bestScores)) $bestScores[] = $score;
					}

					arsort($bestScores);

					$threeBestScore = array_slice($bestScores, 0, 3);

					$groupItemScore = array_sum($threeBestScore);

					$fn = 'setItem'.$item->getNo().'Score';

					$groupScore->$fn($groupItemScore);
					$groupScore->setTotalScore();
					$groupScore->setSchoolGroup($schoolGroup);
				}

				$groupScores[] = $groupScore;
			}

			$resultGroupRanking = RankingCollection::implementRanking($groupScores, 'getTotalScore')
															->from($postGroup['from'])
															->to($postGroup['to']);

	        // make ranking by item
			$itemRanking = [];

			foreach ($items as $item) {
				$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($players, 'getItemBestScoreValue', 
															[$item, 'FinalScore'])
															->from($postItem['from'])
															->to($postItem['to']);
			}

			$dataCertificate = [
				'group' => $resultGroupRanking,
				'single' => $resultSingleRanking,
				'item' => $resultItemRanking
			];

			$dir = md5(time());
			$path = APPPATH . '../uploads/';
			$files = array();

			if(!mkdir($path . $dir, 0777, true)) throw new Exception('permission denied!');

			if(count($dataCertificate)) {
				foreach($dataCertificate as $type => $certificates) {
					if($type != 'item')
					{
						$data = [
							'certificates' => $certificates,
							'game' => $game,
							'type' => $type
						];

						$template = 'gymnastics/admin/organization/print_'.$type;

						$html = $this->load->view($template, $data, true);

						$mpdf = new mPDF('+aCJK','A3','','',12.7, 12.7, 12.7, 12.7, 8, 8);

						$mpdf->SetDisplayMode(100,'two');
						$mpdf->autoLangToFont = true;
						$mpdf->debug = true;
						$mpdf->WriteHTML($html);

						$file = $type . '_' . date("YmdHis").'.pdf';

						$files[] =  $file;
						
						$mpdf->Output($path . $dir . DIRECTORY_SEPARATOR . $file , 'F');
					}
					else {
						foreach ($certificates as $item => $players) {

							$data = [
								'players' => $players,
								'game' => $game,
								'type' => $type,
								'item' => $item,
							];

							$template = 'gymnastics/admin/organization/print_'.$type;

							$html = $this->load->view($template, $data, true);
							
							$mpdf = new mPDF('+aCJK','A3','','',12.7, 12.7, 12.7, 12.7, 8, 8);

							$mpdf->SetDisplayMode(100,'two');
							$mpdf->autoLangToFont = true;
							$mpdf->debug = true;
							$mpdf->WriteHTML($html);

							$oitem = $items->filter(function($i) use ($item) {
								return $i->getName() == $item;
							})->first();

							$file = $type . $oitem->getNo().'_' . date("YmdHis").'.pdf';

							$files[] =  $file;
							
							$mpdf->Output($path . $dir . DIRECTORY_SEPARATOR . $file , 'F');
						}
					}
				}
			}

			chdir($path.$dir);

			$cmd = 'zip -l certificate.zip ' . implode(' ', $files);
			
			shell_exec($cmd);

			$output_file = $path . $dir . '/certificate.zip'; 

			if (headers_sent()) {
    			throw new Exception('HTTP header already sent');
			} else {
			    if (!is_file($output_file)) {
			        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			        throw new Exception('File not found or did not have data');
			    } else if (!is_readable($output_file)) {
			        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
			        throw new Exception('File not readable');
			    } else {
			        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
			        header("Content-Type: application/zip");
			        header("Content-Transfer-Encoding: Binary");
			        header("Content-Length: ".filesize($output_file));
			        header("Content-Disposition: attachment; filename=\"".basename($output_file)."\"");
			        readfile($output_file);

			        //remove file
			        chdir($path);
			        shell_exec('rm -rf ' . $dir);
			        
			        exit;
			    }
			}
		}

		$this->load->view('gymnastics/admin/organization/certificate_output', [
			'items' => $items,
			'game' => $game,
			]);
	}
}

<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Finalist extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();

		$gameId = $this->input->get('gid');

		$this->game = $this->getRepository('game')->find($gameId);

		if(!$this->game) throw new Exception('Game not found!');
	}

	public function index()
	{
		// lay tat ca nguoi choi (player) trong giai dau
		$players = $this->getRepository('Player')->getPlayersResults($this->game);

		$rankCommon = RankingCollection::implementRanking($players, 'getTotalFinalScore');

		$playAsGroup = array_filter($players, function($p){
			return $p->isPlayAsGroup();
		});

		$rankGroup = RankingCollection::implementRanking($playAsGroup, 'getTotalFinalScore');

		$playAsSingle = array_filter($players, function($p){
			return $p->isPlayAsSingle();
		});

		$rankSingle = RankingCollection::implementRanking($playAsSingle, 'getTotalFinalScore');

		// make item ranking //
		$items = $this->game->getItems();

		foreach ($items as $item) {
			$rankItems[(string) $item] = RankingCollection::implementRanking($players, 'getItemBestScoreValue', [$item, 'FinalScore']);
		}

		$numberPlayerInGroupComeFinal = $this->game->getGroupPassNumber();
		$numberPlayerInSingleComeFinal = $this->game->getSinglePassNumber();
		$numberPlayerInItemComeFinal = $this->game->getItemPassNumber();

		$count1 = $count2 = 1;

		$chooseFinalFromGroup = $rankGroup->filter(function($r) use (&$count1, $numberPlayerInGroupComeFinal) {
			if($count1 <= $numberPlayerInGroupComeFinal)
			{
				$count1++; return $r;
			}
		});

		$chooseFinalFromSingle = $rankCommon->filter(function($r) use (&$count2, $numberPlayerInSingleComeFinal, $chooseFinalFromGroup) {
			if($count2 <= $numberPlayerInSingleComeFinal && !$chooseFinalFromGroup->containsElement($r->getElement()) && $r->getElement()->isPlayAsSingle())
			{
				$count2++; return $r;
			}
		});

		$chooseFinalFromItem = new RankingCollection;

		foreach ($items as $item) {

			$count3 = 1;

			$lastItem = (isset($lastItem)) ? $lastItem : null;

			$rankItems[(string) $item]->map(function($r) use (&$count3, $numberPlayerInItemComeFinal, 
												$chooseFinalFromGroup, $chooseFinalFromSingle, $lastItem, $chooseFinalFromItem) {

												if($count3 <= $numberPlayerInItemComeFinal && !$chooseFinalFromGroup->containsElement($r->getElement()) && !$chooseFinalFromSingle->containsElement($r->getElement()) && !$chooseFinalFromItem->containsElement($r->getElement()))
												{
													$count3++; $chooseFinalFromItem->add($r);
													
												}
											});

			$lastItem = $item;
		}


		$order = $this->input->get('orderby');

		if(!$order) $order = 'group';

		switch ($order) {

			case 'single':

				uasort($players, function($a, $b) {
					return $a->getTotalFinalScore() > $b->getTotalFinalScore() ? -1 : 1;
				});

				break;

			case 'item':

				$item = $this->input->get('item');
				$item = $this->game->findItem($item);

				uasort($players, function($a, $b) use ($item) {
					return $a->getItemBestScoreValue($item, 'FinalScore') > $b->getItemBestScoreValue($item, 'FinalScore') ? -1 : 1;
				});

				break;
			
			default:
				
				uasort($players, function($a, $b) {
					if($a->getFlag() == $b->getFlag())
						return $a->getTotalFinalScore() > $b->getTotalFinalScore() ? -1 : 1;
					return $a->getFlag() > $b->getFlag() ? -1 : 1;
				});

				break;
		}

		$finalGame = $this->getRepository('Game')->findOneBy(array(
				'tournament' => $this->game->getTournament(),
				'sex' => $this->game->getSex(),
				'class' => '決勝',
				));

		$this->load->view('gymnastics/admin/organization/finalist_list', array(
			'game'              => $this->game,
			'items'             => $items,
			'players' 			=> $players,
			'rankGroup'			=> $rankGroup,
			'rankSingle'		=> $rankSingle,
			'rankCommon'		=> $rankCommon,
			'rankItems'			=> $rankItems,
			'finalGame'			=> $finalGame,
			'chooseFinalFromItem'		=> $chooseFinalFromItem,
			'chooseFinalFromGroup'		=> $chooseFinalFromGroup,
			'chooseFinalFromSingle'		=> $chooseFinalFromSingle,
		));
	}

	/**
	 * setting
	 * 
	 * setting team group for final list
	 * @author Terry
	 */

	public function setting()
	{
		$data = [];
		$team = (!$this->input->get('team')) ? 1 : $this->input->get('team');

		$tournament = $this->game->getTournament();

		$setting = $this->getRepository('FinalSetting')->findOneBy(array(
			'team' => $team,
			'tournament' => $tournament
			));

		if($setting)
		{
			$data = unserialize($setting->getSetting());
		}

		$games = $tournament->getGames();

		$finalPlayers = [];

		if($this->input->post())
		{
			$data = $this->input->post('data');
			$team = $this->input->post('team');

			$sex = $this->game->getSex();

			foreach ($data as $heat => $players) {
				foreach ($players as $player) {
					$class = explode('_', $player['class']);

					$type = $player['type'];
					$rank = $player['rank'];

					$item = $this->game->findItem($class[1]);

					$result = $this->getRepository('Player')->findPlayerByRank([
							'tournament' => $tournament,
							'sex' => $sex,
							'class' => $class[0],
							'type' => $type,
							'item' => ($item) ? $item->getNo() : null,
							'rank' => $rank
						]);

					if(!$player) throw new Exception("There is no player for fit");
					
					$finalPlayers[] = [
						'player' 	=> $result,
						'group' 	=> $team,
						'heat' 		=> $heat,
						'type'		=> $type
					];
				}
			}

			foreach ($this->game->getPlayers() as $player) {
				$this->game->removePlayer($player);
				$this->em->remove($player);
			}

			foreach ($this->game->getSchoolGroups() as $school) {
				$this->game->removeSchoolGroup($school);
				$this->em->remove($school);
			}

			$rotationSettings = $this->game->getRotationSettings();
            foreach ($rotationSettings as $rst) {
            	$this->game->removeRotationSetting($rst);
                $this->em->remove($rst);
            }

            $rotations = $this->game->getRotations();
            foreach ($rotations as $r) {
            	$this->game->removeRotation($r);
                $this->em->remove($r);
            }

			$this->em->persist($this->game);
            $this->em->flush();

			foreach ($finalPlayers as $player) {
				$newPlayer = clone($player['player']);
				$newPlayer->setGame($this->game);
				$newPlayer->setFlag(convertGameTypeStringToBool($player['type']));
				$newPlayer->setGroup($player['group']);
				$newPlayer->setHeat($player['heat']);

				if (!$this->game->checkSchoolExist($player['player']->getSchool()->getSchoolNameAb())) 
                {
                	$orgSchool = $player['player']->getSchool();
                    $newSchool = new \Entities\SchoolGroup;

                    $newSchool->setSchoolName($orgSchool->getSchoolName());
                    $newSchool->setSchoolNameAb($orgSchool->getSchoolNameAb());
                    $newSchool->setSchoolNo($orgSchool->getSchoolNo());
                    $newSchool->setSchoolPrefecture($orgSchool->getSchoolPrefecture());

                    $newSchool->setGame($this->game);
                    $this->game->addSchoolGroup($newSchool);

                    $newPlayer->setSchool($newSchool);
                }
                else
                {
                	$school = $this->game->getSchoolGroups()->filter(function($s) use ($player){
                		return $s->getSchoolNameAb() == $player['player']->getSchool()->getSchoolNameAb();
                	})->first();

                	$newPlayer->setSchool($school);
                }

                $this->game->addPlayer($newPlayer);

                $this->em->persist($this->game);
                $this->em->persist($newSchool);
                $this->em->persist($newPlayer);
			}

			$setting = ($setting) ? $setting : new \Entities\FinalSetting;

			$setting->setTeam($team);
			$setting->setTournament($this->game->getTournament());
			$setting->setSetting(serialize($data));

			$this->em->persist($setting);
            $this->em->flush();
		}

		$numberTeam = 1;

		if($tournament->getFinalSettings()->count())
		{
			$numberTeam = $tournament->getFinalSettings()->count();
		}

		$this->load->view('gymnastics/admin/organization/finalist_setting', array(
				'game' 		=> $this->game,
				'setting' 	=> $data,
				'team' 		=> $team,
				'numberTeam' => $numberTeam,
				'tournament' => $tournament
			));
	}

	public function accept ()
	{
		try {

			$players = $this->input->post('players');

			$tournament = $this->game->getTournament();

			$settings = $this->getRepository('FinalSetting')->findBy(array(
				'tournament' => $tournament
				));

			if($settings)
			{
				foreach ($settings as $setting) {
					$data[] = unserialize($setting->getSetting());
				}
			}

			$ids 	= array_column($players, 'id');

			array_multisort($ids, SORT_ASC, $players);
			
			$flags 			= array_column($players, 'pass_single_or_group');
			$commonRanks 	= array_column($players, 'common_rank');
			$singleRanks 	= array_column($players, 'single_rank');
			$groupRanks 	= array_column($players, 'group_rank');

			$items = $this->game->getItems();

			foreach ($items as $item) {
				$itemRanks[$item->getNo()] = array_column($players, 'item'.$item->getNo().'_rank');
			}

			$playersInfo = $this->getRepository('Player')->findBy(array('id' => $ids));

			$tournament = $this->game->getTournament();

			$finalGame = $this->getRepository('Game')->findOneBy(array(
				'tournament' => $this->game->getTournament(),
				'sex' => $this->game->getSex(),
				'class' => '決勝',
				));

			foreach ($playersInfo as $player) {

				// index of player id post player list
				$key  = array_search($player->getId(), $ids);
				
				$flag 			= $flags[$key];
				$commonRank   	= $commonRanks[$key];
				$singleRank   	= $singleRanks[$key];
				$groupRank   	= $groupRanks[$key];

                $singleTotalScore = $player->getSingleTotalScore();

                if(!$singleTotalScore)  
            	{
            		$singleTotalScore = new  \Entities\SingleTotalScore;
            	}

                $singleTotalScore->setPassRank($commonRank);
                $singleTotalScore->setSingleRank($singleRank);
                $singleTotalScore->setGroupRank($groupRank);

                $itemTotalScore = $player->getItemTotalScore();

                if(!$itemTotalScore)  $itemTotalScore = new \Entities\ItemTotalScore;

                foreach ($items as $item) {
                	$function  = 'setItem'.$item->getNo().'Rank';
					$itemTotalScore->$function($itemRanks[$item->getNo()][$key]);
				}
                $this->em->persist($itemTotalScore);
				$this->em->persist($singleTotalScore);
			}

            $this->em->flush();

            $responds = array('status' => 'success', 'message' => '更新しました。');

		} catch (Exception $e) {

			$responds = array('status' => 'failed', 'message' => $e->getMessage());
		}

		echo json_encode($responds);
	}

	private function checkPlayer($settings = [], $player = [])
	{
		foreach ($settings as $team => $heats) {
			foreach ($heats as $heat => $settingPlayers) {
				foreach ($settingPlayers as $p) {

					$classPassType = explode('_', $p['class']);

					if($p['type'] == $player['pass_play_type'] && $classPassType[0] == $this->game->getClass())
					{
						if($player['pass_play_type'] == '種目別')
						{
							$item = $this->game->findItem($classPassType[1]);

							if($item)
							{
								if($player['item'.$item->getNo().'_rank'].'位' == $p['rank']) {
									return [
										'team' => $team+1,
										'heat' => $heat,
 									];
								};
							}
						}
						else if($player['pass_play_type'] == '団体')
						{
							if($player['group_rank'].'位' == $p['rank']) {
								return [
										'team' => $team+1,
										'heat' => $heat,
 									];
							}
						}
						else
						{
							if($player['single_rank'].'位' == $p['rank']){
								return [
										'team' => $team+1,
										'heat' => $heat,
 									];
							};
						}
					}
				}
			}
		}

		return false;
	}
}

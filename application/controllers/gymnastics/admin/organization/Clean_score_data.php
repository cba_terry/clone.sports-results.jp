<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clean_score_data extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index ()
	{
		// find game
		$gameId = $this->input->post('gid');

		$game = $this->getRepository('game')->find($gameId);

		if(!$game) 
		{
			$response = ['status' => 'failed', 'message' => 'Game not found!'];

			echo json_encode($response);
			exit();
		}

		// get all player
		// in order to find single_item_core and sing_total_score to delete.

		$players = $game->getPlayers();

		foreach ($players as $key => $player) {

			$singleTotalScore = $player->getSingleTotalScore();

			$player->removeSingleTotalScore();

			if($singleTotalScore) $this->em->remove($singleTotalScore);

			$itemTotalScore = $player->getItemTotalScore();

			$player->removeItemTotalScore();

			if($itemTotalScore) $this->em->remove($itemTotalScore);

			$singleItemScores = $player->getSingleItemScores();

			foreach ($singleItemScores as $score) {
				$singleItemScores->removeElement($score);
				$this->em->remove($score);
			}

			$scores = $player->getScores();

			foreach ($scores as $score) {
				$scores->removeElement($score);
				$this->em->remove($score);
			}

			$this->em->persist($player);
		}

		// get all schoolgroup 
		// in order to find group_score to delete
		$schoolGroups = $game->getSchoolGroups();

		foreach ($schoolGroups as $school) {

			$groupScore = $school->getGroupScore();

			$school->removeGroupScore();

			if($groupScore) $this->em->remove($groupScore);

			$this->em->persist($school);
		}

		$this->em->flush();

		$response = ['status' => 'success', 'message' => 'You\'ve cleaned score data.'];

		echo json_encode($response);
		exit();
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Player extends Organization_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$gameId = $this->input->get('gid');

		$game = $this->getRepository('game')->find((int)$gameId);

		if(!$game) throw new Exception('Game not found!');

		$conditions = [
			'game' => $game,
			'player_id' => $this->input->get('player_id'),
			'player_no' => $this->input->get('player_no'),
			'player_name' => $this->input->get('player_name'),
			'school_name' => $this->input->get('school_name'),
			'grade' => $this->input->get('grade'),
		];

		$offset = $this->input->get('per_page');

		$players = $this->getRepository('Player')->getPagedPlayer($conditions, $offset, 100);
		$config = admin_pagination_config();
		$config['per_page'] = 100;
		$config['total_rows'] = count($players);

		$pagination = $this->pagination->initialize($config);

		$this->load->view('gymnastics/admin/organization/player_list', array(
			'players' => $players,
			'conditions' => $conditions,
			'pagination' => $pagination,
			'game' => $game
			));
	}

	public function edit($id)
	{	
		// Get player detail
		$player         = $this->getRepository('Player')->find($id);
		if(!$player) throw new Exception('Player not found!');
		//all heats and groups
		$groups = $this->getRepository('Player')->getResultList([
			'game'  => $player->getGame(),
			'group_by' => 'group'
		]);
		$heats  = $this->getRepository('Player')->getResultList([
			'game'  => $player->getGame(),
			'group_by' => 'heat'
		]);
		$schools = $player->getGame()->getSchoolGroups();

		if ($this->input->post())
		{
			$player->setPlayerName($this->input->post('name'));
			$player->setPlayerNo($this->input->post('player_no'));
			$player->setGrade($this->input->post('grade'));
			$player->setFlag($this->input->post('flag'));
			$player->setGroup($this->input->post('group'));
			$player->setHeat($this->input->post('heat'));
			$player->setFlagCancle($this->input->post('flag_cancle'));
			//$player->setOrder($this->input->post('order'));

			$int_school_id = $this->input->post('school_id');
			if (isset($int_school_id)) {
				$obj_school = $this->getRepository('SchoolGroup')->find($int_school_id);
				$player->setSchool($obj_school);
			}

			$this->em->persist($player);
			$this->em->flush();

			$uri_redirect  = '/gymnastics/admin/organization/player?gid='. $player->getGame()->getId();

			redirect($uri_redirect);
		}
		else
		{
			$this->load->view('gymnastics/admin/organization/player_edit', [
				'player'  => $player,
				'schools' => $schools,
				'groups'  => $groups,
				'heats'   => $heats,
				'gameId' => $player->getGame()->getId(),
			]);
		}
	}

	public function delete ()
	{
		$id = $this->input->post('player_id');
		$player = $this->getRepository('Player')->find($id);

		if($player){
			$this->em->remove($player);
			$this->em->flush();

			redirect('/admin/organization/player?delete=success');
		}

		echo json_encode(array('id' => $id));
		exit;
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends Organization_Controller {
	public $str_query;
	public $url;

	public $obj_tournament = null;
	public $game       = null;

	public function __construct()
	{
		parent::__construct();

		$this->game = $this->getRepository('game')->find($this->input->get('gid'));
	}

	public function index()
	{
		$schoolsCheck = array();

		$offset            = $this->input->get('per_page');

		$params = array(
			'game' 			=> $this->game,
			'school_no'		=> $this->input->get('school_no'),
			'school_name'   => $this->input->get('school_name'),
		);

		$schools  = $this->getRepository('SchoolGroup')->getPagedSchools($params, $offset, 10);

		if ( ! empty($this->game) && $schools->count() > 0) {
			$where = array(
				'game'       => $this->game,
				'school_id'     => null
			);

			foreach ($schools as $key => $school) {
				
				$school_no = $school->getSchoolNo();
				$where['school_no'] = $school_no;

				// Check condition flag and active
				$check_flag       = $this->getRepository('Player')->getFlagOrActiveForSchool($where);

				if ( ! empty($check_flag)) {
					$schoolsCheck['flag'][$school->getId()] = true;
				}
			}
		}

		// Set config for pagination
		$config = admin_pagination_config();
		$config['total_rows'] = $schools->count();

		$this->load->view('gymnastics/admin/organization/group_list', array(
			'schools' => $schools,
			'schoolsCheck' => $schoolsCheck,
			'game' => $this->game,
			'pagination' => $this->pagination->initialize($config),
			));
	}

	public function edit($id)
	{
		try{

			$school = $this->getRepository('SchoolGroup')->find($id);
			if(!$school) throw new Exception('School Group not found!');

			$where['school_no'] = $school->getSchoolNo();
			$where['game']      = $this->game;
			// Check condition flag and active
			$checkFlag          = $this->getRepository('Player')->getFlagOrActiveForSchool($where);

			if ($this->form_validation->run('school_group') !== FALSE) {
				$school->setSchoolName($this->input->post('school_name'));
				$school->setSchoolNameAb($this->input->post('school_name_ab'));
				$school->setSchoolPrefecture($this->input->post('school_prefecture'));
				$school->setCancelFlag($this->input->post('flag_cancel'));

				$this->em->persist($school);
				$this->em->flush();

				redirect('/admin/organization/group?gid=' . $this->game->getId());
			} else {
				$this->load->view('gymnastics/admin/organization/group_edit', array(
					'game'      => $this->game,
					'school'    => $school,
					'checkFlag' => $checkFlag
				));
			}

		} catch(Exception $e) {
			redirect('/admin/organization/group?gid='.$this->game->getId());
		}
	}
}

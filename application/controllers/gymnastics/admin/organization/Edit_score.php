<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_score extends MY_Controller {

	public function index()
	{
		$data['title'] = '全審判得点確認';
		$this->load->view('gymnastics/admin/organization/edit_score', $data);
	}
}

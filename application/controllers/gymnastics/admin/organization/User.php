<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Organization_Controller {

	public function index($page=null)
	{
		$user  = $this->session->userdata('user');
		$user  = $this->getRepository('User')->find($user->getId());

		
		$data = array();

		$conditions = array();

		if ($user->getAssociation() !== null) {
			$conditions['association_id'] = $user->getAssociation()->getId();
		}

		if($this->input->get('keyword')){
			$conditions['keyword'] = $this->input->get('keyword');
		}

		$data['users'] = $this->getRepository('User')->getPagedUsers($conditions, $page, 5);

		// Set config pagination
		$config               = admin_pagination_config();
		$config['base_url']   = '/gymnastics/admin/organization/user/pager';
		$config['total_rows'] = $data['users']->count();
		$config['per_page']   = 5;

		$data['pagination'] = $this->pagination->initialize($config);

		$this->load->view('gymnastics/admin/organization/user_list', $data);
	}

	public function edit($id)
	{
		$data = array();
		$data['user'] = $this->getRepository('User')->find($id);

		if ($this->form_validation->run('user') == FALSE) {
			$this->load->view('gymnastics/admin/organization/user_edit', $data);
		} else {
			$user = $data['user'];

			$user->setUserCode($this->input->post('user_code'));
			$user->setUserName($this->input->post('user_name'));
			$user->setPassword($this->input->post('password'));

			$this->em->persist($user);
			$this->em->flush();

			redirect('/admin/organization/user', 'refresh');
		}
	}

	public function add()
	{
		$data = array();

		$association = $this->getRepository('Association')->find(1);

		if($this->form_validation->run('user') == FALSE) {
			$this->load->view('gymnastics/admin/organization/user_add', $data);
		}else {
			$user = new Entities\User;

			$user->setAssociation($association);
			$user->setUserCode($this->input->post('user_code'));
			$user->setUserName($this->input->post('user_name'));
			$user->setPassword($this->input->post('password'));
			$user->setHash(do_hash($this->input->post('password')));
			$user->setCreated(new DateTime('now'));
			$user->setUpdated(new DateTime('now'));

			$this->em->persist($user);
			$this->em->flush();

			redirect('/admin/organization/user', 'refresh');
		}
	}

	public function delete ()
	{
		$id = $this->input->post('user_id');
		$user = $this->getRepository('User')->find($id);

		if($user){
			$this->em->remove($user);
			$this->em->flush();
		}

		echo json_encode(array('id' => $id));
		exit;
	}
}

<?php
use Entities\Collection\GroupPlayCollection;
use Entities\Collection\RotationCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends Referee_Controller {
	
	protected $accessAllow = array('MAIN','CHIEF');

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * index
	 *
	 * navigation screen
	 * 
	 * @param
	 * @return
	 */
	
	public function index()
	{
		$gameId = $this->rfeGame->getId();

		$arr_url = [
			'result'   => '/gymnastics/monitor/result/game/' . $gameId . '/heat/',
			'ranking'  => '/gymnastics/monitor/ranking/single/' . $gameId,
			'rotation' => '/gymnastics/monitor/rotation/' . $gameId,
			'score'    => '/gymnastics/monitor/scores?gid=' . $gameId
		];

		// Get list item
		$index_item    = ($this->rfeGame->getSex()) ? 'male' : 'female';
		$index_item    = 'item_' . $index_item;
		$items_default = $this->config->item('property')[ $index_item ];
		$items_default = array_values($items_default);

		$group 		= $this->rfeGame->findGroupCurrentPlay();
		$rotate 	= $this->rfeGame->findRotateCurrentPlay();

		$rotationSettings 	= $this->rfeGame->getRotationSettings();
		$groupRotations		= $this->rfeGame->findFirstRotationGroup($group);

		$rotateFirsts 		= new RotationCollection($groupRotations, $rotationSettings);

		$rotationSettings 	= $this->rfeGame->getRotationSettings();

		$currentItemHeats 	= $rotateFirsts->doRotate($rotate-1);

		$this->load->view('gymnastics/admin/referee/game_class', [
			'referee'		=> $this->referee,
			'game'          => $this->rfeGame,
			'urls'			=> $arr_url,
			'items_default' => $items_default,
			'rotations' 	=> $currentItemHeats
		]);
	}


	/**
	 * all
	 *
	 * return all game's result in tournament
	 * 
	 * @param
	 * @return
	 */

	public function all()
	{
		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersResults($this->rfeGame, $group, $heat);

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$groups 		= $this->getRepository('Player')->getGroupList($this->rfeGame);

		$items 			= $this->rfeGame->getItems();

		$scoreStatus 	= $this->config->item('property')['score_status'];

		$this->load->view('gymnastics/admin/referee/game_all', array(
			'game'              => $this->rfeGame,
			'referee'			=> $this->referee,
			'items'             => $items,
			'groups'			=> $groups,
			'score_status'      => $scoreStatus,
			'groupHeatPlayers' 	=> $groupHeatPlayers,
		));
	}

	/**
	 * item
	 *
	 * return single item game's result in tournament
	 * 
	 * @param array
	 * @return array
	 */

	public function item()
	{
		$item = $this->input->get('item');

		if(!$item) $item = $this->referee->getItem();

		$group = $heat = 1;
		// Set referer link
		$this->session->set_userdata('redirect', $this->input->server('REQUEST_URI'));

		if($this->input->get('group')) $group = $this->input->get('group');
		if($this->input->get('heat'))  $heat  = $this->input->get('heat');

		// chief referee will take score from single_item_score table
		// main and normal take score from score table
		if($this->referee->isChiefReferee() || $this->rfeGame->isInputPaper())
		{	
			$players = $this->getRepository('Player')->getPlayersResults($this->rfeGame, $group, $heat, $item);
		}
		else
		{
			$players = $this->getRepository('Player')->getPlayersScores($this->rfeGame, $group, $heat, $item);
		}

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$groups = $this->getRepository('Player')->getGroupList($this->rfeGame);

		// find object item
		$item = $this->rfeGame->findItem($item);

		$template = ($this->referee->isChiefReferee() || $this->rfeGame->isInputPaper()) ? '1' : '';

		$template .= ($item->isPlayTwice()) ? '2' : '1';

		$this->load->view('gymnastics/admin/referee/game_item'.$template, [
			'game'  => $this->rfeGame,
			'item' 	=> $item,
			'grus'	=> $groups,
			'nrfd'	=> $item->getNumberRefereeD(),
			'nrfe'	=> $item->getNumberRefereeE(),
			'referee' => $this->referee,
			'groupHeatPlayers' 	=> $groupHeatPlayers
		]);
		
	}

	/**
	 * order_setting
	 *
	 * return single item game's result in tournament
	 * 
	 * @param array
	 * @return array
	 */

	public function order_setting ()
	{
		$get = $this->input->get();

		$item 	= $this->input->get('item');
		$group 	= $this->input->get('group');
		$heat 	= $this->input->get('heat');

		if(!$item) $item = $this->referee->getItem();

		$item = $this->rfeGame->findItem($item);

		$players = $this->getRepository('Player')->getPlayersOrders($this->rfeGame, $group, $heat);

		if($this->input->post('order')) {
			//update order
			$data_order = $this->input->post('order');
			
			if($data_order) {
				foreach ($data_order as $player_id => $order) {

					$filter = array_filter($players, function($p) use ($player_id){
						return $p->getId() == $player_id;
					});

					$player = end($filter);
					
					$player->setOrder(new \Entities\Order($item, $order));
					$this->em->persist($player);
				}

				$this->em->flush();
			}
		}

		$this->load->view('gymnastics/admin/referee/order_setting01', array(
			'players'     => $players,
			'game'        => $this->rfeGame,
			'item'        => $item,
			'referee'	=> $this->referee,
			'request_uri' => $this->session->userdata('redirect')
		));

	}

	/**
	 * updateStatus
	 *
	 * using ajax for update score status at game_list (admin), 
	 * all_game_list (referee) screen
	 *
	 * @author Thom
	 * @return json
	 */
	public function updateStatus()
	{
		try{
			// array of rows score id, 
			// which was selected from the list screen.
			$ids 	= $this->input->post('ids'); 
			$status = $this->input->post('status');

			foreach ($ids as $id) {

				if(!$this->rfeItem) $item = $this->input->get('item');
				else $item = $this->rfeItem;

				$item = $this->rfeGame->findItem($item);

				if($this->rfeGame->isInputNormal())
				{
					$player = $this->getRepository('Player')->getPlayerScores($id);

					$scores = $player->getScores()->filter(function($s) use ($item){
						return $s->getItem() == $item->getName();
					});

					foreach ($scores as $score) {

						$singleItemScore = $player->getSingleItemScores()->filter(function($s) use ($score, $item) {
							return $s->getRound() == $score->getRound() && $s->getItem() == $item->getName();
						})->first();

						if(!$singleItemScore) $singleItemScore = new \Entities\SingleItemScore;

						//persist score status
						$score->setStatus($status);
						$score->setUnPick();

						$this->em->persist($score);

						// persist singleItemscore
						$singleItemScore->setPlayer($player);
						$singleItemScore->setItem($score->getItem());
						$singleItemScore->setRound($score->getRound());

						$numberReferreD = $item->getNumberRefereeD();
						$numberReferreE = $item->getNumberRefereeE();

						$fnSetScore = 'set'.$score->getType().'Score';

						if(method_exists($singleItemScore, $fnSetScore)) 
						{
							$singleItemScore->$fnSetScore($score->getValue());
						}

						$fnSetScoreStatus = $fnSetScore.'Status';

						if(method_exists($singleItemScore, $fnSetScoreStatus))
						{
							$singleItemScore->$fnSetScoreStatus($status);
						}

						// Notice: please keep order of set Score
						// If you change order, the Score's result can be wrong
						$singleItemScore->setDScore(null, $numberReferreD);
						$singleItemScore->setEScore(null, $numberReferreE);
						$singleItemScore->setDemeritScore();
						$singleItemScore->setFinalScore();
						$singleItemScore->setStatus($status);

						$player->registerSingleItemScore($singleItemScore, $item);
						
						$this->em->persist($player);
					}
				}
				else
				{
					$player = $this->getRepository('Player')->getPlayerResults($id, $this->rfeItem);

					$singleItemScores = $player->getSingleItemScores();

					$singleItemScores = $singleItemScores->filter(function($s) use ($item) {
						return $s->getItem() == $item->getName();
					});

					foreach ($singleItemScores as $singleItemScore) {

						$singleItemScore->setStatus($status);

						$numberReferreD = $item->getNumberRefereeD();
						$numberReferreE = $item->getNumberRefereeE();

						// Score calculation
						for ($i=1; $i <= $numberReferreD; $i++) { 
							// set status
							$function = 'setD'.$i.'ScoreStatus';
							$singleItemScore->$function($status);
						}

						for ($i=1; $i <= $numberReferreE; $i++) { 
							// set status
							$function = 'setE'.$i.'ScoreStatus';
							$singleItemScore->$function($status);
						}

						$singleItemScore->setTime1ScoreStatus($status);
						$singleItemScore->setTime2ScoreStatus($status);
						$singleItemScore->setLine1ScoreStatus($status);
						$singleItemScore->setLine2ScoreStatus($status);

						$player->registerSingleItemScore($singleItemScore, $item);

						$this->em->persist($player);
					}
				}

				$this->em->flush();

				// push task
				$singleItemScores = $player->getSingleItemScores()->filter(function($s) use ($item) {
					return $s->getItem() == $item->getName();
				});
				
				foreach ($singleItemScores as $singleItemScore) 
				{
					$singleItemScoreId = $singleItemScore->getId();

					$task = $this->getRepository('Task')->findOneBy([
										'single_item_score_id' => $singleItemScoreId
						]);

					if(!$task) $task = new \Entities\Task;

					$task->setSingleItemScoreId($singleItemScoreId);

					$this->em->persist($task);
				}
			}

			$this->em->flush();

			$response = ['status' => 'success'];

		} catch (Exception $e){
			$response = ['status' => 'failed', 'message' => $e->getMessage()];
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * update_status_total
	 * using ajax for update score status at single_total_score, 
	 */
	public function update_status_total()
	{
		try{
			// array of rows score id, 
			// which was selected from the list screen.
			$ids 	= $this->input->post('ids'); 
			$status = $this->input->post('status');

			$players = $this->getRepository('Player')->findBy(['id' => $ids]);

			foreach ($players as $player) {

				$singleTotalScore = $player->getSingleTotalScore();

				if($singleTotalScore) $singleTotalScore->setStatus($status);

				$this->em->persist($singleTotalScore);
			}

			$this->em->flush();

			if ( ! $status) throw new Exception('Update status single total score fail');

			$response = ['status' => 'success'];

		} catch (Exception $e){
			$response = ['status' => 'failed', 'message' => $e->getMessage()];
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * reInput
	 *
	 * The main referee force normal referee re-input score again
	 * 
	 * @author Terry
	 * @param array
	 * @param array
	 */

	public function reInput ()
	{
		$scoreId = $this->input->post('id');
		$scoreType = $this->input->post('type');

		try {

			if($this->rfeGame->isInputNormal())
			{
				$score = $this->getRepository('Score')->find($scoreId);

				if(!$score) throw new Exception('Score did not found');

				$score->setStatus(0);
				
				$this->em->persist($score);

				$this->em->flush();
			}
			else
			{

				$singleItemScore = $this->getRepository('SingleItemScore')->find($scoreId);

				if(!$singleItemScore) throw new Exception('Did you miss something?');

				$function = 'set' . $scoreType . 'ScoreStatus';
				
				// set status back to 0
				$singleItemScore->$function(0);

				// trong tai thuong nhap bang giay
				if($this->rfeGame->getInputType() == 1)
				{
					// set status
					$singleItemScore->setStatus(0);
				}

				$this->em->persist($singleItemScore);

				$this->em->flush();

			}

			echo json_encode(array(
				'status' => 'success',
				'message' => 'status updated for score id:' . $scoreId,
				));

		} catch (Exception $e) {
			echo json_encode(array(
				'status' => 'failed',
				'message' => $e->getMessage(),
				));
		}
		
	}

	public function reinput_timeline()
	{
		$playerId = $this->input->post('id');
		$round = (int) $this->input->post('round');

		$item = $this->rfeGame->findItem($this->rfeItem);

		try {

			if($this->rfeGame->isInputNormal())
			{
				$player = $this->getRepository('Player')->getPlayerScores($playerId, $item);

				if(!$player) throw new Exception('Player not found');
				
				$time1 = $player->getScore($item, 'Time1', $round);

				if($time1)
				{
					$time1->setStatus(0);
					$this->em->persist($time1);
				}

				$time2 = $player->getScore($item, 'Time2', $round);

				if($time2)
				{
					$time2->setStatus(0);
					$this->em->persist($time2);
				}

				$line1 = $player->getScore($item, 'Line1', $round);

				if($line1)
				{
					$line1->setStatus(0);
					$this->em->persist($line1);
				}

				$line2 = $player->getScore($item, 'Line2', $round);

				if($line2)
				{
					$line2->setStatus(0);
					$this->em->persist($line2);
				}

				$this->em->flush();
			}
			else
			{
				$player = $this->getRepository('Player')->getPlayerResults($playerId, $item);

				if(!$player) throw new Exception('Player not found');

				$singleItemScore = $player->getItemScore($item, $round);

				if($singleItemScore)
				{
					foreach (\Entities\Referee::$refereeTypeMap as $type) {

						$function = 'set' . $type . 'ScoreStatus';
						// set status back to 0
						$singleItemScore->$function(0);
						// set status
						$singleItemScore->setStatus(0);
						
					}

					$this->em->persist($singleItemScore);

					$this->em->flush();
				} 
			}

			echo json_encode(array(
				'status' => 'success',
				'message' => 'status updated',
				));

		} catch (Exception $e) {
			echo json_encode(array(
				'status' => 'failed',
				'message' => $e->getMessage(),
				));
		}
	}

	public function filter()
	{
		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		$items 			= $this->rfeGame->getItems();

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersResults($this->rfeGame, $group, $heat);

		$playerName = $this->input->get('player_name');
		$schoolName = $this->input->get('school_name');
		$prefecture = $this->input->get('prefecture');

		if(strlen($playerName))
		{
			$players = array_filter($players, function($player) use ($playerName){
				return $player->getPlayerName() == $playerName;
			});
		}

		if(strlen($prefecture))
		{
			$players = array_filter($players, function($player) use ($prefecture){
				return $player->getPrefecture() == $prefecture;
			});
		}

		if(strlen($schoolName))
		{
			if(strlen($schoolName))
			{
				$players = array_filter($players, function($player) use ($schoolName){
					return $player->getSchool()->getSchoolNameAb() == $schoolName;
				});
			}

			$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		}

		if(strlen($playerName) || strlen($prefecture))
		{
			$this->load->view('gymnastics/admin/referee/filter', array(
				'players'     => $players,
				'game'        => $this->rfeGame,
				'items'        => $items,
			));
		}else
		{
			$this->load->view('gymnastics/admin/referee/filter1', array(
				'game'              => $this->rfeGame,
				'items'             => $items,
				'groupHeatPlayers' 	=> $groupHeatPlayers,
			));
		}
	}

	public function filter_options()
	{
		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		if($heat <= 0) $heat = null;

		$items 			= $this->rfeGame->getItems();

		// danh sach player voi score va ranking 
		// hien thi o cac man hinh game list
		$players = $this->getRepository('Player')->getPlayersByGroupHeat($this->rfeGame, $group, $heat);

		$getType = $this->input->get('type');
		$getValue = $this->input->get('value');

		$schoolList = [];
		$playerList = [];
		
		if($getType == 'pref' && strlen($getValue))
		{
			foreach ($players as $player) {
				if($player->getSchool()->getSchoolPrefecture() == $getValue && 
					FALSE === array_search($player->getSchool()->getSchoolNameAb(), $schoolList))
				{
					$schoolList[] = $player->getSchool()->getSchoolNameAb();
				}
			}

			foreach ($players as $player) {
				if(FALSE !== array_search($player->getSchool()->getSchoolNameAb(), $schoolList)
					&& FALSE === array_search($player->getPlayerName(), $playerList))
				{
					$playerList[] = $player->getPlayerName();
				}
			}
		}

		else if($getType == 'pref' && !strlen($getValue))
		{
			foreach ($players as $player) {
				if(FALSE === array_search($player->getSchool()->getSchoolNameAb(), $schoolList))
				{
					$schoolList[] = $player->getSchool()->getSchoolNameAb();
				}
			}
		}

		if($getType == 'school' && strlen($getValue))
		{
			foreach ($players as $player) {
				if($player->getSchool()->getSchoolNameAb() == $getValue)
				{
					$playerList[] = $player->getPlayerName();
				}
			}
		}

		else if($getType == 'school' && !strlen($getValue))
		{
			foreach ($players as $player) {
				$playerList[] = $player->getPlayerName();
			}
		}

		$response = [
			'schools' => $schoolList,
			'players' => $playerList
		];

		echo json_encode($response);
		exit();
	}
}

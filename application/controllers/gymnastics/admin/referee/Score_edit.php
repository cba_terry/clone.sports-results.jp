<?php
use Doctrine\Common\Collections\ArrayCollection;
use Entities\SingleItemScore;
use Entities\SingleTotalScore;

defined('BASEPATH') OR exit('No direct script access allowed');

class Score_edit extends Referee_Controller {

	public $player;
	public $item;

	protected $accessAllow = array('MAIN','CHIEF','NORMAL');

	public function __construct()
	{
		parent::__construct();

		// specified default item
		$this->item 	= urldecode($this->input->get('item'));

		// referee, who is normal referee 
		// is can not see these item by param, they only the see item was set for them
		if($this->referee->getRefereeType() != 2 && $this->item !== $this->referee->getItem())
			throw new Exception('You do not have permission to get there.');


		$playerId 		= $this->input->get('player_id');

		if($this->rfeGame->isInputNormal())
		{
			$this->player 	= $this->getRepository('Player')->getPlayerScores($playerId);
		}
		else
		{
			$this->player = $this->getRepository('Player')->getPlayerResults($playerId);
		}
		
		if(!$this->player) $this->player 	= $this->getRepository('Player')->find($playerId);

		// assert player
		if ( !$this->player ) throw new Exception('Player was not specified.');

		
		if( !$this->item ) $this->item = $this->rfeGame->getDefaultItemName();

	}

	public function index()
	{
		$postData 	= $this->input->post();

		$item = $this->rfeGame->findItem($this->item);

		$data = array(
			'game' 		=> $this->rfeGame,
			'player' 	=> $this->player,
			'referee' 	=> $this->referee,
			'item' 		=> $item,
			);

		$template = ($item->isPlayTwice()) ? 'score_edit02' : 'score_edit01';
		$fnComplete = ($item->isPlayTwice()) ? 'complete02' : 'complete01';

		if ($postData && isset($postData['submit'], $postData['submit']['confirm'])){
			$data['post'] = $postData;
			$this->load->view('gymnastics/admin/referee/'.$template.'_confirm', $data);
		}elseif($postData && isset($postData['submit'],$postData['submit']['complete'])){
			$this->$fnComplete($postData, $item);
		}else{
			$this->load->view('gymnastics/admin/referee/'.$template, $data);
		}
	}
	
	private function complete02($data, $item){

		$scores[0] = $this->player->getItemScore($item, SingleItemScore::ROUND1);
		$scores[1] = $this->player->getItemScore($item, SingleItemScore::ROUND2);

		if(!$scores[0]) $scores[0]  =  new SingleItemScore;
		if(!$scores[1]) $scores[1]  =  new SingleItemScore;

		for ($i=0; $i < 2; $i++) {

			$round = $i+1;

			$scores[$i]->setStatus(SingleItemScore::MODIFY);
			$scores[$i]->setRound($round);

			$numberReferreD = $item->getNumberRefereeD();
			$numberReferreE = $item->getNumberRefereeE();

			// Score calculation
			for ($j=1; $j <= $numberReferreD; $j++) { 
				
				if(isset($data['d'.$j.'_score'])) call_user_func_array([$scores[$i], 'setD'.$j.'Score'], [$data['d'.$j.'_score'][$i]]);

				if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
					($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
				{
					call_user_func_array([$scores[$i], 'setD'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
				}
			}

			for ($j=1; $j <= $numberReferreE; $j++) { 

				if(isset($data['e'.$j.'_score'])) {
					call_user_func_array([$scores[$i], 'setE'.$j.'Score'], [$data['e'.$j.'_score'][$i]]);
				}

				if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
					($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
				{
					call_user_func_array([$scores[$i], 'setE'.$j.'ScoreStatus'], [SingleItemScore::PUBLISH]);
				}
			}

			if(isset($data['time1_score1'])) $scores[$i]->setTime1Score($data['time1_score1'][$i].':'.$data['time1_score2'][$i]);
			if(isset($data['time2_score1'])) $scores[$i]->setTime2Score($data['time2_score1'][$i].':'.$data['time2_score2'][$i]);
			if(isset($data['line1_score'])) $scores[$i]->setLine1Score($data['line1_score'][$i]);
			if(isset($data['line2_score'])) $scores[$i]->setLine2Score($data['line2_score'][$i]);

			$scores[$i]->setTime1ScoreStatus(SingleItemScore::MODIFY);
			$scores[$i]->setTime2ScoreStatus(SingleItemScore::MODIFY);
			$scores[$i]->setLine1ScoreStatus(SingleItemScore::MODIFY);
			$scores[$i]->setLine2ScoreStatus(SingleItemScore::MODIFY);


			if(isset($data['time_demerit_score'])) 	$scores[$i]->setTimeDemeritScore($data['time_demerit_score'][$i]);
			if(isset($data['line_demerit_score'])) 	$scores[$i]->setLineDemeritScore($data['line_demerit_score'][$i]);
			if(isset($data['other_demerit'])) 		$scores[$i]->setOtherDemerit($data['other_demerit'][$i]);
			if(isset($data['e_demerit_score'])) 	$scores[$i]->setEDemeritScore($data['e_demerit_score'][$i]);

			
			// Notice: please keep order of set Score
			// If you change order, the Score's result can be wrong
			$scores[$i]->setDScore(null, $numberReferreD);
			$scores[$i]->setEScore(null, $numberReferreE);
			$scores[$i]->setDemeritScore();
			$scores[$i]->setFinalScore();

			$this->player->registerSingleItemScore($scores[$i], $item);
		}

		if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
			($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
		{
			$scores[0]->setStatus(SingleItemScore::PUBLISH);
			$scores[1]->setStatus(SingleItemScore::PUBLISH);

			$scores[0]->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[0]->setTime2ScoreStatus(SingleItemScore::PUBLISH);

			$scores[1]->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$scores[1]->setTime2ScoreStatus(SingleItemScore::PUBLISH);
		}

		$this->em->persist($this->player);

		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		
		if($this->rfeGame->isInputNormal()) {

			for ($i=0; $i < 2; $i++) {

				$round = $i+1;

				$numberReferreD = $item->getNumberRefereeD();
				$numberReferreE = $item->getNumberRefereeE();

				// D's Score registration
				for ($j=1; $j <= $numberReferreD; $j++) {

					$this->registerScore([
						'item' 	=> $item, 
						'value' => $data['d'.$j.'_score'][$i],
						'type' 	=> 'D'.$j,
						'round' => $round
					]);
				}

				// E's Score registration
				for ($j=1; $j <= $numberReferreE; $j++) { 

					$this->registerScore([
						'item' 	=> $item, 
						'value' => $data['e'.$j.'_score'][$i],
						'type' 	=> 'E'.$j,
						'round' => $round
					]);
				}

				// Time1 Score registration
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['time1_score1'][$i].':'.$data['time1_score2'][$i],
					'type' 	=> 'Time1',
					'round' => $round
				]);
				
				// Time2 Score registration				
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['time2_score1'][$i].':'.$data['time2_score2'][$i],
					'type' 	=> 'Time2',
					'round' => $round
				]);

				// Line1 Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['line1_score'][$i],
					'type' 	=> 'Line1',
					'round' => $round
				]);

				// Line2 Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['line2_score'][$i],
					'type' 	=> 'Line2',
					'round' => $round
				]);

				// Time demerit Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['time_demerit_score'][$i],
					'type' 	=> 'TimeDemerit',
					'round' => $round
				]);

				// Line demerit Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['line_demerit_score'][$i],
					'type' 	=> 'LineDemerit',
					'round' => $round
				]);

				// Other demerit Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['other_demerit'][$i],
					'type' 	=> 'OtherDemerit',
					'round' => $round
				]);

				// E demerit Score
				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['e_demerit_score'][$i],
					'type' 	=> 'EDemerit',
					'round' => $round
				]);
			}

			$this->em->persist($this->player);
		}

		$this->em->flush();


		foreach ($scores as $score) {
			
			$singleItemScoreId = $score->getId();

			$task = $this->getRepository('Task')->findOneBy([
								'single_item_score_id' => $singleItemScoreId
				]);

			if(!$task) $task = new \Entities\Task;

			$task->setSingleItemScoreId($singleItemScoreId);

			$this->em->persist($task);
		}

		$this->em->flush();

		if($this->referee->getRefereeType() == 0)
			redirect('/admin/referee/all_list?group='.$this->player->getGroup().'&heat='.$this->player->getHeat());

		redirect('/admin/referee/game/item?gid='.$this->rfeGame->getId().'&item='.$item->getName().'&group='.$this->player->getGroup().'&heat='.$this->player->getHeat());
	}


	private function complete01($data, $item){

		$singleItemscore = $this->player->getItemScore($item, SingleItemScore::ROUND1);

		if(!$singleItemscore) $singleItemscore = new SingleItemScore;

		$singleItemscore->setStatus(SingleItemScore::MODIFY);
		$singleItemscore->setRound(SingleItemScore::ROUND1);

		$numberReferreD = $item->getNumberRefereeD();
		$numberReferreE = $item->getNumberRefereeE();

		// Score D calculation
		for ($i=1; $i <= $numberReferreD; $i++) { 

			if(isset($data['d'.$i.'_score'])) {
				call_user_func_array([$singleItemscore, 'setD'.$i.'Score'], [$data['d'.$i.'_score']]);
			}

			if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
				($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
			{
				call_user_func_array([$singleItemscore, 'setD'.$i.'ScoreStatus'], [SingleItemScore::PUBLISH]);
			}
		}

		// Score E calculation
		for ($i=1; $i <= $numberReferreE; $i++) { 

			if(isset($data['e'.$i.'_score'])) {
				call_user_func_array([$singleItemscore, 'setE'.$i.'Score'], [$data['e'.$i.'_score']]);
			}

			if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
				($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
			{
				call_user_func_array([$singleItemscore, 'setE'.$i.'ScoreStatus'], [SingleItemScore::PUBLISH]);
			}
		}

		if(isset($data['time1_score1'])) $singleItemscore->setTime1Score($data['time1_score1'].':'.$data['time1_score2']);
		if(isset($data['time2_score1'])) $singleItemscore->setTime2Score($data['time2_score1'].':'.$data['time2_score2']);
		if(isset($data['line1_score'])) $singleItemscore->setLine1Score($data['line1_score']);
		if(isset($data['line2_score'])) $singleItemscore->setLine2Score($data['line2_score']);

		$singleItemscore->setTime1ScoreStatus(SingleItemScore::MODIFY);
		$singleItemscore->setTime2ScoreStatus(SingleItemScore::MODIFY);
		$singleItemscore->setLine1ScoreStatus(SingleItemScore::MODIFY);
		$singleItemscore->setLine2ScoreStatus(SingleItemScore::MODIFY);

		if(isset($data['time_demerit_score'])) 	$singleItemscore->setTimeDemeritScore($data['time_demerit_score']);
		if(isset($data['line_demerit_score'])) 	$singleItemscore->setLineDemeritScore($data['line_demerit_score']);

		if(isset($data['other_demerit'])) 		$singleItemscore->setOtherDemerit($data['other_demerit']);
		if(isset($data['e_demerit_score'])) 	$singleItemscore->setEDemeritScore($data['e_demerit_score']);
	
		// Notice: please keep order of set Score
		// If you change order, the Score's result can be wrong
		$singleItemscore->setDScore(null, $numberReferreD);
		$singleItemscore->setEScore(null, $numberReferreE);
		$singleItemscore->setDemeritScore();
		$singleItemscore->setFinalScore();
		$singleItemscore->setBestScoreFlag(true);

		if((!$this->referee->isNormalReferee() && $data['submit']['complete'] == '公開する') ||
			($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee()))
		{
			$singleItemscore->setStatus(SingleItemScore::PUBLISH);
			$singleItemscore->setLine1ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemscore->setLine2ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemscore->setTime1ScoreStatus(SingleItemScore::PUBLISH);
			$singleItemscore->setTime2ScoreStatus(SingleItemScore::PUBLISH);
		}

		$this->player->registerSingleItemScore($singleItemscore, $item);

		$this->em->persist($this->player);
		
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		//////////////////////////////////////////////
		
		if($this->rfeGame->isInputNormal()) {

			$round = 1;

			$numberReferreD = $item->getNumberRefereeD();
			$numberReferreE = $item->getNumberRefereeE();

			// D's Score registration
			for ($j=1; $j <= $numberReferreD; $j++) {

				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['d'.$j.'_score'],
					'type' 	=> 'D'.$j,
					'round' => $round
				]);
			}

			// E's Score registration
			for ($j=1; $j <= $numberReferreE; $j++) { 

				$this->registerScore([
					'item' 	=> $item, 
					'value' => $data['e'.$j.'_score'],
					'type' 	=> 'E'.$j,
					'round' => $round
				]);
			}

			// Time1 Score registration
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['time1_score1'].':'.$data['time1_score2'],
				'type' 	=> 'Time1',
				'round' => $round
			]);
			
			// Time2 Score registration				
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['time2_score1'].':'.$data['time2_score2'],
				'type' 	=> 'Time2',
				'round' => $round
			]);

			// Line1 Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['line1_score'],
				'type' 	=> 'Line1',
				'round' => $round
			]);

			// Line2 Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['line2_score'],
				'type' 	=> 'Line2',
				'round' => $round
			]);

			// Time demerit Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['time_demerit_score'],
				'type' 	=> 'TimeDemerit',
				'round' => $round
			]);

			// Line demerit Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['line_demerit_score'],
				'type' 	=> 'LineDemerit',
				'round' => $round
			]);

			// Other demerit Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['other_demerit'],
				'type' 	=> 'OtherDemerit',
				'round' => $round
			]);

			// E demerit Score
			$this->registerScore([
				'item' 	=> $item, 
				'value' => $data['e_demerit_score'],
				'type' 	=> 'EDemerit',
				'round' => $round
			]);

			$this->em->persist($this->player);

		}

		$this->em->flush();

		$singleItemScoreId = $singleItemscore->getId();

		$task = $this->getRepository('Task')->findOneBy([
							'single_item_score_id' => $singleItemScoreId
			]);

		if(!$task) $task = new \Entities\Task;

		$task->setSingleItemScoreId($singleItemScoreId);

		$this->em->persist($task);

		$this->em->flush();

		if($this->referee->isNormalReferee())
			redirect('/admin/referee/all_list?group='.$this->player->getGroup().'&heat='.$this->player->getHeat());

		redirect('/admin/referee/game/item?gid='.$this->rfeGame->getId().'&item='.$item->getName().'&group='.$this->player->getGroup().'&heat='.$this->player->getHeat());
	}

	/**
	 * [registerScore description]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	private function registerScore($data = [])
	{
		extract($data);

		if(isset($value) && strlen($value))
		{
			$score = $this->player->getScore($item, $type, $round);

			if(!$score) $score = new \Entities\Score;

			$score->setValue($value);
			$score->setType($type);
			$score->setRound($round);
			$score->setItem((string)$item);

			if(!$this->referee->isNormalReferee() && $this->input->post('submit')['complete'] == '公開する')
			{
				$score->setStatus(\Entities\Score::PUBLISH);
			}

			else if($this->rfeGame->isInputPaper() && $this->referee->isNormalReferee())
			{
				$score->setStatus(\Entities\Score::MODIFY);
			}

			else 
			{
				$score->setStatus($this->player->getItemScoreStatus($item));
			}

			$score->setUnPick();

			$this->player->addScore($score);
		}
	}
}

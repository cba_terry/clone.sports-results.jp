<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller {
	public function index(){
		$referee = $this->getRepository('Referee')->find($this->session->userdata('referee')->getId());
		$tournament = $referee->getGame()->getTournament();
		$this->session->unset_userdata('referee');
		$this->load->helper('cookie');
		delete_cookie('referee');
		redirect('/admin/referee/login/'.$tournament->getId(), 'refresh');
	}
}
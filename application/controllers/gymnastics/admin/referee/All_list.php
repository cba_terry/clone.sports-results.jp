<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_list extends Referee_Controller {

	protected $accessAllow = array('NORMAL','CHIEF','MAIN');

	public function __construct()
	{
		parent::__construct();

		if($this->referee->getRefereeType() === \Entities\Referee::CHIEF)
		{
			redirect('/admin/referee/game');
		}
		else if($this->referee->getRefereeType() === \Entities\Referee::MAIN)
		{
			redirect('/admin/referee/game/item');
		}
		else if (($this->referee->getGame() && ! $this->referee->getGame()->getInputType())
			|| preg_match('/'.REFEREE_TIME_LINE.'/', $this->referee->getRefereeName())
		) {
			redirect('/admin/referee/top');
		}
	}
	
	public function index()
	{
		$get = $this->input->get();

		if ( ! isset($get['group'], $get['heat'])) {
			redirect('/admin/referee/all_list?group=1&heat=1');
		}

		$data['score_status'] = $this->config->item('property')['score_status'];

		//calc number group and heat

		$groups = $this->getRepository('Player')->getGroupList($this->rfeGame);

		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		$players = $this->getRepository('Player')->getPlayersResults($this->rfeGame, $group, $heat);

		$sex 	= $this->referee->getGame()->getStrSex();

		$items 	= $this->config->item('property')['item_'.$sex];

		$this->load->view('gymnastics/admin/referee/all_list', array(
			'game' 		=> $this->rfeGame,
			'referee' 	=> $this->referee,
			'rfeItem'	=> $this->rfeItem,
			'players' 	=> $players,
			'items' 	=> $items,
			'groups' 	=> $groups
			));
	}
}

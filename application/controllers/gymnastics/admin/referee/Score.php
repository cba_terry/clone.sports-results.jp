<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Score extends Referee_Controller {

	protected $accessAllow = array('NORMAL');

	public function index()
	{
		$playerId = $this->input->get('player');

		$player = $this->getRepository('Player')->find($playerId);
		
		if(!$player) throw new Exception('player not found.');

		if($this->input->post('score'))
		{
			$round = ($this->input->get('round')) ?  $this->input->get('round') : 1;

			$scoreValue  	= $this->input->post('score');
			$scoreType  	= $this->referee->getScoreTypeMap();
			$scoreItem  	= $this->rfeItem;
			$scoreStatus 	= \Entities\Score::MODIFY;

			$score = $this->getRepository('Score')->findOneBy([
								'player' => $player->getId(),
								'round' => $round,
								'item' => $scoreItem,
								'type' => $scoreType,
								]);

			if(!$score) {
				// create
				$score = new \Entities\Score;
				$score->setValue($scoreValue);
				$score->setItem($scoreItem);
				$score->setType($scoreType);
				$score->setStatus($scoreStatus);
				$score->setRound($round);
				$score->setUnPick();

				$player->addScore($score);
			}
			else
			{
				// update
				$score->setValue($scoreValue);
				$score->setStatus($scoreStatus);
				$score->setUnPick();
			}

			$this->em->persist($player);
			$this->em->flush();

            $params = array(
            	'group' => $this->input->get('group'),
            	'heat'  => $this->input->get('heat'),
            	'round' => $this->input->get('round')
            	);

            $uri = http_build_query($params);

            redirect('/admin/referee/player?'.$uri);
		}

		$this->load->view('gymnastics/admin/referee/score', array(
			'player' => $player,
			'rfeGame' => $this->rfeGame,
			'tourInf' => $this->tourInf,
			'rfeItem' => $this->rfeItem,
			'referee' => $this->referee,
			'scoreType' => $this->referee->getScoreType()
			));
	}
}

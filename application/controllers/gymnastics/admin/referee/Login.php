<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Referee_Controller {
	protected $accessAllow = array('MAIN','CHIEF','NORMAL');

	public function index($tournament_id = 0)
	{
		$tournament = $this->getRepository('Tournament')->find($tournament_id);

		if ($this->session->userdata('referee')){
			redirect('/admin/referee/all_list', 'refesh');
		}

		$post             = $this->input->post();
		$data             = array();
		
		$data['referees'] = $this->getRepository('Referee')->getRefereeActive($tournament);

		if ($post && $this->form_validation->run('admin_login')){
			$remember_me = isset($post['remember-me'])?$post['remember-me']:NULL;
			$login = $this->do_login($post['username'], $post['password'], $remember_me, $tournament);

			if ($login){
				redirect('/admin/referee/all_list', 'refesh');
			}else{
				$data['error'] = 'Username or password is not correct.';
			}
		}
		$this->load->view('gymnastics/admin/referee/login', $data);
	}

	private function do_login($username, $password, $remember_me, $tournament){
		
		$referee = $this->em->getRepository('Entities\Referee')->getRefereeLogin($username,$password,$tournament);

		if ($referee) {
			$this->session->set_userdata('referee', $referee);
			//Remember me functionality
			if ($remember_me) {
				$hash = md5(time());
				$referee->setHash($hash);
				$this->em->persist($referee);
				$this->em->flush();
				$this->load->helper('cookie');
				set_cookie('referee', $username . '_' . $hash, 604800, '', '/', '', false, true);
			}

			//permission screen
			$get_referee_type    = $referee->getRefereeType();
			$get_referee_name    = $referee->getRefereeName();
			$get_game_input_type = true;

			// xac dinh cach cham diem cua trong tai la cham nhap truc tiep hoac cham tren giay
			// thong qua flag input_type trong bang game
			if ($referee->getGame() !== null) {
				$get_game_input_type = ($referee->getGame()->getInputType()) ? true : false;
			}
			switch ($get_referee_type) {
				case 0:
					$url = ($get_game_input_type) ? 'all_list' : 'top';
					// Referee time|line is use layout sp
					if (preg_match('/' . REFEREE_TIME_LINE . '/', $get_referee_name)) {
						$url = 'top';
					}
					$url = '/admin/referee/' . $url;
					break;
				case 1:
					$url = '/admin/referee/game/item';
					break;
				case 2:
					$url = '/admin/referee/game';
					break;
				default:
					$url = '/admin/referee/all_list';
					break;
			}
			redirect($url);
			return true;
		}
		return false;
	}
}

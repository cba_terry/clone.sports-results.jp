<?php
use Doctrine\Common\Collections\ArrayCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Player extends Referee_Controller {

	protected $accessAllow = array('NORMAL');

	public function index()
	{
		$get  = $this->input->get();

		if ( ! isset($get['group'], $get['heat']))
			throw new Exception('Please choose group and heat');

		$group = $this->input->get('group');
		$heat = $this->input->get('heat');

		// player list in group heat
		$players = $this->getRepository('Player')->getPlayersScores($this->rfeGame, $group, $heat);

		$item = $this->rfeGame->findItem($this->rfeItem);

		$template = ($item->isPlayTwice()) ? '02' : '01';

		$this->load->view('gymnastics/admin/referee/player' . $template, array(
			'rfeGame' => $this->rfeGame,
			'tourInf' => $this->tourInf,
			'referee' => $this->referee,
			'players' => $players,
			'item' 	=> $item,
			));
	}
}

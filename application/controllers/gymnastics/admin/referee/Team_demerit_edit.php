<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_demerit_edit extends Referee_Controller  {

	protected $accessAllow = array('MAIN','CHIEF');
	
	public function index()
	{
		$schoolId = $this->input->get('school');
		$gameId = $this->input->get('game');

		// find schoolgroup id by schoolNo and gameId
		$schoolGroup = $this->getRepository('SchoolGroup')->find($schoolId);

		if(!$schoolGroup) throw new Exception('School group not available!');
		
		// find groupscore by schoolgroup
		
		$groupScore = $schoolGroup->getGroupScore();

		if(!$groupScore) {
			$groupScore = new \Entities\GroupScore;
			$schoolGroup->setGroupScore($groupScore);
		}

		// find game's items
		$items = $this->rfeGame->getItems();

		// handle update
		if($this->input->post())
		{
			foreach ($items as $key => $item) {
				
				$itemIndex = ($key+1);// start index at 1

				// make function name
				$fnSetDemeritScore = 'setItem' . $itemIndex . 'DemeritScore';
				$fnSetDemeritReason = 'setItem' . $itemIndex . 'DemeritReason';

				$groupScore->setSchoolGroup($schoolGroup);

				// do function
				$groupScore->$fnSetDemeritScore($this->input->post('item' . $itemIndex . '_demerit_score'));
				$groupScore->$fnSetDemeritReason($this->input->post('item' . $itemIndex . '_demerit_reason'));
			}

			// commit change
			$this->em->persist($groupScore);
			$this->em->flush();
		}

		$this->load->view('gymnastics/admin/referee/team_demerit_edit', [
						'game' => $this->rfeGame, 
						'items' => $items, 
						'school' => $schoolGroup,
						'groupScore' => $groupScore, 
						'referee'	=> $this->referee
					]);
	}
}
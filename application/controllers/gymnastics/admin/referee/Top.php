<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Top extends Referee_Controller {

	protected $accessAllow = array('NORMAL');

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$groups = $this->getRepository('Player')->getGroupHeats($this->rfeGame);

		$this->load->view('gymnastics/admin/referee/top', array(
			'referee' => $this->referee,
			'rfeGame' => $this->rfeGame,
			'rfeItem' => $this->rfeItem,
			'tourInf' => $this->tourInf,
			'groups' => $groups
			));
	}
}

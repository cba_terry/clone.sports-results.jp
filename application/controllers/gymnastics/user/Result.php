<?php
use Entities\Collection\GroupPlayCollection;
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends MY_Controller {

	public function index($gameId = 0)
	{
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game');

		$items = $game->getItems();

		$players = $this->getRepository('Player')->getPlayersUserResults($game);

		$groupHeatPlayers = GroupPlayCollection::pushIntoGroupHeat($players);

		$resultRanking 	= RankingCollection::implementRanking($players, 'getTotalFinalScore');

		$this->load->view('gymnastics/user/result', [
			'game'             => $game,
			'items'            => $items,
			'tournament'       => $game->getTournament(),
			'groupHeatPlayers' => $groupHeatPlayers,
			'resultRanking'    => $resultRanking,
			'group_current'    => $game->findGroupCurrentPlay()
		]);
	}
}

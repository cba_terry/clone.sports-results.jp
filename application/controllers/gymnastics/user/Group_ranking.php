<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Group_ranking extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($gameId = 0)
	{
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game');

		$items 		= $game->getItems();
		$players 	= $this->getRepository('Player')->getPlayersUserResults($game);

		$groupScores 	= [];
		$schoolGroups 	= $game->getSchoolGroups();

		foreach ($schoolGroups as $schoolGroup) {

			$groupScore = new \Entities\GroupScore;

			// find persons in same school
			$schoolPlayers = array_filter($players, function($p) use ($schoolGroup) {
				return $p->getSchool() == $schoolGroup;
			});

			// calculate score for each item
			foreach ($items as $item) {

				$bestScores = [];

				foreach ($schoolPlayers as $player) {
					
					$score = $player->getItemBestScoreValue($item, 'FinalScore');
					
					if(!in_array($score, $bestScores)) $bestScores[] = $score;
				}

				arsort($bestScores);

				$threeBestScore = array_slice($bestScores, 0, 3);

				$groupItemScore = array_sum($threeBestScore);

				$fn = 'setItem'.$item->getNo().'Score';

				$groupScore->$fn($groupItemScore);
				$groupScore->setTotalScore();
				$groupScore->setSchoolGroup($schoolGroup);
			}

			$groupScores[] = $groupScore;
		}

		$resultGroupRanking = RankingCollection::implementRanking($groupScores, 'getTotalScore');

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($groupScores, 'getItem'.$item->getNo().'Score');
		}

		$this->load->view('gymnastics/user/group_ranking', [
			'game' => $game,
			'items' => $items,
			'tournament' => $game->getTournament(),
			'resultGroupRanking' => $resultGroupRanking,
			'resultItemRanking'	=> $resultItemRanking
			]);
	}
}

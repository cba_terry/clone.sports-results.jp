<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$tournament_id = $this->uri->segment(3);
		$tournament_id = ( ! empty($tournament_id)) ? $tournament_id : 1;

		$data               = array();
		$data['tournament'] = $this->getRepository('Tournament')->find($tournament_id);
		$data['game']       = $this->getRepository('Game')->findBy([
			'tournament' => $tournament_id
		], ['sex' => 'DESC']);
		if ( ! empty($data['game'])) {
			$arr_game = array();
			foreach ($data['game'] as $key => $info_game) {
				if ( ! $info_game->getPlayers()->isEmpty()) {
					$arr_game[$info_game->getSex()][] = $info_game;
				}
			}
			$data['game'] = $arr_game;
		}
		$this->load->view('gymnastics/user/game_list', $data);
	}
}

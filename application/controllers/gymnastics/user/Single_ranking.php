<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Single_ranking extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($gameId = 0)
	{
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game');

		$items = $game->getItems();

		$players = $this->getRepository('Player')->getPlayersUserResults($game);

		//single player
		// $singlePlayers = array_filter($players, function($player){
		// 	return $player->isPlayAsSingle();
		// });

		$resultSingleRanking = RankingCollection::implementRanking($players, 'getTotalFinalScore');
		// Filter case player is cancle and not score
		$filterSingleRanking = $resultSingleRanking->partition(function($k, $rank) {
			return ($rank->getElement()->getFlagCancle() && !$rank->getElement()->hasItemScoreFull());
		});

		foreach ($items as $item) {
			$resultItemRanking[$item->getName()] = RankingCollection::implementRanking($players, 'getItemBestScoreValue', [$item, 'FinalScore']);
		}

		$this->load->view('gymnastics/user/single_ranking', [
			'game'                => $game,
			'items'               => $items,
			'tournament'          => $game->getTournament(),
			'resultSingleRanking' => $filterSingleRanking[1],
			'resultSingleCancle'  => $filterSingleRanking[0],
			'resultItemRanking'   => $resultItemRanking,
		]);
	}
}

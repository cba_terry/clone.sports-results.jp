<?php
use Entities\Collection\RankingCollection;

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_ranking extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($gameId = 0)
	{
		$game = $this->getRepository('Game')->find($gameId);

		if(!$game) throw new Exception('Please select a game');

		$items = $game->getItems();

		$event = $this->input->get('event');

		$item = $items->filter(function($i) use ($event) {
			return $i->getNo() == $event;
		})->first();

		if(!$item) $item = $items->first();

		$players = $this->getRepository('Player')->getPlayersUserResults($game);

		$resultItemRanking = RankingCollection::implementRanking($players, 'getItemEventFinalScore', [$item]);
		// Filter case player is cancle and not score
		$filterItemRanking = $resultItemRanking->partition(function($k, $rank) use ($item) {
			return ($rank->getElement()->getFlagCancle() && ! $rank->getElement()->getBestItemScore($item));
		});

		$this->load->view('gymnastics/user/event_ranking', [
			'game'              => $game,
			'item'              => $item,
			'items'             => $items,
			'event'             => $event,
			'tournament'        => $game->getTournament(),
			'resultItemCancle'  => $filterItemRanking[0],
			'resultItemRanking' => $filterItemRanking[1]
		]);
	}
}

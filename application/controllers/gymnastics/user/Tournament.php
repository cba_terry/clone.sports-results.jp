<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tournament extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$association_id = $this->input->get('aid');
		$association_id = isset($association_id) ? $association_id : 1;
		$conditions = [
			'status'         => 1,
			'orderby'        => ['t.id' => 'DESC', 't.startTime' => 'DESC'],
			'association_id' => $association_id,
			'data_type'      => 'all'
		];
		$data['association'] = $this->getRepository('Association')->find($association_id);
		$data['tournaments'] = $this->getRepository('Tournament')->getPagedTournaments($conditions);
		$this->load->view('gymnastics/user/tournament_list', $data);
	}
}
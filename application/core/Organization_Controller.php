<?php
class Organization_Controller extends MY_Controller {

	protected $association;
	protected $urlorg = '/gymnastics/admin/organization/';

	public function __construct(){
		parent::__construct();

		// authentication
		if (!$this->session->userdata('user')){
			$cookie = get_cookie('user');
			if ($cookie){
				$cookie_user = explode('_', $cookie);
				$user = $this->getRepository('User')->findOneBy(array('user_name' => $cookie_user[0], 'hash' => $cookie_user[1]));
				if ($user){
					$this->session->set_userdata('user', $user);
					redirect($this->urlorg . 'tournament', 'refesh');
				}
			}
		}

		if (!$this->session->userdata('user') && !preg_match('/gymnastics\/admin\/organization\/login/', $_SERVER['REQUEST_URI'])){
			redirect($this->urlorg .'login', 'refesh');
		}


		if($this->session->userdata('user')) {
			$tournament_ids    = [];
			$user              = $this->getRepository('User')->find($this->session->userdata('user'));
			$association       = $user->getAssociation();
			$this->association = $association;
			$tournaments       = $this->getRepository('Tournament')->findBy([
				'association' => $association
			]);

			if ( ! empty($tournaments)) {
				foreach ($tournaments as $key => $tournament) {
					$tournament_ids[] = $tournament->getId();
				}
			}

			$this->session->set_userdata('association_name', $association->getName());
			$this->session->set_userdata('tournament_id', $tournament_ids);
		}
	}
}
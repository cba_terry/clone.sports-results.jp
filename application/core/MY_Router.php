<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Router extends CI_Router {
	public function __construct()
	{
		parent::__construct();
	}

	protected function _set_request($segments = array())
	{
		// Filter args
		$pattern_uri  = implode('/', $segments);
		if ( ! empty($pattern_uri)) {
			$tmp_segments = $segments;
			$subfolder    = 'gymnastics';
			if ( ! preg_match("/^{$subfolder}/", $pattern_uri)) {
				$segments = [$subfolder];
				$segments = array_merge($segments, $tmp_segments);
			}
		}

		$segments = $this->_validate_request($segments);

		// If we don't have any segments left - try the default controller;
		// WARNING: Directories get shifted out of the segments array!
		if (empty($segments))
		{
			$this->_set_default_controller();
			return;
		}

		if ($this->translate_uri_dashes === TRUE)
		{
			$segments[0] = str_replace('-', '_', $segments[0]);
			if (isset($segments[1]))
			{
				$segments[1] = str_replace('-', '_', $segments[1]);
			}
		}

		$this->set_class($segments[0]);
		if (isset($segments[1]))
		{
			$this->set_method($segments[1]);
		}
		else
		{
			$segments[1] = 'index';
		}

		array_unshift($segments, NULL);
		unset($segments[0]);
		$this->uri->rsegments = $segments;
	}
}
/* End of file MY_Router.php */
/* Location: ./application/core/MY_Router.php */
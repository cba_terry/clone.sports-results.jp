<?php
class Referee_Controller extends MY_Controller {

	protected $referee;
	protected $rfeGame;
	protected $rfeItem;
	protected $tourInf;
	protected $accessAllow;

	// referee type map
	protected $rfeRole = array(
		2 => 'CHIEF',
		1 => 'MAIN',
		0 => 'NORMAL'
		);

	public function __construct(){

		parent::__construct();

		// check if exist cookie referee to auto login
		if (!$this->session->userdata('referee')){

			$cookie = get_cookie('referee');

			if ($cookie){
				$cookie_referee = explode('_', $cookie);
				
				$referee = $this->getRepository('Referee')->findOneBy(array(
								'referee_name' => $cookie_referee[0], 
								'hash' => $cookie_referee[1])
				);

				if ($referee){
					$this->session->set_userdata('referee', $referee);
					redirect('/admin/referee/top', 'refesh');
				}
			}
		}

		if (!$this->session->userdata('referee') && 
			!preg_match('/gymnastics\/admin\/referee\/login/', $_SERVER['REQUEST_URI'])){
			redirect('/admin/referee/login', 'refesh');
		}

		if ($this->session->userdata('referee')) {

			$referee = $this->session->userdata('referee');
			$referee = $this->getRepository('Referee')->getRefereeInfo($referee->getId());

			if(!$referee || !$this->checkAccessAllow($referee)) 
				throw new Exception('You do not have permission to get there');

			$this->referee = $referee;
		
			$this->rfeGame = $this->referee->getGame();
			$this->rfeItem = $this->referee->getItem();
			$this->tourInf = $this->rfeGame->getTournament();

		}
	}

	protected function checkAccessAllow($referee)
	{
		$rfeType = $this->rfeRole[$referee->getRefereeType()];

		if( ! in_array($rfeType, $this->accessAllow) ) return false;

		return true;
	}
}
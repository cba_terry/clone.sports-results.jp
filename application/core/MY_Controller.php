<?php
use Doctrine\Common\ClassLoader;

class MY_Controller extends CI_Controller {
	
	public $em;
	public $args;
	public $total_item;
	public $dynamic_order;
	public $dynamic_heat;

	public function __construct(){
		parent::__construct();
		$this->em = $this->doctrine->em;
	}

	protected function getRepository($repository)
	{
		return $this->em->getRepository('Entities\\' .$repository);
	}
}

include 'Referee_Controller.php';
include 'Organization_Controller.php';

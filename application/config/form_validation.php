<?php
$config = [
	"player" => [
		[
			'field' => 'name',
			'label' => 'Player Name',
			'rules' => 'required'
		],[
			'field' => 'gender',
			'label' => 'Player Gender',
			'rules' => 'required'
		],[
			'field' => 'playerNo',
			'label' => 'Player No',
			'rules' => 'required'
		],
	],
	"user" => [
		[
			'field' => 'user_name',
			'label' => 'ユーザー名',
			'rules' => 'required'
		],[
			'field' => 'user_code',
			'label' => 'ユーザーID',
			'rules' => 'required'
		],[
			'field' => 'password',
			'label' => 'パスワード',
			'rules' => 'required'
		],[
			'field' => 'repassword',
			'label' => 'パスワード（確認）',
			'rules' => 'required|matches[password]'
		],
	],
	"tournament" => [
		[
			'field' => 'name',
			'label' => '大会名',
			'rules' => 'required'
		],[
			'field' => 'endTime',
			'label' => '会期',
			'rules' => ''
		],
	],
	"referee" => [
		[
			'field' => 'referee_name',
			'label' => '審判�?',
			'rules' => 'required'
		],[
			'field' => 'password',
			'label' => 'パスワー�?',
			'rules' => 'required'
		],[
			'field' => 'score_type',
			'label' => '記録種別',
			'rules' => 'required'
		],
	],
	"admin_login" => [
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required',
			'errors' => array(
				'required' => '%s can not be empty.',
			),
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required',
			'errors' => array(
				'required' => '%s can not be empty.',
			),
		],
	],
	"game_class_setting" => [
		[
			'field' => 'item[]',
			'label' => '審判�?',
			'rules' => 'required'
		],
	],
	"school_group" => [
		[
			'field' => 'school_name',
			'label' => '学校名',
			'rules' => 'required'
		],
	],
];
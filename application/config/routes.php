<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/organization/tournament/(:num)/game_class'] = 'gymnastics/admin/organization/game_class/index/$1';
$route['admin/organization/tournament/(:num)/game_class/setting'] = 'gymnastics/admin/organization/game_class/setting/$1';
$route['admin/organization/tournament/(:num)/game_class/download_csv'] = 'gymnastics/admin/organization/game_class/download_csv/$1';
#$route['admin/organization/tournament/(:any)/class/(:any)/gender/(:any)/player'] = 'gymnastics/admin/organization/player/index/$1/$2/$3';
$route['admin/organization/tournament/(:any)/class/(:any)/gender/(:any)/referee'] = 'gymnastics/admin/organization/referee/index/$1/$2/$3';
$route['admin/organization/tournament/(:any)/class/(:any)/gender/(:any)/excel_output'] = 'gymnastics/admin/organization/excel_output/index/$1/$2/$3';
$route['admin/organization/user/pager/(:num)'] = 'gymnastics/admin/organization/user/index/$1';
$route['admin/organization/user/pager'] = 'gymnastics/admin/organization/user';
$route['admin/organization/referee/pager/(:num)'] = 'gymnastics/admin/organization/referee/index/$1';
$route['admin/organization/referee/pager'] = 'gymnastics/admin/organization/referee';

$route['admin/([a-z0-9_]+)/([a-z0-9_]+)/(:num)'] = 'gymnastics/admin/$1/$2/index/$3';


// Set route for the user pages
$route['user/game_list']             = 'gymnastics/user/game';
$route['user/game_list/(:num)']      = 'gymnastics/user/game/index/$1';
$route['user/result/(:num)']         = 'gymnastics/user/result/index/$1';
$route['user/ranking/event/(:num)']  = 'gymnastics/user/event_ranking/index/$1';
$route['user/ranking/single/(:num)'] = 'gymnastics/user/single_ranking/index/$1';
$route['user/ranking/group/(:num)']  = 'gymnastics/user/group_ranking/index/$1';

$route['admin/referee/tournament/(:num)/game_class'] = 'gymnastics/admin/referee/game_class/index/$1';
$route['admin/referee/all_game_list/class/(:any)/sex/(:num)'] = 'gymnastics/admin/referee/all_game_list/index/$1/$2';

$route['monitor/result/game/(:num)/heat/(:num)'] = 'gymnastics/monitor/result/index/$1/$2';
$route['monitor/ranking/single/(:num)'] = 'gymnastics/monitor/ranking/single/$1';
$route['monitor/ranking/group/(:num)']  = 'gymnastics/monitor/ranking/group/$1';
$route['monitor/rotation/(:num)']  = 'gymnastics/monitor/rotation/index/$1';

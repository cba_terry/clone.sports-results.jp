<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile {
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('user_agent');
	}

	public function view_set()
	{
		if ($this->valid('only-smartphone')) {
			$this->ci->load->view_path_orverride('sp/');
			return;
		} elseif ($this->valid('only-desktop')) {
			return;
		} elseif ($this->ci->agent->is_mobile()) {
			$this->ci->load->view_path_orverride('sp/');
		}
	}

	public function display()
	{
		echo $this->ci->output->get_output();
	}

	public function valid($type = null)
	{
		$uri = $this->ci->input->server('REDIRECT_URL');

		$sp_pages = '/referee\/(top|player|score)$/';
		$str_page = preg_replace("/^\/gymnastics\/admin\//", "", $uri);
		switch ($type) {
			case 'only-smartphone':
				return preg_match($sp_pages, $uri);
				break;
			case 'only-desktop':
				return ( ! preg_match($sp_pages, $uri) && ! preg_match("/organization\/login|referee\/login|gymnastics\/user/", $str_page));
				break;
			default:
				return false;
				break;
		}
	}
}
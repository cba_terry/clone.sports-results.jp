<?php
/**
 * Created by PhpStorm.
 * User: terry
 * Date: 22/11/2015
 * Time: 8:42 CH
 */

class ExceptionHook
{
    public function SetExceptionHandler()
    {
        set_exception_handler(array($this, 'HandleExceptions'));
    }

    public function HandleExceptions($exception)
    {

        $msg = 'Exception of type \'' . get_class($exception) . '\' occurred with Message: ' . $exception->getMessage() . ' in File ' . $exception->getFile() . ' at Line ' . $exception->getLine();

        $msg .= "\r\n Backtrace \r\n";
        $msg .= $exception->getTraceAsString();

        echo $exception->getMessage();;

        log_message('error', $msg, TRUE);

       // mail('dev-mail@example.com', 'An Exception Occurred', $msg, 'From: test@example.com');

    }
}
<?php
use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\Common\Cache\ApcCache,
    Doctrine\DBAL\Logging\EchoSQLLogger;

class Doctrine {

    public $em = null;

    public function __construct()
    {
        // load database configuration from CodeIgniter
        require APPPATH. 'config/' . ENVIRONMENT . '/database.php';

        // Set up class loading. You could use different autoloaders, provided by your favorite framework,
        // if you want to.
        //require_once APPPATH.'libraries/Doctrine/Common/ClassLoader.php';

        // We use the Composer Autoloader instead - just set
        // $config['composer_autoload'] = TRUE; in application/config/config.php
        //require_once APPPATH.'vendor/autoload.php';

        //A Doctrine Autoloader is needed to load the models
        $entitiesClassLoader = new ClassLoader('Entities', APPPATH."models");
        $entitiesClassLoader->register();

        // Set up caches
        $config = new Configuration;
        //$cache = new ArrayCache;

        if (ENVIRONMENT == "development") {
            $cache = new ArrayCache;
        } else {
            $cache = new ApcCache;
        }

        $cache->setNamespace('gymnastics_');

        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(array(APPPATH.'models/Entities'));
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);
        $config->setResultCacheImpl(new ApcCache());

        // Proxy configuration
        $config->setProxyDir(APPPATH.'models/proxies');
        $config->setProxyNamespace('Proxies');

        // Set up logger
        // $logger = new EchoSQLLogger;
        // $config->setSQLLogger($logger);

        if (ENVIRONMENT == "development") {
            $config->setAutoGenerateProxyClasses( TRUE );
        } else {
            $config->setAutoGenerateProxyClasses( FALSE );
        }

        // Database connection information
        $connectionOptions = array(
            'driver'        => 'pdo_mysql',
            'user'          => $db['default']['username'],
            'password'      => $db['default']['password'],
            'host'          => $db['default']['hostname'],
            'dbname'        => $db['default']['database'],
            'charset'       => $db['default']['char_set'],
            'driverOptions' => array(1002 => 'SET NAMES utf8')
        );

        // Create EntityManager
        $this->em = EntityManager::create($connectionOptions, $config);
    }
}

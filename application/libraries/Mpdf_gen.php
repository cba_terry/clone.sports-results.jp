<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Mpdf_gen {
    public function __construct(){
        require_once APPPATH.'/libraries/mpdf60/mpdf.php';
        $ci =& get_instance();
        //mPDF($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P') {
        $ci->mpdf =  new mPDF('+aCJK','A3','','',-5,-5,8,8,16,13);
    }
}
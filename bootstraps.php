<?php
require __DIR__ . '/vendor/autoload.php';

define('APPPATH', dirname(__FILE__).DIRECTORY_SEPARATOR . 'application' .DIRECTORY_SEPARATOR);
define('BASEPATH', APPPATH);
define('ENVIRONMENT', 'production');

require APPPATH.'libraries/Doctrine.php';

$doctrine = new Doctrine();
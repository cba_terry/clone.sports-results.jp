<?php

class ScoreTest extends PHPUnit_Framework_Testcase
{
	public $score;

	public function setUp()
	{
		$game = new Entities\Game;

		$player = new Entities\Player;

		$this->score = new Entities\Score($player, $game);
	}

	/**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Score must set for a player
     */

	public function test_score_must_have_a_player ()
	{
		$score = new Entities\Score(null, new Entities\Game);
	}

	/**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Score must set for a game
     */

	public function test_score_must_have_a_game ()
	{
		$score = new Entities\Score(new Entities\Player);
	}
}
<?php

class PlayerTest extends PHPUnit_Framework_Testcase
{
	public $player;

	public function setUp()
	{
		$this->player = new Entities\Player;
	}

	public function test_player_set_name ()
	{
		$this->player->setName('Terry');
		$this->assertEquals('Terry', $this->player->getName());
	}
}
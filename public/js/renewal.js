(function($) {
	$(function() {
		$.fn.previewImage = function(options){
			var setting = $.extend({
				elm: '.preview-image',
				callDone: function(res) {},
				callFail: function(res) {},
			}, options);
			$(this).on('change', function () {
				if (typeof (FileReader) != 'undefined') {
					var elmPreview = $(setting.elm).empty();
					var reader = new FileReader();
					reader.onload = function (e) {
						$('<img />', {'src': e.target.result}).appendTo(elmPreview);
						setting.callDone();
					}
					elmPreview.show();
					reader.readAsDataURL($(this)[0].files[0]);
				} else {
					setting.callFail();
				}
			});
		}
		$.fn.clearForm = function() {
			return this.each(function() {
				var _that = $(this),
					type = this.type,
					tag = this.tagName.toLowerCase();
				if (tag == 'form')
					return $(':input', this).clearForm();
				if (type == 'text' || type == 'password')
					_that.attr('value', '');
				else if (tag == 'textarea')
					_that.html('');
				else if (tag == 'select')
					_that.children('option').removeAttr('selected');
			});
		};
		$.monitorNext =  function(options) {
			var setting = $.extend({
				url: '',
				time: 5000,
				request: '',
				method: 'get',
				data: {},
				callDone: function(res){},
				callFail: function(res){}
			}, options);
			var func;
			switch(setting.request) {
				case 'ajax':
					func = function(){
						$.ajax({
							url: setting.url,
							type: setting.method,
							dataType: 'json',
							data: setting.data,
							success: function(res){
								setting.callDone(res);
							},
							error: function(res){
								setting.callFail(res);
							}
						});
					}
					break;
				default:
					func = function(){}
					break;
			}
			setInterval(func, setting.time);
		};
	});
})(jQuery);

$(document).ready(function() {
	$('.file').previewImage();
	$('.elm-orderby').change(function(event) {
		var orderby = $(this).val();
		$('input[name="orderby"]').attr('value', orderby);
		$('#form_toolbar').submit();
	});
	$('.btn-reset').on('click', function(){
		$('#form_modal').clearForm();
	});
});
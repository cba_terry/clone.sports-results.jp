(function($) {
	$(function() {

		/*$(window).bind('load resize', function(){
			var contents = $('#contents'),
				minHeight = $(window).height()
							- $('#footer').outerHeight(true),
				spacingH = contents.outerHeight(true) - contents.height();
			contents.css('min-height', minHeight - spacingH);
		});*/

		// ==============================================================
		// ページの先頭へスクロール
		// ==============================================================
		// .pageTop aをクリックでページの先頭へ
		$(".pageTop a").click(function() {
			$("html, body").animate({ scrollTop : 0 });
			return false;
		});

		// ==============================================================
		// ホバー処理
		// ==============================================================
		$("a.hover, .hover").hover(function() {
			$(this).stop().fadeTo("fast", 0.6);
		},function(){
			$(this).stop().fadeTo("fast", 1.0);
		});

		//loginPage: middle Screen.
		function middeScreen() {
		var $lBlock = $('.loginBlock')
			, winW = $(window).width()
			, winH = $(window).height()
			, lH = $lBlock.height()
			, mDef = 148
			;
			if(winH > lH){
				$lBlock.css({'margin-top': (winH - lH)/2, 'margin-bottom': 0});
			} else {
				$lBlock.css({'margin-top': mDef, 'margin-bottom': mDef});
			}
		}
		middeScreen();

		$(window).bind('load resize',function(){
			setTimeout(function(){
				middeScreen();
				fBottom();
			},200);
		});

		// ==============================================================
		// input
		// ==============================================================
		// $('input[type="text"], input[type="password"], input[type="tel"], textarea').addClass("idleField")
		// .focus(function() {
		// 	$(this).removeClass("idleField")
		// 		.addClass("focusField");
		// 	if (this.value == this.defaultValue) {
		// 		this.value = '';
		// 	} else {
		// 		this.select();
		// 	};
		// })
		// .blur(function() {
		// 	$(this).removeClass("focusField")
		// 		.addClass("idleField fillField");
		// 	if ($.trim(this.value) == ''){
		// 		this.value = (this.defaultValue ? this.defaultValue : '');
		// 		$(this).removeClass("fillField");
		// 	}
		// })
		// .each(function() {
		// 	if ($.trim(this.value) == this.defaultValue) {
		// 		$(this).removeClass("fillField");
		// 	} else {
		// 		$(this).addClass("fillField");
		// 	}
		// });

		// ==============================================================
		// jQuery UI カレンダー
		// ==============================================================
		if ($().datepicker) {
			$("input.datepicker").datepicker({
				dateFormat : "yy-mm-dd"
			});
		}

		// ==============================================================
		// chooseBox
		// ==============================================================
		 $('.chooseBox').each(function(){
			var $inBox = $(this).find('input'),
				$laBox = $(this).find('label');
			// $inBox.attr('checked',false);
			$laBox.click(function() {
				$laBox.removeClass('checked');
				$(this).addClass('checked');
				$inBox.attr('checked',false);
				$inBox.eq($(this).parent().index()).attr('checked','checked');
			});
		});

		// ==============================================================
		// checkAll
		// ==============================================================
		var $check = $('.tableGame td input[type="checkbox"]')
			,$checkAll = $('.tableGame th input[type="checkbox"]')
			,len = $check.length;
		$check.each(function(){
			$(this).click(function(){
				var checked = $check.filter(':checked').length;
				if( len == checked ) $checkAll.attr('checked','checked');
				else $checkAll.attr('checked',false);
			});
		});
		$checkAll.click(function(){
			if($(this).attr('checked')) $check.attr('checked','checked');
			else $check.attr('checked',false);
		});

		// ==============================================================
		// tab
		// ==============================================================
		$(".tabHeader li a").click(function() {
			var elm = $(this);
			 if (!elm.hasClass("active")){
				elm.parents(".tabHeader")
					.find("li a.active")
					.removeClass("active");
				elm.addClass("active");
				elm.parents(".tabHeader")
					.siblings(".tabContent")
					.hide();
				$(this.hash).fadeIn();
			}
			return false;
		});

		// ==============================================================
		// fBottom
		// ==============================================================
		function fBottom() {
			var $footer = $('#footer'),
				winH = $(window).height(),
				wrapperH =  $('#header').height() +
							$('#contents').height() +
							$('#footer').height();
			if ( winH > wrapperH ) {
				$footer.addClass('fix');
			} else {
				$footer.removeClass('fix');
			}
		}
		fBottom();


		// ==============================================================
		// tab
		// ==============================================================

		$(".tab a").click(function() {
		var elm = $(this);
		if (!elm.parent("li").hasClass("active"))
		{
			elm.parent("li")
				.siblings()
				.removeClass("active");
			elm.parent("li").addClass("active");
			elm.parents(".tab")
				.next()
				.children(".tabBox")
				.hide();
			$(this.hash).fadeIn();
		}
		return false;
		});

		$('.CSVList input').change(function(e) {
			$(this).parents('li').find('.uploadFile').text(this.value);
		});

		// ==============================================================
		// dublicate item when clicking add button
		// ==============================================================
		$('.add').click(function(count){
			var $box = $(this).parent();
			var ul = $box.children(':last').clone();
			$box.append(ul).children(':last').addClass('mt10');
			var cur = $box.find('select:last').attr('name').match(/\[(\d+)\]/);
			ul.find('select').each(function(e){
				var next  = parseInt(cur[1])+1;
				var id = $(this).attr('id').replace(/_\d+_/, '_'+next+'_');
				var name = $(this).attr('name').replace(/\d+/, next);
				$(this).attr({'id':id,'name':name});
				$(this).parents('li').find('label').attr('for',id);
			});
		});

		// ==============================================================
		// replace path when clicking upload button
		// ==============================================================
		$('input[type="file"]').change(function(){
			var $file = $(this).parent().next('.uploadFile');
			$file.html($file.text().replace('C:\\fakepath\\',''));
		});

		// ==============================================================
		// groupList
		// ==============================================================
		$('.groupList li.radio').click(function() {
			$(this).find('input').attr('checked', 'checked');
		});

		// ==============================================================
		// Custom
		// ==============================================================
		$('.btn-flag-active').click(function(){
			$('#form_referee_list').submit();
		});

		$('input[type=reset]').click(function(){
			$('.searchBox input[type=text]').attr('value','');
		});
	});
})(jQuery);

$(document).ready(function(){
	$('.btn_deny').click(function (){
		var ids = [];
		ids.push($(this).attr('data-id'));
		update_status(ids, '0');
	});
	
	$('.update_status').click(function (){
		var ids = [];

		if($(this).attr('data-id').length)
		{
			ids.push($(this).data('id'));
			var name_radio = "status["+$(this).data('id')+"]";
			var status     = $('input[name="'+name_radio+'"]:checked').val();

			if(ids.length) update_status(ids, status);
		}
		else alert('You have not input score yet, please input first.');
	});

	$('.btn_update_all').click(function (){
		var status = $(this).parents('p').find('select').val();
		if(status == 'ステータスを公開に') {
			alert('チェック入れていた試合のステータスを選択してください。');
			return false;
		}

		var list_id = [];
		$("input[name='check']").each(function(){
			if (this.checked) list_id.push(this.value);
		})

		if (list_id.length == 0) {
			alert("ステータスを変更したい試合にチェックを入れてください。");
			return false;
		}

		if (confirm("チェックした試合のステータスを更新してもよろしいですか。")) {
			update_status(list_id, status);
		}
	});

	$('.againButton ').click(function(e){
		if($(this).hasClass('againGray')) return false;
		if($(this).hasClass('againTimeLine')) return false;

		var singleItemScoreId = $(this).data('id');
		var singleItemScoreType = $(this).data('type');

		var self = $(this);

		$.ajax({
			url:"/gymnastics/admin/referee/game/reinput",
			type:"POST",
			data:{id: singleItemScoreId, type: singleItemScoreType},
			dataType:'json',
			success:function(response){

				if(response.status == 'success'){
					console.log(response);
					self.addClass('againGray');
				}
			}
		});
	});

	$('.againTimeLine').click(function(e){

		var playerId = $(this).data('player');
		var round = $(this).data('round');

		var self = $(this);

		$.ajax({
			url:"/gymnastics/admin/referee/game/reinput_timeline",
			type:"POST",
			data:{id: playerId, round: round},
			dataType:'json',
			success:function(response){

				if(response.status == 'success'){
					console.log(response);
					self.addClass('againGray');
				}
			}
		});
	})
});


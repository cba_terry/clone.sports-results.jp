$(document).ready(function(){
	
	var d_score = e_score = demerit_score = time_demerit_score = line_demerit_score = 0;

	// Init value
	d_score = dScore();
	e_score = eScore();
	demerit_score = demeritScore();
	final_score = finalScore(d_score, e_score, demerit_score);
	function getValue(value){
		return (typeof value != 'undefined') ? value : 0;
	}
	// calculate total d score
	function dScore(index){

		var finalDscore = 0;
		var elmScore 	= '.d_score';

		$(elmScore).each(function(k,v){
			finalDscore += Number(getValue($(v).val()));
		});

		finalDscore = finalDscore / $(elmScore).length;
		$('#d_score').text(finalDscore.toFixed(2));

		return finalDscore;
	}

	function eScore(){

		var finalEscore = 0;
		var countEScore = $('.e_score').length;
		var eDemeritScore = Number(getValue($('#e_demerit_score').val()));

		if(countEScore == 1)
		{
			// truong hop chi 1 Escore, lay escore do tru di edemerit score
			finalEscore = Number(getValue($($('.e_score')[0]).val())) - eDemeritScore;

		}else if(countEScore == 2)
		{
			// truong hop co 2 diem Escore, lay trung binh cua 2 diem Escore do tru di edemerit score
			finalEscore = (Number(getValue($($('.e_score')[0]).val())) + Number(getValue($($('.e_score')[1]).val()))) / 2 - eDemeritScore;

		}else if(countEScore == 3)
		{
			// truong hop co 3 diem Escore, lay trung binh 2 diem cao nhat sau do tru di edemerit score
			var sortScore = [];

			$('.e_score').each(function(k,v){
				sortScore[k] = Number(getValue($(v).val()));
			});

			sortScore.sort(function(a, b){return b-a});

			finalEscore = (sortScore[0] + sortScore[1]) / 2 - eDemeritScore;

		}else
		{
			// truong hop co lon hon 3 Escore, loai ra diem cao nhat va thap nhat
			// sau do tinh trung binh cua nhung diem con lai
			var sortScore = [];

			$('.e_score').each(function(k,v){
				sortScore[k] = Number(getValue($(v).val()));
			});

			sortScore.sort(function(a, b){return b-a});

			var totalEscore = 0;

			sortScore.splice(0,1); // loai ra diem cao nhat
			sortScore.splice(sortScore.length-1,1); // loai ra diem that nhat

			$(sortScore).each(function(k,v){
				totalEscore += v;
			});

			// lay trung binh
			if (totalEscore > 0)
				totalEscore = totalEscore / sortScore.length;

			finalEscore = totalEscore - eDemeritScore;
		}

		if(finalEscore < 0) finalEscore = 0;

		$('#e_score').text(finalEscore.toFixed(2));

		return finalEscore;
	}

	function demeritScore(){
		other_demerit      = getValue($('#other_demerit').val());
		time_demerit_score = getValue($('#time_demerit_score').val());
		line_demerit_score = getValue($('#line_demerit_score').val());
		demerit_score = Number(other_demerit) + Number(time_demerit_score) + Number(line_demerit_score);

		$('#demerit_score').text(-demerit_score.toFixed(2));
		
		return demerit_score;
	}

	function finalScore(d_score, e_score, demerit_score){
		var final_score = (d_score + e_score) - demerit_score;
		$('#final_score').text(final_score.toFixed(2));

		return final_score;
	}

	$('.d_score, .e_score, #e_demerit_score, #line_demerit_score, #time_demerit_score, #line1_score, #line2_score, #other_demerit').keyup(function(){
		var value = this.value.replace(/[^0-9\.]/,'');
		$(this).val(value);

		var d_score = dScore();
		var e_score = eScore();
		var demerit_score = demeritScore();
		final_score = finalScore(d_score, e_score, demerit_score);
	});

	$('#time1_score1, #time1_score2, #time2_score1, #time2_score2').keyup(function(){
		var value = this.value.replace(/[^0-9]/,'');
		$(this).val(value);
	});
})
$(document).ready(function(){
	var d_score = e_score = demerit_score = time_demerit_score = line_demerit_score = 0;

	// init value
	$([0,1]).each(function(v){
		d_score = dScore(v);
		e_score = eScore(v);
		demerit_score = demeritScore(v);
		final_score = finalScore(v, d_score, e_score, demerit_score);
	});
	function getValue(value){
		return (typeof value != 'undefined') ? value : 0;
	}

	// calculate total d score
	function dScore(index){

		var finalDscore = 0;
		var elmScore 	= '.d_score_'+index;

		$(elmScore).each(function(k,v){
			finalDscore += Number(getValue($(v).val()));
		});

		finalDscore = finalDscore / $(elmScore).length;

		$('#d_score'+index).text(finalDscore.toFixed(2));

		return finalDscore;
	}

	// calculate total e score
	function eScore(index){

		var finalEscore = 0;
		var elmScore 	= '.e_score_'+index;
		var countEScore = $(elmScore).length;
		var eDemeritScore = Number(getValue($('#e_demerit_score'+index).val()));

		if(countEScore == 1)
		{
			// truong hop chi 1 Escore, lay escore do tru di edemerit score
			finalEscore = Number(getValue($($(elmScore)[0]).val())) - eDemeritScore;

		}else if(countEScore == 2)
		{
			// truong hop co 2 diem Escore, lay trung binh cua 2 diem Escore do tru di edemerit score
			finalEscore = (Number(getValue($($(elmScore)[0]).val())) + Number(getValue($($(elmScore)[1]).val()))) / 2 - eDemeritScore;

		}else if(countEScore == 3)
		{
			// truong hop co 3 diem Escore, lay trung binh 2 diem cao nhat sau do tru di edemerit score
			var sortScore = [];

			$(elmScore).each(function(k,v){
				sortScore[k] = Number(getValue($(v).val()));
			});

			sortScore.sort(function(a, b){return b-a});

			finalEscore = (sortScore[0] + sortScore[1]) / 2 - eDemeritScore;

		}else
		{
			// truong hop co lon hon 3 Escore, loai ra diem cao nhat va thap nhat
			// sau do tinh trung binh cua nhung diem con lai
			var sortScore = [];

			$(elmScore).each(function(k,v){
				sortScore[k] = Number(getValue($(v).val()));
			});

			sortScore.sort(function(a, b){return b-a});

			var totalEscore = 0;

			sortScore.splice(0,1); // loai ra diem cao nhat
			sortScore.splice(sortScore.length-1,1); // loai ra diem that nhat

			$(sortScore).each(function(k,v){
				totalEscore += v;
			});

			// lay trung binh
			if (totalEscore > 0)
				totalEscore = totalEscore / sortScore.length;
			finalEscore = totalEscore - eDemeritScore;
		}

		if(finalEscore < 0) finalEscore = 0; 

		$('#e_score'+index).text(finalEscore.toFixed(2));
		return finalEscore;
	}

	// calculate demerit score

	function demeritScore(index){
		var other_demerit      = getValue($('#other_demerit'+index).val());
		var time_demerit_score = getValue($('#time_demerit_score'+index).val());
		var line_demerit_score = getValue($('#line_demerit_score'+index).val());
		var demerit_score = Number(other_demerit) + Number(time_demerit_score) + Number(line_demerit_score);

		$('#demerit_score'+index).text(-demerit_score.toFixed(2));
		
		return demerit_score;
	}

	// calculate final score

	function finalScore(index, d_score, e_score, demerit_score){
		var final_score = d_score + e_score - demerit_score;
		$('#final_score'+index).text(final_score.toFixed(2));

		return final_score;
	}

	$('.d_score_0, .e_score_0, #e_demerit_score0, #line1_score0, #line2_score0, #other_demerit0, .d_score_1, .e_score_1, #e_demerit_score1, #line1_score1, #line2_score1, #other_demerit1, #line_demerit_score0, #line_demerit_score1,#time_demerit_score0,#time_demerit_score1').keyup(function(){
		if(this.value.match(/[^0-9\.]/)){
			var value = this.value.replace(/[^0-9\.]/,'');
			$(this).val(value);
		}

		var elmId = $(this).attr('id');
		var index = elmId.substring(elmId.length-1);

		var d_score = dScore(index);
		var e_score = eScore(index);
		var demerit_score = demeritScore(index);
		final_score = finalScore(index, d_score, e_score, demerit_score);
	});

	$('#time1_score10, #time1_score20, #time2_score10, #time2_score20, #time1_score11, #time1_score21, #time2_score11, #time2_score21').keyup(function(){
		if(this.value.match(/[^0-9]/)){
			var value = this.value.replace(/[^0-9]/,'');
			$(this).val(value);
		}
	});

	//Remember active tab
	$('ul.tab li a').click(function(){
		$('input#tab').val($(this).parent().index()+1);
	});
})
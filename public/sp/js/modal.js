(function($){
	$.fn.modal = function(options){
		options = $.extend({
			trigger:'.openModal',
			modals: '.modal',
			closeButton: '.close',
			opacity: 0.7,
			backgroundColor: '000',
			effect: 'fadein',
			effectSpeed: 300,
			docClose: true,
			moveOnScroll: true,
			moveOnResize: true,
			escapeKey: true,
			openOnLoad: false,
			alignTop: false,
			top: 100,
			onAfterShow: function(){},
			onAfterHide: function(){}
		}, options);
		
		if ( ! $('.overlay').length ) {
			$('body').append('<div class="overlay"></div>');
		}
		var isOpened = false,
			trigger = $(options.trigger),
			modal = $(options.modals),
			olay = $('.overlay'),
			close = $(options.closeButton),
			win = $(window);
		if (options.effect == 'fadein') options.effect = 'fadeIn';
		if (options.effect == 'slidedown') options.effect = 'slideDown';
		olay.css({
			background: '#' + options.backgroundColor,
			opacity: options.opacity
		});
		close.bind( 'click', closeModal );
		trigger.bind( 'click', openModal );
		if (options.openOnLoad) {
			openModal();
		}
		if (options.docClose) {
			olay.bind( 'click', closeModal );
		}
		if (options.escapeKey) {
			escape();
		}
		if (options.moveOnResize) {
			$(window).bind( 'resize', resizeWindow );
		}
		if (!options.moveOnScroll) {
			$(options.modals).css('position','absolute');
		}
		
		function openModal() {
			resizeWindow();
			modal.hide();
			if ( !isOpened ) {
				$(options.modals).css({
					top: options.alignTop ? options.top + win.scrollTop() : Math.abs((win.height() - $(options.modals).outerHeight()) / 2 + win.scrollTop()),
					left: Math.abs((win.width() - $(options.modals).outerWidth() ) / 2 + win.scrollLeft())
				});
			}
			olay[options.effect](options.effectSpeed);
			modal.delay(options.effectSpeed)[options.effect](options.effectSpeed,options.onAfterShow);
			isOpened = true;
			return false;
		}
		function closeModal() {
			modal.fadeOut(100, function(){
				olay.delay(100).fadeOut(options.effectSpeed,options.onAfterHide);
			});
			return false;
		}
		function escape() {
			$(document).bind('keyup', function(e){
				if (e.keyCode == 27) {
					closeModal();
				}
				return;
			});
		}
		function resizeWindow() {
			$(options.modals).css({
				top: options.alignTop ? options.top + win.scrollTop() : Math.abs((win.height() - $(options.modals).outerHeight()) / 2 + win.scrollTop()),
				left: Math.abs((win.width() - $(options.modals).outerWidth() ) / 2 + win.scrollLeft())
			});
		}
		// PUBLIC FUNCTIONS
		this.open = function() {
			openModal();
		};
		this.close = function() {
			closeModal();
		};
		return this;
	};
})(jQuery);
$(document).ready(function(){
	$('body').modal();
});
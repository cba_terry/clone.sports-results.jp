(function($) {
	$(function() {
		// ==============================================================
		// accordion
		// ==============================================================
		$('.accordion').click(function() {
			var elm = $(this);
			if (elm.next('.accordionBox').is(':visible')) {
				elm.removeClass('active').next('.accordionBox').slideUp(400);
			} else {
				elm.next('.accordionBox').slideDown(400);
				var otherAccor = elm.parent('.section').siblings('.section');
				otherAccor.find('.accordionBox').slideUp(400);
				otherAccor.find('.accordion').removeClass('active');
				elm.addClass('active');
			}
		});

		// ==============================================================
		// tab
		// ==============================================================
		$(".tab a").click(function() {
			var elm = $(this);
			if (!elm.parent("li").hasClass("active"))
			{
				elm.parent("li")
					.siblings()
					.removeClass("active");
				elm.parent("li").addClass("active");
				elm.parents(".tab")
					.next()
					.children(".tabBox")
					.hide();
				$(this.hash).fadeIn();
			}
			return false;
		});

		// ==============================================================
		// fBottom
		// ==============================================================
		function fBottom() {
			var $footer = $('#footer'),
				winH = $(window).height(),
				wrapperH =  $('#header').height() + 
							$('#contents').height() + 
							$('#footer').height();
			if ( winH > wrapperH ) {
				$footer.addClass('fix');
			} else {
				$footer.removeClass('fix');
			}
		}
		fBottom();

		$('.menu-tag').on('change', function(){
			var pathname = $(this).val(),
				hostname = window.location.hostname;
			window.location.href = '//' + hostname + pathname;
		});
	});
})(jQuery);
